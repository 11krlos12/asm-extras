
include jcmac.txt
;--------------------------------------------------
;Directiva para indicar el tamaño del programa
;--------------------------------------------------
.286
.model small
.stack
;--------------------------------------------------
;Directiva para indicar datos (variables)
;--------------------------------------------------
.data


tiempobloques db 0



handler     dw ? 

conpuntaje db 0

bdsalidajuego db 0
colorcarro db 0
vniveljuegosel db 0
vnivelnumero dw 0 ,'$'
puntajeconv DB ?,?, 0ah,0dh,'$' 
msmpuntaje DB 'puntaje a mostrar :', 0ah,0dh,'$' 


;----------------------------------------------
rcorte db 50 dup("-"),0ah,0dh,'$'
rtopjc   db  '                   Top 10           ',0ah,0dh,'$'
rtopjc1  db  '1.juan carlos        97          10',0ah,0dh,'$'
rtopjc2  db  '2.esmeralda          83          6',0ah,0dh,'$'
rtopjc3  db  '3.mynor              68          13',0ah,0dh,'$'
rtopjc4  db  '4.cesar              66          12',0ah,0dh,'$'
rtopjc5  db  '5.diego              47          15',0ah,0dh,'$'
rtopjc6  db  '6.jorge              40          14',0ah,0dh,'$'
rtopjc7  db  '7.ervin              38          9',0ah,0dh,'$'
rtopjc8  db  '8.hilda              32          8',0ah,0dh,'$'
rtopjc9  db  '9.yoly               28          7',0ah,0dh,'$'
rtopjc10  db '10.mary              03          5',0ah,0dh,'$'


rtopjc11  db  '1.diego              47          15',0ah,0dh,'$'
rtopjc12  db  '2.jorge              40          14',0ah,0dh,'$'
rtopjc13  db  '3.mynor              68          13',0ah,0dh,'$'
rtopjc14  db  '4.cesar              66          12',0ah,0dh,'$'
rtopjc15  db  '5.juan carlos        97          10',0ah,0dh,'$'
rtopjc16  db  '6.ervin              38          9',0ah,0dh,'$'
rtopjc17  db  '7.hilda              32          8',0ah,0dh,'$'
rtopjc18  db  '8.yoly               28          7',0ah,0dh,'$'
rtopjc19  db  '9.esmeralda          83          6',0ah,0dh,'$'
rtopjc20  db  '10.mary              03          5',0ah,0dh,'$'


;----------------------------------------------
;contadordelay db 0
;-------------------------
;VARIABLES COMPLEMENTO MACRO PARA MOSTRAR ARREGLO
;-------------------------
viniciocarro        dw 0,'$'
vinibloque        dw 0,'$'
vinibloqueverde   dw 0,'$'
diverde db 0

misNumeros      dw  25 dup(0) , '$'
misNumeros2     db  25 dup(0) , '$'
ordenados       db  25 dup(0) , '$'
desordenados    db  25 dup(0) , '$'

misNumerosjc    db  25 dup(0) , '$'

;TFJC db 16 dup(31 dup("-"),0ah),0dh,'$'  

decimal       db  0,0,0,0,0,'$'


resAx       dw 0,'$'
resBx       dw 0,'$'
resCx       dw 0,'$'
resDx       dw 0,'$'
resSi       dw 0,'$'
resDi       dw 0,'$'


coma        db ',','$'

varTmp      db 0,0,'$'

contador1   db 0,'$'
contador2   dw 0,'$'

vpintar        db 'tratando de pintar ','$'

car1        db "Ordenamiento:",'$'
bbs         db "BubbleSort",'$'
qks         db "QuickSort",'$'
shs         db "ShellSort",'$'

tTime       db "Tiempo: "
tMinut      db "00:"
tSecond     db "00:"
tMili       db "000",'$'



vVelo       db "Velocidad: "
vVelo2      db "0",'$'

vtop10puntos db "Top 10 Por puntuacion",'$'
vtop10tiempo db "Top 10 Por tiempo",'$'

vuser        db "Usuario:",'$'
vnivel       db "Nivel: "
vnivel2      db "0",'$'

vnomuser     db "juan carlos",'$'



niveljuego db 0

vpuntos       db "Puntos: ",'$'
;vpuntos2      db "0",'$'



cursor1     db 1,'$'
cursor2     db 0,'$'

num1        db 0,0,'$'

xCor        dw 0,'$'
yCor        dw 0,'$'


varX        dw 0,'$'
varX2       dw 0,'$'
varY        dw 0,'$'
varEsp      dw 0,'$'


xPar        dw 0,'$'
yPar        dw 0,'$'


anchoG      db 0,'$'
lateral     db 0,'$'

varTmp1     dw 0,'$'
varColor    db 0,'$'
varTope1    dw 0,'$'
varTope2    dw 0,'$'


varByte     db 0,'$'


retraso     dw 0,'$'


fecha       db  "Fecha: "   
date        db  "00/00/0000",32
hora        db  "Hora: "
time        db  "00:00:00"

seleccion   db 0

orden       db 0,'$'
varTipo     db 0,'$'



;-------------------------
;SALTO DE LINEA
;-------------------------
salto         db  13,10,'$'
vsalto db 0ah,0dh,'$'
;-------------------------
;DATOS DEL MENU PRINCIPAL
;-------------------------
vmenuinicial db 0ah,0dh, 
'Menu Inicial',0ah,0dh,0ah,0dh,
'1) Ingresar',0ah,0dh, 
'2) Registrar',0ah,0dh, 
'3) especial admin',0ah,0dh, 
'4) Salir', '$'

vopcion1 db 0ah,0dh, 'Ingrese una opcion: ','$'
vopincorrecta db 0ah,0dh, 'Opcion incorrecta',0ah,0dh,'$'
vmsingresar db 0ah,0dh, 'Ingresando..',0ah,0dh,'$'
vmsregistrar db 0ah,0dh, 'Registrando..',0ah,0dh,'$'
;-------------------------
;OPCION INGRESAR VARIABLES
;-------------------------
vlogin db 0ah,0dh, 'Iniciar sesion',0ah,0dh,'$'
vusuario db 0ah,0dh, 'Usuario: ','$'
vcontrasena db 0ah,0dh, 'Contrasena: ','$'
;-------------------------
;OPCION REGISTRAR VARIABLES
;-------------------------

;-------------------------
;OPCION SALIR VARIABLES
;-------------------------
vmsgsalida db 0ah,0dh, 'Salida exitosa !',0ah,0dh,'$'
;-------------------------
;VARIABLE ENCABEZADO
;-------------------------
Vencabezado db 0ah,0dh, 
'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA',0ah,0dh,
'FACULTAD DE INGENIERIA', 0ah,0dh, 
'ESCUELA DE CIENCIAS Y SISTEMAS',0ah,0dh,
'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1 ',0ah,0dh, 
'NOMBRE: JUAN CARLOS MALDONADO SOLORZANO',0ah,0dh,
'CARNET: 201222687',0ah,0dh,
'SECCION: A','$'

;==============================================================================
; ---------------------------------- REPORTE ----------------------------------

repXML      db '<Arqui>',13,10
            db 09,'<Encabezado>',13,10
            db 09,09,'<Universidad>Universidad de San Carlos de Guatemala</Universidad>',13,10
            db 09,09,'<Facultad>Facultad de Ingenieria</Facultad>',13,10
            db 09,09,'<Escuela>Escuela de Ciencas y Sistemas</Escuela>',13,10
            db 09,09,'<Curso>',13,10
            db 09,09,09,'<Nombre>Arquitectura de Computadores y Ensambladores 1</Nombre>',13,10
            db 09,09,09,'<Seccion>Seccion A</Seccion>',13,10
            db 09,09,'</Curso>',13,10
            db 09,09,'<Ciclo>Primer Semestre 2020</Ciclo>',13,10
            db 09,09,'<Fecha>',13,10
tDia        db 09,09,09,'<Dia>00</Dia>',13,10
tMes        db 09,09,09,'<Mes>00</Mes>',13,10
tAnio       db 09,09,09,'<Año>0000</Año>',13,10
            db 09,09,'</Fecha>',13,10
            db 09,09,'<Hora>',13,10
tHora       db 09,09,09,'<Hora>00</Hora>',13,10
tMinuto     db 09,09,09,'<Minutos>00</Minutos>',13,10       
tSegundo    db 09,09,09,'<Segundos>00</Segundos>',13,10
            db 09,09,'</Hora>',13,10
            db 09,09,'<Alumno>',13,10
            db 09,09,09,'<Nombre>Juan Carlos Maldonado Solorzano</Nombre>',13,10
            db 09,09,09,'<Carnet>201222687</Carnet>',13,10
            db 09,09,'</Alumno>',13,10
            db 09,'</Encabezado>',13,10
            db 09,'<Resultados>',13,10,'$'

listaE1     db 09,09,09,'<Lista_Entrada>','$'
listaE2     db '</Lista_Entrada>',13,10,'$'


listaO1     db 09,09,09,'<Lista_Ordenada>','$'
listaO2     db '</Lista_Ordenada>',13,10,'$'


tipo1       db 09,09,09,'<Tipo>Descendente</Tipo>',13,10,'$'
tipo2       db 09,09,09,'<Tipo>Ascendente</Tipo>',13,10,'$'


algo11      db 09,09,'<Ordenamiento_BubbleSort>',13,10,'$'
algo12      db 09,09,'</Ordenamiento_BubbleSort>',13,10,'$'


algo21      db 09,09,'<Ordenamiento_QuickSort>',13,10,'$'
algo22      db 09,09,'</Ordenamiento_QuickSort>',13,10,'$'


algo31      db 09,09,'<Ordenamiento_ShellSort>',13,10,'$'
algo32      db 09,09,'</Ordenamiento_ShellSort>',13,10,'$'

result      db 09,09,09,'<Velocidad>'
velot       db '0</Velocidad>',13,10
            db 09,09,09,'<Tiempo>',13,10
            db 09,09,09,09,'<Minutos>'
ttMinu      db '00</Minutos>',13,10
            db 09,09,09,09,'<Segundos>'
ttSecond    db '00</Segundos>',13,10
            db 09,09,09,09,'<Milisegundos>'
ttMili      db '000</Milisegundos>',13,10
            db 09,09,09,'</Tiempo>',13,10,'$'


finRep      db 09,'</Resultados>',13,10
            db '</Arqui>','$'           

; -------------------------------- FIN REPORTE --------------------------------
;=============================================================================


;-------------------------
;SESION DE USUARIO VARIABLES
;-------------------------
vmenusuario db 0ah,0dh, 
'Menu Usuario',0ah,0dh,0ah,0dh,
'1) Iniciar Juego',0ah,0dh, 
'2) Cargar Juego',0ah,0dh, 
'3) Salir', '$'

vmsgjuego db 0ah,0dh, 'Juego ','$'
;-------------------------
;SESION DE ADMINISTRADOR
;-------------------------
vmenuadmin db 0ah,0dh, 
'Menu Administrador',0ah,0dh,0ah,0dh,
'1) Top 10 puntos',0ah,0dh, 
'2) Top 10 tiempo',0ah,0dh, 
'3) Salir', '$'

vmalgoordena db 13,10,
            'Seleccione el algoritmo de ordenamiento:',13,10,13,10,
            '1) Ordenamiento BubbleSort',13,10,
            '2) Ordenamiento QuickSort',13,10,
            '3) Ordenamiento ShellSort',13,10,'$'

;-------------------------
;VARIABLES PARA GRAFICAS
;-------------------------
vmsgvelocidad       db 13,10,
                    'Velocidad de la animacion:',13,10,13,10,
                    'Ingrese una velocidad [0-9]: ',13,10,'$'

vmsordasdes db '========================================================',13,10,
               'Seleccione un tipo de ordenamiento:',13,10,13,10,
               '1) Ordenamiento Descendente',13,10,
               '2) Ordenamiento Ascendente',13,10,13,10,'$'
;--------------------------------------------------
;Variables para la hora 
;--------------------------------------------------
ho DB ?, '$'
mi DB ?, '$'
se DB ?, '$'
hconv db ?, ?, '$'
miconv db ?, ?, '$'
seconv db ?, ?, '$'
msj_separador DB " :",'$'
msjhora db "Hora: ",'$'
;--------------------------------------------------
;Variables para la fecha
;--------------------------------------------------
diaF DB ?, '$'
mesF DB ?, '$'
dmconv DB ?, ?, '$'
mconv DB ?, ?, '$'
fechaseparador DB " /",'$'
msjfecha db 'Fecha: ','$'
fechaanio DB '2020','$'


;VARIABLES PARA TODOS LOS ORDENAMIENTOS


varStart    dw  0,'$'
varEnd      dw  0,'$'
varP        dw  0,'$'
varL        dw  0,'$'
varH        dw  0,'$'
varPiv      dw  0,'$'
varIdx      dw  0,'$'
varTemp     dw  0,'$'
varOpera    dw  0,'$'
varOpera2   dw  0,'$'
varOpera3   dw  0,'$'
varN        dw  0,'$'
varN2       dw  0,'$'
varGap      dw  0,'$'
varI        dw  0,'$'
varJ        dw  0,'$'
var1        dw  0,'$'


varOtra     dw  0,'$'


varFin      dw '$'
varCero     dw 0,'$'

elTiempo    dw 0,'$'
cDelay      dw 0,'$'


nombre      db  "c:\Puntos.rep",00h

bufferAux   db 25601 dup('$')

punteroB    dw 0,'$'


frequency       db 0,'$'

;--------------------------------------------------
;Variables para el juego 
;--------------------------------------------------
vfizq db 0ah,0dh, 'presiono flecha izquierda','$'
vfder db 0ah,0dh, 'presiono flecha derecha','$'
vesca db 0ah,0dh, 'presiono escape pausa el juego','$'
vbarra db 0ah,0dh, 'presiono barra termina juego','$'

vjugando db 0ah,0dh, 'iniciando la partida ','$'

seconds db 100  ;◄■■ VARIABLE IN DATA SEGMENT.

erroru db 'Error al abrir el archivo puede que no exista', 0ah,0dh,'$' 
errord db 'Error al cerrar el archivo', 0ah,0dh,'$' 
errort db 'Error al escribir en el archivo', 0ah,0dh,'$' 
errorc db 'Error al crear el archivo', 0ah,0dh,'$' 
errorleer db 'Error al leer el archivo', 0ah,0dh,'$' 

vreportejuego db 'Reporte del juego generado correctamente.', 0ah,0dh,'$' 

vcargajuego db  0ah,0dh,'Carga del juego correctamente.', 0ah,0dh,'$' 

entradArchivo db 'C:\juego.rep',00h,'$'
handlerentrada dw ?

bufferentrada db 50 dup('$')

bufferInformacion db 515 dup('$')

EjmGuardar db 'c:\nombrearchivo.arq', 0ah,0dh,'$'

.code

;--------------------------------------------------
;MACRO PARA OBTENER RUTA
;--------------------------------------------------
ObtenerRuta macro buffer
LOCAL obtenerchar,FinOT
xor si,si ; igual a mov si,0
obtenerchar:
getchar
cmp al,0dh 
; ascii del salto de linea en hexadecimal
je FinOT
mov buffer[si],al ;mov destino,fuente
inc si ;si=si+1
jmp obtenerchar
FinOT:
mov al,00h ;ascii del signo null
mov buffer[si],al
endm

;--------------------------------------------------
;MACRO PARA ABRIR ARCHIVO
;--------------------------------------------------
Abrir macro buffer,handler
mov ah,3dh
mov al,02h
lea dx,buffer
int 21h
jc Error1
mov handler,ax
endm
;--------------------------------------------------
;MACRO PARA CERRAR ARCHIVO
;--------------------------------------------------
Cerrar macro handler
mov ah,3eh
mov bx,handler
int 21h
jc Error2
endm
;--------------------------------------------------
;MACRO PARA LEER ARCHIVO
;--------------------------------------------------
Leer macro handler,buffer,numbytes
mov ah,3fh
mov bx,handler
mov cx,numbytes
lea dx,buffer
int 21h
jc  Error3
endm

;--------------------------------------------------
;MACRO PARA CREAR ARCHIVO
;--------------------------------------------------
CrearArchivo macro buffer,handler
mov ah,3ch
mov cx,00h
lea dx,buffer
int 21h
jc Error4
mov handler,ax
endm
;--------------------------------------------------
;MACRO PARA ESCRIBIR ARCHIVO
;--------------------------------------------------
Escribir macro handler,buffer,numbytes
mov ah,40h
mov bx,handler
mov cx,numbytes
lea dx,buffer
int 21h
jc Error5
endm
;--------------------------------------------------
;MACRO PARA LIMPIAR VARIABLES
;--------------------------------------------------
Limpiar macro buffer,numbytes,caracter
LOCAL Repetir
xor si,si
xor cx,cx
mov cx,numbytes
Repetir:
mov buffer[si],caracter
inc si
Loop Repetir
endm
;-------------------------
;MACRO PARA COUNT
;-------------------------
count_c MACRO string
    LOCAL CICLO,FIN
   XOR cx,cx
   MOV SI, 0
CICLO:   CMP string[SI],'$'
         JE FIN
         INC cx
         INC SI
         JMP CICLO
FIN:
ENDM

put_bcd2 proc
    PUSH    ax
    SHR     ax, 04
    AND     ax, 0fh
    ADD     ax, '0'
    MOV     [bx], al
    INC     bx
    POP     ax

    AND     ax, 0fh
    ADD     ax, '0'
    MOV     [bx], al

    INC     bx

    RET
put_bcd2 endp
get_time proc
    mov     ah, 02h
    int     1ah

    mov     bx, offset time
    mov     al, ch
    call    put_bcd2

    inc     bx
    mov     al, cl
    call    put_bcd2

    inc     bx
    mov     al, dh
    call    put_bcd2

    ret
get_time endp

get_date proc
    mov     ah, 04h
    int     1ah

    mov     bx, offset date
    mov     al, dl
    call    put_bcd2

    inc     bx
    mov     al, dh
    call    put_bcd2

    inc     bx
    mov     al, ch
    call    put_bcd2
    mov     al, cl
    call    put_bcd2

    ret
get_date endp
show_rep proc

    call    get_date
    call    get_time

; -------------- MOVIENDO DIA --------------
    XOR ax,ax
    MOV al,date[0]
    MOV tDia[8],al
    MOV al,date[1]
    MOV tDia[9],al

; -------------- MOVIENDO MES --------------
    MOV al,date[3]
    MOV tMes[8],al
    MOV al,date[4]
    MOV tMes[9],al

; -------------- MOVIENDO AÑO --------------
    MOV al,date[6]
    MOV tAnio[9],al
    MOV al,date[7]
    MOV tAnio[10],al
    MOV al,date[8]
    MOV tAnio[11],al
    MOV al,date[9]
    MOV tAnio[12],al

; -------------- MOVIENDO HORA --------------
    MOV al,time[0]
    MOV tHora[09],al
    MOV al,time[1]
    MOV tHora[10],al

; -------------- MOVIENDO MINUTOS --------------
    MOV al,time[3]
    MOV tMinuto[12],al
    MOV al,time[4]
    MOV tMinuto[13],al

; -------------- MOVIENDO SEGUNDOS --------------
    MOV al,time[6]
    MOV tSegundo[13],al
    MOV al,time[7]
    MOV tSegundo[14],al

;; -------------- FIN MOVIMIENTOS --------------

    MOV    ah,3ch
    MOV    cx,00
    LEA    dx,nombre
    INT    21h

    MOV    handler,ax

    MOV    ah,40h
    MOV    bx,handler
    count_c repXML
    LEA    dx,repXML
    INT    21h

;==================================================
;---------------- MOVER RESULTADOS ----------------
    
    
    MOV    ah,40h
    MOV    bx,handler
    count_c bufferAux
    LEA    dx,bufferAux
    INT    21h

;---------------- MOVER RESULTADOS ----------------
;==================================================
    
    MOV    ah,40h    
    MOV    bx,handler    
    count_c rtopjc
    LEA    dx,rtopjc
    INT    21h

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc1
    LEA    dx,rtopjc1
    INT    21h

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc2
    LEA    dx,rtopjc2
    INT    21h

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc3
    LEA    dx,rtopjc3
    INT    21h    

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc4
    LEA    dx,rtopjc4
    INT    21h    

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc5
    LEA    dx,rtopjc5
    INT    21h    

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc6
    LEA    dx,rtopjc6
    INT    21h    

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc7
    LEA    dx,rtopjc7
    INT    21h    

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc8
    LEA    dx,rtopjc8
    INT    21h    

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc9
    LEA    dx,rtopjc9
    INT    21h    

    MOV    ah,40h    
    MOV    bx,handler        
    count_c rtopjc10
    LEA    dx,rtopjc10
    INT    21h    


    MOV    ah,40h
    MOV    bx,handler
    count_c finRep
    LEA    dx,finRep
    INT    21h
    
    MOV    ah,3eh
    MOV    bx,handler
    INT    21h

    ret 

show_rep endp

;--------------------------------------------------
;CONVERTIR UN DB A DECIMAL
;--------------------------------------------------
CambioDbDecimal macro XdbNumero,XdbConvertido
    mov al, [XdbNumero]
    mov cl, 10
    mov ah, 0
    div cl
    or ax, 3030h
    mov bx, offset XdbConvertido
    mov [bx], al
    inc bx
    mov [bx], ah
endm


WriteString proc
        ;--------------------------------------------------------------------;
        ;   Recibe:      [bp+4] apunta a la cadena                           ;
        ;                                                                    ;
        ;   Devuelve:    Nada.                                               ;
        ;                                                                    ;
        ;   Comentarios: Recibe una cadena con terminacion nula y            ;
        ;                lo imprime en la salida estandar                    ;
        ;--------------------------------------------------------------------;

        ;Subrutina proglogo
            push bp                    ;almacenamos el puntero base
            mov  bp,sp                 ;ebp contiene la direccion de esp
            pusha
        ;Ini Codigo--
            mov ah,09
            mov dx,word ptr[bp+4]
            int 21h
        ;Fin Codigo--

        ;Subrutina epilogo
            popa
            mov sp,bp               ;esp vuelve apuntar al inicio y elimina las variables locales
            pop bp                  ;restaura el valor del puntro base listo para el ret
        ret 2
    WriteString endp

    printvideo macro cadena
    push offset cadena
    call WriteString
    endm
delay proc
        ;--------------------------------------------------------------------
        ;   Recibe:      AX = constante                                      
        ;                                                                    
        ;   Devuelve:    Nada                                                
        ;                                                                    
        ;   Comentarios: provoca un retardo hipergemetrico por software              
        ;--------------------------------------------------------------------

        ;ini Subrutina proglogo
            push si
            push di
        ;fin Surutina prologo

        ;Ini Codigo--
            
            mov si,ax                   ;constante
            D1:
                dec si
                jz Fin
                mov di,ax               ;constante
            D2:
                dec di
                jnz D2
                jmp D1 
        ;Fin Codigo--

        ;ini Subrutina epilogo
            Fin:
                pop di
                pop si
                ret
            ;fin etiqueta
        ;fin Subrutina epilogo
delay endp

mDelay macro constante
    push ax
    mov ax,constante
    call delay
    pop ax
endm


SetCursor proc
        ;--------------------------------------------------------------------
        ;   Recibe:      [bp+4] columna
        ;                [bp+6] fila                                
        ;                                                                    
        ;   Devuelve:    nada                                                
        ;                                                                    
        ;   Comentarios: Setea el cursor en la fila y columna enviada                             
        ;--------------------------------------------------------------------

        ;ini Subrutina proglogo
            push bp                    ;almacenamos el puntero base
            mov  bp,sp                 ;ebp contiene la direccion de esp
            push ax
            push bx
            push dx
        ;fin Subrutina prologo

        ;Ini Codigo--
            mov ah , 02H                    ;MOVE CURSOR TO CENTER OF SCREEN
            mov bh , 0                      ;pagina 
            mov dh , byte ptr[bp+6]         ;ROW
            mov dl , byte ptr[bp+4]         ;COL
            int 10h
        ;Fin Codigo--


        ;ini Subrutina epilogo
            FIN:
                pop dx
                pop bx
                pop ax
                mov sp,bp               ;esp vuelve apuntar al inicio y elimina las variables locales
                pop bp                  ;restaura el valor del puntro base listo para el ret
                ret 4
            ;fin etiqueta
        ;fin Subrutna epilogo
    SetCursor endp

mSetCursor macro fila,columna   ;para colocar el cursor donde queramos en el modo video y hace poder imprimir
    push fila 
    push columna
    call SetCursor
endm

;--------------------------------------------------
;MACRO PINTAR CARRO
;--------------------------------------------------

pintarcarro macro  pos,color,colorllanta
LOCAL comienzo,aumento,sinaumento,disminuye,sindecremento
push dx
mov di, pos
comienzo:
mov dl,6
cmp es:[di],dl
je aumento
cmp es:[di+1],dl
je aumento
cmp es:[di+2],dl
je aumento
cmp es:[di+3],dl
je aumento
cmp es:[di+4],dl
je aumento
cmp es:[di+5],dl
je aumento
cmp es:[di+6],dl
je aumento
cmp es:[di+7],dl
je aumento
cmp es:[di+8],dl
je aumento
cmp es:[di+9],dl
je aumento
cmp es:[di+10],dl
je aumento
cmp es:[di+11],dl
je aumento
cmp es:[di+12],dl
je aumento
cmp es:[di+13],dl
je aumento
cmp es:[di+14],dl
je aumento
cmp es:[di+15],dl
je aumento
cmp es:[di+16],dl
je aumento
cmp es:[di+17],dl
je aumento
cmp es:[di+18],dl
je aumento
cmp es:[di+19],dl
je aumento
cmp es:[di+20],dl
je aumento
jmp sinaumento
aumento:
inc conpuntaje

sinaumento:
mov dl,2
cmp es:[di],dl
je disminuye
cmp es:[di+1],dl
je disminuye
cmp es:[di+2],dl
je disminuye
cmp es:[di+3],dl
je disminuye
cmp es:[di+4],dl
je disminuye
cmp es:[di+5],dl
je disminuye
cmp es:[di+6],dl
je disminuye
cmp es:[di+7],dl
je disminuye
cmp es:[di+8],dl
je disminuye
cmp es:[di+9],dl
je disminuye
cmp es:[di+10],dl
je disminuye
cmp es:[di+11],dl
je disminuye
cmp es:[di+12],dl
je disminuye
cmp es:[di+13],dl
je disminuye
cmp es:[di+14],dl
je disminuye
cmp es:[di+15],dl
je disminuye
cmp es:[di+16],dl
je disminuye
cmp es:[di+17],dl
je disminuye
cmp es:[di+18],dl
je disminuye
cmp es:[di+19],dl
je disminuye
cmp es:[di+20],dl
je disminuye
jmp sindecremento

disminuye:
dec conpuntaje

sindecremento:
mov dl, color


mov es:[di],dl
mov es:[di+1],dl
mov es:[di+2],dl
mov es:[di+3],dl
mov es:[di+4],dl
mov es:[di+5],dl
mov es:[di+6],dl
mov es:[di+7],dl
mov es:[di+8],dl
mov es:[di+9],dl
mov es:[di+10],dl
mov es:[di+11],dl
mov es:[di+12],dl
mov es:[di+13],dl
mov es:[di+14],dl
mov es:[di+15],dl
mov es:[di+16],dl
mov es:[di+17],dl
mov es:[di+18],dl
mov es:[di+19],dl
mov es:[di+20],dl


mov es:[di +320],dl
mov es:[di +321],dl
mov es:[di +322],dl
mov es:[di +323],dl
mov es:[di +324],dl
mov es:[di +325],dl
mov es:[di +326],dl
mov es:[di +327],dl
mov es:[di +328],dl
mov es:[di +329],dl
mov es:[di +330],dl
mov es:[di +331],dl
mov es:[di +332],dl
mov es:[di +333],dl
mov es:[di +334],dl
mov es:[di +335],dl
mov es:[di +336],dl
mov es:[di +337],dl
mov es:[di +338],dl
mov es:[di +339],dl
mov es:[di +340],dl

mov es:[di +640],dl
mov es:[di +641],dl
mov es:[di +642],dl
mov es:[di +643],dl
mov es:[di +644],dl
mov es:[di +645],dl
mov es:[di +646],dl
mov es:[di +647],dl
mov es:[di +648],dl
mov es:[di +649],dl
mov es:[di +650],dl
mov es:[di +651],dl
mov es:[di +652],dl
mov es:[di +653],dl
mov es:[di +654],dl
mov es:[di +655],dl
mov es:[di +656],dl
mov es:[di +657],dl
mov es:[di +658],dl
mov es:[di +659],dl
mov es:[di +660],dl

mov dl, colorllanta
mov es:[di +957],dl
mov es:[di +958],dl
mov es:[di +959],dl
mov dl, color

mov es:[di +960],dl
mov es:[di +961],dl
mov es:[di +962],dl
mov es:[di +963],dl
mov es:[di +964],dl
mov es:[di +965],dl
mov es:[di +966],dl
mov es:[di +967],dl
mov es:[di +968],dl
mov es:[di +969],dl
mov es:[di +970],dl
mov es:[di +971],dl
mov es:[di +972],dl
mov es:[di +973],dl
mov es:[di +974],dl
mov es:[di +975],dl
mov es:[di +976],dl
mov es:[di +977],dl
mov es:[di +978],dl
mov es:[di +979],dl
mov es:[di +980],dl

mov dl, colorllanta
mov es:[di +981],dl
mov es:[di +982],dl
mov es:[di +983],dl

mov es:[di +1277],dl
mov es:[di +1278],dl
mov es:[di +1279],dl

mov dl, color


mov es:[di +1280],dl
mov es:[di +1281],dl
mov es:[di +1282],dl
mov es:[di +1283],dl
mov es:[di +1284],dl
mov es:[di +1285],dl
mov es:[di +1286],dl
mov es:[di +1287],dl
mov es:[di +1288],dl
mov es:[di +1289],dl
mov es:[di +1290],dl
mov es:[di +1291],dl
mov es:[di +1292],dl
mov es:[di +1293],dl
mov es:[di +1294],dl
mov es:[di +1295],dl
mov es:[di +1296],dl
mov es:[di +1297],dl
mov es:[di +1298],dl
mov es:[di +1299],dl
mov es:[di +1300],dl

mov dl, colorllanta
mov es:[di +1301],dl
mov es:[di +1302],dl
mov es:[di +1303],dl

mov es:[di +1597],dl
mov es:[di +1598],dl
mov es:[di +1599],dl

mov dl, color



mov es:[di +1600],dl
mov es:[di +1601],dl
mov es:[di +1602],dl
mov es:[di +1603],dl
mov es:[di +1604],dl
mov es:[di +1605],dl
mov es:[di +1606],dl
mov es:[di +1607],dl
mov es:[di +1608],dl
mov es:[di +1609],dl
mov es:[di +1610],dl
mov es:[di +1611],dl
mov es:[di +1612],dl
mov es:[di +1613],dl
mov es:[di +1614],dl
mov es:[di +1615],dl
mov es:[di +1616],dl
mov es:[di +1617],dl
mov es:[di +1618],dl
mov es:[di +1619],dl
mov es:[di +1620],dl

mov dl, colorllanta
mov es:[di +1621],dl
mov es:[di +1622],dl
mov es:[di +1623],dl
mov dl, color



mov es:[di +1920],dl
mov es:[di +1921],dl
mov es:[di +1922],dl
mov es:[di +1923],dl
mov es:[di +1924],dl
mov es:[di +1925],dl
mov es:[di +1926],dl
mov es:[di +1927],dl
mov es:[di +1928],dl
mov es:[di +1929],dl
mov es:[di +1930],dl
mov es:[di +1931],dl
mov es:[di +1932],dl
mov es:[di +1933],dl
mov es:[di +1934],dl
mov es:[di +1935],dl
mov es:[di +1936],dl
mov es:[di +1937],dl
mov es:[di +1938],dl
mov es:[di +1939],dl
mov es:[di +1940],dl

mov es:[di +2240],dl
mov es:[di +2241],dl
mov es:[di +2242],dl
mov es:[di +2243],dl
mov es:[di +2244],dl
mov es:[di +2245],dl
mov es:[di +2246],dl
mov es:[di +2247],dl
mov es:[di +2248],dl
mov es:[di +2249],dl
mov es:[di +2250],dl
mov es:[di +2251],dl
mov es:[di +2252],dl
mov es:[di +2253],dl
mov es:[di +2254],dl
mov es:[di +2255],dl
mov es:[di +2256],dl
mov es:[di +2257],dl
mov es:[di +2258],dl
mov es:[di +2259],dl
mov es:[di +2260],dl

mov es:[di +2560],dl
mov es:[di +2561],dl
mov es:[di +2562],dl
mov es:[di +2563],dl
mov es:[di +2564],dl
mov es:[di +2565],dl
mov es:[di +2566],dl
mov es:[di +2567],dl
mov es:[di +2568],dl
mov es:[di +2569],dl
mov es:[di +2570],dl
mov es:[di +2571],dl
mov es:[di +2572],dl
mov es:[di +2573],dl
mov es:[di +2574],dl
mov es:[di +2575],dl
mov es:[di +2576],dl
mov es:[di +2577],dl
mov es:[di +2578],dl
mov es:[di +2579],dl
mov es:[di +2580],dl

mov es:[di +2880],dl
mov es:[di +2881],dl
mov es:[di +2882],dl
mov es:[di +2883],dl
mov es:[di +2884],dl
mov es:[di +2885],dl
mov es:[di +2886],dl
mov es:[di +2887],dl
mov es:[di +2888],dl
mov es:[di +2889],dl
mov es:[di +2890],dl
mov es:[di +2891],dl
mov es:[di +2892],dl
mov es:[di +2893],dl
mov es:[di +2894],dl
mov es:[di +2895],dl
mov es:[di +2896],dl
mov es:[di +2897],dl
mov es:[di +2898],dl
mov es:[di +2899],dl
mov es:[di +2900],dl

mov es:[di +3200],dl
mov es:[di +3201],dl
mov es:[di +3202],dl
mov es:[di +3203],dl
mov es:[di +3204],dl
mov es:[di +3205],dl
mov es:[di +3206],dl
mov es:[di +3207],dl
mov es:[di +3208],dl
mov es:[di +3209],dl
mov es:[di +3210],dl
mov es:[di +3211],dl
mov es:[di +3212],dl
mov es:[di +3213],dl
mov es:[di +3214],dl
mov es:[di +3215],dl
mov es:[di +3216],dl
mov es:[di +3217],dl
mov es:[di +3218],dl
mov es:[di +3219],dl
mov es:[di +3220],dl

mov es:[di +3520],dl
mov es:[di +3521],dl
mov es:[di +3522],dl
mov es:[di +3523],dl
mov es:[di +3524],dl
mov es:[di +3525],dl
mov es:[di +3526],dl
mov es:[di +3527],dl
mov es:[di +3528],dl
mov es:[di +3529],dl
mov es:[di +3530],dl
mov es:[di +3531],dl
mov es:[di +3532],dl
mov es:[di +3533],dl
mov es:[di +3534],dl
mov es:[di +3535],dl
mov es:[di +3536],dl
mov es:[di +3537],dl
mov es:[di +3538],dl
mov es:[di +3539],dl
mov es:[di +3540],dl

mov dl, colorllanta
mov es:[di +3837],dl
mov es:[di +3838],dl
mov es:[di +3839],dl
mov dl, color

mov es:[di +3840],dl
mov es:[di +3841],dl
mov es:[di +3842],dl
mov es:[di +3843],dl
mov es:[di +3844],dl
mov es:[di +3845],dl
mov es:[di +3846],dl
mov es:[di +3847],dl
mov es:[di +3848],dl
mov es:[di +3849],dl
mov es:[di +3850],dl
mov es:[di +3851],dl
mov es:[di +3852],dl
mov es:[di +3853],dl
mov es:[di +3854],dl
mov es:[di +3855],dl
mov es:[di +3856],dl
mov es:[di +3857],dl
mov es:[di +3858],dl
mov es:[di +3859],dl
mov es:[di +3860],dl

mov dl, colorllanta
mov es:[di +3861],dl
mov es:[di +3862],dl
mov es:[di +3863],dl

mov es:[di +4157],dl
mov es:[di +4158],dl
mov es:[di +4159],dl

mov dl, color

mov es:[di +4160],dl
mov es:[di +4161],dl
mov es:[di +4162],dl
mov es:[di +4163],dl
mov es:[di +4164],dl
mov es:[di +4165],dl
mov es:[di +4166],dl
mov es:[di +4167],dl
mov es:[di +4168],dl
mov es:[di +4169],dl
mov es:[di +4170],dl
mov es:[di +4171],dl
mov es:[di +4172],dl
mov es:[di +4173],dl
mov es:[di +4174],dl
mov es:[di +4175],dl
mov es:[di +4176],dl
mov es:[di +4177],dl
mov es:[di +4178],dl
mov es:[di +4179],dl
mov es:[di +4180],dl

mov dl, colorllanta
mov es:[di +4181],dl
mov es:[di +4182],dl
mov es:[di +4183],dl

mov es:[di +4477],dl
mov es:[di +4478],dl
mov es:[di +4479],dl

mov dl, color

mov es:[di +4480],dl
mov es:[di +4481],dl
mov es:[di +4482],dl
mov es:[di +4483],dl
mov es:[di +4484],dl
mov es:[di +4485],dl
mov es:[di +4486],dl
mov es:[di +4487],dl
mov es:[di +4488],dl
mov es:[di +4489],dl
mov es:[di +4490],dl
mov es:[di +4491],dl
mov es:[di +4492],dl
mov es:[di +4493],dl
mov es:[di +4494],dl
mov es:[di +4495],dl
mov es:[di +4496],dl
mov es:[di +4497],dl
mov es:[di +4498],dl
mov es:[di +4499],dl
mov es:[di +4500],dl

mov dl, colorllanta
mov es:[di +4501],dl
mov es:[di +4502],dl
mov es:[di +4503],dl

mov es:[di +4797],dl
mov es:[di +4798],dl
mov es:[di +4799],dl

mov dl, color

mov es:[di +4800],dl
mov es:[di +4801],dl
mov es:[di +4802],dl
mov es:[di +4803],dl
mov es:[di +4804],dl
mov es:[di +4805],dl
mov es:[di +4806],dl
mov es:[di +4807],dl
mov es:[di +4808],dl
mov es:[di +4809],dl
mov es:[di +4810],dl
mov es:[di +4811],dl
mov es:[di +4812],dl
mov es:[di +4813],dl
mov es:[di +4814],dl
mov es:[di +4815],dl
mov es:[di +4816],dl
mov es:[di +4817],dl
mov es:[di +4818],dl
mov es:[di +4819],dl
mov es:[di +4820],dl

mov dl, colorllanta
mov es:[di +4821],dl
mov es:[di +4822],dl
mov es:[di +4823],dl
mov dl, color

mov es:[di +5120],dl
mov es:[di +5121],dl
mov es:[di +5122],dl
mov es:[di +5123],dl
mov es:[di +5124],dl
mov es:[di +5125],dl
mov es:[di +5126],dl
mov es:[di +5127],dl
mov es:[di +5128],dl
mov es:[di +5129],dl
mov es:[di +5130],dl
mov es:[di +5131],dl
mov es:[di +5132],dl
mov es:[di +5133],dl
mov es:[di +5134],dl
mov es:[di +5135],dl
mov es:[di +5136],dl
mov es:[di +5137],dl
mov es:[di +5138],dl
mov es:[di +5139],dl
mov es:[di +5140],dl

pop dx
endm
;--------------------------------------------------
;MACRO PINTAR MARGEN JUEGO
;--------------------------------------------------
;mov [di],30h
pintarmargen macro color
LOCAL primera,segunda,tercera,cuarta
mov dl,color
; empiza en pixel (i,j) = (20,0)= 320*20 +0 = 6400
;barra horizontal superior 
mov di,6405
primera:
mov es:[di],dl
inc di
cmp di,6714 
jne primera
;barra horizontal inferior
;empieza en el pixel (i,j)=(190,0)= 320*190 + 0 = 60800
mov di,60805
segunda:
mov es:[di],dl
inc di
cmp di,61114
jne segunda

; barra vertical izquierda
mov di,6405 
tercera:
mov es:[di],dl
add di, 320
cmp di, 60805
jne tercera

; barra vertical derecha
mov di,6714 
cuarta:
mov es:[di],dl
add di, 320
cmp di, 61114
jne cuarta

endm
;--------------------------------------------------
;MACRO MODO VIDEO
;--------------------------------------------------
modovideo macro
mov ah,00h
mov al ,13h
int 10h
mov ax, 0A000h
;mov ds, ax  ; DS = A000h (memoria de graficos).
mov es, ax  ; DS = A000h (memoria de graficos).
endm



partition MACRO vP,vStart,vEnd
    LOCAL w1,w2,w3,w4,s1,s2
    
    mov ax,vStart
    mov varL, ax

    mov ax,vEnd
    mov varH, ax
    sub varH,2

    mov si,vP
    xor ax,ax
    mov al,misNumeros2[si]
    mov varPiv, ax

    mov ax,vP
    mov varOpera,ax

    mov ax,vEnd
    mov varOpera2,ax
    dec varOpera2

    swap varOpera,varOpera2

w1: mov ax,varH
    cmp varL,ax
    jnb s1

w2: mov bx,varL
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jnb w3

    inc varL
    jmp w1

w3: mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jnae w4

    dec varH
    jmp w1

w4: 
    mov ax,varL
    mov varOpera,ax

    mov ax,varH
    mov varOpera2,ax

    swap varOpera,varOpera2
    jmp w1

s1: mov ax,varH
    mov varIdx,ax

    mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]

    cmp ax,varPiv
    jnb s2

    inc varIdx
s2: 
    mov ax,vEnd
    mov varOpera,ax
    dec varOpera

    mov ax,varIdx
    mov varOpera2,ax

    swap varOpera,varOpera2

    mov ax,varIdx
    mov varP,ax

ENDM



iterativeQsort MACRO
    local w1,w2, fin3
    
    push varFin
    push varCero
    push contador2


w1: pop var1

    cmp var1,'$'
    je fin3

    mov ax,var1
    mov varEnd,ax

    pop varStart

    mov ax,varEnd
    sub ax,varStart
    cmp ax,2
    jnb w2

    jmp w1


w2: mov ax,varEnd
    sub ax,varStart
    sar ax,1
    add ax,varStart
    mov varP,ax

    mov ax,varP
    mov varOpera,ax

    mov ax,varStart
    mov varOpera2,ax

    mov ax, varEnd
    mov varOpera3,ax

    partition varOpera,varOpera2,varOpera3

    inc varP
    push varP
    push varEnd
    push varStart
    dec varP
    push varP

    jmp w1

fin3:

ENDM

swap MACRO vI,vJ

    mov ax,vI
    mov varOtra,ax


    mov ax,vJ
    mov varOtra,ax

    xor ax,ax
    xor si,si
    xor di,di

    mov di,vI
    mov si,vJ

    mov al,misNumeros2[di]
    mov varTemp,ax
    mov frequency,al

    xor ax,ax
    mov al,misNumeros2[si]
    mov misNumeros2[di],al

    mov ax,varTemp
    mov misNumeros2[si],al

    call emit_beep
    call repaint
    call end_beep

ENDM



partition2 MACRO vP,vStart,vEnd
    LOCAL w1,w2,w3,w4,s1,s2
    
    mov ax,vStart
    mov varL, ax

    mov ax,vEnd
    mov varH, ax
    sub varH,2

    mov si,vP
    xor ax,ax
    mov al,misNumeros2[si]
    mov varPiv, ax

    mov ax,vP
    mov varOpera,ax

    mov ax,vEnd
    mov varOpera2,ax
    dec varOpera2

    swap varOpera,varOpera2

w1: mov ax,varH
    cmp varL,ax
    jnb s1

w2: mov bx,varL
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jna w3

    inc varL
    jmp w1

w3: mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jnbe w4

    dec varH
    jmp w1

w4: 
    mov ax,varL
    mov varOpera,ax

    mov ax,varH
    mov varOpera2,ax

    swap varOpera,varOpera2
    jmp w1

s1: mov ax,varH
    mov varIdx,ax

    mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]

    cmp ax,varPiv
    jna s2

    inc varIdx
s2: 
    mov ax,vEnd
    mov varOpera,ax
    dec varOpera

    mov ax,varIdx
    mov varOpera2,ax

    swap varOpera,varOpera2

    mov ax,varIdx
    mov varP,ax

ENDM





iterativeQsort2 MACRO
    local w1,w2, fin3
    
    push varFin
    push varCero
    push contador2


w1: pop var1

    cmp var1,'$'
    je fin3

    mov ax,var1
    mov varEnd,ax

    pop varStart

    mov ax,varEnd
    sub ax,varStart
    cmp ax,2
    jnb w2

    jmp w1


w2: mov ax,varEnd
    sub ax,varStart
    sar ax,1
    add ax,varStart
    mov varP,ax

    mov ax,varP
    mov varOpera,ax

    mov ax,varStart
    mov varOpera2,ax

    mov ax, varEnd
    mov varOpera3,ax

    partition2 varOpera,varOpera2,varOpera3

    inc varP
    push varP
    push varEnd
    push varStart
    dec varP
    push varP

    jmp w1

fin3:

ENDM


;-------------------------
;MACRO PARA imprimir 2
;-------------------------
display_string2 MACRO cadena,fila

    mov resDx,dx
    mov resCx,cx
    mov resBx,bx
    mov resAx,ax

    mov al,1
    xor bh,bh
    mov bl,15
    count_c cadena

    mov dh,fila
    mov dl,cursor1
    push ds
    pop es
    mov bp, offset cadena
    mov ah,13h
    add cursor1,cl
    inc cursor1
    int 10h

    mov ax,resAx
    mov bx,resBx
    mov cx,resCx
    mov dx,resDx


ENDM

;-------------------------
;MACRO PARA LEER OPCION 
;-------------------------
read_choice proc

    MOV ah,01h
    int 21h
    sub al,30h
    mov seleccion,al

    mov ah,09
    lea dx,salto
    int 21h

    ret

read_choice endp
;-------------------------
;MACRO PARA EL ESPACIO ENTRE LAS BARRAS
;-------------------------
c_space proc

    mov anchoG,0
    mov lateral,13
    mov cursor2,0
    mov varX,12
    mov varX2,8

    cmp contador1,10
    jne s1

    mov anchoG,19

    mov varX2,20
    mov varEsp,9
        
        
    mov varX,24
    mov cursor2,19

    jmp s16


s1: cmp contador1,11
    jne s2

    mov anchoG,18

    mov varX2,20
    mov varEsp,7
        
        
    mov varX,24
    mov cursor2,17

    jmp s16


s2: cmp contador1,12
    jne s3

    mov anchoG,17

    mov varX2,19
    mov varEsp,7
        
        
    mov varX,23
    mov cursor2,15

    jmp s16


s3: cmp contador1,13
    jne s4


    mov anchoG,16

    mov varX2,18
    mov varEsp,6
        
        
    mov varX,21
    mov cursor2,13

    jmp s16


s4: cmp contador1,14
    jne s5

    mov anchoG,15

    mov varX2,21
    mov varEsp,5
        
        
    mov varX,23
    mov cursor2,11

    jmp s16


s5: cmp contador1,15
    jne s6

    mov anchoG,14

    mov varX2,19
    mov varEsp,5
        
        
    mov varX,21
    mov cursor2,10

    jmp s16


s6: cmp contador1,16
    jne s7


    mov anchoG,13

    mov varX2,17
    mov varEsp,5
        
        
    mov varX,19
    mov cursor2,9

    jmp s16



s7: cmp contador1,17
    jne s8

    mov anchoG,13

    mov varX2,16
    mov varEsp,4
        
        
    mov varX,18
    mov cursor2,8

    jmp s16


s8: cmp contador1,18
    jne s9

    mov anchoG,12

    mov varX2,16
    mov varEsp,4
        
        
    mov varX,17
    mov cursor2,7

    jmp s16

s9: cmp contador1,19
    jne s10

    mov anchoG,11

    mov varX2,16
    mov varEsp,4
        
        
    mov varX,17
    mov cursor2,6

    jmp s16


s10:    cmp contador1,20
        jne s11

        mov anchoG,12

        mov varX2,11
        mov varEsp,3
        
        
        mov varX,13
        mov cursor2,6

        jmp s16


s11:    cmp contador1,21
        jne s12

        mov anchoG,11

        mov varX2,13
        mov varEsp,3
        
        
        mov varX,14
        mov cursor2,5

        jmp s16


s12:    cmp contador1,22
        jne s13

        mov anchoG,10

        mov varX2,17
        mov varEsp,3
        
        
        mov varX,17
        mov cursor2,4

        jmp s16


s13:    cmp contador1,23
        jne s14

        mov anchoG,10

        mov varX2,11
        mov varEsp,3
        
        
        mov varX,12
        mov cursor2,4

        jmp s16


s14:    cmp contador1,24
        jne s15

        mov anchoG,9

        mov varX2,16
        mov varEsp,3
        
        
        mov varX,16
        mov cursor2,3

        jmp s16


s15:    cmp contador1,25
        jne s16

        mov anchoG,8

        mov varX2,12
        mov varEsp,4
        
        
        mov varX,12
        mov cursor2,3

        jmp s16
    
s16:
        ret

c_space endp


;-------------------------
;MACRO PINTAR EN PANTALLA
;-------------------------

display_string Macro string

    mov resAx,ax
    mov resDx,dx


    MOV  dx, offset string
    MOV  ah, 9
    INT  21h

    mov ax,resAx
    mov dx,resDx

ENDM
;-------------------------
;MACRO PARA CONVERTIR DECIMAL
;-------------------------

convert_decimal MACRO numero
    LOCAL FIN, C1, C2, C3, C4, C5, S1, S2, S3, S4, S5

    MOV CX,numero

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C1

    MOV decimal[0],dl

    XOR DX,DX
    MOV AX,CX
    MOV SI, 1000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C2

    MOV decimal[1],dl

    XOR DX,DX
    MOV AX,CX
    MOV SI, 100
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C3

    MOV decimal[2],dl 

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C4

    MOV decimal[3],dl

    XOR DX,DX
    MOV AX,CX
    MOV SI, 1
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C5

    MOV decimal[4],dl
    MOV AH,06H
    INT 21H 

    JMP FIN

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C1: MOV decimal[0],dl
    MOV AH,06H
    INT 21H

    XOR DX,DX
    MOV AX,CX
    MOV SI, 1000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C2: MOV decimal[1],dl
    MOV AH,06H
    INT 21H

    XOR DX,DX
    MOV AX,CX
    MOV SI, 100
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C3: MOV decimal[2],dl 
    MOV AH,06H
    INT 21H

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C4: MOV decimal[3],dl
    MOV AH,06H
    INT 21H


    XOR DX,DX
    MOV AX,CX
    MOV SI, 1
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C5: MOV decimal[4],dl
    MOV AH,06H
    INT 21H 


FIN:
ENDM
;-------------------------
;MACRO PARA MOSTRAR ARREGLO
;-------------------------
mostrar_arreglo proc

    mov resCx,cx
    mov resBx,bx
    
    XOR BX,BX
    XOR CX,CX
    
C1: convert_decimal misNumeros[BX]
    display_string coma
    INC BX
    INC BX
    CMP BX, 48
    JA FIN2
    JMP C1
FIN2:
    display_string salto

    mov cx,resCx
    mov bx,resBx 

    ret


mostrar_arreglo endp

;--------------------------------------------------
;MACRO PARA IMPRIMIR EN PANTALLA
;--------------------------------------------------
imprimir macro texto
    mov ax,@data    
    mov ds,ax  
    mov ah,09h ; numero de funcion para imprimir cadena en pantalla
;igual a lea dx,cadena,inicializa en dx la posicion donde comienza la cadena
    mov dx,offset texto 
    int 21h
endm


HoraActual macro horas,minutos,segundos,horas_conv,min_conv,seg_conv

mov ah, 2Ch
int 21h

mov  horas, ch
mov  minutos, cl
mov  segundos, dh

mov al, [horas]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset horas_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [minutos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset min_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [segundos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset seg_conv
mov [bx], al
inc bx
mov [bx], ah

endm

FechaActual macro  dia_mes,mes,dia_mes_conv,mes_ano_conv
	mov ah, 2AH
	int 21h
	;mov  fechaanio, cx
	mov  mes, dh
	mov  dia_mes, dl	
;Obtener el número del día del mes
;Dividimos entre 10 para obtener el número en decimal
;Luego lo transformamos a ASCII
mov al, [dia_mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset dia_mes_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset mes_ano_conv 
mov [bx], al
inc bx
mov [bx], ah

endm
;--------------------------------------------------
;MACRO PARA OBTENER CARACTER
;--------------------------------------------------
getchar macro
mov ah,0dh
int 21h
mov ah,01h
int 21h
endm


;--------------------------------------------------
;MACRO PARA LIMPIAR CONSOLA
;--------------------------------------------------
LimpiarConsola macro
mov ax,0600h
mov bh,89h
mov cx,0000h
mov dx,184Fh
int 10h
endm
;--------------------------------------------------
;MACRO PARA LIMPIAR PANTALLA
;--------------------------------------------------
clear_screen proc

    mov  ah, 0
    mov  al, 3
    int  10H

    ret

clear_screen endp
;--------------------------------------------------
;MACRO PARA LLEVAR LA RAPIDEZ
;--------------------------------------------------
mov_delay proc

    xor dx,dx
    xor ax,ax

    mov ax,750
    mov dl,seleccion
    mov elTiempo,dx
    inc elTiempo

    mov vVelo2[0],dl
    add vVelo2[0],48
    mul dx

    mov retraso,ax

    ret

mov_delay endp

;--------------------------------------------------
;MACRO PARA MOVIMIENTO DE LOS NUMEROS
;--------------------------------------------------
mov_misNumeros proc

    xor si,si
    xor di,di

c1: cmp di,contador2
    je c2

    xor ax,ax
    mov al,desordenados[di]
    mov misNumeros[si],ax

    inc di
    add si,2

    jmp c1

c2:
    ret
mov_misNumeros endp

;--------------------------------------------------
;MACRO PARA DIBUJAR
;--------------------------------------------------

draw_square proc

        mov cursor1,1

        display_string2 car1,0
        cmp orden,1
        jne m1

        display_string2 bbs,0
        jmp m4 

m1:     cmp orden,2
        jne m2

        display_string2 qks,0
        jmp m4 


m2:     cmp orden,3
        jne m4

        display_string2 shs,0

m4:     
        display_string2 vVelo,0
        
        mov cursor1, 1
        display_string2 tTime,1

        mov xCor,6
        mov yCor,20

c1:     cmp xCor,313
        je c2
        draw_pixel xCor,yCor,15
        inc xCor
        jmp c1

c2:     cmp yCor,20
        jne s1
        inc yCor
        mov xCor,6
        JMP c1

s1:     cmp yCor,21
        jne s2
        inc yCor
        mov xCor,6
        JMP c1

s2:     cmp yCor,22
        jne s3
        mov yCor,192
        mov xCor,6
        jmp c1

s3:     cmp yCor,192
        jne s4
        inc yCor
        mov xCor,6
        jmp c1

s4:     cmp yCor,193
        jne c3
        inc yCor
        mov xCor,6
        jmp c1

c3:     mov xCor,6
        mov yCor,20

c4:     cmp yCor,195
        je s5
        draw_pixel xCor,yCor,15
        inc yCor
        jmp c4

s5:     cmp xCor,6
        jne s6
        inc xCor
        mov yCor,20
        jmp c4

s6:     cmp xCor,7
        jne s7
        inc xCor
        mov yCor,20
        jmp c4

s7:     cmp xCor,8
        jne s8
        mov xCor,311
        mov yCor,20
        jmp c4

s8:     cmp xCor,311
        jne s9
        inc xCor
        mov yCor,20
        jmp c4

s9:     cmp xCor,312
        jne fin2
        inc xCor
        mov yCor,20
        jmp c4

fin2:    ret


draw_square endp

;--------------------------------------------------
;MACRO PARA DIBUJAR
;--------------------------------------------------

draw_square2 proc

        mov cursor1,1
        display_string2 vtop10puntos,0
        ;cmp orden,1
        ;jne m1
        ;display_string2 bbs,0
        ;jmp m4
;m1:     cmp orden,2
;        jne m2
;        display_string2 qks,0
;        jmp m4
;m2:     cmp orden,3
;        jne m4
;        display_string2 shs,0
m4:     
        ;display_string2 vpuntos,0                
        ;mov cursor1, 1
        ;display_string2 tTime,1

        mov xCor,6;6
        mov yCor,20;20

c1:     cmp xCor,313
        je c2
        draw_pixel xCor,yCor,15
        inc xCor
        jmp c1

c2:     cmp yCor,20
        jne s1
        inc yCor
        mov xCor,6
        JMP c1

s1:     cmp yCor,21
        jne s2
        inc yCor
        mov xCor,6
        JMP c1

s2:     cmp yCor,22
        jne s3
        mov yCor,192
        mov xCor,6
        jmp c1

s3:     cmp yCor,192
        jne s4
        inc yCor
        mov xCor,6
        jmp c1

s4:     cmp yCor,193
        jne c3
        inc yCor
        mov xCor,6
        jmp c1

c3:     mov xCor,6
        mov yCor,20

c4:     cmp yCor,195
        je s5
        draw_pixel xCor,yCor,15
        inc yCor
        jmp c4

s5:     cmp xCor,6
        jne s6
        inc xCor
        mov yCor,20
        jmp c4

s6:     cmp xCor,7
        jne s7
        inc xCor
        mov yCor,20
        jmp c4

s7:     cmp xCor,8
        jne s8
        mov xCor,311
        mov yCor,20
        jmp c4

s8:     cmp xCor,311
        jne s9
        inc xCor
        mov yCor,20
        jmp c4

s9:     cmp xCor,312
        jne fin2
        inc xCor
        mov yCor,20
        jmp c4

fin2:    ret


draw_square2 endp

draw_square3 proc

        mov cursor1,1
        display_string2 vtop10tiempo,0
        ;cmp orden,1
        ;jne m1
        ;display_string2 bbs,0
        ;jmp m4
;m1:     cmp orden,2
;        jne m2
;        display_string2 qks,0
;        jmp m4
;m2:     cmp orden,3
;        jne m4
;        display_string2 shs,0
m4:     
        ;display_string2 vpuntos,0                
        ;mov cursor1, 1
        ;display_string2 tTime,1

        mov xCor,6;6
        mov yCor,20;20

c1:     cmp xCor,313
        je c2
        draw_pixel xCor,yCor,15
        inc xCor
        jmp c1

c2:     cmp yCor,20
        jne s1
        inc yCor
        mov xCor,6
        JMP c1

s1:     cmp yCor,21
        jne s2
        inc yCor
        mov xCor,6
        JMP c1

s2:     cmp yCor,22
        jne s3
        mov yCor,192
        mov xCor,6
        jmp c1

s3:     cmp yCor,192
        jne s4
        inc yCor
        mov xCor,6
        jmp c1

s4:     cmp yCor,193
        jne c3
        inc yCor
        mov xCor,6
        jmp c1

c3:     mov xCor,6
        mov yCor,20

c4:     cmp yCor,195
        je s5
        draw_pixel xCor,yCor,15
        inc yCor
        jmp c4

s5:     cmp xCor,6
        jne s6
        inc xCor
        mov yCor,20
        jmp c4

s6:     cmp xCor,7
        jne s7
        inc xCor
        mov yCor,20
        jmp c4

s7:     cmp xCor,8
        jne s8
        mov xCor,311
        mov yCor,20
        jmp c4

s8:     cmp xCor,311
        jne s9
        inc xCor
        mov yCor,20
        jmp c4

s9:     cmp xCor,312
        jne fin2
        inc xCor
        mov yCor,20
        jmp c4

fin2:    ret


draw_square3 endp

;--------------------------------------------------
;MACRO PARA LOS BYTES
;--------------------------------------------------

convert_Byte MACRO vByte,vWord

    mov resAx,ax

    mov ax,vWord
    mov vByte,al

    mov ax,resAx

ENDM
;--------------------------------------------------
;MACRO PARA DIBUJAR LAS BARRAS
;--------------------------------------------------

draw_bar MACRO alto,ancho,corX
    local s1,s2,s3,s4,s5,c1,fin,fin2,c2
    mov resAx,ax
    mov resBx,bx
    mov resCx,cx
    mov resDx,dx

    xor dx,dx
    xor cx,cx
    xor bx,bx
    xor ax,ax

    mov dl,alto

    mov varTope1,175
    xor ax,ax
    mov ax,corX
    mov varTope2,ax

    mov ax,175
    sub al,alto

    mov cx,corX
    mov bl,ancho
    add cx,bx

    cmp dl,20
    jnbe s1
    mov varColor,4
    jmp c1

s1: cmp dl,40
    jnbe s2
    mov varColor,1
    jmp c1

s2: cmp dl,60
    jnbe s3
    mov varColor,14
    jmp c1

s3: cmp dl,80
    jnbe s4
    mov varColor,10
    jmp c1

s4: cmp dl,99
    jnbe c1
    mov varColor,15  

c1: cmp varTope1,ax
    jb fin

    mov bx,corX
    mov varTope2,bx

c2: cmp varTope2,cx
    ja fin2

    draw_pixel varTope2,varTope1,varColor

    inc varTope2
    jmp c2

fin2:   dec varTope1
        jmp c1

fin:
    mov ax,resAx
    mov bx,resBx
    mov cx,resCx
    mov dx,resDx
ENDM

;-------------------------------------------------
;MACRO PARA MOV_D
;--------------------------------------------------

mov_d MACRO dstn,orign

    mov resAx, ax

    mov ax,orign
    mov dstn,ax

    mov ax,resAx

ENDM

;-------------------------------------------------
;MACRO PARA NUMERO
;--------------------------------------------------
draw_numberD MACRO number,x,y
    local s1,s2,s3,s4,s5,s6,s7,s8,s9,s10

    mov resAx,ax
    mov resDx,dx


    mov ax,number
    mov dx,10
    div dl

    cmp al,0
    jne s1

    draw_0 x,y
    jmp s10

s1: cmp al,1
    jne s2

    draw_1 x,y
    jmp s10

s2: cmp al,2
    jne s3

    draw_2 x,y
    jmp s10

s3: cmp al,3
    jne s4

    draw_3 x,y
    jmp s10


s4: cmp al,4
    jne s5

    draw_4 x,y
    jmp s10


s5: cmp al,5
    jne s6

    draw_5 x,y
    jmp s10


s6: cmp al,6
    jne s7

    draw_6 x,y
    jmp s10


s7: cmp al,7
    jne s8

    draw_7 x,y
    jmp s10


s8: cmp al,8
    jne s9

    draw_8 x,y
    jmp s10


s9: cmp al,9
    jne s10

    draw_9 x,y

s10:

    mov ax,resAx
    mov dx,resDx 

ENDM

;-------------------------------------------------
;MACRO PARA NUMERO
;--------------------------------------------------
draw_numberU MACRO number,x,y
    local s1,s2,s3,s4,s5,s6,s7,s8,s9,s10

    mov resAx,ax
    mov resDx,dx


    mov ax,number
    mov dx,10
    div dl

    mov al,ah

    cmp al,0
    jne s1

    draw_0 x,y
    jmp s10

s1: cmp al,1
    jne s2

    draw_1 x,y
    jmp s10

s2: cmp al,2
    jne s3

    draw_2 x,y
    jmp s10

s3: cmp al,3
    jne s4

    draw_3 x,y
    jmp s10


s4: cmp al,4
    jne s5

    draw_4 x,y
    jmp s10


s5: cmp al,5
    jne s6

    draw_5 x,y
    jmp s10


s6: cmp al,6
    jne s7

    draw_6 x,y
    jmp s10


s7: cmp al,7
    jne s8

    draw_7 x,y
    jmp s10


s8: cmp al,8
    jne s9

    draw_8 x,y
    jmp s10


s9: cmp al,9
    jne s10

    draw_9 x,y

s10:

    mov ax,resAx
    mov dx,resDx 

ENDM

wait_key proc

c1: mov  ah, 7
    int  21h

    ret 

wait_key endp

;--------------------------------------------------
;PROCEDIMIENTO PARA DIBUJAR
;--------------------------------------------------
draw_footer proc

            xor di,di
            xor si,si

            xor ax,ax
            mov al,contador1
            mov varTmp1,ax

sql1:       cmp di,varTmp1
            je sql2

            convert_Byte varByte,misNumeros[si]
            draw_bar varByte,anchoG,varX2

            xor ax,ax
            mov al,anchoG
            add varX2,ax
            mov cx,varEsp
            add varX2,cx

            
            draw_numberD misNumeros[si],varX,180
            add varX,5
            draw_numberU misNumeros[si],varX,180
            add varX,4


            xor ax,ax
            mov al,cursor2
            add varX,ax

            inc di
            add si,2
            jmp sql1

sql2:       ret

draw_footer endp

;--------------------------------------------------
;PROCEDIMIENTO PARA ORDENAR 
;--------------------------------------------------
pasar_arreglo1 proc

    xor bx,bx
    xor si,si
    xor ax,ax

    mov al,contador1
    mov contador2,ax

c1: cmp si,contador2
    je c2 

    mov ax,misNumeros[bx]
    mov misNumeros2[si],al

    inc si
    add bx,2
    jmp c1

c2: ret 

pasar_arreglo1 endp

emit_beep proc

    mov al, 182
    out 43h,al

    cmp frequency,20
    jnb s1

    mov ax, 11931

    jmp s5

s1: cmp frequency,40
    jnb s2
    mov ax, 3977

    jmp s5

s2: cmp frequency,60
    jnb s3
    mov ax, 2386

    jmp s5

s3: cmp frequency,80
    jnb s4
    mov ax, 1704

    jmp s5

s4: mov ax, 1325

s5: out 42h, al
    mov al, ah
    out 42h, al
    in al, 61h

    or al, 00000011b
    out 61h, al

    ret

emit_beep endp


end_beep proc

    in  al, 61h
    and al, 11111100b
    out 61h, al 

    ret

end_beep endp


delay_0 proc

    MOV SI,250
T1: call milis
    DEC SI
    JZ U1
    MOV DI,1400
D1: DEC DI
    JNZ D1
    JMP T1

U1: 
    ret

delay_0 endp

c_tiempo proc

    push si
    mov si,1

    inc tSecond[si]

    cmp tSecond[si],58
    jne o1

    mov tSecond[si],48
    dec si
    inc tSecond[si]

    cmp tSecond[si],54
    jne o1

    mov tSecond[si],48

    inc si
    inc tMinut[si]

    cmp tMinut[si],58
    jne o1

    mov tMinut[si],48
    dec si
    inc tMinut[si]

    

o1: 
    mov cursor1, 1
    display_string2 tTime,1
    pop si
    ret

c_tiempo endp


c_tiempojuego proc

    call milisjuego
    ;---------------------------
    push si
    mov si,1    
    
    inc tSecond[si]

    cmp tSecond[si],58 ; es ascci :
    jne o1

    mov tSecond[si],48 ; es ascci 0 :
    dec si
    inc tSecond[si]
    cmp tSecond[si],54 ; es ascci 6 :
    jne o1
    mov tSecond[si],48 ; es ascci 0

    inc si
    inc tMinut[si]
    cmp tMinut[si],58 ; es ascci :
    jne o1
    mov tMinut[si],48; es ascci 0 :
    dec si
    inc tMinut[si]
    
o1:     
    mSetCursor 1,10
    printvideo tTime
    pop si
    ret

c_tiempojuego endp

NivelDeljuego macro nivel 
LOCAL  nivel1,nivel2,nivel3,nivel4,nivel5,nivel6,finivel,salir

    mov al, nivel

    cmp al, 1 
    je nivel1
    cmp al, 2 
    je nivel2
    cmp al, 3 
    je nivel3
    cmp al, 4 
    je nivel4
    cmp al, 5 
    je nivel5
    cmp al, 6 
    je nivel6
    jmp salir 

    nivel1:     
    mov vnivel2[0],49
    mov vnivelnumero,900
    jmp finivel
    nivel2:     
    mov vnivel2[0],50
    mov vnivelnumero,800
    jmp finivel
    nivel3:     
    mov vnivel2[0],51
    mov vnivelnumero,700
    jmp finivel
    nivel4:     
    mov vnivel2[0],52
    mov vnivelnumero,600
    jmp finivel
    nivel5:     
    mov vnivel2[0],53
    mov vnivelnumero,500
    jmp finivel
    nivel6:     
    mov vnivel2[0],54
    mov vnivelnumero,400    
    finivel:    
    mSetCursor 0,20
    printvideo vnivel

    salir:

endm 

puntajejuego macro     
    CambioDbDecimal conpuntaje, puntajeconv                
    mSetCursor 0,37
    printvideo puntajeconv
endm 

milisjuego proc

    push si

     
    inc tMili[2]
    inc tMili[2]
    cmp tMili[2],58 ; ascii dos puntos ;
    jne s1

    mov tMili[2],48 ; ascii cero  ;

    inc tMili[1]    
    inc tMili[1]    
    cmp tMili[1],58 ; ascii dos puntos ;
    jne s1

    mov tMili[1],48 ; ascii cero  ;

    inc tMili[0]
    inc tMili[0]
    cmp tMili[0],58
    jne s1

    mov tMili[0],48
s1:
    ;mov cursor1, 1
    ;display_string2 tTime,1
    mSetCursor 1,10
    printvideo tTime
    pop si
    ret

milisjuego endp


pasar_arreglo2 proc

    mov resBx,bx
    mov resSi,si
    mov resAx,ax

    xor bx,bx
    xor si,si
    xor ax,ax

c1: cmp si,contador2
    je c2 

    xor ax,ax
    mov al,misNumeros2[si]
    mov misNumeros[bx],ax

    inc si
    add bx,2
    jmp c1


    mov bx,resBx
    mov si,resSi
    mov bx,resBx

c2: 
    ret

pasar_arreglo2 endp


repaint proc

    mov ax,0

c1: cmp ax,elTiempo
    je s1
    call delay_0
    inc ax
    inc cDelay
    cmp cDelay,4
    jne c1

    mov cDelay,0
    call c_tiempo

    jmp c1

s1: 
    call pasar_arreglo2
    mov ah,0 
    mov al,13h
    int 10h
    call draw_square
    call c_space
    call draw_footer
    ret


repaint endp


milis proc

    push si

    inc tMili[2]
    cmp tMili[2],58
    jne s1

    mov tMili[2],48

    inc tMili[1]
    cmp tMili[1],58
    jne s1

    mov tMili[1],48

    inc tMili[0]
    cmp tMili[0],58
    jne s1

    mov tMili[0],48
s1:
    mov cursor1, 1
    display_string2 tTime,1
    pop si
    ret

milis endp

;--------------------------------------------------
;PROCEDIMIENTO PARA bubbleshort2
;--------------------------------------------------
bubbleSort2 proc

    xor si,si
    mov ax,contador2
    mov varN,ax

    dec varN

c1: cmp si,varN
    je c2

    xor bx,bx
    mov di,1
    mov ax,contador2
    mov varN2,ax

    sub varN2,si
    dec varN2

c3: cmp bx,varN2
    je c4

    xor dx,dx
    mov dl,misNumeros2[di]
    cmp misNumeros2[bx],dl
    jnb c5

    xor ax,ax
    mov al,misNumeros2[bx]
    mov varTemp,ax
    mov frequency,al

    mov misNumeros2[bx],dl

    mov ax,varTemp
    mov misNumeros2[di],al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax
    

    

c5: inc di
    inc bx
    jmp c3

c4: inc si
    jmp c1

c2:
    ret

bubbleSort2 endp

;--------------------------------------------------
;PROCEDIMIENTO PARA bubblesort
;--------------------------------------------------
bubbleSort proc

    xor si,si
    mov ax,contador2
    mov varN,ax

    dec varN

c1: cmp si,varN
    je c2

    xor bx,bx
    mov di,1
    mov ax,contador2
    mov varN2,ax

    sub varN2,si
    dec varN2

c3: cmp bx,varN2
    je c4

    xor dx,dx
    mov dl,misNumeros2[di]
    cmp misNumeros2[bx],dl
    jna c5

    xor ax,ax
    mov al,misNumeros2[bx]
    mov varTemp,ax
    mov frequency,al

    mov misNumeros2[bx],dl

    mov ax,varTemp
    mov misNumeros2[di],al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax
    

    

c5: inc di
    inc bx
    jmp c3

c4: inc si
    jmp c1

c2:
    ret

bubbleSort endp
;--------------------------------------------------
;PROCEDIMIENTO PARA shellshort 
;--------------------------------------------------
shellSort2 proc

    mov ax,contador2
    mov varN,ax

    sar ax,1
    mov varGap,ax

c0: cmp varGap,0
    jna c1

    mov ax,varGap
    mov varI,ax

c3: mov cx,contador2
    cmp varI,cx
    jnb c2

    mov si,varI
    xor ax,ax
    mov al,misNumeros2[si]
    mov varTemp,ax

    mov ax,varI
    mov varJ,ax

c5: mov cx,varGap
    cmp varJ,cx
    jnae c4

    mov si,varJ
    sub si,varGap
    xor ax,ax
    mov al, misNumeros2[si]
    
    cmp ax,varTemp
    jnb c4

    mov si, varJ
    mov misNumeros2[si],al
    mov frequency,al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax

    sub varJ,cx
    jmp c5
c4:
    mov si,varJ
    mov ax,varTemp
    mov misNumeros2[si],al
    inc varI
    jmp c3
c2:
    sar varGap,1
    jmp c0
c1:
    ret

shellSort2 endp

;--------------------------------------------------
;PROCEDIMIENTO PARA shellshort
;--------------------------------------------------
shellSort proc

    mov ax,contador2
    mov varN,ax

    sar ax,1
    mov varGap,ax

c0: cmp varGap,0
    jna c1

    mov ax,varGap
    mov varI,ax

c3: mov cx,contador2
    cmp varI,cx
    jnb c2

    mov si,varI
    xor ax,ax
    mov al,misNumeros2[si]
    mov varTemp,ax

    mov ax,varI
    mov varJ,ax

c5: mov cx,varGap
    cmp varJ,cx
    jnae c4

    mov si,varJ
    sub si,varGap
    xor ax,ax
    mov al, misNumeros2[si]
    
    cmp ax,varTemp
    jna c4

    mov si, varJ
    mov misNumeros2[si],al
    mov frequency,al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax

    sub varJ,cx
    jmp c5
c4:
    mov si,varJ
    mov ax,varTemp
    mov misNumeros2[si],al
    inc varI
    jmp c3
c2:
    sar varGap,1
    jmp c0
c1:
    ret

shellSort endp

mov_listaE proc

    mov si,punteroB
    xor di,di
    xor ax,ax

c1: cmp listaE1[di],'$'
    je c2

    mov al,listaE1[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c1


c2: mov di,0

c3: cmp di,contador2
    je c4

    cmp desordenados[di],'$'
    je c4

    cmp di,0
    je s1

    mov bufferAux[si],','
    inc si

s1: xor ax,ax
    mov al,desordenados[di]
    mov dl,10

    div dl
    cmp al,0
    je s2

    mov bufferAux[si],al
    add bufferAux[si],48
    inc si

s2: mov bufferAux[si],ah
    add bufferAux[si],48
    inc si

    inc di

    jmp c3


c4: mov di,0

    xor ax,ax
c5: cmp listaE2[di],'$'
    je c6

    mov al,listaE2[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c5

c6: mov punteroB,si
    ret

mov_listaE endp


arreglar_resul proc

    mov si,punteroB
    xor di,di
    xor ax,ax

    cmp orden,1
    jne t1

q1: cmp algo11[di],'$'
    je t3

    mov al,algo11[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q1


t1: cmp orden,2
    jne t2


q3: cmp algo21[di],'$'
    je t3

    mov al,algo21[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q3


t2: cmp orden,3
    jne t3


q5: cmp algo31[di],'$'
    je t3

    mov al,algo31[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q5

t3: xor di,di
    xor ax,ax

    cmp varTipo,1
    jne r1

r0: cmp tipo1[di],'$'
    je r3

    mov al,tipo1[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp r0

r1: cmp varTipo,2
    jne r3

r2: cmp tipo2[di],'$'
    je r3

    mov al,tipo2[di]
    mov bufferAux[si],al

    inc si
    inc di

    jmp r2

r3: mov punteroB,si
    call mov_listaE
    xor di,di

c1: cmp listaO1[di],'$'
    je c2

    mov al,listaO1[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c1


c2: mov di,0

c3: cmp di,contador2
    je c4

    cmp misNumeros2[di],'$'
    je c4

    cmp di,0
    je s1

    mov bufferAux[si],','
    inc si

s1: xor ax,ax
    mov al,misNumeros2[di]
    mov dl,10

    div dl
    cmp al,0
    je s2

    mov bufferAux[si],al
    add bufferAux[si],48
    inc si

s2: mov bufferAux[si],ah
    add bufferAux[si],48
    
    inc si
    inc di

    jmp c3


c4: mov di,0

    xor ax,ax
c5: cmp listaO2[di],'$'
    je c6

    mov al,listaO2[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c5

c6: mov punteroB,si
    
    mov al,vVelo2[0]
    mov velot[0],al

    mov al,tMinut[0]
    mov ttMinu[0],al

    mov al,tMinut[1]
    mov ttMinu[1],al

    mov al,tSecond[0]
    mov ttSecond[0],al

    mov al,tSecond[1]
    mov ttSecond[1],al

    mov al,tMili[0]
    mov ttMili[0],al

    mov al,tMili[1]
    mov ttMili[1],al

    mov al,tMili[1]
    mov ttMili[1],al

    xor di,di

z0: cmp result[di],'$'
    je z1

    mov al,result[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp z0

z1: xor di,di

    cmp orden,1
    jne t4

q6: cmp algo12[di],'$'
    je t6

    mov al,algo12[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q6


t4: cmp orden,2
    jne t5


q8: cmp algo22[di],'$'
    je t6

    mov al,algo22[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q8


t5: cmp orden,3
    jne t6

q10:cmp algo32[di],'$'
    je t6

    mov al,algo32[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q10

t6: mov punteroB,si

    ret

arreglar_resul endp

;--------------------------------------------------
;PROCEDIMIENTO PARA ORDENAR 
;--------------------------------------------------
begin_sort proc

    call pasar_arreglo1

    mov cDelay,0

    mov tMinut[0],48
    mov tMinut[0],48
    mov tMinut[1],48
    mov tMinut[1],48
    
    mov tSecond[0],48
    mov tSecond[0],48
    mov tSecond[1],48
    mov tSecond[1],48

    mov tMili[0],48
    mov tMili[0],48
    mov tMili[1],48
    mov tMili[1],48
    mov tMili[2],48
    mov tMili[2],48

    cmp orden,1
    jne s1

    cmp varTipo,1
    jne m1

    call bubbleSort2

    jmp fin2

m1: cmp varTipo,2
    jne fin2

    call bubbleSort
    jmp fin2

s1: cmp orden,2
    jne s2

    cmp varTipo,1
    jne m2

    iterativeQsort2
    jmp fin2

m2: cmp varTipo,2
    jne fin2

    iterativeQsort
    jmp fin2

s2: cmp orden,3
    jne fin2

    cmp varTipo,1
    jne m3

    call shellSort2
    jmp fin2

m3: cmp varTipo,2
    jne fin2

    call shellSort

fin2:
    

c1: mov ah, 02h
    int 1ah

    mov ax,13
    mul dh

    mov dl,100
    div dl

    mov tMili[0],al
    add tMili[0],48

    mov al,ah
    mov ah,0

    mov dl,10
    div dl

    mov tMili[1],al
    add tMili[1],48

    mov tMili[2],ah
    add tMili[2],48

    mov cursor1, 1
    display_string2 tTime,1
    call wait_key


    call arreglar_resul
    ret


begin_sort endp

;--------------------------------------------------
;PROCEDIMIENTO PARA LEER DESDE EL TECLADO
;--------------------------------------------------
oyendo proc
mov ah ,06H
mov dl,0ffh
int 21H
;ZF = 1 no hay entrada activa
;AL = guarda el caracter
ret
oyendo endp

;--------------------------------------------------
;MACRO PINTAR  BLOQUE AMARILLO
;--------------------------------------------------
pintarbloque macro pos,color
LOCAL exitpintar,exitsinaumento,aumento,noaumento
push dx
mov di, pos

;mov dl,4
;cmp es:[di],dl
;je exitpintar

mov dl, color
;-----------------------------------------
mov es: [di],dl
mov es: [di+1],dl

mov es:[di+319],dl
mov es:[di+320],dl
mov es:[di+321],dl
mov es:[di+322],dl

mov es:[di +638],dl
mov es:[di +639],dl
mov es:[di +640],dl
mov es:[di +641],dl
mov es:[di +642],dl
mov es:[di +643],dl

mov es:[di+959],dl
mov es:[di+960],dl
mov es:[di+961],dl
mov es:[di+962],dl

mov es:[di+1280],dl
mov es:[di+1281],dl

;----------------------------------------------
mov es: [di+100],dl
mov es: [di+101],dl

mov es:[di+419],dl
mov es:[di+420],dl
mov es:[di+421],dl
mov es:[di+422],dl

mov es:[di +738],dl
mov es:[di +739],dl
mov es:[di +740],dl
mov es:[di +741],dl
mov es:[di +742],dl
mov es:[di +743],dl

mov es:[di+1059],dl
mov es:[di+1060],dl
mov es:[di+1061],dl
mov es:[di+1062],dl

mov es:[di+1380],dl
mov es:[di+1381],dl
;--------------------------------------------

;--------------------------------------------
mov es: [di+200],dl
mov es: [di+201],dl

mov es:[di+519],dl
mov es:[di+520],dl
mov es:[di+521],dl
mov es:[di+522],dl

mov es:[di +838],dl
mov es:[di +839],dl
mov es:[di +840],dl
mov es:[di +841],dl
mov es:[di +842],dl
mov es:[di +843],dl

mov es:[di+1159],dl
mov es:[di+1160],dl
mov es:[di+1161],dl
mov es:[di+1162],dl

mov es:[di+1480],dl
mov es:[di+1481],dl
;--------------------------------------------

;----------------------------------------------
mov es: [di+270],dl
mov es: [di+271],dl

mov es:[di+589],dl
mov es:[di+590],dl
mov es:[di+591],dl
mov es:[di+592],dl

mov es:[di +908],dl
mov es:[di +909],dl
mov es:[di +910],dl
mov es:[di +911],dl
mov es:[di +912],dl
mov es:[di +913],dl

mov es:[di+1229],dl
mov es:[di+1230],dl
mov es:[di+1231],dl
mov es:[di+1232],dl

mov es:[di+1550],dl
mov es:[di+1551],dl
;jmp exitsinaumento
;--------------------------------------------
;exitpintar:
;inc conpuntaje
;exitsinaumento:
pop dx
endm
;--------------------------------------------------
;MACRO PINTAR  BLOQUE VERDE
;--------------------------------------------------
pintabkverde macro pos,color

push dx
mov di, pos

mov dl, color

mov es: [di],dl
mov es: [di+1],dl

mov es:[di+319],dl
mov es:[di+320],dl
mov es:[di+321],dl
mov es:[di+322],dl

mov es:[di +638],dl
mov es:[di +639],dl
mov es:[di +640],dl
mov es:[di +641],dl
mov es:[di +642],dl
mov es:[di +643],dl

mov es:[di+959],dl
mov es:[di+960],dl
mov es:[di+961],dl
mov es:[di+962],dl

mov es:[di+1280],dl
mov es:[di+1281],dl

;----------------------------------------------
mov es: [di+100],dl
mov es: [di+101],dl

mov es:[di+419],dl
mov es:[di+420],dl
mov es:[di+421],dl
mov es:[di+422],dl

mov es:[di +738],dl
mov es:[di +739],dl
mov es:[di +740],dl
mov es:[di +741],dl
mov es:[di +742],dl
mov es:[di +743],dl

mov es:[di+1059],dl
mov es:[di+1060],dl
mov es:[di+1061],dl
mov es:[di+1062],dl

mov es:[di+1380],dl
mov es:[di+1381],dl
;--------------------------------------------

;--------------------------------------------
mov es: [di+200],dl
mov es: [di+201],dl

mov es:[di+519],dl
mov es:[di+520],dl
mov es:[di+521],dl
mov es:[di+522],dl

mov es:[di +838],dl
mov es:[di +839],dl
mov es:[di +840],dl
mov es:[di +841],dl
mov es:[di +842],dl
mov es:[di +843],dl

mov es:[di+1159],dl
mov es:[di+1160],dl
mov es:[di+1161],dl
mov es:[di+1162],dl

mov es:[di+1480],dl
mov es:[di+1481],dl
;--------------------------------------------

;----------------------------------------------
mov es: [di+270],dl
mov es: [di+271],dl

mov es:[di+589],dl
mov es:[di+590],dl
mov es:[di+591],dl
mov es:[di+592],dl

mov es:[di +908],dl
mov es:[di +909],dl
mov es:[di +910],dl
mov es:[di +911],dl
mov es:[di +912],dl
mov es:[di +913],dl

mov es:[di+1229],dl
mov es:[di+1230],dl
mov es:[di+1231],dl
mov es:[di+1232],dl

mov es:[di+1550],dl
mov es:[di+1551],dl


pop dx
endm

;-------------------------------------IMPORTANTE COLORES DEL JUEGO
;colores 0 negro
;colores 1 azul  x 
;colores 2 verde x
;colores 3 celeste
;colores 4 rojo   x 
;colores 5 morado
;colores 6 naranja
;colores 7 gris blanco x 
;colores 8 gris oscuro
;colores 9 celeste

fulljuego proc 
        
    mov tMinut[0],48
    mov tMinut[0],48
    mov tMinut[1],48
    mov tMinut[1],48
    
    mov tSecond[0],48
    mov tSecond[0],48
    mov tSecond[1],48
    mov tSecond[1],48

    mov tMili[0],48
    mov tMili[0],48
    mov tMili[1],48
    mov tMili[1],48
    mov tMili[2],48
    mov tMili[2],48

    mov vniveljuegosel,6 ;variable para el nivel del juego 

    NivelDeljuego  vniveljuegosel

    mov colorcarro,1 ;variable para el color del carro

    mov conpuntaje,3 ; variable del puntaje

    CambioDbDecimal conpuntaje, puntajeconv                                     

    modovideo        

    mSetCursor 0,0
    printvideo vuser 
    mSetCursor 0,8
    printvideo vnomuser   
    mSetCursor 0,20
    printvideo vnivel
    mSetCursor 0,30
    printvideo vpuntos
    mSetCursor 0,37
    printvideo puntajeconv
    mSetCursor 1,10
    printvideo tTime    


    pintarmargen 5   

    
    mov viniciocarro, 54560
                ; fila   + columna
    inicioss:
    mov vinibloque,7060 ; 22(320)+20
                         ; fila   + columna                              
    mov vinibloqueverde,17660 ; 55(320)+60

     cicloob:
     mov vinibloque,7060 ; 22(320)+20
                         ; fila   + columna                              
     jmp accion 
     ciblooc: 
     mov vinibloqueverde,17660 ; 55(320)+60
     jmp accion        

     accion:  

       puntajejuego      
              
       call c_tiempojuego 
       call meneandocarro
        
       cmp conpuntaje,0
       je  salidajuego
       cmp bdsalidajuego, 1
       je salidajuego
       cmp tiempobloques, 10
       je cambiodenivel
     
       pintarbloque vinibloque,0 ; (110,160) = 110*320 + 160
       pintabkverde vinibloqueverde,0

       add vinibloque,1280     
       add vinibloqueverde,1280     

       pintarbloque vinibloque,6
       pintabkverde vinibloqueverde,2

       mDelay vnivelnumero      ;180 es el tiempo del delay

       cmp vinibloque,58260 ; multipo de vinibloque + 1280 
       je salirobstaculo 

       cmp vinibloqueverde,57340
       je  salirobstaculodos            

     jmp accion  

     salirobstaculo:
        pintarbloque vinibloque,0 ; (110,160) = 110*320 + 160
        inc tiempobloques
     jmp cicloob
     salirobstaculodos:
        pintabkverde vinibloqueverde,0        
        inc tiempobloques
     jmp ciblooc
     ;jmp salidajuego
      salidajuego:
      mov bdsalidajuego,0
      ;-----------------------------------------------REPORTE DEL JUEGO
        CrearArchivo entradArchivo,handlerentrada       
        Escribir handlerentrada,vuser,SIZEOF vuser   
        Escribir handlerentrada,vnomuser,SIZEOF vnomuser   
        Escribir handlerentrada,vnivel,SIZEOF vnivel
        Escribir handlerentrada,vnivel2,SIZEOF vnivel2
        Escribir handlerentrada,vpuntos,SIZEOF vpuntos
        CambioDbDecimal conpuntaje, puntajeconv  
        Escribir handlerentrada,puntajeconv,SIZEOF puntajeconv   
        Escribir handlerentrada,tTime,SIZEOF tTime   
        Escribir handlerentrada,tMinut,SIZEOF tMinut   
        Escribir handlerentrada,tSecond,SIZEOF tSecond   
        Escribir handlerentrada,tMili,SIZEOF tMili
        Cerrar handlerentrada 

        jmp salidabuena
    ;-----------------------------------------------REPORTE DEL JUEGO

            Error1:
            imprimir erroru
            jmp salidabuena

            Error2:
            imprimir errord
            jmp salidabuena

            Error3:
            imprimir errorleer
            jmp salidabuena

            Error4:
            imprimir errorc 
            jmp salidabuena

            Error5:
            imprimir errort
            jmp salidabuena 


     cambiodenivel:
     dec vniveljuegosel
     NivelDeljuego  vniveljuegosel
     mov tiempobloques,0
     cmp vniveljuegosel,0
     je  salidajuego
     mov colorcarro,15
     jmp inicioss


      salidabuena:            
      mov ax,0003h
      int 10h  
      ;call clear_screen 
      imprimir vreportejuego         
        ret    
fulljuego endp

meneandocarro proc        
            
            
            pintarcarro  viniciocarro,colorcarro,4  ;(170,160) = 170*320+160      
            etjugandoahora:                                    
            
            call oyendo

            cmp al,04dh       ; derecha
            je etfderecha
            cmp al,04bh       ; izuierda
            je etfizquierda
            cmp al,01bH       ;escape
            je etpausa
            cmp al,20H
            je etbarra            
            jmp etjuegotermina

            etfderecha: 
            cmp di,54686
            je etjugandoahora   ;   170*320+160+21=  maximo  54719
            pintarcarro viniciocarro,0,0 ; (170,160) = 170*320 + 160                       
            add viniciocarro,21                                                 
            
            cmp di,54686
            je etjugandoahora   ;   170*320+160+21=  maximo  54719
            pintarcarro viniciocarro,colorcarro,4

            ;inc conpuntaje
            jmp etjugandoahora 

            etfizquierda:                            
            cmp di, 54413
            je etjugandoahora       
            pintarcarro viniciocarro,0,0 ; (170,160) = 170*320 + 160
            sub viniciocarro,21     

            cmp di, 54413
            je etjugandoahora
            pintarcarro viniciocarro,colorcarro,4   

            ;dec conpuntaje                     
            jmp etjugandoahora

            etpausa:
            
            mov  ah, 08h
            int  21h

            cmp al,01bH       ;escape
            jne etpausa

            jmp etjugandoahora

            etbarra:
            mov bdsalidajuego,1;            

            etjuegotermina:  

            
        ret    
meneandocarro endp

barrasjc proc

   comienzo:
        ;LimpiarConsola
        call clear_screen
        imprimir salto        

            mov misNumeros[0],97
            inc contador1 ; cuenta la cantidad de numero ingresados en el arreglo            
            mov misNumeros[2],38            
            inc contador1
            mov misNumeros[4],83            
            inc contador1
            mov misNumeros[6],28            
            inc contador1
            mov misNumeros[8],47            
            inc contador1
            mov misNumeros[10],40            
            inc contador1
            mov misNumeros[12],66            
            inc contador1
            mov misNumeros[14],32            
            inc contador1
            mov misNumeros[16],68            
            inc contador1
            mov misNumeros[18],3
            inc contador1
            ;mov misNumeros[20],11
            ;mov misNumeros[22],12
            ;mov misNumeros[24],13
            ;mov misNumeros[26],14
            ;mov misNumeros[28],15
            ;mov misNumeros[30],16
            ;mov misNumeros[32],17
            ;mov misNumeros[34],18
            ;mov misNumeros[36],19
            ;mov misNumeros[38],20
            ;mov misNumeros[40],21
            ;mov misNumeros[42],22
            ;mov misNumeros[44],23
            ;mov misNumeros[46],24
            ;mov misNumeros[48],25
            ;convert_decimal ordenados[0]
            ;convert_decimal  
            imprimir vsalto
            

            call mostrar_arreglo
            display_string salto
            call c_space
            getchar
            ;---------------------------------------------------
            MOV AX,0013H
            INT 10H

            call mov_misNumeros
            call draw_square2;draw_square
            ;call c_space
            call draw_footer

            mov  ah, 7
            int  21h ; in este momento el software espera la continuacion
            ; presionando una tecla cualquiera
            ;---------------------------------------------------
            xor ax,ax
            mov al,anchoG
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto

            xor ax,ax
            mov al,lateral
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto

            xor ax,ax
            mov al,cursor2
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto
            ;getchar
            ;temporal
            jmp etordenamientos

            etordenamientos: 
            call clear_screen
            display_string vmalgoordena
            call read_choice
            xor ax,ax
            mov al,seleccion
            mov orden,al
            display_string salto

            call clear_screen
            display_string vmsgvelocidad
            call read_choice
            call mov_delay
            display_string salto

            call clear_screen
            display_string vmsordasdes
            call read_choice
            xor ax,ax
            mov al,seleccion
            mov varTipo, al

            MOV AX,0013H
            INT 10H

            call mov_misNumeros
            call draw_square
            call c_space
            call draw_footer

            mov  ah, 7
            int  21h ; presione cualquier tecla para continuar.
            
            call begin_sort           
    
            mov contador1,0
            mov contador2,0

            getchar
            call clear_screen
            call mostrar_arreglo 
            ;convert_decimal misNumeros[0]
            ;imprimir  misNumeros[0]
            ;jmp comienzo    
            ;CambioDbDecimal misNumeros[0], misNumeros[0]
            ;imprimir misNumeros[0]
            imprimir rcorte
            imprimir rtopjc
            imprimir rtopjc1
            imprimir rtopjc2
            imprimir rtopjc3
            imprimir rtopjc4
            imprimir rtopjc5
            imprimir rtopjc6
            imprimir rtopjc7
            imprimir rtopjc8
            imprimir rtopjc9                       
            imprimir rtopjc10
            imprimir rcorte 

            call show_rep      


ret
barrasjc endp


barrasjc2 proc

   comienzo:
        ;LimpiarConsola
            call clear_screen
            imprimir salto        

            mov misNumeros[0],97
            inc contador1 ; cuenta la cantidad de numero ingresados en el arreglo            
            mov misNumeros[2],38            
            inc contador1
            mov misNumeros[4],83            
            inc contador1
            mov misNumeros[6],28            
            inc contador1
            mov misNumeros[8],47            
            inc contador1
            mov misNumeros[10],40            
            inc contador1
            mov misNumeros[12],66            
            inc contador1
            mov misNumeros[14],32            
            inc contador1
            mov misNumeros[16],68            
            inc contador1
            mov misNumeros[18],3
            inc contador1
            ;mov misNumeros[20],11
            ;mov misNumeros[22],12
            ;mov misNumeros[24],13
            ;mov misNumeros[26],14
            ;mov misNumeros[28],15
            ;mov misNumeros[30],16
            ;mov misNumeros[32],17
            ;mov misNumeros[34],18
            ;mov misNumeros[36],19
            ;mov misNumeros[38],20
            ;mov misNumeros[40],21
            ;mov misNumeros[42],22
            ;mov misNumeros[44],23
            ;mov misNumeros[46],24
            ;mov misNumeros[48],25
            ;convert_decimal ordenados[0]
            ;convert_decimal  
            imprimir vsalto
            ;imprimir misNumeros[18]
            call mostrar_arreglo
            display_string salto
            call c_space
            getchar
            ;---------------------------------------------------
            MOV AX,0013H
            INT 10H

            call mov_misNumeros
            call draw_square3;draw_square
            ;call c_space
            call draw_footer

            mov  ah, 7
            int  21h ; in este momento el software espera la continuacion
            ; presionando una tecla cualquiera
            ;---------------------------------------------------
            xor ax,ax
            mov al,anchoG
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto

            xor ax,ax
            mov al,lateral
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto

            xor ax,ax
            mov al,cursor2
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto
            ;getchar
            ;temporal
            jmp etordenamientos

            etordenamientos: 
            call clear_screen
            display_string vmalgoordena
            call read_choice
            xor ax,ax
            mov al,seleccion
            mov orden,al
            display_string salto

            call clear_screen
            display_string vmsgvelocidad
            call read_choice
            call mov_delay
            display_string salto

            call clear_screen
            display_string vmsordasdes
            call read_choice
            xor ax,ax
            mov al,seleccion
            mov varTipo, al

            MOV AX,0013H
            INT 10H

            call mov_misNumeros
            call draw_square
            call c_space
            call draw_footer

            mov  ah, 7
            int  21h ; presione cualquier tecla para continuar.
            
            call begin_sort           
    
            mov contador1,0
            mov contador2,0

            getchar
            call clear_screen
            call mostrar_arreglo 
            ;jmp comienzo    

            imprimir rcorte
            imprimir rtopjc
            imprimir rtopjc11
            imprimir rtopjc12
            imprimir rtopjc13
            imprimir rtopjc14
            imprimir rtopjc15
            imprimir rtopjc16
            imprimir rtopjc17
            imprimir rtopjc18
            imprimir rtopjc19                       
            imprimir rtopjc20
            imprimir rcorte

            call show_rep  

ret
barrasjc2 endp

;--------------------------------------------------
;PROCEDIMIENTO PRINCIPAL
;--------------------------------------------------
main  proc              
;--------------------------------------------------
;INICIA PROCESO 
;--------------------------------------------------        
            mov ax,@data    
            mov ds,ax  

    etmenuinicial:
            ;LimpiarConsola
            ;mov misNumeros[0],48
            ;imprimir misNumeros[0]
            ;imprimir vsalto
            ;mov ax,@data    
            ;mov ds,ax  
            ;call juegofull
            ;call fulljuego
            ;call c_tiempojuego
            ;modovideo    
            ;mSetCursor 0,15                         
            ;printvideo vpintar
            ;imprimir   vpintar
            ;mSetCursor 0,30
            ;mov ax,@data                
            ;imprimir   vpintar    
            ;mSetCursor 0,3
            ;imprimir vpintar
            ;pintarmargen 5 
;           imprimir misNumerosjc
            ;mov misNumeros[0],48

            

            ;imprimir misNumeros
            ;jmp etsalir


            imprimir vmenuinicial
            imprimir vsalto
            imprimir vopcion1 
            getchar
            cmp al,31h ;codigo ascii del numero 1 en hexadecimal
            je etingresar 
            cmp al,32h ;codigo ascii del numero 2 en hexadecimal 
            je etregistrar 
            cmp al,33h ;codigo ascii del numero 3 en hexadecimal 
            je etmenuadmin
            cmp al,34h ;codigo ascii del numero 3 en hexadecimal 
            je etexit      
            ;Si es cualquier otro caracter regresa al etmenuinicial
            jmp etregreso1
    etingresar:
        imprimir vmsingresar 
        imprimir vlogin 
        imprimir vusuario  
        getchar 
        imprimir vcontrasena
        getchar                 
        imprimir vsalto
        imprimir Vencabezado
        imprimir vsalto
        jmp etusuarionormal      
    etregistrar:
        imprimir vmsregistrar
        getchar
        jmp etmenuinicial
    etexit:
        imprimir vmsgsalida
        jmp etsalir
    etregreso1:
        imprimir vopincorrecta
        getchar
        jmp etmenuinicial    
    etmenuadmin:
        imprimir vmenuadmin
        imprimir vsalto  
        imprimir vopcion1 
        getchar        
        cmp al,31h ;codigo ascii del numero 1 en hexadecimal
        je ettop10puntos 
        cmp al,32h ;codigo ascii del numero 2 en hexadecimal 
        je ettop10tiempos
        cmp al,33h ;codigo ascii del numero 3 en hexadecimal 
        je etexitadmin      
        ;Si es cualquier otro caracter regresa al etmenuinicial
        jmp etregreso2   
    ettop10puntos:
    call barrasjc
    jmp etmenuadmin

    ettop10tiempos:    
    call barrasjc2
    jmp etmenuadmin

    etexitadmin:
    imprimir vmsgsalida
    jmp etmenuinicial

    etregreso2: 
    imprimir vopincorrecta
    getchar
    jmp etmenuadmin

    etusuarionormal:
        imprimir vmenusuario
        imprimir vsalto  
        imprimir vopcion1 
        getchar        
        cmp al,31h ;codigo ascii del numero 1 en hexadecimal
        je etinijuego
        cmp al,32h ;codigo ascii del numero 2 en hexadecimal 
        je etcargarjuego
        cmp al,33h ;codigo ascii del numero 3 en hexadecimal 
        je etsalirmuser      
        ;Si es cualquier otro caracter regresa al etmenuinicial
        jmp etregreso3   

        etinijuego:
        call fulljuego
        jmp etusuarionormal

        etcargarjuego: 

        imprimir EjmGuardar
        Limpiar bufferentrada,SIZEOF bufferentrada,24h
        ObtenerRuta bufferentrada 
        ;mostrar bufferentrada 
        Abrir bufferentrada,handlerentrada  
        Limpiar bufferInformacion,SIZEOF bufferInformacion,24h
        Leer handlerentrada,bufferInformacion,SIZEOF bufferInformacion 
        imprimir  bufferInformacion
        Cerrar handlerentrada 

        imprimir vcargajuego


        jmp etusuarionormal

        etsalirmuser:
        imprimir vmsgsalida
        jmp etmenuinicial

        etregreso3: 
        imprimir vopincorrecta
        getchar
        jmp etusuarionormal

            Error1:
            imprimir erroru
            jmp etusuarionormal

            Error2:
            imprimir errord
            jmp etusuarionormal

            Error3:
            imprimir errorleer
            jmp etusuarionormal

            Error4:
            imprimir errorc 
            jmp etusuarionormal

            Error5:
            imprimir errort
            jmp etusuarionormal 
;---------------------------------------------------          
    etjugando:
            
            modovideo
            
            pintarmargen 5
            
            mov viniciocarro, 54560
            
            pintarcarro  viniciocarro,1,4    ;(170,160) = 170*320+160            

            etjugandoahora:                 
            
            mov  ah, 08h;7
            int  21h
            
            cmp al,04dh       ; derecha
            je etfderecha
            cmp al,04bh       ; izuierda
            je etfizquierda
            cmp al,01bH       ;escape
            je etpausa
            cmp al,20H         ; salir 
            je etjuegotermina
            jmp etjugandoahora

            etfderecha: 
            cmp di,54686
            je etjugandoahora   ;   170*320+160+21=  maximo  54719
            pintarcarro viniciocarro,0,0 ; (170,160) = 170*320 + 160                       
            add viniciocarro,21                                                 
            ;mov [di+20],dl 
            cmp di,54686
            je etjugandoahora   ;   170*320+160+21=  maximo  54719
            pintarcarro viniciocarro,1,4
            jmp etjugandoahora 

            etfizquierda:                            
            cmp di, 54413
            je etjugandoahora       ;             54400
            pintarcarro viniciocarro,0,0 ; (170,160) = 170*320 + 160
            sub viniciocarro,21            
            cmp di, 54413
            je etjugandoahora
            pintarcarro viniciocarro,1,4                        
            jmp etjugandoahora

            etpausa:
            imprimir vesca
            ;getchar    
            jmp etjugandoahora

            etjuegotermina:            
            jmp etsalir                               
;---------------------------------------------------          

           

            etsalir:
   	        mov  ah,4ch       
	        xor  al,al
	        int  21h          
 
main  endp             
;--------------------------------------------------
;Termina proceso
;--------------------------------------------------
end main