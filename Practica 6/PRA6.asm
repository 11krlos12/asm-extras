clear_string MACRO string
  LOCAL CICLO, FIN
    MOV SI,0
CICLO:  CMP string[SI],'$'
    JE FIN
    MOV string[SI],'$'
    INC SI
    JMP CICLO
FIN:
ENDM


partition2 MACRO vP,vStart,vEnd
    LOCAL w1,w2,w3,w4,s1,s2
    
    mov ax,vStart
    mov varL, ax

    mov ax,vEnd
    mov varH, ax
    sub varH,2

    mov si,vP
    xor ax,ax
    mov al,misNumeros2[si]
    mov varPiv, ax

    mov ax,vP
    mov varOpera,ax

    mov ax,vEnd
    mov varOpera2,ax
    dec varOpera2

    swap varOpera,varOpera2

w1: mov ax,varH
    cmp varL,ax
    jnb s1

w2: mov bx,varL
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jna w3

    inc varL
    jmp w1

w3: mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jnbe w4

    dec varH
    jmp w1

w4: 
    mov ax,varL
    mov varOpera,ax

    mov ax,varH
    mov varOpera2,ax

    swap varOpera,varOpera2
    jmp w1

s1: mov ax,varH
    mov varIdx,ax

    mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]

    cmp ax,varPiv
    jna s2

    inc varIdx
s2: 
    mov ax,vEnd
    mov varOpera,ax
    dec varOpera

    mov ax,varIdx
    mov varOpera2,ax

    swap varOpera,varOpera2

    mov ax,varIdx
    mov varP,ax

ENDM


iterativeQsort2 MACRO
    local w1,w2, fin3
    
    push varFin
    push varCero
    push contador2


w1: pop var1

    cmp var1,'$'
    je fin3

    mov ax,var1
    mov varEnd,ax

    pop varStart

    mov ax,varEnd
    sub ax,varStart
    cmp ax,2
    jnb w2

    jmp w1


w2: mov ax,varEnd
    sub ax,varStart
    sar ax,1
    add ax,varStart
    mov varP,ax

    mov ax,varP
    mov varOpera,ax

    mov ax,varStart
    mov varOpera2,ax

    mov ax, varEnd
    mov varOpera3,ax

    partition2 varOpera,varOpera2,varOpera3

    inc varP
    push varP
    push varEnd
    push varStart
    dec varP
    push varP

    jmp w1

fin3:

ENDM


fread MACRO numbytes,databuffer,handle 
    MOV ah,3fh 
    MOV bx,handle 
    MOV cx,numbytes 
    LEA dx,databuffer 
    INT 21h 
ENDM 


fclose MACRO handle 
    MOV ah,3eh 
    MOV bx,handle 
    INT 21h 
ENDM


clear_string MACRO string
    LOCAL CICLO, FIN
        MOV SI,0
CICLO:  CMP string[SI],'$'
        JE FIN
        MOV string[SI],'$'
        INC SI
        JMP CICLO
FIN:
ENDM


fopen MACRO filename,handle 
    LEA dx,filename
    MOV ah,3dh 
    MOV al, 00h 
    INT 21h 
    MOV handle,ax 
    JNC S1
    MOV ax,-1 
S1:
ENDM


count_c MACRO string
    LOCAL CICLO,FIN
   XOR cx,cx
   MOV SI, 0
CICLO:   CMP string[SI],'$'
         JE FIN
         INC cx
         INC SI
         JMP CICLO
FIN:
ENDM



display_string2 MACRO cadena,fila

    mov resDx,dx
    mov resCx,cx
    mov resBx,bx
    mov resAx,ax

    mov al,1
    xor bh,bh
    mov bl,15
    count_c cadena

    mov dh,fila
    mov dl,cursor1
    push ds
    pop es
    mov bp, offset cadena
    mov ah,13h
    add cursor1,cl
    inc cursor1
    int 10h

    mov ax,resAx
    mov bx,resBx
    mov cx,resCx
    mov dx,resDx


ENDM


mov_d MACRO dstn,orign

    mov resAx, ax

    mov ax,orign
    mov dstn,ax

    mov ax,resAx

ENDM


draw_bar MACRO alto,ancho,corX
    local s1,s2,s3,s4,s5,c1,fin,fin2,c2
    mov resAx,ax
    mov resBx,bx
    mov resCx,cx
    mov resDx,dx

    xor dx,dx
    xor cx,cx
    xor bx,bx
    xor ax,ax

    mov dl,alto

    mov varTope1,175
    xor ax,ax
    mov ax,corX
    mov varTope2,ax

    mov ax,175
    sub al,alto

    mov cx,corX
    mov bl,ancho
    add cx,bx

    cmp dl,20
    jnbe s1
    mov varColor,4
    jmp c1

s1: cmp dl,40
    jnbe s2
    mov varColor,1
    jmp c1

s2: cmp dl,60
    jnbe s3
    mov varColor,14
    jmp c1

s3: cmp dl,80
    jnbe s4
    mov varColor,10
    jmp c1

s4: cmp dl,99
    jnbe c1
    mov varColor,15  

c1: cmp varTope1,ax
    jb fin

    mov bx,corX
    mov varTope2,bx

c2: cmp varTope2,cx
    ja fin2

    draw_pixel varTope2,varTope1,varColor

    inc varTope2
    jmp c2

fin2:   dec varTope1
        jmp c1

fin:
    mov ax,resAx
    mov bx,resBx
    mov cx,resCx
    mov dx,resDx
ENDM


draw_numberD MACRO number,x,y
    local s1,s2,s3,s4,s5,s6,s7,s8,s9,s10

    mov resAx,ax
    mov resDx,dx


    mov ax,number
    mov dx,10
    div dl

    cmp al,0
    jne s1

    draw_0 x,y
    jmp s10

s1: cmp al,1
    jne s2

    draw_1 x,y
    jmp s10

s2: cmp al,2
    jne s3

    draw_2 x,y
    jmp s10

s3: cmp al,3
    jne s4

    draw_3 x,y
    jmp s10


s4: cmp al,4
    jne s5

    draw_4 x,y
    jmp s10


s5: cmp al,5
    jne s6

    draw_5 x,y
    jmp s10


s6: cmp al,6
    jne s7

    draw_6 x,y
    jmp s10


s7: cmp al,7
    jne s8

    draw_7 x,y
    jmp s10


s8: cmp al,8
    jne s9

    draw_8 x,y
    jmp s10


s9: cmp al,9
    jne s10

    draw_9 x,y

s10:

    mov ax,resAx
    mov dx,resDx 

ENDM


convert_Byte MACRO vByte,vWord

    mov resAx,ax

    mov ax,vWord
    mov vByte,al

    mov ax,resAx

ENDM


draw_numberU MACRO number,x,y
    local s1,s2,s3,s4,s5,s6,s7,s8,s9,s10

    mov resAx,ax
    mov resDx,dx


    mov ax,number
    mov dx,10
    div dl

    mov al,ah

    cmp al,0
    jne s1

    draw_0 x,y
    jmp s10

s1: cmp al,1
    jne s2

    draw_1 x,y
    jmp s10

s2: cmp al,2
    jne s3

    draw_2 x,y
    jmp s10

s3: cmp al,3
    jne s4

    draw_3 x,y
    jmp s10


s4: cmp al,4
    jne s5

    draw_4 x,y
    jmp s10


s5: cmp al,5
    jne s6

    draw_5 x,y
    jmp s10


s6: cmp al,6
    jne s7

    draw_6 x,y
    jmp s10


s7: cmp al,7
    jne s8

    draw_7 x,y
    jmp s10


s8: cmp al,8
    jne s9

    draw_8 x,y
    jmp s10


s9: cmp al,9
    jne s10

    draw_9 x,y

s10:

    mov ax,resAx
    mov dx,resDx 

ENDM


swap MACRO vI,vJ

    mov ax,vI
    mov varOtra,ax


    mov ax,vJ
    mov varOtra,ax

    xor ax,ax
    xor si,si
    xor di,di

    mov di,vI
    mov si,vJ

    mov al,misNumeros2[di]
    mov varTemp,ax
    mov frequency,al

    xor ax,ax
    mov al,misNumeros2[si]
    mov misNumeros2[di],al

    mov ax,varTemp
    mov misNumeros2[si],al

    call emit_beep
    call repaint
    call end_beep

ENDM


partition MACRO vP,vStart,vEnd
    LOCAL w1,w2,w3,w4,s1,s2
    
    mov ax,vStart
    mov varL, ax

    mov ax,vEnd
    mov varH, ax
    sub varH,2

    mov si,vP
    xor ax,ax
    mov al,misNumeros2[si]
    mov varPiv, ax

    mov ax,vP
    mov varOpera,ax

    mov ax,vEnd
    mov varOpera2,ax
    dec varOpera2

    swap varOpera,varOpera2

w1: mov ax,varH
    cmp varL,ax
    jnb s1

w2: mov bx,varL
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jnb w3

    inc varL
    jmp w1

w3: mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]
    cmp ax,varPiv
    jnae w4

    dec varH
    jmp w1

w4: 
    mov ax,varL
    mov varOpera,ax

    mov ax,varH
    mov varOpera2,ax

    swap varOpera,varOpera2
    jmp w1

s1: mov ax,varH
    mov varIdx,ax

    mov bx,varH
    xor ax,ax
    mov al,misNumeros2[bx]

    cmp ax,varPiv
    jnb s2

    inc varIdx
s2: 
    mov ax,vEnd
    mov varOpera,ax
    dec varOpera

    mov ax,varIdx
    mov varOpera2,ax

    swap varOpera,varOpera2

    mov ax,varIdx
    mov varP,ax

ENDM


iterativeQsort MACRO
    local w1,w2, fin3
    
    push varFin
    push varCero
    push contador2


w1: pop var1

    cmp var1,'$'
    je fin3

    mov ax,var1
    mov varEnd,ax

    pop varStart

    mov ax,varEnd
    sub ax,varStart
    cmp ax,2
    jnb w2

    jmp w1


w2: mov ax,varEnd
    sub ax,varStart
    sar ax,1
    add ax,varStart
    mov varP,ax

    mov ax,varP
    mov varOpera,ax

    mov ax,varStart
    mov varOpera2,ax

    mov ax, varEnd
    mov varOpera3,ax

    partition varOpera,varOpera2,varOpera3

    inc varP
    push varP
    push varEnd
    push varStart
    dec varP
    push varP

    jmp w1

fin3:

ENDM


display_string Macro string

    mov resAx,ax
    mov resDx,dx


    MOV  dx, offset string
    MOV  ah, 9
    INT  21h

    mov ax,resAx
    mov dx,resDx

ENDM


convert_decimal MACRO numero
    LOCAL FIN, C1, C2, C3, C4, C5, S1, S2, S3, S4, S5

    MOV CX,numero

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C1

    MOV decimal[0],dl

    XOR DX,DX
    MOV AX,CX
    MOV SI, 1000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C2

    MOV decimal[1],dl

    XOR DX,DX
    MOV AX,CX
    MOV SI, 100
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C3

    MOV decimal[2],dl 

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C4

    MOV decimal[3],dl

    XOR DX,DX
    MOV AX,CX
    MOV SI, 1
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

    CMP dl,48
    JNE C5

    MOV decimal[4],dl
    MOV AH,06H
    INT 21H 

    JMP FIN

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C1: MOV decimal[0],dl
    MOV AH,06H
    INT 21H

    XOR DX,DX
    MOV AX,CX
    MOV SI, 1000
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C2: MOV decimal[1],dl
    MOV AH,06H
    INT 21H

    XOR DX,DX
    MOV AX,CX
    MOV SI, 100
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C3: MOV decimal[2],dl 
    MOV AH,06H
    INT 21H

    XOR DX,DX
    MOV AX,CX
    MOV SI, 10
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C4: MOV decimal[3],dl
    MOV AH,06H
    INT 21H


    XOR DX,DX
    MOV AX,CX
    MOV SI, 1
    DIV SI

    MOV CX,DX
    ADD AX,48
    MOV DX,AX

C5: MOV decimal[4],dl
    MOV AH,06H
    INT 21H 


FIN:
ENDM

include macros.txt
.286
.model large
.stack 
.fardata

;==============================================================================
; ---------------------------------- REPORTE ----------------------------------

repXML      db '<Arqui>',13,10
            db 09,'<Encabezado>',13,10
            db 09,09,'<Universidad>Universidad de San Carlos de Guatemala</Universidad>',13,10
            db 09,09,'<Facultad>Facultad de Ingenieria</Facultad>',13,10
            db 09,09,'<Escuela>Escuela de Ciencas y Sistemas</Escuela>',13,10
            db 09,09,'<Curso>',13,10
            db 09,09,09,'<Nombre>Arquitectura de Computadores y Ensambladores 1</Nombre>',13,10
            db 09,09,09,'<Seccion>Seccion A</Seccion>',13,10
            db 09,09,'</Curso>',13,10
            db 09,09,'<Ciclo>Primer Semestre 2019</Ciclo>',13,10
            db 09,09,'<Fecha>',13,10
tDia        db 09,09,09,'<Dia>00</Dia>',13,10
tMes        db 09,09,09,'<Mes>00</Mes>',13,10
tAnio       db 09,09,09,'<Año>0000</Año>',13,10
            db 09,09,'</Fecha>',13,10
            db 09,09,'<Hora>',13,10
tHora       db 09,09,09,'<Hora>00</Hora>',13,10
tMinuto     db 09,09,09,'<Minutos>00</Minutos>',13,10       
tSegundo    db 09,09,09,'<Segundos>00</Segundos>',13,10
            db 09,09,'</Hora>',13,10
            db 09,09,'<Alumno>',13,10
            db 09,09,09,'<Nombre>Ariel Alejandro Bautista Méndez</Nombre>',13,10
            db 09,09,09,'<Carnet>201503910</Carnet>',13,10
            db 09,09,'</Alumno>',13,10
            db 09,'</Encabezado>',13,10
            db 09,'<Resultados>',13,10,'$'

listaE1     db 09,09,09,'<Lista_Entrada>','$'
listaE2     db '</Lista_Entrada>',13,10,'$'


listaO1     db 09,09,09,'<Lista_Ordenada>','$'
listaO2     db '</Lista_Ordenada>',13,10,'$'


tipo1       db 09,09,09,'<Tipo>Descendente</Tipo>',13,10,'$'
tipo2       db 09,09,09,'<Tipo>Ascendente</Tipo>',13,10,'$'


algo11      db 09,09,'<Ordenamiento_BubbleSort>',13,10,'$'
algo12      db 09,09,'</Ordenamiento_BubbleSort>',13,10,'$'


algo21      db 09,09,'<Ordenamiento_QuickSort>',13,10,'$'
algo22      db 09,09,'</Ordenamiento_QuickSort>',13,10,'$'


algo31      db 09,09,'<Ordenamiento_ShellSort>',13,10,'$'
algo32      db 09,09,'</Ordenamiento_ShellSort>',13,10,'$'

result      db 09,09,09,'<Velocidad>'
velot       db '0</Velocidad>',13,10
            db 09,09,09,'<Tiempo>',13,10
            db 09,09,09,09,'<Minutos>'
ttMinu      db '00</Minutos>',13,10
            db 09,09,09,09,'<Segundos>'
ttSecond    db '00</Segundos>',13,10
            db 09,09,09,09,'<Milisegundos>'
ttMili      db '000</Milisegundos>',13,10
            db 09,09,09,'</Tiempo>',13,10,'$'


finRep      db 09,'</Resultados>',13,10
            db '</Arqui>','$'           

; -------------------------------- FIN REPORTE --------------------------------
;=============================================================================

encabezado  db "========================================================",13,10
            db "UNIVERSIDAD DE SAN CARLOS DE GUATEMALA",13,10
            db "FACULTAD DE INGENIERIA",13,10
            db "CIENCIAS Y SISTEMAS",13,10
            db "ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1",13,10
            db "NOMBRE: ARIEL ALEJANDRO BAUTISTA MENDEZ",13,10
            db "CARNE: 201503910",13,10
            db "SECCION A",13,10,13,10
            db "========================================================",13,10

menu        db "1) Cargar Archivo", 13, 10
            db "2) Ordenar", 13, 10
            db "3) Generar Reporte", 13, 10
            db "4) Salir", 13,10, 13, 10, '$'


menu2       db "========================================================",13,10
            db "CARGA DE ARCHIVO:",13,10,13,10
            db "RUTA: ",'$'


menu3       db "========================================================",13,10
            dB "SELECCIONE EL ALGORITMO DE ORDENAMIENTO:",13,10,13,10
            db "1) Ordenamiento BubbleSort",13,10
            db "2) Ordenamiento QuickSort",13,10
            db "3) Ordenamiento ShellSort",13,10,13,10,'$'


menu4       db "========================================================",13,10
            dB "VELOCIDAD DE LA ANIMACION:",13,10,13,10
            db "VELOCIDAD [0-9]: ",13,10,13,10,'$'


menu5       db "========================================================",13,10
            dB "SELECCIONE UN TIPO DE ORDENAMIENTO:",13,10,13,10
            db "1) Ordenamiento Descendente",13,10
            db "2) Ordenamiento Ascendente",13,10,13,10,'$'


menu6       db "========================================================",13,10
            db "GENERACION DE REPORTE",13,10,13,10
            db "Reporte Generado Con Exito!!!",13,10
            db "Pulse Para Continuar...",13,10,13,10,'$'


orden       db 0,'$'
varTipo     db 0,'$'


misNumeros      dw  25 dup(0) , '$'
misNumeros2     db  25 dup(0) , '$'
ordenados       db  25 dup(0) , '$'
desordenados    db  25 dup(0) , '$'     

decimal       db  0,0,0,0,0,'$'


salto         db  13,10,'$'


contenedor    dw  25 dup(0000),'$'


incorrecto  db "Seleccion incorrecta",13,10,13,10,'$'


seleccion   db 0


fin         db "Finalizando el programa...",13,10
            db "Pulse cualquier tecla para continuar",13,10,13,10,'$'


error   db 'Error Al Abrir El Archivo','$'


file        db 100 dup('$')


file2       db 100 dup('$')


handler     dw ? 


cadselect   db "La direccion es:",13,10,13,10,'$'


mensaje     db 'Archivo Leido','$'


buffer      db 1500 dup('$')


cartel      db 'prueba','$'


caracter    db 0,'$'


resAx       dw 0,'$'
resBx       dw 0,'$'
resCx       dw 0,'$'
resDx       dw 0,'$'
resSi       dw 0,'$'
resDi       dw 0,'$'


coma        db ',','$'

varTmp      db 0,0,'$'

contador1   db 0,'$'
contador2   dw 0,'$'


car1        db "Ordenamiento:",'$'
bbs         db "BubbleSort",'$'
qks         db "QuickSort",'$'
shs         db "ShellSort",'$'

tTime       db "Tiempo: "
tMinut      db "00:"
tSecond     db "00:"
tMili       db "000",'$'

vVelo       db "Velocidad: "
vVelo2      db "0",'$'


cursor1     db 1,'$'
cursor2     db 0,'$'

num1        db 0,0,'$'

xCor        dw 0,'$'
yCor        dw 0,'$'


varX        dw 0,'$'
varX2       dw 0,'$'
varY        dw 0,'$'
varEsp      dw 0,'$'


xPar        dw 0,'$'
yPar        dw 0,'$'


anchoG      db 0,'$'
lateral     db 0,'$'

varTmp1     dw 0,'$'
varColor    db 0,'$'
varTope1    dw 0,'$'
varTope2    dw 0,'$'


varByte     db 0,'$'


retraso     dw 0,'$'


fecha       db  "Fecha: "   
date        db  "00/00/0000",32
hora        db  "Hora: "
time        db  "00:00:00"


;VARIABLES PARA TODOS LOS ORDENAMIENTOS


varStart    dw  0,'$'
varEnd      dw  0,'$'
varP        dw  0,'$'
varL        dw  0,'$'
varH        dw  0,'$'
varPiv      dw  0,'$'
varIdx      dw  0,'$'
varTemp     dw  0,'$'
varOpera    dw  0,'$'
varOpera2   dw  0,'$'
varOpera3   dw  0,'$'
varN        dw  0,'$'
varN2       dw  0,'$'
varGap      dw  0,'$'
varI        dw  0,'$'
varJ        dw  0,'$'
var1        dw  0,'$'


varOtra     dw  0,'$'


varFin      dw '$'
varCero     dw 0,'$'

elTiempo    dw 0,'$'
cDelay      dw 0,'$'


nombre      db  "c:\Rep.xml",00h

bufferAux   db 25601 dup('$')

punteroB    dw 0,'$'


frequency       db 0,'$'




.code

main proc

    ASSUME ds:@fardata
    mov  ax, @fardata
    mov  ds, ax

    INICIO: ;PUSH '$'
        call clear_screen

INIT:   display_string encabezado
        call read_choice

        display_string salto

        CMP seleccion,1
        JE SELECCION1

        CMP seleccion,2
        JE SELECCION2

        CMP seleccion,3
        JE SELECCION3

        CMP seleccion, 4
        JE FINALIZAR

        display_string incorrecto
        mov  ah, 7
        int  21h
        JMP INICIO


SELECCION1: call clear_screen
            display_string menu2
            call read_url
            display_string salto
            call fix_url
            fopen file2, handler
            JNC ABRIR

            display_string salto
            display_string error
            display_string salto
            display_string salto

            MOV  ah, 7
            INT  21h

            JMP INICIO

ABRIR:      display_string cadselect
            display_string file2
            fread 1500,buffer,handler
            fclose handler

CM3:        display_string salto
            ;display_string buffer
            display_string salto
            display_string salto
            display_string mensaje
            display_string salto

            call read_file
            call mostrar_arreglo
            display_string salto
            call c_space

            xor ax,ax
            mov al,anchoG
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto

            xor ax,ax
            mov al,lateral
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto

            xor ax,ax
            mov al,cursor2
            mov varTmp1,ax
            convert_decimal varTmp1
            display_string salto

            clear_string file
            clear_string file2

            MOV  ah, 7
            INT  21h

            JMP INICIO


SELECCION2: call clear_screen
            

            display_string menu3
            call read_choice
            xor ax,ax
            mov al,seleccion
            mov orden,al
            display_string salto

            call clear_screen
            display_string menu4
            call read_choice
            call mov_delay
            display_string salto

            call clear_screen
            display_string menu5
            call read_choice
            xor ax,ax
            mov al,seleccion
            mov varTipo, al

            MOV AX,0013H
            INT 10H

            call mov_misNumeros
            call draw_square
            call c_space
            call draw_footer

            mov  ah, 7
            int  21h
            
            call begin_sort

            JMP INICIO


SELECCION3: call clear_screen
            display_string menu6
            call show_rep
            call wait_key
            JMP INICIO



FINALIZAR:    display_string fin

              call wait_key

              mov  ax, 4c00h
              int  21h


main endp


emit_beep proc

    mov al, 182
    out 43h,al

    cmp frequency,20
    jnb s1

    mov ax, 11931

    jmp s5

s1: cmp frequency,40
    jnb s2
    mov ax, 3977

    jmp s5

s2: cmp frequency,60
    jnb s3
    mov ax, 2386

    jmp s5

s3: cmp frequency,80
    jnb s4
    mov ax, 1704

    jmp s5

s4: mov ax, 1325

s5: out 42h, al
    mov al, ah
    out 42h, al
    in al, 61h

    or al, 00000011b
    out 61h, al

    ret

emit_beep endp


end_beep proc

    in  al, 61h
    and al, 11111100b
    out 61h, al 

    ret

end_beep endp


mov_listaE proc

    mov si,punteroB
    xor di,di
    xor ax,ax

c1: cmp listaE1[di],'$'
    je c2

    mov al,listaE1[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c1


c2: mov di,0

c3: cmp di,contador2
    je c4

    cmp desordenados[di],'$'
    je c4

    cmp di,0
    je s1

    mov bufferAux[si],','
    inc si

s1: xor ax,ax
    mov al,desordenados[di]
    mov dl,10

    div dl
    cmp al,0
    je s2

    mov bufferAux[si],al
    add bufferAux[si],48
    inc si

s2: mov bufferAux[si],ah
    add bufferAux[si],48
    inc si

    inc di

    jmp c3


c4: mov di,0

    xor ax,ax
c5: cmp listaE2[di],'$'
    je c6

    mov al,listaE2[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c5

c6: mov punteroB,si
    ret

mov_listaE endp


arreglar_resul proc

    mov si,punteroB
    xor di,di
    xor ax,ax

    cmp orden,1
    jne t1

q1: cmp algo11[di],'$'
    je t3

    mov al,algo11[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q1


t1: cmp orden,2
    jne t2


q3: cmp algo21[di],'$'
    je t3

    mov al,algo21[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q3


t2: cmp orden,3
    jne t3


q5: cmp algo31[di],'$'
    je t3

    mov al,algo31[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q5

t3: xor di,di
    xor ax,ax

    cmp varTipo,1
    jne r1

r0: cmp tipo1[di],'$'
    je r3

    mov al,tipo1[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp r0

r1: cmp varTipo,2
    jne r3

r2: cmp tipo2[di],'$'
    je r3

    mov al,tipo2[di]
    mov bufferAux[si],al

    inc si
    inc di

    jmp r2

r3: mov punteroB,si
    call mov_listaE
    xor di,di

c1: cmp listaO1[di],'$'
    je c2

    mov al,listaO1[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c1


c2: mov di,0

c3: cmp di,contador2
    je c4

    cmp misNumeros2[di],'$'
    je c4

    cmp di,0
    je s1

    mov bufferAux[si],','
    inc si

s1: xor ax,ax
    mov al,misNumeros2[di]
    mov dl,10

    div dl
    cmp al,0
    je s2

    mov bufferAux[si],al
    add bufferAux[si],48
    inc si

s2: mov bufferAux[si],ah
    add bufferAux[si],48
    
    inc si
    inc di

    jmp c3


c4: mov di,0

    xor ax,ax
c5: cmp listaO2[di],'$'
    je c6

    mov al,listaO2[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp c5

c6: mov punteroB,si
    
    mov al,vVelo2[0]
    mov velot[0],al

    mov al,tMinut[0]
    mov ttMinu[0],al

    mov al,tMinut[1]
    mov ttMinu[1],al

    mov al,tSecond[0]
    mov ttSecond[0],al

    mov al,tSecond[1]
    mov ttSecond[1],al

    mov al,tMili[0]
    mov ttMili[0],al

    mov al,tMili[1]
    mov ttMili[1],al

    mov al,tMili[1]
    mov ttMili[1],al

    xor di,di

z0: cmp result[di],'$'
    je z1

    mov al,result[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp z0

z1: xor di,di

    cmp orden,1
    jne t4

q6: cmp algo12[di],'$'
    je t6

    mov al,algo12[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q6


t4: cmp orden,2
    jne t5


q8: cmp algo22[di],'$'
    je t6

    mov al,algo22[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q8


t5: cmp orden,3
    jne t6

q10:cmp algo32[di],'$'
    je t6

    mov al,algo32[di]
    mov bufferAux[si],al

    inc di
    inc si

    jmp q10

t6: mov punteroB,si

    ret

arreglar_resul endp


mov_orden proc

    xor si,si
    xor ax,ax

c1: cmp si,25
    je c2 

    mov al,misNumeros2[si]
    mov ordenados[si],al

    inc si
    jmp c1
c2:
    ret
mov_orden endp



mov_deso proc

    xor si,si
    xor ax,ax

c1: cmp si,25
    je c2 

    mov al,misNumeros2[si]
    mov desordenados[si],al

    inc si
    jmp c1
c2:
    ret

mov_deso endp


show_rep proc

    call    get_date
    call    get_time

; -------------- MOVIENDO DIA --------------
    XOR ax,ax
    MOV al,date[0]
    MOV tDia[8],al
    MOV al,date[1]
    MOV tDia[9],al

; -------------- MOVIENDO MES --------------
    MOV al,date[3]
    MOV tMes[8],al
    MOV al,date[4]
    MOV tMes[9],al

; -------------- MOVIENDO AÑO --------------
    MOV al,date[6]
    MOV tAnio[9],al
    MOV al,date[7]
    MOV tAnio[10],al
    MOV al,date[8]
    MOV tAnio[11],al
    MOV al,date[9]
    MOV tAnio[12],al

; -------------- MOVIENDO HORA --------------
    MOV al,time[0]
    MOV tHora[09],al
    MOV al,time[1]
    MOV tHora[10],al

; -------------- MOVIENDO MINUTOS --------------
    MOV al,time[3]
    MOV tMinuto[12],al
    MOV al,time[4]
    MOV tMinuto[13],al

; -------------- MOVIENDO SEGUNDOS --------------
    MOV al,time[6]
    MOV tSegundo[13],al
    MOV al,time[7]
    MOV tSegundo[14],al

;; -------------- FIN MOVIMIENTOS --------------

    MOV    ah,3ch
    MOV    cx,00
    LEA    dx,nombre
    INT    21h

    MOV    handler,ax

    MOV    ah,40h
    MOV    bx,handler
    count_c repXML
    LEA    dx,repXML
    INT    21h

;==================================================
;---------------- MOVER RESULTADOS ----------------
    
    
    MOV    ah,40h
    MOV    bx,handler
    count_c bufferAux
    LEA    dx,bufferAux
    INT    21h

;---------------- MOVER RESULTADOS ----------------
;==================================================


    MOV    ah,40h
    MOV    bx,handler
    count_c finRep
    LEA    dx,finRep
    INT    21h

    MOV    ah,3eh
    MOV    bx,handler
    INT    21h

    ret 

show_rep endp


repaint proc

    mov ax,0

c1: cmp ax,elTiempo
    je s1
    call delay_0
    inc ax
    inc cDelay
    cmp cDelay,4
    jne c1

    mov cDelay,0
    call c_tiempo

    jmp c1

s1: 
    call pasar_arreglo2
    mov ah,0 
    mov al,13h
    int 10h
    call draw_square
    call c_space
    call draw_footer
    ret


repaint endp


milis proc

    push si

    inc tMili[2]
    cmp tMili[2],58
    jne s1

    mov tMili[2],48

    inc tMili[1]
    cmp tMili[1],58
    jne s1

    mov tMili[1],48

    inc tMili[0]
    cmp tMili[0],58
    jne s1

    mov tMili[0],48
s1:
    mov cursor1, 1
    display_string2 tTime,1
    pop si
    ret

milis endp


delay_0 proc

    MOV SI,250
T1: call milis
    DEC SI
    JZ U1
    MOV DI,1400
D1: DEC DI
    JNZ D1
    JMP T1

U1: 
    ret

delay_0 endp


mostrar_arreglo proc

    mov resCx,cx
    mov resBx,bx
    
    XOR BX,BX
    XOR CX,CX
    
C1: convert_decimal misNumeros[BX]
    display_string coma
    INC BX
    INC BX
    CMP BX, 48
    JA FIN2
    JMP C1
FIN2:
    display_string salto

    mov cx,resCx
    mov bx,resBx 

    ret


mostrar_arreglo endp


read_choice proc

    MOV ah,01h
    int 21h
    sub al,30h
    mov seleccion,al

    mov ah,09
    lea dx,salto
    int 21h

    ret

read_choice endp


clear_screen proc

    mov  ah, 0
    mov  al, 3
    int  10H

    ret

clear_screen endp


fix_url proc

        MOV SI, 0
        CMP file[SI],'%'
        JNE QUITAR
        INC SI

        MOV DI,0

CICLOZ: CMP file[SI],'%'
        JE QUITAR

        MOV al,file[SI]
        MOV file2[DI],al

        INC SI
        INC DI
        JMP CICLOZ

QUITAR: 
        MOV file2[DI],00h
        ret

fix_url endp


read_url proc

    MOV ah, 3fh
    MOV bx, 00
    MOV cx, 100
    MOV dx, offset [file]
    int 21h

    ret

read_url endp


read_file proc

    call clear_array

    xor si,si
    xor di,di 
    xor bx,bx
    xor dx,dx
    mov contador1,0
    mov varTmp[0],0
    mov varTmp[1],0

B1: CMP buffer[si],'>'
    je B2
    inc si
    jmp B1

B2: inc si
B3: cmp buffer[si],'>'
    je B4
    inc si
    jmp B3

B4: inc si
    CMP buffer[si],49
    JB FIN2

    CMP buffer[si],57
    JA FIN2

    mov bx,1
    xor dx,dx

B5: cmp buffer[si],'<'
    je B6

    mov dl,buffer[si]
    mov varTmp[bx],dl
    inc si
    dec bx
    jmp B5


B6: inc si
    mov resSi,si
    xor ax,ax
    xor dx,dx

    sub varTmp[1],48
    sub varTmp[0],48

    cmp bx,0
    je U1

    mov al,varTmp[1]
    mov dl,10
    mul dl
    add al,varTmp[0]

    mov misNumeros[di],ax
    jmp M1

U1: mov al,varTmp[1]
    mov misNumeros[di],ax

M1: inc di
    inc di

    mov varTmp[0],48
    mov varTmp[1],48
    inc contador1
    mov si,resSi
    jmp B1
    

FIN2:
    clear_string buffer
    call pasar_arreglo1
    call mov_deso
    call mov_deso
    ret

read_file endp


display_array proc

    mov resAx,ax
    mov resBx,bx

C1:     cmp misNumeros[bx],0
        JE FIN2


FIN2:   mov ax,resAx
        mov bx,resBx

        ret 

display_array endp


draw_square proc

        mov cursor1,1

        display_string2 car1,0
        cmp orden,1
        jne m1

        display_string2 bbs,0
        jmp m4 

m1:     cmp orden,2
        jne m2

        display_string2 qks,0
        jmp m4 


m2:     cmp orden,3
        jne m4

        display_string2 shs,0

m4:     
        display_string2 vVelo,0
        
        mov cursor1, 1
        display_string2 tTime,1

        mov xCor,6
        mov yCor,20

c1:     cmp xCor,313
        je c2
        draw_pixel xCor,yCor,15
        inc xCor
        jmp c1

c2:     cmp yCor,20
        jne s1
        inc yCor
        mov xCor,6
        JMP c1

s1:     cmp yCor,21
        jne s2
        inc yCor
        mov xCor,6
        JMP c1

s2:     cmp yCor,22
        jne s3
        mov yCor,192
        mov xCor,6
        jmp c1

s3:     cmp yCor,192
        jne s4
        inc yCor
        mov xCor,6
        jmp c1

s4:     cmp yCor,193
        jne c3
        inc yCor
        mov xCor,6
        jmp c1

c3:     mov xCor,6
        mov yCor,20

c4:     cmp yCor,195
        je s5
        draw_pixel xCor,yCor,15
        inc yCor
        jmp c4

s5:     cmp xCor,6
        jne s6
        inc xCor
        mov yCor,20
        jmp c4

s6:     cmp xCor,7
        jne s7
        inc xCor
        mov yCor,20
        jmp c4

s7:     cmp xCor,8
        jne s8
        mov xCor,311
        mov yCor,20
        jmp c4

s8:     cmp xCor,311
        jne s9
        inc xCor
        mov yCor,20
        jmp c4

s9:     cmp xCor,312
        jne fin2
        inc xCor
        mov yCor,20
        jmp c4

fin2:    ret


draw_square endp


clear_array proc

    xor bx,bx

c1: cmp bx,50
    je c2

    mov misNumeros[bx],0
    inc bx
    jmp c1

c2: ret

clear_array endp


draw_array proc

    mov resBx,bx
    mov resSi,si

    mov si,0

    mov bx, resBx
    mov si,resSi

    ret

draw_array endp


c_space proc

    mov anchoG,0
    mov lateral,13
    mov cursor2,0
    mov varX,12
    mov varX2,8

    cmp contador1,10
    jne s1

    mov anchoG,19

    mov varX2,20
    mov varEsp,9
        
        
    mov varX,24
    mov cursor2,19

    jmp s16


s1: cmp contador1,11
    jne s2

    mov anchoG,18

    mov varX2,20
    mov varEsp,7
        
        
    mov varX,24
    mov cursor2,17

    jmp s16


s2: cmp contador1,12
    jne s3

    mov anchoG,17

    mov varX2,19
    mov varEsp,7
        
        
    mov varX,23
    mov cursor2,15

    jmp s16


s3: cmp contador1,13
    jne s4


    mov anchoG,16

    mov varX2,18
    mov varEsp,6
        
        
    mov varX,21
    mov cursor2,13

    jmp s16


s4: cmp contador1,14
    jne s5

    mov anchoG,15

    mov varX2,21
    mov varEsp,5
        
        
    mov varX,23
    mov cursor2,11

    jmp s16


s5: cmp contador1,15
    jne s6

    mov anchoG,14

    mov varX2,19
    mov varEsp,5
        
        
    mov varX,21
    mov cursor2,10

    jmp s16


s6: cmp contador1,16
    jne s7


    mov anchoG,13

    mov varX2,17
    mov varEsp,5
        
        
    mov varX,19
    mov cursor2,9

    jmp s16



s7: cmp contador1,17
    jne s8

    mov anchoG,13

    mov varX2,16
    mov varEsp,4
        
        
    mov varX,18
    mov cursor2,8

    jmp s16


s8: cmp contador1,18
    jne s9

    mov anchoG,12

    mov varX2,16
    mov varEsp,4
        
        
    mov varX,17
    mov cursor2,7

    jmp s16

s9: cmp contador1,19
    jne s10

    mov anchoG,11

    mov varX2,16
    mov varEsp,4
        
        
    mov varX,17
    mov cursor2,6

    jmp s16


s10:    cmp contador1,20
        jne s11

        mov anchoG,12

        mov varX2,11
        mov varEsp,3
        
        
        mov varX,13
        mov cursor2,6

        jmp s16


s11:    cmp contador1,21
        jne s12

        mov anchoG,11

        mov varX2,13
        mov varEsp,3
        
        
        mov varX,14
        mov cursor2,5

        jmp s16


s12:    cmp contador1,22
        jne s13

        mov anchoG,10

        mov varX2,17
        mov varEsp,3
        
        
        mov varX,17
        mov cursor2,4

        jmp s16


s13:    cmp contador1,23
        jne s14

        mov anchoG,10

        mov varX2,11
        mov varEsp,3
        
        
        mov varX,12
        mov cursor2,4

        jmp s16


s14:    cmp contador1,24
        jne s15

        mov anchoG,9

        mov varX2,16
        mov varEsp,3
        
        
        mov varX,16
        mov cursor2,3

        jmp s16


s15:    cmp contador1,25
        jne s16

        mov anchoG,8

        mov varX2,12
        mov varEsp,4
        
        
        mov varX,12
        mov cursor2,3

        jmp s16
    
s16:
        ret

c_space endp


mov_misNumeros proc

    xor si,si
    xor di,di

c1: cmp di,contador2
    je c2

    xor ax,ax
    mov al,desordenados[di]
    mov misNumeros[si],ax

    inc di
    add si,2

    jmp c1

c2:
    ret
mov_misNumeros endp


draw_footer proc

            xor di,di
            xor si,si

            xor ax,ax
            mov al,contador1
            mov varTmp1,ax

sql1:       cmp di,varTmp1
            je sql2

            convert_Byte varByte,misNumeros[si]
            draw_bar varByte,anchoG,varX2

            xor ax,ax
            mov al,anchoG
            add varX2,ax
            mov cx,varEsp
            add varX2,cx

            
            draw_numberD misNumeros[si],varX,180
            add varX,5
            draw_numberU misNumeros[si],varX,180
            add varX,4


            xor ax,ax
            mov al,cursor2
            add varX,ax

            inc di
            add si,2
            jmp sql1

sql2:       ret

draw_footer endp


pasar_arreglo2 proc

    mov resBx,bx
    mov resSi,si
    mov resAx,ax

    xor bx,bx
    xor si,si
    xor ax,ax

c1: cmp si,contador2
    je c2 

    xor ax,ax
    mov al,misNumeros2[si]
    mov misNumeros[bx],ax

    inc si
    add bx,2
    jmp c1


    mov bx,resBx
    mov si,resSi
    mov bx,resBx

c2: 
    ret

pasar_arreglo2 endp


bubbleSort2 proc

    xor si,si
    mov ax,contador2
    mov varN,ax

    dec varN

c1: cmp si,varN
    je c2

    xor bx,bx
    mov di,1
    mov ax,contador2
    mov varN2,ax

    sub varN2,si
    dec varN2

c3: cmp bx,varN2
    je c4

    xor dx,dx
    mov dl,misNumeros2[di]
    cmp misNumeros2[bx],dl
    jnb c5

    xor ax,ax
    mov al,misNumeros2[bx]
    mov varTemp,ax
    mov frequency,al

    mov misNumeros2[bx],dl

    mov ax,varTemp
    mov misNumeros2[di],al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax
    

    

c5: inc di
    inc bx
    jmp c3

c4: inc si
    jmp c1

c2:
    ret

bubbleSort2 endp


bubbleSort proc

    xor si,si
    mov ax,contador2
    mov varN,ax

    dec varN

c1: cmp si,varN
    je c2

    xor bx,bx
    mov di,1
    mov ax,contador2
    mov varN2,ax

    sub varN2,si
    dec varN2

c3: cmp bx,varN2
    je c4

    xor dx,dx
    mov dl,misNumeros2[di]
    cmp misNumeros2[bx],dl
    jna c5

    xor ax,ax
    mov al,misNumeros2[bx]
    mov varTemp,ax
    mov frequency,al

    mov misNumeros2[bx],dl

    mov ax,varTemp
    mov misNumeros2[di],al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax
    

    

c5: inc di
    inc bx
    jmp c3

c4: inc si
    jmp c1

c2:
    ret

bubbleSort endp


shellSort2 proc

    mov ax,contador2
    mov varN,ax

    sar ax,1
    mov varGap,ax

c0: cmp varGap,0
    jna c1

    mov ax,varGap
    mov varI,ax

c3: mov cx,contador2
    cmp varI,cx
    jnb c2

    mov si,varI
    xor ax,ax
    mov al,misNumeros2[si]
    mov varTemp,ax

    mov ax,varI
    mov varJ,ax

c5: mov cx,varGap
    cmp varJ,cx
    jnae c4

    mov si,varJ
    sub si,varGap
    xor ax,ax
    mov al, misNumeros2[si]
    
    cmp ax,varTemp
    jnb c4

    mov si, varJ
    mov misNumeros2[si],al
    mov frequency,al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax

    sub varJ,cx
    jmp c5
c4:
    mov si,varJ
    mov ax,varTemp
    mov misNumeros2[si],al
    inc varI
    jmp c3
c2:
    sar varGap,1
    jmp c0
c1:
    ret

shellSort2 endp


shellSort proc

    mov ax,contador2
    mov varN,ax

    sar ax,1
    mov varGap,ax

c0: cmp varGap,0
    jna c1

    mov ax,varGap
    mov varI,ax

c3: mov cx,contador2
    cmp varI,cx
    jnb c2

    mov si,varI
    xor ax,ax
    mov al,misNumeros2[si]
    mov varTemp,ax

    mov ax,varI
    mov varJ,ax

c5: mov cx,varGap
    cmp varJ,cx
    jnae c4

    mov si,varJ
    sub si,varGap
    xor ax,ax
    mov al, misNumeros2[si]
    
    cmp ax,varTemp
    jna c4

    mov si, varJ
    mov misNumeros2[si],al
    mov frequency,al

    push ax
    push bx
    push cx
    push dx
    push si
    push di

    call emit_beep
    call repaint
    call end_beep

    pop di
    pop si
    pop dx
    pop cx
    pop bx
    pop ax

    sub varJ,cx
    jmp c5
c4:
    mov si,varJ
    mov ax,varTemp
    mov misNumeros2[si],al
    inc varI
    jmp c3
c2:
    sar varGap,1
    jmp c0
c1:
    ret

shellSort endp


mov_delay proc

    xor dx,dx
    xor ax,ax

    mov ax,750
    mov dl,seleccion
    mov elTiempo,dx
    inc elTiempo

    mov vVelo2[0],dl
    add vVelo2[0],48
    mul dx

    mov retraso,ax

    ret

mov_delay endp


begin_sort proc

    call pasar_arreglo1

    mov cDelay,0

    mov tMinut[0],48
    mov tSecond[1],48
    mov tSecond[0],48
    mov tSecond[1],48
    mov tMili[0],48
    mov tMili[1],48
    mov tMili[2],48

    cmp orden,1
    jne s1

    cmp varTipo,1
    jne m1

    call bubbleSort2

    jmp fin2

m1: cmp varTipo,2
    jne fin2

    call bubbleSort
    jmp fin2

s1: cmp orden,2
    jne s2

    cmp varTipo,1
    jne m2

    iterativeQsort2
    jmp fin2

m2: cmp varTipo,2
    jne fin2

    iterativeQsort
    jmp fin2

s2: cmp orden,3
    jne fin2

    cmp varTipo,1
    jne m3

    call shellSort2
    jmp fin2

m3: cmp varTipo,2
    jne fin2

    call shellSort

fin2:
    

c1: mov ah, 02h
    int 1ah

    mov ax,13
    mul dh

    mov dl,100
    div dl

    mov tMili[0],al
    add tMili[0],48

    mov al,ah
    mov ah,0

    mov dl,10
    div dl

    mov tMili[1],al
    add tMili[1],48

    mov tMili[2],ah
    add tMili[2],48

    mov cursor1, 1
    display_string2 tTime,1
    call wait_key


    call arreglar_resul
    ret


begin_sort endp


c_tiempo proc

    push si
    mov si,1

    inc tSecond[si]

    cmp tSecond[si],58
    jne o1

    mov tSecond[si],48
    dec si
    inc tSecond[si]

    cmp tSecond[si],54
    jne o1

    mov tSecond[si],48

    inc si
    inc tMinut[si]

    cmp tMinut[si],58
    jne o1

    mov tMinut[si],48
    dec si
    inc tMinut[si]

    

o1: 
    mov cursor1, 1
    display_string2 tTime,1
    pop si
    ret

c_tiempo endp


pasar_arreglo1 proc

    xor bx,bx
    xor si,si
    xor ax,ax

    mov al,contador1
    mov contador2,ax

c1: cmp si,contador2
    je c2 

    mov ax,misNumeros[bx]
    mov misNumeros2[si],al

    inc si
    add bx,2
    jmp c1

c2: ret 

pasar_arreglo1 endp



wait_key proc

c1: mov  ah, 7
    int  21h

    ret 

wait_key endp


get_date proc
    mov     ah, 04h
    int     1ah

    mov     bx, offset date
    mov     al, dl
    call    put_bcd2

    inc     bx
    mov     al, dh
    call    put_bcd2

    inc     bx
    mov     al, ch
    call    put_bcd2
    mov     al, cl
    call    put_bcd2

    ret
get_date endp


get_time proc
    mov     ah, 02h
    int     1ah

    mov     bx, offset time
    mov     al, ch
    call    put_bcd2

    inc     bx
    mov     al, cl
    call    put_bcd2

    inc     bx
    mov     al, dh
    call    put_bcd2

    ret
get_time endp


put_bcd2 proc
    PUSH    ax
    SHR     ax, 04
    AND     ax, 0fh
    ADD     ax, '0'
    MOV     [bx], al
    INC     bx
    POP     ax

    AND     ax, 0fh
    ADD     ax, '0'
    MOV     [bx], al

    INC     bx

    RET
put_bcd2 endp


end main