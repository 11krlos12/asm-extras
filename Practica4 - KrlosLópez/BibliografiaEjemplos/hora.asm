;--------------------------------------------------
;Directiva para indicar el tama�o del programa
;--------------------------------------------------
.model small
.stack
;--------------------------------------------------
;Directiva para indicar datos (variables)
;--------------------------------------------------
.data
;Variables para guardar los valores en hexadecimal

;---------------------------------------------------
;DATOS DEL ENCABEZADO
;---------------------------------------------------
enc0 db  'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA', 0ah,0dh, '$'
enc1 db  'FACULTAD DE INGENIERIA', 0ah,0dh, '$'
enc2 db  'CIENCIAS Y SISTEMAS', 0ah,0dh, '$'
enc3 db  'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1', 0ah,0dh, '$'
enc4 db  'NOMBRE: JUAN CARLOS MALDONADO SOLORZANO', 0ah,0dh, '$'
enc5 db  'CARNET: 201222687', 0ah,0dh, '$'
enc6 db  'SECCION: A',0ah,0dh, '$'

;-------------------------
;DATOS DEL MENU
;-------------------------
LineaVacia db  0ah,0dh, '$'

msg1 db '1.Iniciar Juego.', 0ah,0dh,'$'
msg2 db '2.Cargar Juego.',0ah,0dh,'$'
msg3 db '3.Salir.', 0ah,0dh,'$'                             

Vopcion db 'Seleccione la opcion que desee: ',0ah,0dh,'$' 

msga db 'juego iniciando', 0ah,0dh,'$' 
msgb db 'cargando el juego.', 0ah,0dh,'$' 
msgc db 'finalizando juego.', 0ah,0dh,'$' 

;--------------------------------------------------
;Directiva para indicar codigo
;--------------------------------------------------

;--------------------------------------------------
;Variables para la hora
;--------------------------------------------------
ho DB ?, '$'
mi DB ?, '$'
se DB ?, '$'
hconv db ?, ?, '$'
miconv db ?, ?, '$'
seconv db ?, ?, '$'
msj_separador DB " :",'$'
msjhora db "Hora: ",'$'
;--------------------------------------------------
;Variables para la fecha
;--------------------------------------------------
diaF DB ?, '$'
mesF DB ?, '$'
dmconv DB ?, ?, '$'
mconv DB ?, ?, '$'
fechaseparador DB " /",'$'
msjfecha db 'Fecha: ','$'
fechaanio DB '2020','$'

.code

;--------------------------------------------------
;MACRO PARA IMPRIMIR EN PANTALLA
;--------------------------------------------------
mostrar macro texto
    mov ax,@data    
    mov ds,ax  
    mov ah,09h ; numero de funcion para imprimir cadena en pantalla
;igual a lea dx,cadena,inicializa en dx la posicion donde comienza la cadena
    mov dx,offset texto 
    int 21h
endm


HoraActual macro horas,minutos,segundos,horas_conv,min_conv,seg_conv

mov ah, 2Ch
int 21h

mov  horas, ch
mov  minutos, cl
mov  segundos, dh

mov al, [horas]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset horas_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [minutos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset min_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [segundos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset seg_conv
mov [bx], al
inc bx
mov [bx], ah

endm

FechaActual macro  dia_mes,mes,dia_mes_conv,mes_ano_conv
	mov ah, 2AH
	int 21h
	;mov  fechaanio, cx
	mov  mes, dh
	mov  dia_mes, dl	
;Obtener el n�mero del d�a del mes
;Dividimos entre 10 para obtener el n�mero en decimal
;Luego lo transformamos a ASCII
mov al, [dia_mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset dia_mes_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset mes_ano_conv 
mov [bx], al
inc bx
mov [bx], ah

endm
;--------------------------------------------------
;MACRO PARA OBTENER CARACTER
;--------------------------------------------------
getchar macro
mov ah,01h
int 21h
endm


;--------------------------------------------------
;procedimiento principal
;--------------------------------------------------
main  proc              

;Inicia proceso
;--------------------------------------------


   encabezado:

	   mostrar enc0
	   mostrar enc1
	   mostrar enc2
	   mostrar enc3
	   mostrar enc4
	   mostrar enc5
	   mostrar enc6



     FechaActual diaF,mesF,dmconv,mconv

     mostrar msjfecha
     mostrar dmconv
     mostrar fechaseparador
     mostrar mconv
     mostrar fechaseparador
     mostrar fechaanio
;---------------------------------------------
;MOSTRAMOS EL ENCABEZADO
;----------------------------------------------	

     salir:
   	   mov  ah,4ch       
	   xor  al,al
	   int  21h          
 
main  endp             

;--------------------------------------------------
;Termina proceso
;--------------------------------------------------
end main