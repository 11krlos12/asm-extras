;########################################################################################################################
;------------------------------------------ DECLARACION DE MACROS -------------------------------------------------------
;########################################################################################################################

;----------------------------------------IMPRIMIR UNA CADENA EN PANTALLA-------------------------------------------------
print macro cadena 
LOCAL ETIQUETA 
ETIQUETA: 
	mov ah,09h 
	mov dx,@data 
	mov ds,dx 
	mov dx,offset cadena 
	int 21h
endm

;-------------------------------------OBTIENE UN CARACTER DEL TECLADO----------------------------------------------------
getChar macro
mov ah,0dh
int 21h
mov ah,01h
int 21h
endm

;-----------------------------------LEE LA RUTA QUE INGRESAMOS DESDE EL TECLADO------------------------------------------
getRuta macro buffer
LOCAL INICIO,FIN
	xor si,si
INICIO:
	getChar
	cmp al,0dh ;13 en retorno de carro, si es se va al final
	je FIN
	mov buffer[si],al
	inc si
	jmp INICIO
FIN:
	mov buffer[si],00h ;finaliza con caracter NULL
endm

;------------------------------------------ABRIR UN ARCHIVO---------------------------------------------------------------
abrirF macro ruta,handle
mov ah,3dh
mov al,010b
lea dx,ruta
int 21h
jc ERROR_ABRIR ; Si bandera carry=1, se activa hubo un error al abrirlo, jump carry , ErrorAbrir es etiqueta de error por si no llego a abrir el archivo
mov handle,ax ; ax devolvio el handler, lo movemos a nuestra variable
endm

;----------------------------------------LEER UN ARCHIVO-------------------------------------------------------------------
leerF macro numbytes,buffer,handle
mov ah,3fh
mov bx,handle
mov cx,numbytes
lea dx,buffer
int 21h
jc ERROR_LEER
endm

;----------------------------------------CREAR UN ARCHIVO-------------------------------------------------------------------
crearF macro ruta, handle
mov ah,3ch
mov cx,00h
lea dx,ruta
int 21h
jc ERROR_CREAR
mov handle,ax
endm

;----------------------------------------ESCRIBIR UN ARCHIVO-------------------------------------------------------------------
escribirF macro handle, numBytes, buffer
mov ah,40h
mov bx,handle
mov cx,numBytes
lea dx,buffer
int 21h
jc ERROR_ESCRIBIR
endm

;----------------------------------------CERRAR UN ARCHIVO---------------------------------------------------------------------
cerrarF macro handle
mov ah,3eh
mov bx,handle
int 21h
jc ERROR_CERRAR
endm

;-----------------------------------LIMPIAR EL VECTOR DE RUTA PARA UNO NUEVO----------------------------------------------------
limpiarF macro buffer,numbytes,caracter
LOCAL Repetir
xor si,si
xor cx,cx
mov cx,numbytes
Repetir:
	mov buffer[si],caracter
	inc si
	Loop Repetir
endm

;----------------------------------OBTIENE EL NUMERO REAL DE ELEMENTOS EN EL ARREGLO------------------------------------------
obtenerTamanio macro buffer
LOCAL Inicio, Exit
xor si,si
xor bx,bx
Inicio:
  mov dh, buffer[si]
  cmp dh, 24h ; si es signo de $, termina de contar 
  je Exit 
  inc si  
  jmp Inicio
Exit:
endm

;--------------------------------------OBTENER HORA---------------------------------------------------------------------------
obtenerHora macro buffer
LOCAL INICIO_H, MENOR_9, MAYOR_9,MINUTOS,MENOR_9,MAYOR_9_MIN,SEGUNDOS,MENOR_9_SEG,MAYOR_9_SEG,END_
xor si,si
mov ah,2ch
int 21h
xor al,al
xor ah,ah
;ch=hora
;cl=minutos
;dh=segundos
;HORA
INICIO_H:		
	mov al,ch
	cmp al,10
	jl MENOR_9
	;sino es menor que 9
	jmp MAYOR_9

MENOR_9:
	mov buffer[si],48
	inc si
	add al,48
	mov buffer[si],al
	inc si
	mov buffer[si],58
	inc si
	JMP MINUTOS

MAYOR_9:	
	mov ah,00
	mov bl,10
	div bl
	add al,48
	add ah,48
	mov buffer[si],al
	inc si
	mov buffer[si],ah
	inc si
	mov buffer[si],58
	inc si
	JMP MINUTOS

;MINUTOS
MINUTOS:
	mov al,cl
	cmp al,10
	jl MENOR_9_MIN
	;sino es menor que 9
	jmp MAYOR_9_MIN

MENOR_9_MIN:
	mov buffer[si],48
	inc si
	add al,48
	mov buffer[si],al
	inc si
	mov buffer[si],58
	inc si
	JMP SEGUNDOS

MAYOR_9_MIN:	
	mov ah,00
	mov bl,10
	div bl
	add al,48
	add ah,48
	mov buffer[si],al
	inc si
	mov buffer[si],ah
	inc si
	mov buffer[si],58
	inc si
	JMP SEGUNDOS

;SEGUNDOS
SEGUNDOS:
	mov al,dh
	cmp al,10
	jl MENOR_9_SEG
	;sino es menor que 9
	jmp MAYOR_9_SEG

MENOR_9_SEG:
	mov buffer[si],48
	inc si
	add al,48
	mov buffer[si],al		
	JMP END_

MAYOR_9_SEG:	
	mov ah,00
	mov bl,10
	div bl
	add al,48
	add ah,48
	mov buffer[si],al
	inc si
	mov buffer[si],ah	
END_:
endm

;--------------------------------------CONVIERTE EL TEXTO A MAYUSCULAS----------------------------------------------------------
convertMayus macro bufferMayus
LOCAL CMP_INI,COMPROBAR_MENOR_Q_Z,CNV_MAYUS,END_READ, U_DIERESIS, A_TILDE, E_TILDE, I_TILDE, O_TILDE, U_TILDE, N_TILDE
xor si,si
xor bl,bl
CMP_INI:
	mov bl, bufferMayus[si]		
	cmp bl, 252 ;la ü
	je U_DIERESIS
	cmp bl, 225 ;la á
	je A_TILDE
	cmp bl, 233 ;la é
	je E_TILDE
	cmp bl, 237 ;la í
	je I_TILDE
	cmp bl, 243 ;la ó
	je O_TILDE
	cmp bl, 250 ;la ú
	je U_TILDE
	cmp bl, 241 ;la ñ
	je ENIE
	;COMPROBAR SI SON LETRAS MINUSCULAS
	cmp bl,61h; a minusculas = 97
	jge COMPROBAR_MENOR_Q_Z ;si es mayor o igual q 97 salta a Z
	cmp bl,24h; compara el fin de lectura de archivo
	je END_READ
	inc si
	jmp CMP_INI
;comprueba que la letra este en el rango de A-Z
COMPROBAR_MENOR_Q_Z:
	cmp bl,7ah     ;z minuscual = 122
	jle CNV_MAYUS  ;convierte la letra minuscula a Mayuscula
	inc si         ;sino es letra minusucla incrementa el contador y sigue
	jmp CMP_INI 
CNV_MAYUS:
	sub bl,32
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
U_DIERESIS:
	mov bl,220
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
A_TILDE:
	mov bl,193	
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
E_TILDE:
	mov bl,201
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
I_TILDE:
	mov bl,205
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
O_TILDE:
	mov bl,211
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
U_TILDE:
	mov bl,218
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
ENIE:
	mov bl,209
	mov bufferMayus[si],bl
	inc si
	jmp CMP_INI
;fin de la conversion de MAYUSCULAS
END_READ:
	mov bufferMayus[si],'$'
endm

;--------------------------------------CONVIERTE EL TEXTO A MINUSCULAS----------------------------------------------------------
convertMinus macro bufferMinus
LOCAL CMP_INI, COMPROBAR_MENOR_Q_Z, CNV_MINUS, END_READ, U_DIERESIS_MY, A_TILDE_MY, E_TILDE_MY, I_TILDE_MY, O_TILDE_MY, U_TILDE_MY, N_TILDE_MY
xor si,si
xor bl,bl
CMP_INI:
	mov bl, bufferMinus[si]		
	cmp bl, 220 ;de la Ü
	je U_DIERESIS_MY
	cmp bl, 193 ;de la Á
	je A_TILDE_MY
	cmp bl, 201 ;de la É
	je E_TILDE_MY
	cmp bl, 205 ;de la Í
	je I_TILDE_MY
	cmp bl, 211 ;de la Ó
	je O_TILDE_MY
	cmp bl, 218 ;de la Ú
	je U_TILDE_MY
	cmp bl, 209 ;de la Ñ
	je ENIE_MY
	;COMPROBAR SI SON LETRAS MAYUSCULAS
	cmp bl,41h; a MAYUSCULA = 65
	jge COMPROBAR_MENOR_Q_Z ;si es mayor o igual q 65 salta a Z
	cmp bl,24h; compara el fin de lectura de archivo
	je END_READ
	inc si
	jmp CMP_INI
;comprueba que la letra este en el rango de A-Z
COMPROBAR_MENOR_Q_Z:
	cmp bl,5ah     ;z MAYUSCULA = 90
	jle CNV_MINUS  ;convierte la letra mayuscula a minuscula
	inc si         ;sino es letra minusucla incrementa el contador y sigue
	jmp CMP_INI 
CNV_MINUS:
	add bl,32
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
U_DIERESIS_MY:
	mov bl,252
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
A_TILDE_MY:
	mov bl,225
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
E_TILDE_MY:
	mov bl,233
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
I_TILDE_MY:
	mov bl,237
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
O_TILDE_MY:
	mov bl,243
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
U_TILDE_MY:
	mov bl,250
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
ENIE_MY:
	mov bl,241
	mov bufferMinus[si],bl
	inc si
	jmp CMP_INI
;fin de la conversion de MAYUSCULAS
END_READ:
	mov bufferMinus[si],'$'
endm

;-----------------------------------------CONVIERTE A CAPITAL---------------------------------------------------------------------
convertCapital macro bufferCap
LOCAL CAPITAL_INI, CAPITAL_INI2, COMPROBAR_MENOR_Z_MAYUS, COMPROBAR_MENOR_Z_MINUS, ACEPTAR_LETRA, NO_ES_LETRA, SEGUIR_VERIFI, HACER_CAPITAL, CONVERTIR_A_MY, CONTINUAR, FIN_CAPITAL, U_DIERESIS, A_TILDE, E_TILDE, I_TILDE, O_TILDE, U_TILDE, ENIE
xor si,si
xor di,di
xor bl,bl
CAPITAL_INI:
	mov bl, bufferCap[si]
	push si
	cmp bl,252 ;ü
	je ACEPTAR_LETRA
	cmp bl,225 ;á
	je ACEPTAR_LETRA
	cmp bl,233 ;é
	je ACEPTAR_LETRA
	cmp bl,237 ;í
	je ACEPTAR_LETRA
	cmp bl,243 ;ó
	je ACEPTAR_LETRA
	cmp bl,250 ;ú
	je ACEPTAR_LETRA
	cmp bl,241 ;ñ
	je ACEPTAR_LETRA
	cmp bl,220 ;Ü
	je ACEPTAR_LETRA
	cmp bl,193 ;Á
	je ACEPTAR_LETRA
	cmp bl,201 ;É
	je ACEPTAR_LETRA
	cmp bl,205 ;Í
	je ACEPTAR_LETRA
	cmp bl,211 ;Ó
	je ACEPTAR_LETRA
	cmp bl,218 ;Ú
	je ACEPTAR_LETRA
	cmp bl,209 ;Ñ
	je ACEPTAR_LETRA
	;comprobar si es letra normal
	cmp bl,65 ; a mayuscula
	jge COMPROBAR_MENOR_Z_MAYUS	; es mayor o igual que 65	
	cmp bl,36 ; $ en decimal
	je FIN_CAPITAL
	jmp NO_ES_LETRA

COMPROBAR_MENOR_Z_MAYUS:
	cmp bl,90 ; z mayuscula
	jle ACEPTAR_LETRA ; la letra es menor o igual que 90 es maysucula
	cmp bl,97 ;a minuscula, se va con las minusculas
	jge COMPROBAR_MENOR_Z_MINUS
	;sino es letra saltar a estado donde entra cualquier cosa
	jmp NO_ES_LETRA

COMPROBAR_MENOR_Z_MINUS:
	cmp bl,122 ;z minuscula	
	jle ACEPTAR_LETRA	
	jmp NO_ES_LETRA

ACEPTAR_LETRA:
	inc si
	jmp CAPITAL_INI2

CAPITAL_INI2:
	mov bl, bufferCap[si]
	cmp bl,252 ;ü
	je ACEPTAR_LETRA
	cmp bl,225 ;á
	je ACEPTAR_LETRA
	cmp bl,233 ;é
	je ACEPTAR_LETRA
	cmp bl,237 ;í
	je ACEPTAR_LETRA
	cmp bl,243 ;ó
	je ACEPTAR_LETRA
	cmp bl,250 ;ú
	je ACEPTAR_LETRA
	cmp bl,241 ;ñ
	je ACEPTAR_LETRA
	cmp bl,220 ;Ü
	je ACEPTAR_LETRA
	cmp bl,193 ;Á
	je ACEPTAR_LETRA
	cmp bl,201 ;É
	je ACEPTAR_LETRA
	cmp bl,205 ;Í
	je ACEPTAR_LETRA
	cmp bl,211 ;Ó
	je ACEPTAR_LETRA
	cmp bl,218 ;Ú
	je ACEPTAR_LETRA
	cmp bl,209 ;Ñ
	je ACEPTAR_LETRA	
	;comprobar si es letra	
	cmp bl,65 ; a mayuscula
	jge COMPROBAR_MENOR_Z_MAYUS	; es mayor o igual que 65
	cmp bl,36 ; $ en decimal
	je HACER_CAPITAL
	jmp NO_ES_LETRA

NO_ES_LETRA:
	cmp bl,10
	je HACER_CAPITAL
	cmp bl,13
	je HACER_CAPITAL
	cmp bl,32
	je HACER_CAPITAL
	cmp bl,09
	je HACER_CAPITAL
	cmp bl,11
	je HACER_CAPITAL
	cmp bl,36 ; $ en decimal
	je FIN_CAPITAL
	mov di,si
	pop si
	mov si,di
	inc si
	jmp SEGUIR_VERIFI

SEGUIR_VERIFI:
	mov bl, bufferCap[si]
	cmp bl,10
	je HACER_CAPITAL
	cmp bl,13
	je HACER_CAPITAL
	cmp bl,32
	je HACER_CAPITAL
	cmp bl,09
	je HACER_CAPITAL
	cmp bl,11
	je HACER_CAPITAL	
	cmp bl,36 ; $ en decimal
	je FIN_CAPITAL
	inc si
	jmp SEGUIR_VERIFI

HACER_CAPITAL:
	mov di,si ;guardo la ref. actual
	pop si
	mov bl,bufferCap[si]
	cmp bl,252 ;ü
	je U_DIERESIS
	cmp bl,225 ;á
	je A_TILDE
	cmp bl,233 ;é
	je E_TILDE
	cmp bl,237 ;í
	je I_TILDE
	cmp bl,243 ;ó
	je O_TILDE
	cmp bl,250 ;ú
	je U_TILDE
	cmp bl,241 ;ñ
	je ENIE	
	cmp bl,97
	jge CONVERTIR_A_MY
	jmp CONTINUAR

U_DIERESIS:
	mov bl,220
	mov bufferCap[si],bl	
	jmp CONTINUAR
A_TILDE:
	mov bl,193	
	mov bufferCap[si],bl	
	jmp CONTINUAR
E_TILDE:
	mov bl,201
	mov bufferCap[si],bl	
	jmp CONTINUAR
I_TILDE:
	mov bl,205
	mov bufferCap[si],bl	
	jmp CONTINUAR
O_TILDE:
	mov bl,211
	mov bufferCap[si],bl	
	jmp CONTINUAR
U_TILDE:
	mov bl,218
	mov bufferCap[si],bl	
	jmp CONTINUAR
ENIE:
	mov bl,209
	mov bufferCap[si],bl	
	jmp CONTINUAR

CONVERTIR_A_MY:
	sub bl,32
	mov bufferCap[si],bl
	jmp CONTINUAR

CONTINUAR:
	mov si,di
	inc si
	jmp CAPITAL_INI
FIN_CAPITAL:
	mov bufferCap[si],'$'
endm

;---------------------------------INVERTIR PALABRAS----------------------------------------------------------------------
invertirPalabras macro bufferInvertir
LOCAL INICIO, COMPROBAR_MENOR_Z_MAYUS, COMPROBAR_MENOR_Z_MINUS, ACEPTAR_LETRA, INICIO2, NO_ES_LETRA, SEGUIR_VERIFI, OTRO_CONTINUE, INVERTIR_PALABRA, CAMBIAR_POSICIONES, VERIFICAR_RESTA, TERMINA_INTERCAMBIO, FIN_INVERTIR, ESTADO_EXTRA
xor si,si
xor di,di
xor bl,bl
INICIO:
	mov bl,bufferInvertir[si]
	push si	
	cmp bl,10
	je ESTADO_EXTRA
	cmp bl,32
	je ESTADO_EXTRA
	cmp bl,13
	je ESTADO_EXTRA
	cmp bl,09
	je ESTADO_EXTRA
	cmp bl,11
	je ESTADO_EXTRA

	cmp bl,252 ;ü
	je ACEPTAR_LETRA
	cmp bl,225 ;á
	je ACEPTAR_LETRA
	cmp bl,233 ;é
	je ACEPTAR_LETRA
	cmp bl,237 ;í
	je ACEPTAR_LETRA
	cmp bl,243 ;ó
	je ACEPTAR_LETRA
	cmp bl,250 ;ú
	je ACEPTAR_LETRA
	cmp bl,241 ;ñ
	je ACEPTAR_LETRA
	cmp bl,220 ;Ü
	je ACEPTAR_LETRA
	cmp bl,193 ;Á
	je ACEPTAR_LETRA
	cmp bl,201 ;É
	je ACEPTAR_LETRA
	cmp bl,205 ;Í
	je ACEPTAR_LETRA
	cmp bl,211 ;Ó
	je ACEPTAR_LETRA
	cmp bl,218 ;Ú
	je ACEPTAR_LETRA
	cmp bl,209 ;Ñ
	je ACEPTAR_LETRA
	cmp bl,36 ; $ en decimal
	je FIN_INVERTIR
	;comprobar si es letra	
	cmp bl,65 ; a mayuscula
	jge COMPROBAR_MENOR_Z_MAYUS	; es mayor o igual que 65
	jmp NO_ES_LETRA

COMPROBAR_MENOR_Z_MAYUS:
	cmp bl,90 ; z mayuscula
	jle ACEPTAR_LETRA ; la letra es menor o igual que 90 es maysucula
	cmp bl,97 ;a minuscula, se va con las minusculas
	jge COMPROBAR_MENOR_Z_MINUS
	;sino es letra saltar a estado donde entra cualquier cosa
	jmp NO_ES_LETRA

COMPROBAR_MENOR_Z_MINUS:
	cmp bl,122 ;z minuscula	
	jle ACEPTAR_LETRA
	jmp NO_ES_LETRA

ACEPTAR_LETRA:
	inc si
	jmp INICIO2

INICIO2:
	mov bl, bufferInvertir[si]
	cmp bl,252 ;ü
	je ACEPTAR_LETRA
	cmp bl,225 ;á
	je ACEPTAR_LETRA
	cmp bl,233 ;é
	je ACEPTAR_LETRA
	cmp bl,237 ;í
	je ACEPTAR_LETRA
	cmp bl,243 ;ó
	je ACEPTAR_LETRA
	cmp bl,250 ;ú
	je ACEPTAR_LETRA
	cmp bl,241 ;ñ
	je ACEPTAR_LETRA
	cmp bl,220 ;Ü
	je ACEPTAR_LETRA
	cmp bl,193 ;Á
	je ACEPTAR_LETRA
	cmp bl,201 ;É
	je ACEPTAR_LETRA
	cmp bl,205 ;Í
	je ACEPTAR_LETRA
	cmp bl,211 ;Ó
	je ACEPTAR_LETRA
	cmp bl,218 ;Ú
	je ACEPTAR_LETRA
	cmp bl,209 ;Ñ
	je ACEPTAR_LETRA
	cmp bl,36 ; $ en decimal
	je INVERTIR_PALABRA	
	;comprobar si es letra	
	cmp bl,65 ; a mayuscula
	jge COMPROBAR_MENOR_Z_MAYUS	; es mayor o igual que 65	
	jmp NO_ES_LETRA

NO_ES_LETRA:
	cmp bl,10 ; SALTO
	je INVERTIR_PALABRA
	cmp bl,13 ; RETORNO DE CARRO
	je INVERTIR_PALABRA
	cmp bl,32 ;ESPACIO
	je INVERTIR_PALABRA
	cmp bl,09 ; TABULADOR HORINTAL
	je INVERTIR_PALABRA
	cmp bl,11 ;TABULADOR VERTICAL
	je INVERTIR_PALABRA	
	mov di,si
	pop si
	mov si,di
	inc si
	jmp SEGUIR_VERIFI

SEGUIR_VERIFI:
	mov bl,bufferInvertir[si]
	cmp bl,10
	je OTRO_CONTINUE
	cmp bl,13
	je OTRO_CONTINUE
	cmp bl,32
	je OTRO_CONTINUE
	cmp bl,09
	je OTRO_CONTINUE
	cmp bl,11
	je OTRO_CONTINUE
	cmp bl,36 ; $ en decimal
	je FIN_INVERTIR
	inc si
	jmp SEGUIR_VERIFI
OTRO_CONTINUE:
	inc si
	jmp INICIO

ESTADO_EXTRA:
	mov di,si
	pop si
	mov si,di
	inc si
	jmp Inicio

INVERTIR_PALABRA:
	mov di,si ;GUARDA REFRENCIA ACTUAL
	pop si	 ;METE LA REF. ACTUAL A LA PILA
	push di  ; SACA DE LA PILA DONDE EMPEZO A LEER LA PALABRA
	dec di
	mov bx,si
	mov dx,di
	sub dx,bx
	cmp dx,1
	je TERMINA_INTERCAMBIO
	jmp CAMBIAR_POSICIONES

CAMBIAR_POSICIONES:
	mov bl,bufferInvertir[si]
	mov dl,bufferInvertir[di]
	mov bufferInvertir[si],dl ; hago el intercambio de letras
	mov bufferInvertir[di],bl
	inc si
	dec di
	jmp VERIFICAR_RESTA

VERIFICAR_RESTA:
	mov bx,si
	mov dx,di
	sub dx,bx
	cmp dx,1
	je TERMINA_INTERCAMBIO
	cmp dx,0
	je TERMINA_INTERCAMBIO_2
	jmp CAMBIAR_POSICIONES

TERMINA_INTERCAMBIO:
	mov bl,bufferInvertir[si]
	mov dl,bufferInvertir[di]
	mov bufferInvertir[si],dl ; hago el intercambio de letras
	mov bufferInvertir[di],bl
	pop di
	mov si,di
	inc si
	jmp INICIO

TERMINA_INTERCAMBIO_2:
	pop di
	mov si,di
	inc si
	jmp INICIO

FIN_INVERTIR:
	mov bufferInvertir[si],'$'
endm



