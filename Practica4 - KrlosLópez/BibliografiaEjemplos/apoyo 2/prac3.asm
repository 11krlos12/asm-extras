;########################################################################################################################
;------------------------------------------DECLARACION DE PROGRAMA ------------------------------------------------------
;########################################################################################################################
include aypr3.asm  ; Macro de ayuda 
.model large       ; tamaño del programa
.stack 100h        ; segmento pila

;########################################################################################################################
;------------------------------------------- SEGMENTO DE DATO -----------------------------------------------------------
;########################################################################################################################
.data
;0ah salto de linea en hexa , 0dh retorno de carro, TEXTO, $ final de cadena para assembler
spac db 0ah,0dh,' ','$' 
;--------------------------------------ENCABEZADO-----------------------------------------------------------------------
linea db 0ah,0dh, '******************************************************************','$'
encabezado db 0ah,0dh, 'UNIVERSIDA DE SAN CARLOS DE GUATEMALA',0ah,0dh,
                       'FACULTAD DE INGENIERIA',0ah,0dh,
                       'CIENCIAS Y SISTEMAS',0ah,0dh,
                       'ARQUITECTURA DE COMPUTADORAS Y ENSAMBLADORES 1',0ah,0dh,
                       'SECCION B',0ah,0dh,
                       'NOMBRE: RONALD GEOVANY ORDO',165,'EZ XILOJ',0ah,0dh,
                       'CARNET: 201314564','$'
;------------------------------------------MENU---------------------------------------------------------------------------
menu db 0ah,0dh, 'MENU PRINCIPAL',0ah,0dh,'1)  CARGAR TEXTO',0ah,0dh,'2)  A MAYUSCULA',0ah,0dh,'3)  A MINUSCULA',0ah,0dh,'4)  A CAPITAL','$'
menu2 db 0ah,0dh, '5)  INVERTIR PALABRAS',0ah,0dh,'6)  REPORTE DIPTONGOS',0ah,0dh,'7)  REPORTE TRIPTONGOS',0ah,0dh,'8)  REPORTE FINAL',0ah,0dh,'9)  SALIR',0ah,0dh,'Ingrese Numero de Opcion: ','$'
;-------------------------------------CARGAR ARCHIVO----------------------------------------------------------------------
ingresarRuta db 0ah,0dh, '********************* CARGAR ARCHIVO ********************',0ah,0dh,
                         'Ingrese Ruta: ','$'                         
exitoRuta    db 0ah,0dh, '********** !.Archivo Cargado Con Exito.! ************','$'
; guarda la ruta que ingresemos desde el teclado
vectorRuta   db 50 dup('$')        
; toma el numero de archivo que le asigna el sistema al archivo que abrimos
handlerRuta  dw ?                
; guarda todo el contenido que tiene el archivo de entrada   
bufferContenido db 5000 dup('$')

;------------------------------------HORA DEL SISTEMA--------------------------------------------------------------------
bufferHora db 8 dup('$')
bufferFecha db 10,13,'02-10-2019',10,13
;-----------------------------------ARCHIVO DE SALIDA TXT-----------------------------------------------------------------
encSalida db 'USAC - ARQUI 1 - A ',10,13,
             'Ronald Geovany Ordoñez Xiloj',10,13,
             '201314564',10,13
rutaSalida db 'Filtros.txt',00
handleSalida dw ?

;-------------------------------------CONVERTIR A MAYUSCULAS---------------------------------------------------------------
encMayusculas db 10,13,10,13,'FILTRO DE MAYUSCULAS APLICADO: ',10,13
exitoMayus db 0ah,0dh, '********* MAYUSCULAS APLICADAS EXITOSAMENTE **************','$'

;-------------------------------------CONVERTIR A MINUSCULAS---------------------------------------------------------------
encMinusculas db 10,13,10,13,'FILTRO DE MINUSCULAS APLICADO: ',10,13
exitoMinus db 0ah,0dh, '********* MINUSCULAS APLICADAS EXITOSAMENTE **************','$'

;------------------------------------CONVERTIR A LETRA CAPITAL-------------------------------------------------------------
encCapital db 10,13,10,13,'FILTRO DE CAPITAL APLICADO: ',10,13
exitoCapital db 0ah,0dh, '************ CAPITAL APLICADO EXITOSAMENTE *****************','$'

;------------------------------------------INVERTIR PALABRA----------------------------------------------------------------
encInvertir db 10,13,10,13,'FILTRO DE INVERTIR PALABRAS APLICADO: ',10,13
exitoInvertir db 0ah,0dh, '************ INVERTIR PALABRAS APLICADO EXITOSAMENTE ***************','$'

;----------------------------------------REPORTE FINAL---------------------------------------------------------------------
encReportFinal db 0ah,0dh, '********* REPORTE CREADO CON EXITO EN C:\masm611\BIN\FILTROS.TXT ********','$'

;----------------------------------------DIPTONGOS-------------------------------------------------------------------------
encDiptongos db 0ah,0dh, '****** REPORTE DE DIPTONGOS CREADO EN C:\masm611\BIN\DIPTONGOS.HTML ******','$'
bufferDiptongos db 5000 dup('$')
rutaSalidaDip db 'DIPTONGOS.html',00
handleDiptongo dw ?
plantillaHTML db '<!DOCTYPE html>',10,13,
					'<html>',10,13,
					'<head>',10,13,09,
					'<title>Reporte Diptongos</title>',10,13,
					'</head>',10,13,
					'<body>',10,13,09,
                    '<h4 style="text-align: center',59,'"> USAC - ARQUI1 - B </h4>',10,13,09,
                    '<p  style="text-align: center',59,'"> Ronald G. Ordoñez X.</h4>',10,13,09,
                    '<p  style="text-align: center',59,'"> 201314564</h4>',10,13,09
plantillaHTML2 db '<table style="margin: 0 auto',59,'" border="1">',10,13,09,09,
					'<tr>',10,13,09,09,09,'<th>DIPTONGOS</th>',10,13,09,09,
					'</tr>',10,13,09,09,                    
					'<tr>',10,13,09,09,09,
					'<th>'
plantillaHTML3 db '</th>',10,13,09,09,
					'</tr>',10,13,09,
					'</table>',10,13,
					'</body>',10,13,
					'</html>'

;--------------------------------------Errores-----------------------------------------------------------------------------
errAbrir    db 0ah,0dh, '********** !.Error al ABRIR archivo.! ***********' ,'$'
errLeer     db 0ah,0dh, '********** !.Error al LEER archivo.! ***********'  ,'$'
errCrear    db 0ah,0dh, '********** !.Error al CREAR archivo.! **********' ,'$'
errEscribir db 0ah,0dh, '********** !.Error al ESCRIBIR archivo.! ***********','$'
errCerrar   db 0ah,0dh, '********** !.Error al CERRAR archivo.! **********','$'


;###############################################################################################################################
;--------------------------------------------- SEGMENTO DE CODIGO --------------------------------------------------------------
;###############################################################################################################################
.code
main proc
    ;-------------------------------------------------------IMPRIME EL ENCABEZADO-----------------------------------------------
    ENCABEZADO_:
        print spac ;imprime un espacio
        print linea ;imprime una linea de ****
        print encabezado ;imprime el encabezado       
        print linea ;imprime una linea de ***       
        jmp MENU_
    ;-------------------------------------------------------IMPRIME EL MENU-----------------------------------------------------
    MENU_:
        print spac ;imprime espacio
        print menu ;imprime el menu
        print menu2
        getChar ; obtiene el char que tecleamos
        cmp al,'1'
        je CARGAR_TEXTO
        cmp al,'2'
        je CONVERT_MAYUS
        cmp al,'3'
        je CONVERT_MINUS
        cmp al,'4'
        je CONVERT_CAPITAL
        cmp al,'5'
        je INVERTIR_PALABRAS
        cmp al,'6'
        je REPORTE_DIPTONGO
        cmp al,'7'
        je REPORTE_TRIPTONGO
        cmp al,'8'
        je REPORTE_FINAL
        cmp al,'9'
        je SALIR
        jmp MENU_

    ;---------------------------------------------------CARGA DE ARCHIVO DE ENTRADA------------------------------------------------
    CARGAR_TEXTO:
        print spac
        limpiarF vectorRuta, SIZEOF vectorRuta, 24h   
        limpiarF bufferContenido, SIZEOF bufferContenido, 24h
        print ingresarRuta
        getRuta vectorRuta ; mando el vector que quiero llenar con el nombre del archivo a abrir
        abrirF vectorRuta, handlerRuta ;mando el contenido del vector y un handler para asiganr el numero de archivo que abrirá
        leerF SIZEOF bufferContenido, bufferContenido, handlerRuta             
        print exitoRuta
        cerrarF handlerRuta
        print spac
        ;print bufferContenido
        ;1)crear reporte
        crearF rutaSalida, handleSalida
        escribirF handleSalida, SIZEOF encSalida, encSalida
        escribirF handleSalida,SIZEOF bufferFecha,bufferFecha
        obtenerHora bufferHora
        escribirF handleSalida, SIZEOF bufferHora, bufferHora
        jmp MENU_
    ;-----------------------------------------------CONVIERTE TODO EL TEXTO A MAYUSCULAS-------------------------------------------
    CONVERT_MAYUS:
        print spac
        ;manda el buffer para que convierta todo lo que tiene dentro en MAYUSCULAS
        convertMayus bufferContenido
        ;1)crear reporte
        ;crearF rutaSalida, handleSalida
        ;2)escribir en el reporte     
        escribirF handleSalida, SIZEOF encMayusculas, encMayusculas   ; escribe un encabezado de las mayusculas
        obtenerTamanio bufferContenido
        escribirF handleSalida, si, bufferContenido  ; escribe lo del buffer en el archivo de txt
        ;cerrarF handleSalida
        print exitoMayus
        jmp MENU_
    ;-----------------------------------------------CONVIERTE TODO EL TEXTO A MINUSCULAS-------------------------------------------
    CONVERT_MINUS:
        print spac
        ;manda el buffer para que convierta todo lo que tiene dentro en minusculas
        convertMinus bufferContenido
        ;1)crear reporte
        ;crearF rutaSalida, handleSalida
        ;2)escribir en el reporte  
        escribirF handleSalida, SIZEOF encMinusculas, encMinusculas      
        obtenerTamanio bufferContenido
        escribirF handleSalida, si, bufferContenido
        ;cerrarF handleSalida
        print exitoMinus
        jmp MENU_

    ;------------------------------------------CONVIERTE LA PRIMERA LETRA DE UNA PALABRA EN MAYUSCULA-------------------------------
    CONVERT_CAPITAL:
        print spac
        convertCapital bufferContenido
        escribirF handleSalida, SIZEOF encCapital, encCapital
        obtenerTamanio bufferContenido
        escribirF handleSalida, si, bufferContenido
        print exitoCapital
        jmp MENU_

    ;------------------------------------------BUSCA UNA PALABRA Y LA REEMPLAZA POR OTRA INGRESADA----------------------------------
    BUSCAR_Y_REEMPLAZAR:
		jmp SALIR	     
		
    ;-----------------------------------------INVIERTE TODAS LAS PALABRAS DEL TEXTO COMO UN ESPEJO----------------------------------
    INVERTIR_PALABRAS:
        print spac
        invertirPalabras bufferContenido
        escribirF handleSalida, SIZEOF encInvertir, encInvertir
        obtenerTamanio bufferContenido
        escribirF handleSalida, si, bufferContenido
        print exitoInvertir
        jmp MENU_ 

    ;----------------------------------------MANDA EN UN REPORTE TODAS LAS PALABRAS QUE SON DIPTONGO--------------------------------
    REPORTE_DIPTONGO:
        print spac
        ;cerrar el archivo de texto antes de crear un archivo HTML
        ;1)crear el reporte
        crearF rutaSalidaDip, handleDiptongo
        ;2)escribir en el reporte
        escribirF handleDiptongo, SIZEOF plantillaHTML, plantillaHTML
        escribirF handleDiptongo, SIZEOF plantillaHTML2, plantillaHTML2
        escribirF handleDiptongo, SIZEOF plantillaHTML3, plantillaHTML3
        cerrarF handleDiptongo
        print encDiptongos
        jmp MENU_

    ;----------------------------------------MANDA EN UN REPORTE TODAS LAS PALABRAS QUE SON HIATOS----------------------------------
    REPORTE_HIATOS:
        ;cerrar el archivo de texto antes de crear un archivo HTML
        jmp SALIR

    ;----------------------------------------MANDA EN UN REPORTE TODAS LAS PALABRAS QUE SON TRIPTONGO--------------------------------
    REPORTE_TRIPTONGO:
        ;cerrar el archivo de texto antes de crear un archivo HTML
        jmp SALIR  

    REPORTE_FINAL:
        print spac
        cerrarF handleSalida
        print encReportFinal
        jmp MENU_

        ;cerrar el archivo handleSalida  

    ;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ;/////////////////////////////////////////////SECCION DE ERRORES////////////////////////////////////////////////////////////////
    ;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ;-------------------------------------------ERROR AL ABRIR ARCHIVO---------------------------------------------------------------
    ERROR_ABRIR:
	    print errAbrir
        jmp MENU_	    	
    ;-------------------------------------------ERROR AL LEER ARCHIVO---------------------------------------------------------------
    ERROR_LEER:
        print errLeer
	    jmp MENU_
    ;-------------------------------------------ERROR AL CREAR ARCHIVO---------------------------------------------------------------
	ERROR_CREAR:
        print errCrear
	    jmp MENU_	    
    ;-------------------------------------------ERROR AL ESCRIBIR ARCHIVO------------------------------------------------------------
    ERROR_ESCRIBIR:
	    print errEscribir
	    jmp MENU_
    ;-------------------------------------------ERROR AL CERRAR ARCHIVO--------------------------------------------------------------
    ERROR_CERRAR:
	    print errCerrar
	    JMP MENU_
    ;--------------------------------------------------------SALIR -------------------------------------------------------------------
    SALIR:
       mov ah, 4ch  
       int 21h
main endp
;#######################################################################################################################################
;##################################################### FIN DEL PROGRAMA ################################################################
;#######################################################################################################################################
end 