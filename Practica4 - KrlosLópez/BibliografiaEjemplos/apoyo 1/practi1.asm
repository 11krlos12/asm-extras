print macro mensaje
push ax
push dx
xor ax,ax
xor dx,dx
	mov ax,@data ;hmm ¿seg? 
	mov ds,ax ;ds = ax = saludo 
	mov ah,09 ;Function(print string) 
	lea dx,mensaje ;DX = String terminated by "$" 
	int 21h ;Interruptions DOS Functions 
pop dx
pop ax
endm

.model small 
.stack 
;======================================================================|
;							D 	A 	T 	A 		    		   		   |
;======================================================================|	
.data 

encab db "Universidad de San Carlos de Guatemala",0Dh,0Ah,"Facultad de Ingenieria",0Dh,0Ah,"Escuela de Ciencias y Sistemas",0Dh,0Ah,"Arquitectura de Computadores y Ensambladores 1 - A",0Dh,0Ah,"Primer semestre 2018",0Dh,0Ah,"Mynor Styven Sinay Alvarez",0Dh,0Ah,"201403520",0Dh,0Ah,"Practica no. 1",0Dh,0Ah,"$";adena de caracteres ASCII
ent db 0Dh,0Ah,"	<< Presione ENTER >>","$"
opciones1  db 0Dh,0Ah,"		Menu principal",0Dh,0Ah," ",0Dh,0Ah,"1. Cargar Archivo",0Dh,0Ah,"2. Modo Calculadora",0Dh,0Ah,"3. Factorial",0Dh,0Ah,"4. Reporte",0Dh,0Ah,"5. Salir",0Dh,0Ah,"$"
op db 0Dh,0Ah,"Ingrese alguna opcion: ","$"
opciones2  db 10,"Ingrese el nombre del archivo:",0Dh,0Ah,"$"
cadena_en db 50 dup("$")
err_cad db "Error al ingresar el nombre del archivo, pruebe de nuevo", "$" 
handle dw ?
num db 10,"Numero: ","$"
arit db 10,"Operador aritmetico: ","$"
resu db 10,"Resultado: ","$"
salir db 0Dh,0Ah,"Desea salir de la aplicacion?",10,"1. Si",10,"2. No",10,"$"
fact db "Factorial: ","$"
oper db 10,"Operaciones: ",10,"$"
l0 db "0!=1;","$"
l1 db "1!=1;","$"
l2 db "2!=1*2=2;","$"
l3 db "3!=1*2*3=6;","$"
l4 db "4!=1*2*3*4=24;","$"
l5 db "5!=1*2*3*4*5=120;","$"
l6 db "6!=1*2*3*4*5*6=720;","$"
l7 db "7!=1*2*3*4*5*6*7=5040;","$"
f0 db "1","$"
f2 db "2","$"
f3 db "6","$"
f4 db "24","$"
f5 db "120","$"
f6 db "720","$"
f7 db "5040","$"
op_menu1 db 0Dh,0Ah,"	Menu de operaciones",0Dh,0Ah," ",0Dh,0Ah,"1. Resultado",0Dh,0Ah,"2. Salir",0Dh,0Ah,"$"
num1 db 5 dup("$")
num2 db 5 dup("$")
num3 db 2 dup("$")
opar db ?
dec1 dw ?
res dw ?
entr db 10,"Entrada: ","$"
dec2 dw ?
dec3 dw ?
ans dw ?
red_1 db 1 dup (" $")
dec22 db ?

hol db "Hola","$"
adi db "Adios","$"

temporal db "24",10,"$"
temporal2 db "64",10,"$"


handler dw ?
entrada db "C:\reporte.arq$",0
file db "C:\hola.txt$",0h
saludo db "tHola mundo!!!$"
erra db "No se encontro el archivo", "$" 
errarch db "Error en el archivo: se encontro:  ", "$" 
errarch1 db "Se encontro: ", "$" 
c_arch db "++$"
c_arch2 db "+num+$"

varte dw "$"

mensaje_split db "Numeros Palindromos: ",0dh,0ah,"$"
ArrayInfo db 100 dup('$')

cadena_en2 dd 2 dup("$")
cadena_man db 50 dup("$")
cadena_man2 db ?
linea db 10,13,'$'

lineag db 10,13,'-$'
lineags db '*$'

lista_todo db 50 dup('-$')
par db "$"
palab db "$"

cadena_p1 db 300 dup("$")
separ db "----------------------------$"
listapal db 200 dup("-----$")
pp1 dw ?
espejo_0 db 300 dup('$')
espejo_i db 100 dup('     $')
split_1 db 100 dup('$')
split_2 db 100 dup ('     $')

pc1 db ?
pc2 db ?
pc11 db 1 dup (" $")
pc12 db 1 dup (" $")
inicia_cad db "%$"
numero_pos dw ?

mensaje_espejo db  0Dh,0Ah,"Numeros espejo: ",0Dh,0Ah,"$"
mensaje_par db  0Dh,0Ah,"Numeros pares: ",0Dh,0Ah,"$"
mensaje_impar db  0Dh,0Ah,"Numeros impares: ",0Dh,0Ah,"$"
mensaje_pal db  "Numeros split: ",0Dh,0Ah,"$"
cont db 0

nmi dw ?
prueba dw 100  dup (?)
lista_splitf db 100 dup ("     $")
cont_lp dw 0
num_t dw ?

;======================================================================|
;							C   O   D   E		    		   		   |
;======================================================================|	
.code
main proc ;Inicia proceso 
	xor ax, ax
	xor dx, dx
	xor ah, ah
	xor si,si
	call limpiar
	call encabezado

limpiar:
	mov ah, 00h
	mov al, 03h
	int 10h
	ret

encabezado:
	print encab 		; colocar direccion de cadena en DX
	print ent
	mov ah, 08h			;Entrada de caracter sin salida
	int 21h				
	jmp menup

menup:	
	call limpiar
	print opciones1 	; colocar direccion de cadena en DX
	print op
	call Rutina1		;Para hacer un entrada de caracter con salida
	cmp al, 31h
	je Opcion1
	cmp al, 32h
	je calc1
	cmp al, 33h
	je fac
	;cmp al, 34h
	cmp al, 35h
	je Salida
	call limpiar
	jmp encabezado

calc1:
	xor si,si
	call limpiar
	print num
	For1:
		call Rutina1
		cmp al, 0dh			;mira si es enter en hexa
		je calc2 			;Si fuera igual a enter, se va a esta etiqueta
		mov num1[si],al	;meter al arreglo segun la posicion que este en el si, desde el al
		inc si		
		jmp For1 
	ret

calc2:
	print arit
	call Rutina1
	mov opar,al
	jmp calc3
	ret

calc3:
	xor di,di
	print num
	For2:
		call Rutina1
		cmp al, 0dh			;mira si es enter en hexa
		je calc4 			;Si fuera igual a enter, se va a esta etiqueta
		mov num2[di],al	;meter al arreglo segun la posicion que este en el di, desde el al
		inc di		
		jmp For2 
	ret

ne1:
	xor bx,bx			;Siguientes lineas para el primer numero
	xor ax,ax
	mov bl,num1[2]
	mov red_1[0],bl
	mov bl,red_1[0]	
	sub bl,30h

	mov al,num1[1]
	mov red_1[0],al
	mov al,red_1[0]	
	sub al,30h
	mov dl,10
	mul dl				;Multiplica el dx por el ax
	add al,bl			;Ya solo suma la pos 0 con la pos 1 del arreglo
	cbw
	neg AX 				;Complemento a 2 del AL
	mov dec1,ax			;El valor en decimal
	jmp calc41

ne2:
	xor bx,bx			;Siguientes lineas para el segundo numero
	xor ax,ax 			
	xor dx,dx
	mov bl,num2[2]
	mov red_1[0],bl
	mov bl,red_1[0]		
	sub bl,30h
	mov al,num2[1]
	mov red_1[0],al
	mov al,red_1[0]	
	sub al,30h
	mov dl,10
	mul dl				;Multiplica el dx por el ax, para la pos 1
	add al,bl			;Ya solo suma la pos 0 con la pos 1 del arreglo
	neg al
	mov dec22,al
	cbw
	;neg AX 				;Complemento a 2 del AL
	mov dec2,ax			;El valor en decimal
	jmp opera

opera:
	cmp opar,2bh		;+
	je suma
	cmp opar,2dh		;-
	je resta
	cmp opar,2fh		;/
	je divi
	cmp opar,2ah		;*
	je mult

ans1:
	mov ax,ans
	mov dec1,ax
	jmp calc41

ans2:
	mov ax,ans
	mov dec2,ax
	jmp opera

calc4:
	cmp num1[0],2dh
	je ne1
	cmp num1[0],41h
	je ans1

	xor bx,bx			;Siguientes lineas para el primer numero
	xor ax,ax
	mov bl,num1[1]
	mov red_1[0],bl
	mov bl,red_1[0]	
	sub bl,30h

	mov al,num1[0]
	mov red_1[0],al
	mov al,red_1[0]	
	sub al,30h
	mov dl,10
	mul dl				;Multiplica el dx por el ax
	add al,bl			;Ya solo suma la pos 0 con la pos 1 del arreglo
	cbw
	mov dec1,ax			;El valor en decimal
	jmp calc41

calc41:
	cmp num2[0],2dh
	je ne2
	cmp num1[0],41h
	je ans2

	xor bx,bx			;Siguientes lineas para el segundo numero
	xor ax,ax 			
	xor dx,dx
	mov bl,num2[1]
	mov red_1[0],bl
	mov bl,red_1[0]		
	sub bl,30h
	mov al,num2[0]
	mov red_1[0],al
	mov al,red_1[0]	
	sub al,30h
	mov dl,10
	mul dl				;Multiplica el dx por el ax, para la pos 1
	add al,bl			;Ya solo suma la pos 0 con la pos 1 del arreglo
	mov dec22,al
	cbw
	mov dec2,ax			;El valor en decimal
	jmp opera
	
suma:
	;cbw					;Convierte a word el valor 2
	;mov dec3,AX 		;pasa el nuevo valor 2 en word a la variable dec3
	;mov al,dec1			;paso el valor dec1 al AL
	;cbw					;Convierte byte a word
	add ax,dec1			;Suma el valor 1 con valor 2

	test ax,ax
	js nesuma
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma
	mov num3[0],20h		;ya que es positivo, le ponemos espacio en blanco
	jmp patodos

nesuma:
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	neg ax
	;mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma
	mov num3[0],2dh
	jmp patodos

resta:
	xor ax,ax
	mov ax,dec1			;Ponemos el primer valor en el AL
	sub ax,dec2			;resta el valor 1 con valor 2
	;cbw					;Convierte byte a word
	test ax,AX 			;compara entre el mismo registro
	js neresta			;Se activo la bandera del signo, o sea, es negativo
	jns posresta		;positivo

neresta:
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	neg ax
	;mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma
	mov num3[0],2dh
	jmp patodos

posresta:
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma
	mov num3[0],20h		;ya que es positivo, le ponemos espacio en blanco
	jmp patodos

patodos:
	mov di,4	
	call com_num
	dec di
	mov res,ax
	call com_num
	dec di
	mov res,ax
	call com_num
	dec di
	mov res,ax
	call com_num

	print resu
	print num3			;Valor del resultado en hexa			
	print salir
	call Rutina1
	cmp al, 31h
	je menup
	cmp al, 32h
	je calc1
	jmp Salida

mult:
	;cbw					;Convierte a word el valor 2
	;mov dec3,AX 		;pasa el nuevo valor 2 en word a la variable dec3
	;mov al,dec1			;paso el valor dec1 al AL
	;cbw					;Convierte byte a word
	imul dec1			;mult el valor 1 con valor 2
	test ax,AX 			;compara entre el mismo registro
	js nemult			;Se activo la bandera del signo, o sea, es negativo
	jns posmult			;positivo

nemult:
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	neg ax
	;mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma
	mov num3[0],2dh
	jmp patodos

posmult:
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma
	mov num3[0],20h		;ya que es positivo, le ponemos espacio en blanco
	jmp patodos

divi:
	xor ax,ax
	;mov dx,0
	mov ax,dec1			;Ponemos el primer valor en el AL
	;cbw				;Convierte el valor del primer numero que esta en 8 bits a 16 bits
	idiv dec22			;divide el valor 1 de 16 bits dentro del valor 2 de 8 bits
	cbw					;Convierte byte a word el resultado
	test ax,ax 			;compara entre el mismo registro
	js nediv			;Se activo la bandera del signo, o sea, es negativo
	jns posdiv			;positivo

nediv:
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	neg ax
	;mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma
	mov num3[0],2dh
	jmp patodos

posdiv:
	mov ans,AX 			;guardamos el resultado en la var. que podemos volver a utilizar
	mov res,AX 			;pasa a la var. res el valor de la suma	
	mov num3[0],20h		;ya que es positivo, le ponemos espacio en blanco
	jmp patodos

com_num:
	xor ax,ax
	xor bx,bx
	mov ax,res
	mov bx,10
	xor dx, dx
	div bx

	cmp dx,0
	jne n0
	mov num3[di],30h
	jmp finc

n0:
	cmp dx,1
	jne n1
	mov num3[di],31h
	jmp finc
n1:
	cmp dx,2
	jne n2
	mov num3[di],32h
	jmp finc
n2:
	cmp dx,3
	jne n3
	mov num3[di],33h
	jmp finc
n3:
	cmp dx,4
	jne n4
	mov num3[di],34h
	jmp finc
n4:
	cmp dx,5
	jne n5
	mov num3[di],35h
	jmp finc
n5:
	cmp dx,6
	jne n6
	mov num3[di],36h
	jmp finc
n6:
	cmp dx,7
	jne n7
	mov num3[di],37h
	jmp finc
n7:
	cmp dx,8
	jne n8
	mov num3[di],38h
	jmp finc
n8:
	;cmp dx,9
	;jne n9
	mov num3[di],39h
	jmp finc
ret

finc:
	ret

fac:
	xor di,di
	call limpiar
	print fact 			;Cuando pide el no. Factorial
	call Rutina1
	cmp al,30h
	je fa0
	cmp al,31h
	je fa1
	cmp al,32h
	je fa2
	cmp al,33h
	je fa3
	cmp al,34h
	je fa4
	cmp al,35h
	je fa5
	cmp al,36h
	je fa6
	cmp al,37h
	je fa7
	;cmp al,38h
	;je fa8
	ret

fa0:
	print oper
	print l0
	print resu
	print f0
	call espera
	jmp menup
	ret
fa1:
	print oper
	print l0
	print l1
	print resu
	print f0
	call espera
	jmp menup
	ret
fa2:
	print oper
	print l0
	print l1
	print l2
	print resu
	print f2
	call espera
	jmp menup
	ret
fa3:
	print oper
	print l0
	print l1
	print l2
	print l3
	print resu
	print f3
	call espera
	jmp menup
	ret
fa4:
	print oper
	print l0
	print l1
	print l2
	print l3
	print l4
	print resu
	print f4
	call espera
	jmp menup
	ret
fa5:
	print oper
	print l0
	print l1
	print l2
	print l3
	print l4
	print l5
	print resu
	print f5
	call espera
	jmp menup
	ret
fa6:
	print oper
	print l0
	print l1
	print l2
	print l3
	print l4
	print l5
	print l6
	print resu
	print f6
	call espera
	jmp menup
	ret
fa7:
	print oper
	print l0
	print l1
	print l2
	print l3
	print l4
	print l5
	print l6
	print l7
	print resu
	print f7
	call espera
	jmp menup
	ret


espera:
	mov ah, 08h			;Entrada de caracter sin salida
	int 21h	
	ret

repo:
	ret

Rutina1:
	mov ah, 01h			;asignar al reg AX
	int 21h
	ret
	
Salida:
	mov ah, 4ch			; funcion 4C, finalizar ejecucion
	int 21h				; interrupcion DOS

guardar_cad:
  call Rutina1
  mov cadena_en[si],al	;Lo que se registro en AL se pone por partes en el arreglo
  ADD SI, 2
  cmp al,0dh			;Mira si es enter
  ja guardar_cad
  jb guardar_cad
  ret

error_cadena:
	print err_cad
	mov ah, 08h			;Entrada de caracter sin salida
	int 21h				
	jmp Opcion1

;ingresar direccion
Opcion1:
	xor si,si
	xor di,di
	call limpiar
	print opciones2 	;Imprime el que ingrese el nombre
	call guardar_cad 	;Lo que el user ponga, se va a guardar acá
	cmp  cadena_en,26h	;Mira si es &
	jne error_cadena
	cmp  cadena_en[2],26h
	jne error_cadena

	mov di, 0 
	mov cx,si

	mov si,4
		;print cadena_en[si]
	CICLO:
		cmp cadena_en[si],26h
		je solo_sumar
		cmp  cadena_en[si],20h	;Si fuera espacio en blanco
		je solo_sumar
		mov al,cadena_en[si]	;Mete en AL la pos actual del arreglo
		add si,2 
		mov cadena_man[di], al	;Lo que estaba en AL lo mete en el arreglo ya más bonito haha
		solo_sumar:
			inc di	 
;ah y DH
		LOOP CICLO
	mov cadena_man[di-11],"$"
	jmp abrir_arch
	ret

error_abrir: 
	print err_cad
	mov ah, 08h			;asignar al reg AX
	int 21h				;estas 3 lineas esperan el enter
	jmp Opcion1

abrir_arch:
	mov ah,3dh		;Instrucción para abrir el archivo
	mov al,0h 		;0h solo lectura, 1h solo escritura, 2 lectura y escritura
	mov dx,offset cadena_man;Abre el archivo ya con su nombre
	int 21h
	MOV HANDLE,AX   
	jc error_abrir ;Si hubo error

	mov ah,3fh		;Instrucción para leer el archivo
	mov bx,HANDLE
	mov cx,200		;Lo que se va a leer del archivo
	mov dx,offset lista_todo;El contendio del archivo
	int 21h
	jc error_abrir ;Si hubo error

	mov ah,3eh		;Cierre de archivo
	int 21h
	jmp tam_cad_m

tam_cad_m:
	xor si,si
	xor di,di
	mov cx,300

		CICLO1:
			cmp lista_todo[si],3bh;Punto y coma
			jne solo_sumar1

			mov di,si
			mov cx,1;casi terminar
			jmp sc
			solo_sumar1:
				inc si
			sc:
				LOOP CICLO1

	mov cx,si
	xor di,di
	xor al,al

	add di,2
			mov al,inicia_cad
			mov cadena_p1[di],al

		CICLO11:
		xor al,al
			mov al,lista_todo[si]
			mov cadena_p1[di],al
			;print lineags
			;print cadena_p1[di]
			sub si,1  
			add di,2
			LOOP CICLO11

			mov al,lista_todo[si]
			mov cadena_p1[di],al

	mov cx,70

	mov si,di
		CICLO111:
		xor al,al
			  ;	print cadena_p1[di]
			   ;	print lineags
				sub di,2
				cmp cadena_p1[di],2bh
				jne sigg
				cmp cadena_p1[di-2],2bh
				je error_sint
				sigg:

				cmp cadena_p1[di],3bh
				jne CICLO111

		mov di,si
		inc di
		inc di
		mov al,inicia_cad
		mov cadena_p1[di],al 
				inc di
		inc di

		xor si,si
	  	xor bx,bx

		CICLO33:
		mov num_t ,di
		xor al,al
				cmp cadena_p1[si],2bh
				je sigf

				cmp cadena_p1[si],3bh
				je sigf

				cmp cadena_p1[si],25h
				je sigf
			  	;print cadena_p1[si]
			  	xor al,al 
			  	mov al,cadena_p1[si]
			  	mov espejo_0[bx],al
			  	;print lineags
			  	;print cadena_p1[si]
			  	add bx,2

				sigf: 
				add si,2

				cmp si,di
				jne CICLO33
				;print linea
				;print saludo
;4544ddd

		mov al, inicia_cad
		mov espejo_0[bx],al

		xor si,si
		mov cx,bx
		xor bx,bx
		add si,2

		;mov si,cx

		xor di,di
		CICLO33b:

		xor al,al

				cmp espejo_0[si],0Ah
				je ccf

				cmp espejo_0[si],0Dh
				je ccf

				cmp espejo_0[si],25h
				je ccf



			  	xor bl,bl
				mov bl,espejo_0[si]
				mov espejo_i[di],bl
				;print lineags
				;print espejo_i[di]
			  	
				add di,1

				cmp espejo_0[si+2],0Ah
				je cc1
				cmp si,cx
				je fin_c
				add si,2
				;it1

			  	xor bl,bl
				mov bl,espejo_0[si]
				mov espejo_i[di],bl
				;print lineags
				;print espejo_i[di]
				inc di

				cmp espejo_0[si+2],0Ah
				je cc2
				cmp espejo_0[si+2],25h
				je cc2
				cmp si,cx
				je fin_c
				add si,2

				;it2 hast
			  	xor bl,bl
				mov bl,espejo_0[si]
				mov espejo_i[di],bl
				;print lineags
				;print espejo_i[di]
				inc di

				cmp espejo_0[si+2],0Ah
				je cc3
				cmp espejo_0[si+2],25h
				je cc3

				cmp si,cx
				je fin_c
				add si,2

				;it3
			  	xor bl,bl
				mov bl,espejo_0[si]
				mov espejo_i[di],bl
				;print lineags
				;print espejo_i[di]
				inc di

				cmp espejo_0[si+2],0Ah
				je cc4
				cmp espejo_0[si+2],25h
				je cc4
				cmp si,cx
				je fin_c
				add si,2


				;it4
			  	xor bl,bl
				mov bl,espejo_0[si]
				mov espejo_i[di],bl
				;print lineags
				;print espejo_i[di]
				inc di

				cmp espejo_0[si+2],0Ah
				je cc5
				cmp espejo_0[si+2],25h
				je cc5
				cmp si,cx
				je fin_c
				add si,2




				;print saludo
				cc1:
				add di,5
				jmp ccf

				cc2:
				add di,4

				jmp ccf

				cc3:
				add di,3

				jmp ccf

				cc4:
				add di,2

				jmp ccf

				cc5:
				add di,1

				jmp ccf


				ccf:

				add si,2

				;cmp si,di
				cmp si ,cx
				jne CICLO33b

		fin_c:
		;print saludo
		;print linea
		mov numero_pos,di

	call cad_ad
	jmp menu_ac

Aresultado:
	call limpiar
	print entr
	print lista_todo
	print resu
	print temporal
	call espera

menu_ac:
	;call limpiar
	print op_menu1
	print op
	call Rutina1
	cmp al,31h   ;Resultado
	je Aresultado
	cmp al, 32h
	je menup 
	call limpiar
	jmp menu_ac


m_impar:
print mensaje_impar
mov si,numero_pos

mov di,0
	m_ppi:
		sub si,6
		add di,6
		
		mov al,espejo_i[si] 
		mov pc11[0],al

		mov bl,pc11[0]
		sub bl,48

		mov cl,bl
		mtem12i:
		
			;print lineags
			
			cmp cl,1
			jne sigti
			print split_2[di]
			print linea
			jmp sssi

			cmp cl,0
			jne sigti
			;print espejo_i[si]
			jmp sssi

			sigti:

			sub cl,2

			cmp cl,0
			jne mtem12i
		sssi:


		cmp si,0
		jne m_ppi
		jmp menu_ac



m_par:
		
print mensaje_par
mov si,numero_pos
mov di,0
	m_pp:
		sub si,6
		add di,6

		mov al,espejo_i[si] 
		mov pc11[0],al

		mov bl,pc11[0]
		sub bl,48

		mov cl,bl
		mtem12:
		
			;print lineags
			
			cmp cl,0
			jne sigt
			print split_2[di]
			print linea
			jmp sss

			cmp cl,1
			jne sigt
			;print espejo_i[si]
			jmp sss

			sigt:

			sub cl,2

			cmp cl,1
			jne mtem12
		sss:



		cmp si,0
		jne m_pp
jmp menu_ac
m_palin:
xor si,si

print linea
print mensaje_pal


	m_pi:	
		;print linea
		;print split_2[si]
		mov di,0
		;iteracion 1
		mov al,split_2[si] 
		mov pc11[0],al

		;print split_2[si] 

		cmp pc11[0],20h
		je sih1


		inc di

		;iteracion 2
		mov al,split_2[si+1] 
		mov pc11[0],al

		cmp pc11[0],20h
		je sih1

				cmp pc11[0],25h
		je sih1
		inc di

		;iteracion 3
		mov al,split_2[si+2] 
		mov pc11[0],al

		cmp pc11[0],20h
		je sih1
		inc di

		;iteracion 4
		mov al,split_2[si+3] 
		mov pc11[0],al

		cmp pc11[0],20h
		je sih1
		inc di

		;iteracion 4
		mov al,split_2[si+4] 
		mov pc11[0],al

		cmp pc11[0],20h
		je sih1
		inc di

		sih1:
		;mov cx,di
		;print linea
		;mtem:
		;	print lineags
		;	LOOP mtem
		;print linea	

		;cmp al,bl
		cmp di,1
		jne pro1
		call prod1
		pro1:

		cmp di,2
		jne pro2
		call prod2
		pro2:

		cmp di,3
		jne pro3
		call prod3
		pro3:

		cmp di,4
		jne pro4
		call prod4
		pro4:

		cmp di,5
		jne pro5
		call prod5
		pro5:

		add si,6
		cmp si,numero_pos
		jne m_pi
jmp menu_ac		

prod5:

	mov al,split_2[si] 
	mov pc11[0],al

	mov al,split_2[si+1] 
	mov pc12[0],al


	mov cl,pc11[0]
	add cl, pc12[0]
	sub cl,96

	mov bl,cl 


	mov al,split_2[si+3] 
	mov pc11[0],al

	mov al,split_2[si+4] 
	mov pc12[0],al


	mov cl,pc11[0]
	add cl, pc12[0]
	sub cl,96
	
	cmp bl,cl
	jne fin1245	

	print split_2[si] 
	print linea
	fin1245:

ret


prod4:

	mov al,split_2[si] 
	mov pc11[0],al

	mov al,split_2[si+1] 
	mov pc12[0],al


	mov cl,pc11[0]
	add cl, pc12[0]
	sub cl,96

	mov bl,cl 


	mov al,split_2[si+2] 
	mov pc11[0],al

	mov al,split_2[si+3] 
	mov pc12[0],al


	mov cl,pc11[0]
	add cl, pc12[0]
	sub cl,96
	
	cmp bl,cl
	jne fin124	

	print split_2[si] 
	print linea
	fin124:

ret

prod3:

mov al,split_2[si] 
	mov pc11[0],al

	mov al,split_2[si+2] 
	mov pc12[0],al

	mov al,pc12[0]
	;print pc12[0]
	;print pc11[0]
	cmp pc11[0],al
	jne fin12
	print split_2[si] 
	print linea

	fin12:
	
ret

prod2:
	mov al,split_2[si] 
	mov pc11[0],al

	mov al,split_2[si+1] 
	mov pc12[0],al

	mov al,pc12[0]
	;print pc12[0]
	;print pc11[0]
	cmp pc11[0],al
	jne fin1
	print split_2[si] 
	print linea

	fin1:
ret
prod1:
print split_2[si] 
print linea
ret


m_split:
       ;call cad_ad
       print linea 
       print mensaje_split
		xor si,si
		mov di,numero_pos
		sub di,6

				;print linea
		;print espejo_i[di]

		mett11r: 


		;sentencias de calculo 
		mov al,split_2[si] 
		mov pc11[0],al


		mov al,espejo_i[di]
		mov pc12[0],al 
		;print linea
		;print pc12[0]
		mov bl,pc11[0]
		sub bl,30h
		add bl,48

		mov al,pc12[0]
		sub al,30h
		add al,48



		cmp al,bl
		jne sigp


		;--------------------------
		;-----------incremento
		add si,1
		add di,1

				;sentencias de calculo 
		mov al,split_2[si]
		mov pc11[0],al 


		mov al,espejo_i[di]
		mov pc12[0],al 

		


		mov bl,pc11[0]
		sub bl,30h
		add bl,48

		mov al,pc12[0]
		sub al,30h
		add al,48




		cmp al,bl
		jne sigp2
		
		print espejo_i[di-1]
		print linea


;lista_splitf db 100 dup ("     $")
;cont_lp dw 0
		;--------------------------
		;-----------incremento
	
		;print linea
		;print espejo_i[di]
		sigp2:
		sub si,1
		sub di,1


		sigp:
		add si,6
		sub di,6
		cmp si,numero_pos
		jne mett11r
jmp menu_ac

 m_espejo:; espejo
 	
 	mov si,numero_pos
 	print mensaje_espejo
		mett: 
		sub si,6
		print espejo_i[si]
		print linea
		


		cmp si,0
		jne mett



 	jmp menu_ac
error_sint:
	print linea 
	print errarch 
	print c_arch
	print linea
	jmp Salida

error_sint1:
	print linea 
	print errarch 
	print c_arch2
	print linea
	jmp Salida

cad_ad:
xor si,si
	  	xor bx,bx
		;mov bl,di
		;print espejo_i[12]
		
		;print saludo
		;print linea
		xor di ,di
		mov si,num_t
	;	print cadena_p1[si]
	;	print cadena_p1[si-2]
	;	print cadena_p1[si-4]


		CICLO33r:

		xor al,al
				cmp cadena_p1[si],2bh
				je sigfy

				cmp cadena_p1[si],3bh
				je sigfy

				cmp cadena_p1[si],25h
				je sigfy
			  	;print cadena_p1[si]
			  	xor al,al 
			  	mov al,cadena_p1[si]
			  	mov split_1[bx],al
			  	;print lineags
			  	;print cadena_p1[si]
			  	add bx,2

				sigfy: 
				sub si,2

				cmp si,0
				jne CICLO33r

		mov al, inicia_cad
		mov split_1[bx],al

		;print espejo_0[bx-2]
		;print espejo_0[bx-4]
		;print saludo
		xor si,si
		mov cx,bx
		xor bx,bx
		add si,2

		;mov si,cx

		xor di,di
		CICLO33b1:

		xor al,al
			  	;print split_1[si]
			  	;print lineags
			  	;it0
				cmp split_1[si],0Ah
				je ccf1

				cmp split_1[si],0Dh
				je ccf1

				cmp split_1[si],25h
				je ccf1



			  	xor bl,bl
				mov bl,split_1[si]
				mov split_2[di],bl
				;print lineags
				;print split_2[di]
			  	
				add di,1

				cmp split_1[si+2],0dh
				je cc11
				cmp si,cx
				je fin_c1
				add si,2
				;it1

			  	xor bl,bl
				mov bl,split_1[si]
				mov split_2[di],bl
				;print lineags
				;print split_2[di]
				inc di

				cmp split_1[si+2],0dh
				je cc21
				cmp split_1[si+2],25h
				je cc21
				cmp si,cx
				je fin_c1
				add si,2

				;it2 hast
			  	xor bl,bl
				mov bl,split_1[si]
				mov split_2[di],bl
				;print lineags
				;print split_2[di]
				inc di

				cmp split_1[si+2],0dh
				je cc31
				cmp split_1[si+2],25h
				je cc31

				cmp si,cx
				je fin_c1
				add si,2

				;it3
			  	xor bl,bl
				mov bl,split_1[si]
				mov split_2[di],bl
				;print lineags
				;print split_2[di]
				inc di

				cmp split_1[si+2],0dh
				je cc41
				cmp split_1[si+2],25h
				je cc41
				cmp si,cx
				je fin_c1
				add si,2


				;it4
			  	xor bl,bl
				mov bl,split_1[si]
				mov split_2[di],bl
				;print lineags
				;print split_2[di]
				inc di

				cmp split_1[si+2],0dh
				je cc51
				cmp split_1[si+2],25h
				je cc51
				cmp si,cx
				je fin_c1
				add si,2



				;print saludo
				cc11:
				add di,5
				jmp ccf1

				cc21:
				add di,4

				jmp ccf1

				cc31:
				add di,3

				jmp ccf1

				cc41:
				add di,2

				jmp ccf1

				cc51:
				add di,1

				jmp ccf1


				ccf1:


				;dh
				;cmp espejo_0[si],espejo_0[si]
				;print saludo
				add si,2

				;cmp si,di
				cmp si ,cx
				jne CICLO33b1

		fin_c1:		
		;---------------------fin mover hacia adelante split

ret	
	;-------------------------------------------

	main endp ;Termina proceso 
end main