imprimir macro cadena      
	
	mov ah,09               ;utilizamos el servicio 09 de int 21 que nos permite imprimir en pantall
	mov dx,offset cadena    ;cargamos al registro de datos la cadena
	int 21h                 ;interrupcion necesaria para mostrar el mensaje
endm 
.model small 

.stack 
;------segementos de data
.data 

;ATRIBUTOS DEL MENU PRINCIPAL
encabezado1 DB 'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA',13,10,'$'
encabezado2 DB 'FACULTAD DE INGENIERIA',13,10,'$'
encabezado3 DB 'ESCUELA DE CIENCIAS Y SISTEMAS',13,10,'$'
encabezado4 DB 'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1 - A ',13,10,'$'
encabezado5 DB 'Mynor Styven Sinay Alvarez',13,10,'$'
encabezado6 DB '201403520',13,10,'$'

menu DB '-------------------------'
salto1 DB  13,10     
message4 DB ' 1. Ingresar' 
salto4 DB 13,10
message5 DB ' 2. Registrarse '
salto5 DB 13,10
message6 DB ' 3. Salir',13,10,'$'
elegir DB 'Elige una Opcion: ',13,10,'$'

;------ATRIBUTOS MARGEN INTERNO----
puntoinicioy dw 20 
puntoiniciox dw 10

iniciolineaderechahy dw 310 ;coordenadas línea 1 
y01 dw ? 

inicioarriba dw 20
x01 dw ?

inicioabajo dw 190
x02 dw ?

iniciolineaizquierday dw 10 ;coordenadas linea 2 
y02 dw ?
blanco db 15 
;-----------------------------------

;-----------------------------ATRIBUTOS DEL ENCABEZADO DEL JUEGO

ingresar DB 'Ingresa tu Nombre: ',13,10,'$'
usuario db 60 dup('$')              ;Variable utilizada para el nombre del usuario

ni1 DB 'Nivel 1','$'
ni2 DB 'Nivel 2','$'
ni3 DB 'Nivel 3','$'

puntajes DB 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30
contadorpuntos DW 0
unidades DB 0
decenas DB 0
victoria DB 'GANASTE',13,10,'$'
;------------------------------------------------------

longitudserpiente DW 3

cabezax dw 110 ;coordenadas cabeza serpiente
cabezay dw 150 

cuerpo1x dw 109
cuerpo1y dw 150

rastrox dw 108
rastroy dw 150

posicionfuturax dw 0
posicionfuturay dw 0 

posicionactualx dw 0
posicionactualy dw 0 

siguientex dw 0
siguientey dw 0

nuevox     dw 0
nuevoy     dw 0

;-----------------------------VARIABLES PARA LA COMPARACION DE LA CABEZA CON LOS OBSTACULOS
cax 		DW 0
cay			DW 0
;------------------------------------------------------------------------

comidax    dw 0
comiday    dw 0

contador dw ?

coordenadasx DW 40 DUP(0)
coordenadasy DW 40 DUP(0)

;COORDENADA MAYOR EN X 315
;COORDENADA MENOR EN Y 0


											  ;INICIO FRUTAS 2DO NIVEL 				 ;INICIO FRUTAS 3ER NIVEL	
frutasx DW  130,135,70,80,145,50,60,40,35,35,100,120,140,189,200,150,100,75,160,195,50,75,200,140,90,130,175,210,120,70 
frutasy DW  160,170,70,160,30,60,100,50,40,100,40,160,70,80,140,130,40,80,120,160,130,70,150,75,60,160,110,70,80,150
																					

estadonivel2 DW 0
estadonivel3 DW 0

;----------------------------------OBSTACULOS DEL NIVEL 2
obstaculo1nivel2x DW 100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140
obstaculo1nivel2y DW 50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50

obstaculox DW 0
obstaculoy DW 0

obstaculo2nivel2x DW 100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140
obstaculo2nivel2y DW 150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150
;-----------------------------------------------
;OBSTACULOS DEL NIVEL 3
obstaculo1nivel3x   DW 185,185,185,185,185,185,185,185,185,185,185,185,185,185,185,185,185,185,185,185,185
obstaculo1nivel3y   DW  60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80

obstaculo2nivel3x   DW  90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90
obstaculo2nivel3y   DW  120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140

largo2 DW 21

;---------------------------------------------------------

largo DW 40

contadorfrutas DW 0
frutascomidas DW 0
velocidadnivel DW 170

v2 DW 9

color1 db 2h
colorcomida DB 1100b 
negro db 00h 

;--------segmento de codigo 
.code 

mov ax,@data 
mov ds,ax 


menuprincipal:
mov ah,00
mov al,03
int 10h
imprimir encabezado1
imprimir encabezado2
imprimir encabezado3
imprimir encabezado4
imprimir encabezado5
imprimir encabezado6



lea dx, menu ;Aqui se carga el mensaje
mov ah,9h
int 21h

imprimir elegir

mov ah,1h; LEER EL CARACTER
int 21h

cmp al,'1'  
JE juego
cmp al,'3'
JE TerminarPrograma
JMP menuprincipal

TerminarPrograma:
call termina


juego proc

mov si,00
imprimir ingresar

nombre:
  	mov ah,01h         
  	int 21h
  	mov usuario[si],al    
  	inc si
  	cmp al,0dh         
  	jnz nombre
	

;AQUI SE ACTIVA EL MODO VIDEO
mov ah,0h 
mov al,13h 
int 10h 


mov ah,02h  ;----CON ESTO MOVEMOS EL CURSOR
mov dh,1    ;FILA
mov dl,2   ;COLUMNA
int 10h

lea dx,usuario ;Aqui se carga el mensaje
mov ah,9h
int 21h

mov ah,02h  ;-----MOVIENDO EL PUNTERO PARA EL NIVEL
mov dh,1
mov dl,14
int 10h

lea dx,ni1
mov ah,9h
int 21h

;--------------------------------------------------COMIENZO A MOVER LOS PUNTOS
mov ah,02h  ;-----MOVIENDO EL PUNTERO PARA EL PUNTEO
mov dh,1
mov dl,23
int 10h

mov si,contadorpuntos

mov al,puntajes[si]
aam

mov decenas,ah
mov unidades,al

mov dl,decenas
add dl,48
mov ah,2
int 21h

mov ah,02h
mov dh,1
mov dl,24
int 10h

mov dl,unidades
add dl,48
mov ah,2
int 21h

add contadorpuntos,1

;-----------------------------------------FINALIZA DE PONER LOS PUNTOS
call margen
call dibujarcomida
JMP inicio
juego endp 


dibujarcomida proc
mov si,contadorfrutas


mov ah,0ch
mov al,colorcomida
mov cx, frutasx[si]
mov dx, frutasy[si]
int 10h

mov comidax,cx
mov comiday,dx
add contadorfrutas,2

ret
dibujarcomida endp

;----------------------Procedimiento para margen interno
margen proc 
mov ax,puntoinicioy
mov y01,ax 
mov y02,ax

mov ax,puntoiniciox
mov x01,ax
mov x02,ax

lineaderecha: 
mov ah,0ch 
mov al,blanco
mov cx,iniciolineaderechahy 
mov dx,y01 
int 10h 
cmp dx,190
JE lineaizquierda 
inc y01 
jmp lineaderecha 
 
lineaizquierda: 
mov ah,0ch 
mov al,blanco 
mov cx,iniciolineaizquierday 
mov dx,y02 
int 10h 
cmp dx,190
JE larriba 
inc y02 
jmp lineaizquierda

larriba:
mov ah,0ch 
mov al,blanco
mov cx,x01 
mov dx,inicioarriba 
int 10h 
cmp cx,310
JE labajo 
inc x01 
jmp larriba

labajo:
mov ah,0ch 
mov al,blanco
mov cx,x02 
mov dx,inicioabajo
int 10h 
cmp cx,310
JE sales 
inc x02 
jmp labajo
 
sales: 
ret 
margen endp ;termina procedimiento de margen 


inicio proc
;PRIMERO UTILIZO LA PILA PARA INGRESAR LOS VALORES AL VECTOR DONDE ESTARAN LOS SEGMENTOS INICIALES DE LA SERPIENTE
push rastroy
push rastrox

push cuerpo1y
push cuerpo1x

push cabezay
push cabezax

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA INGRESAR A LOS VECTORES

;----------------Inicio del Ciclo For
For1:
pop cx
pop dx 

;INGRESAMOS LOS VALORES A LOS VECTORES
mov coordenadasx[si],cx
mov coordenadasy[si],dx
add si,2

inc bx
cmp bx,longitudserpiente
JNE For1; ------------------------Se termina el ciclo For

;------------------------------------AQUI INICIA EL JUEGO
JMP FlechaDerecha
inicio endp
;........................................................
FlechaDerecha proc 

call dibujar
call ColisionComida
call choquemargenx
call colisionobstaculos

in al,60h
cmp al,48h
JE arriba
cmp al,50h
JE abajo


mov si,0

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
inc coordenadasx[si]
call actualizarPosiciones
call autocolision
JMP FlechaDerecha

fin:
call termina 

;INICIO DE PROCEDIMIENTO SI SE PRESIONO FLECHA ARRIBA
arriba:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
dec coordenadasy[si]
call actualizarPosiciones
JMP FlechaArriba

abajo:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
inc coordenadasy[si]
call actualizarPosiciones
JMP FlechaAbajo

FlechaDerecha endp 
;............................................................................... 


;------------------------------PROCEDIMIENTO PARA MOVER HACIA ARRIBA
FlechaArriba proc
call dibujar
call ColisionComida
call choquemargeny
call colisionobstaculos


in al,60h
cmp al,4Dh
JE derecha
cmp al,4Bh
JE izquierda

mov si,0

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
dec coordenadasy[si]
call actualizarPosiciones
call autocolision
JMP FlechaArriba

fin2:
call termina 

derecha:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
inc coordenadasx[si]
call actualizarPosiciones
JMP FlechaDerecha

izquierda:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
dec coordenadasx[si]
call actualizarPosiciones
JMP FlechaIzquierda

FlechaArriba endp


;----------------------------------PROCEDIMIENTO PARA MOVER HACIA ABAJO
FlechaAbajo proc
call dibujar
call ColisionComida
call choquemargeny
call colisionobstaculos


in al,60h
cmp al,4Bh
JE izquierda2
cmp al,4Dh
JE derecha2

mov si,0

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
inc coordenadasy[si]
call actualizarPosiciones
call autocolision
JMP FlechaAbajo

fin3:
call termina 

izquierda2:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
dec coordenadasx[si]
call actualizarPosiciones
JMP FlechaIzquierda

derecha2:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
inc coordenadasx[si]
call actualizarPosiciones
JMP FlechaDerecha

FlechaAbajo endp


;----------------------------------PROCEDIMIENTO PARA MOVER HACIA IZQUIERDA
FlechaIzquierda proc
call dibujar
call ColisionComida
call choquemargenx
call colisionobstaculos


in al,60h
cmp al,48h
JE arriba2
cmp al,50h
JE abajo2

mov si,0

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
dec coordenadasx[si]
call actualizarPosiciones
call autocolision
JMP FlechaIzquierda

arriba2:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
dec coordenadasy[si]
call actualizarPosiciones
JMP FlechaArriba

abajo2:
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
inc coordenadasy[si]
call actualizarPosiciones
JMP FlechaAbajo

fin4:
call termina 

FlechaIzquierda endp


;------------------------------PROCEDIMIENTO PARA ACTUALIZAR LAS POSICIONES DE LA SERPIENTE
actualizarPosiciones proc

mov bx,0 ; CONTADOR DEL FOR
add si,2; INCREMENTAMOS EL CONTADOR DE LOS VECTORES PARA QUE INICIE EN LA POSICION SIGUIENTE A LA CABEZA
For3:

mov posicionfuturax,cx
mov posicionfuturay,dx

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]

mov posicionactualx,cx
mov posicionactualy,dx

mov cx,posicionfuturax
mov dx,posicionfuturay

mov coordenadasx[si],cx
mov coordenadasy[si],dx

mov cx,posicionactualx
mov dx,posicionactualy

add si,2

inc bx
cmp bx,longitudserpiente
JNE For3; ------------------------Se termina el ciclo For
inc longitudserpiente
ret
actualizarPosiciones endp


;-----------------------------------------------PROCEDIMIENTO PARA DIBUJAR EL CUERPO
dibujar proc 
dec longitudserpiente
;DIBUJANDO EL CUERPO
;PRIMERO DIBUJO LA CABEZA

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES
For2:
mov ah,0ch
mov al,color1
mov cx, coordenadasx[si]
mov dx, coordenadasy[si]
int 10h

add si,2
inc bx 
cmp bx,longitudserpiente
JNE For2; ------------------------Se termina el ciclo For


;---SE TERMINA DE DIBUJAR EL CUERPO

;--------------AHORA SOLO DIBUJO EL RASTRO
mov ah,0ch
mov al,negro
mov cx, coordenadasx[si]
mov dx, coordenadasy[si]
int 10h

call velocidadserpiente

ret
dibujar endp  

;------------------------- PROCEDIMIENTO PARA LA VELOCIDAD DEL NIVEL 1
velocidadserpiente proc
  
mov ax, velocidadnivel ;Tiempo en milliseconds 
mov dx, 3E8h ;MULTIPLICAMOS por 1000 
mul dx ;  Tiempo en microseconds 

xchg ax,dx ;Tiempo 
xchg ax,cx 
mov ah,86h 
int 15h 


xor ax,ax
xor dx,dx
xor cx,cx
ret 
velocidadserpiente endp
;---------------------------- TERMINA PROCEDIMIENTO DE LA VELOCIDAD DEL NIVEL 1

;------------------------------PROCEDIMIENTO PARA REVISAR SI LA SERPIENTE SE CHOCO CONSIGO MISMA
autocolision proc 

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]

mov cax,cx
mov cay,dx
add si,2

autocuerpo:
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]
cmp cx,cax
JE revisary
add si,2
inc bx 
cmp bx,longitudserpiente
JNE autocuerpo  ; ------------------------Se termina el ciclo For
ret

revisary:
cmp dx,cay
JE fi
add si,2
inc bx 
cmp bx,longitudserpiente
JNE autocuerpo  ; ------------------------Se termina el ciclo For
ret

fi:
call termina

autocolision endp;--------------------------------------------------------------------------------------


;-----------------------------PROCEDIMIENTO PARA LA COLISION DE LAS COMIDAS Y LA SUBIDA DE NIVEL
ColisionComida proc
mov si,0
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]

cmp cx,comidax
JE comparary
ret

comparary:
cmp dx,comiday
JE aumentarLongitud
ret

aumentarLongitud:
inc longitudserpiente
mov cx,comidax
mov dx,comiday

mov nuevox,cx
mov nuevoy,dx

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES
agregarCuerpo:
mov cx,coordenadasx[si]
mov dx,coordenadasy[si]

mov siguientex,cx
mov siguientey,dx

mov cx,nuevox
mov dx,nuevoy

mov coordenadasx[si],cx
mov coordenadasy[si],dx

mov cx,siguientex
mov dx,siguientey

mov nuevox,cx
mov nuevoy,dx


add si,2
inc bx 
cmp bx,longitudserpiente
JNE agregarCuerpo; ------Se termina el ciclo For

inc frutascomidas
call puntos
cmp frutascomidas,10
JE subirnivel2
cmp frutascomidas,20
JE subirnivel3
cmp frutascomidas,30
JE ganar

call dibujarcomida
ret

subirnivel2:
;sub v2,4
sub velocidadnivel,100
call nivel2
call dibujarcomida
ret

subirnivel3:
;sub v2,5
sub velocidadnivel,50
call nivel3
call dibujarcomida
ret

ganar:
call triunfo

ColisionComida endp;----------------------------------------------------------------------------------------------

;--------------------------------Procedimiento para verificar el choque del margen interno en x
choquemargenx proc
mov si,0

cmp coordenadasx[si],310
JE finjuego
cmp coordenadasx[si],10
JE finjuego

ret

finjuego:
call termina

choquemargenx endp

;-------------------------------Procedimiento para verificar el choque del margen interno en y
choquemargeny proc
mov si,0

cmp coordenadasy[si],190
JE finjuego2
cmp coordenadasy[si],20
JE finjuego2

ret

finjuego2:
call termina

choquemargeny endp

;-------------------------------------PROCEDIMIENTO PARA CREAR OBSTACULOS DEL NIVEL DOS 
nivel2 proc
;------ ACTIVO EL ESTADO DEL NIVEL 2 PARA PODER EMPEZAR LAS COMPARACIONES
inc estadonivel2

;---------------------------------------CAMBIO EL ENCABEZADO A NIVEL 2
mov ah,02h  ;-----MOVIENDO EL PUNTERO PARA EL NIVEL
mov dh,1
mov dl,14
int 10h

lea dx,ni2
mov ah,9h
int 21h

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES


obstaculo:

mov ah,0ch 
mov al,blanco
mov cx,obstaculo1nivel2x[si] 
mov dx,obstaculo1nivel2y[si] 
int 10h


add si,2
inc bx 
cmp bx,largo
JNE obstaculo  ; ------------------------Se termina el ciclo For

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES

obstaculo2:
mov ah,0ch 
mov al,blanco
mov cx,obstaculo2nivel2x[si] 
mov dx,obstaculo2nivel2y[si] 
int 10h


add si,2
inc bx 
cmp bx,largo
JNE obstaculo2  ; ------------------------Se termina el ciclo For

ret
nivel2 endp;--------------------------------------------------------------------------

;--------------------------------PROCEDIMIENTO PARA CREAR LOS OBSTACULOS DEL NIVEL 3
nivel3 proc
inc estadonivel3 ;------ ACTIVO EL ESTADO DEL NIVEL 3 PARA PODER EMPEZAR LAS COMPARACIONES
dec estadonivel2 ;---------------------------APAGO EL ESTADO DEL NIVEL 2

;---------------------------------------CAMBIO EL ENCABEZADO A NIVEL 3
mov ah,02h  ;-----MOVIENDO EL PUNTERO PARA EL NIVEL
mov dh,1
mov dl,14
int 10h

lea dx,ni3
mov ah,9h
int 21h


mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES


obstaculos:

mov ah,0ch 
mov al,blanco
mov cx,obstaculo1nivel3x[si] 
mov dx,obstaculo1nivel3y[si] 
int 10h

add si,2
inc bx 
cmp bx,largo2
JNE obstaculos  ; ------------------------Se termina el ciclo For

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES

obstaculos2:
mov ah,0ch 
mov al,blanco
mov cx,obstaculo2nivel3x[si] 
mov dx,obstaculo2nivel3y[si] 
int 10h

add si,2
inc bx 
cmp bx,largo2
JNE obstaculos2  ; ------------------------Se termina el ciclo For

ret
nivel3 endp
;----------------------------------------PROCEDIMIENTO PARA LA COLISION DE LOS OBSTACULOS
colisionobstaculos proc 

cmp estadonivel2,1
JE obsnivel2
cmp estadonivel3,1
JE obsnivel3
ret

obsnivel2:
call obstaculosnivel2
ret

obsnivel3:
call obstaculosnivel2
call obstaculosnivel3
ret


colisionobstaculos endp

;-----------------------------PROCEDIMIENTO PARA LOS OBSTACULOS DEL NIVEL 2
obstaculosnivel2 proc 

mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]

mov cax,cx
mov cay,dx

obs1nivel2:
mov cx,obstaculo1nivel2x[si]
mov dx,obstaculo1nivel2y[si]
cmp cx,cax
JE obsy
add si,2
inc bx 
cmp bx,largo
JNE obs1nivel2  ; ------------------------Se termina el ciclo For
mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES
JMP obs2nivel2

obsy:
cmp dx,cay
JE termino
add si,2
inc bx 
cmp bx,largo
JNE obs1nivel2  ; ------------------------Se termina el ciclo For
mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES
JMP obs2nivel2

obs2nivel2:
mov cx,obstaculo2nivel2x[si]
mov dx,obstaculo2nivel2y[si]
cmp cx,cax
JE obs2y
add si,2
inc bx 
cmp bx,largo
JNE obs2nivel2  ; ------------------------Se termina el ciclo For
ret

obs2y:
cmp dx,cay
JE termino
add si,2
inc bx 
cmp bx,largo
JNE obs2nivel2  ; ------------------------Se termina el ciclo For
ret

termino:
call termina

obstaculosnivel2 endp
;--------------------------------------------------------------TERMINA EL PROCEDIMIENTO DE LOS OBSTACULOS NIVEL 2

;--------------------------------------------------------------INICIA EL PROCEDIMIENTO DE LOS OBSTACULOS DEL NIVEL 3
obstaculosnivel3 proc


mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES

mov cx,coordenadasx[si]
mov dx,coordenadasy[si]

mov cax,cx
mov cay,dx

obs1nivel3:
mov cx,obstaculo1nivel3x[si]
mov dx,obstaculo1nivel3y[si]
cmp cx,cax
JE obs1y
add si,2
inc bx 
cmp bx,largo2
JNE obs1nivel3  ; ------------------------Se termina el ciclo For
mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES
JMP obs2nivel3

obs1y:
cmp dx,cay
;JE termino
add si,2
inc bx 
cmp bx,largo2
JNE obs1nivel3  ; ------------------------Se termina el ciclo For
mov bx,0h ;INICIAMOS EL CONTADOR EN CERO
mov si,0; INDICE PARA LEER EN LOS VECTORES
JMP obs2nivel3

obs2nivel3:
mov cx,obstaculo2nivel3x[si]
mov dx,obstaculo2nivel3y[si]
cmp cx,cax
JE obsn2y
add si,2
inc bx 
cmp bx,largo2
JNE obs2nivel3  ; ------------------------Se termina el ciclo For
ret

obsn2y:
cmp dx,cay
;JE termino
add si,2
inc bx 
cmp bx,largo2
JNE obs2nivel3  ; ------------------------Se termina el ciclo For
ret
obstaculosnivel3 endp
;--------------------------------------------------------------TERMINA EL PROCEDIMIENTO DE LOS OBSTACULOS DEL NIVEL 3

puntos proc
mov ah,02h  ;-----MOVIENDO EL PUNTERO PARA EL PUNTEO
mov dh,1
mov dl,23
int 10h


mov si,contadorpuntos
mov al,puntajes[si]
aam

mov decenas,ah
mov unidades,al

mov dl,decenas
add dl,48
mov ah,2
int 21h

mov ah,02h
mov dh,1
mov dl,24
int 10h

mov dl,unidades
add dl,48
mov ah,2
int 21h

add contadorpuntos,1

ret

xor dx,dx
xor ax,ax
xor si,si
puntos endp

;---------------------------------------PROCEDIMIENTO PARA GANAR EL JUEGO 
triunfo proc 

mov ah,00
mov al,03
int 10h

mov ah,0h 
mov al,13h 
int 10h 

mov ah,02h  ;----CON ESTO MOVEMOS EL CURSOR
mov dh,10    ;FILA
mov dl,10   ;COLUMNA
int 10h

lea dx,victoria ;Aqui se carga el mensaje
mov ah,9h
int 21h


  MOV DI,36
  MOV AH, 0
  INT 1Ah
  MOV BX, DX
  Delay1:
  MOV AH, 0
  INT 1Ah
  SUB DX, BX
  CMP DI, DX
  JA Delay1 ;--------------------------TERMINA DELAY

  xor dx,dx
  xor ax,ax
  xor bx,bx
  
  JMP termina

triunfo endp

;----------------------------------------PROCEDIMIENTO PARA TERMINAR EL PROGRAMA
termina proc

mov ah,00
mov al,03
int 10h

mov ah, 04ch
int 21h

termina endp

end ;fin del programa|