;------------------------------
;MACRO PARA IMPRIMIR EN PANTALLA
;----------------------------------
mostrar macro texto
    lea dx, texto
    mov ah,9
    int 21h
    mov ax,@data    
    mov ds,ax
    mov es,ax
endm

iniMax macro 
    MOV AX,@DATA
	MOV DS,AX	

    mov Tablero + 0 * colmax +0, 32; Espacio en blanco 	
    mov Tablero + 1 * colmax +0, 56; Mueve el 8 	
    mov Tablero + 2 * colmax +0, 32; Espacio en blanco
    mov Tablero + 3 * colmax +0, 55; Mueve el 8 
    mov Tablero + 4 * colmax +0, 32; Espacio en blanco
    mov Tablero + 5 * colmax +0, 54; Mueve el 8 
    mov Tablero + 6 * colmax +0, 32; Espacio en blanco
    mov Tablero + 7 * colmax +0, 53; Mueve el 8 
    mov Tablero + 8 * colmax +0, 32; Espacio en blanco
    mov Tablero + 9 * colmax +0, 52; Mueve el 8 
    mov Tablero + 10 * colmax +0, 32; Espacio en blanco
    mov Tablero + 11 * colmax +0, 51; Mueve el 8 
    mov Tablero + 12 * colmax +0, 32; Espacio en blanco
    mov Tablero + 13 * colmax +0, 50; Mueve el 8 
    mov Tablero + 14 * colmax +0, 32; Espacio en blanco
    mov Tablero + 15 * colmax +0, 49; Mueve el 8 
    mov Tablero + 16 * colmax +0, 32; Espacio en blanco

    mov Tablero + 1 * colmax +1, 124
	mov Tablero + 3 * colmax +1, 124
	mov Tablero + 5 * colmax +1, 124
	mov Tablero + 7 * colmax +1, 124
	mov Tablero + 9 * colmax +1, 124
    mov Tablero + 11 * colmax +1, 124
    mov Tablero + 13 * colmax +1, 124
    mov Tablero + 15 * colmax +1, 124

    mov Tablero + 1 * colmax +2, 70
	mov Tablero + 1 * colmax +3, 66

	mov Tablero + 1 * colmax +5, 32
	mov Tablero + 1 * colmax +6, 32

	mov Tablero + 1 * colmax +8, 70
	mov Tablero + 1 * colmax +9, 66

	mov Tablero + 1 * colmax +11, 32
	mov Tablero + 1 * colmax +12, 32

	mov Tablero + 1 * colmax +14, 70
	mov Tablero + 1 * colmax +15, 66

	mov Tablero + 1 * colmax +17, 32
	mov Tablero + 1 * colmax +18, 32

	mov Tablero + 1 * colmax +20, 70
	mov Tablero + 1 * colmax +21, 66

	mov Tablero + 1 * colmax +23, 32
	mov Tablero + 1 * colmax +24, 32

	mov Tablero + 3 * colmax +2, 32
	mov Tablero + 3 * colmax +3, 32

	mov Tablero + 3 * colmax +5, 70
	mov Tablero + 3 * colmax +6, 66

	mov Tablero + 3 * colmax +8, 32
	mov Tablero + 3 * colmax +9, 32

	mov Tablero + 3 * colmax +11, 70
	mov Tablero + 3 * colmax +12, 66

	mov Tablero + 3 * colmax +14, 32
	mov Tablero + 3 * colmax +15, 32

	mov Tablero + 3 * colmax +17, 70
	mov Tablero + 3 * colmax +18, 66

	mov Tablero + 3 * colmax +20, 32
	mov Tablero + 3 * colmax +21, 32
	
	mov Tablero + 3 * colmax +23, 70
	mov Tablero + 3 * colmax +24, 66

	mov Tablero + 5 * colmax +2, 70
	mov Tablero + 5 * colmax +3, 66

	mov Tablero + 5 * colmax +5, 32
	mov Tablero + 5 * colmax +6, 32

	mov Tablero + 5 * colmax +8, 70
	mov Tablero + 5 * colmax +9, 66

	mov Tablero + 5 * colmax +11, 32
	mov Tablero + 5 * colmax +12, 32

	mov Tablero + 5 * colmax +14, 70
	mov Tablero + 5 * colmax +15, 66

	mov Tablero + 5 * colmax +17, 32
	mov Tablero + 5 * colmax +18, 32

	mov Tablero + 5 * colmax +20, 70
	mov Tablero + 5 * colmax +21, 66

	mov Tablero + 5 * colmax +23, 32
	mov Tablero + 5 * colmax +24, 32

	mov Tablero + 7 * colmax +2, 32
	mov Tablero + 7 * colmax +3, 32

	mov Tablero + 7 * colmax +5, 32
	mov Tablero + 7 * colmax +6, 32

	mov Tablero + 7 * colmax +8, 32
	mov Tablero + 7 * colmax +9, 32

	mov Tablero + 7 * colmax +11, 32
	mov Tablero + 7 * colmax +12, 32

	mov Tablero + 7 * colmax +14, 32
	mov Tablero + 7 * colmax +15, 32

	mov Tablero + 7 * colmax +17, 32
	mov Tablero + 7 * colmax +18, 32

	mov Tablero + 7 * colmax +20, 32
	mov Tablero + 7 * colmax +21, 32

	mov Tablero + 7 * colmax +23, 32
	mov Tablero + 7 * colmax +24, 32

	mov Tablero + 9 * colmax +2, 32
	mov Tablero + 9 * colmax +3, 32

	mov Tablero + 9 * colmax +5, 32
	mov Tablero + 9 * colmax +6, 32

	mov Tablero + 9 * colmax +8, 32
	mov Tablero + 9 * colmax +9, 32

	mov Tablero + 9 * colmax +11, 32
	mov Tablero + 9 * colmax +12, 32

	mov Tablero + 9 * colmax +14, 32
	mov Tablero + 9 * colmax +15, 32

	mov Tablero + 9 * colmax +17, 32
	mov Tablero + 9 * colmax +18, 32

	mov Tablero + 9 * colmax +20, 32
	mov Tablero + 9 * colmax +21, 32

	mov Tablero + 9 * colmax +23, 32
	mov Tablero + 9 * colmax +24, 32

	mov Tablero + 11 * colmax +2, 32
	mov Tablero + 11 * colmax +3, 32

	mov Tablero + 11 * colmax +5, 70
	mov Tablero + 11 * colmax +6, 78

	mov Tablero + 11 * colmax +8, 32
	mov Tablero + 11 * colmax +9, 32

	mov Tablero + 11 * colmax +11, 70
	mov Tablero + 11 * colmax +12, 78

	mov Tablero + 11 * colmax +14, 32
	mov Tablero + 11  * colmax +15, 32

	mov Tablero + 11 * colmax +17, 70
	mov Tablero + 11 * colmax +18, 78

	mov Tablero + 11 * colmax +20, 32
	mov Tablero + 11 * colmax +21, 32
	
	mov Tablero + 11 * colmax +23, 70
	mov Tablero + 11 * colmax +24, 78

	mov Tablero + 13 * colmax +2, 70
	mov Tablero + 13 * colmax +3, 78

	mov Tablero + 13 * colmax +5, 32
	mov Tablero + 13 * colmax +6, 32

	mov Tablero + 13 * colmax +8, 70
	mov Tablero + 13 * colmax +9, 78

	mov Tablero + 13 * colmax +11, 32
	mov Tablero + 13 * colmax +12, 32

	mov Tablero + 13 * colmax +14, 70
	mov Tablero + 13 * colmax +15, 78

	mov Tablero + 13 * colmax +17, 32
	mov Tablero + 13 * colmax +18, 32

	mov Tablero + 13 * colmax +20, 70
	mov Tablero + 13 * colmax +21, 78

	mov Tablero + 13 * colmax +23, 32
	mov Tablero + 13 * colmax +24, 78

	mov Tablero + 15 * colmax +2, 32
	mov Tablero + 15 * colmax +3, 32

	mov Tablero + 15 * colmax +5, 70
	mov Tablero + 15 * colmax +6, 78

	mov Tablero + 15 * colmax +8, 32
	mov Tablero + 15 * colmax +9, 32

	mov Tablero + 15 * colmax +11, 70
	mov Tablero + 15 * colmax +12, 78

	mov Tablero + 15 * colmax +14, 32
	mov Tablero + 15  * colmax +15, 32

	mov Tablero + 15 * colmax +17, 70
	mov Tablero + 15 * colmax +18, 78

	mov Tablero + 15 * colmax +20, 32
	mov Tablero + 15 * colmax +21, 32
	
	mov Tablero + 15 * colmax +23, 70
	mov Tablero + 15 * colmax +24, 78

    mov Tablero + 1 * colmax +4, 124
	mov Tablero + 3 * colmax +4, 124
	mov Tablero + 5 * colmax +4, 124
	mov Tablero + 7 * colmax +4, 124
	mov Tablero + 9 * colmax +4, 124
    mov Tablero + 11 * colmax +4, 124
    mov Tablero + 13 * colmax +4, 124
    mov Tablero + 15 * colmax +4, 124

    mov Tablero + 1 * colmax +7, 124
	mov Tablero + 3 * colmax +7, 124
	mov Tablero + 5 * colmax +7, 124
	mov Tablero + 7 * colmax +7, 124
	mov Tablero + 9 * colmax +7, 124
    mov Tablero + 11 * colmax +7, 124
    mov Tablero + 13 * colmax +7, 124
    mov Tablero + 15 * colmax +7, 124

    mov Tablero + 1 * colmax +10, 124
	mov Tablero + 3 * colmax +10, 124
	mov Tablero + 5 * colmax +10, 124
	mov Tablero + 7 * colmax +10, 124
	mov Tablero + 9 * colmax +10, 124
    mov Tablero + 11 * colmax +10, 124
    mov Tablero + 13 * colmax +10, 124
    mov Tablero + 15 * colmax +10, 124


    mov Tablero + 1 * colmax +13, 124
	mov Tablero + 3 * colmax +13, 124
	mov Tablero + 5 * colmax +13, 124
	mov Tablero + 7 * colmax +13, 124
	mov Tablero + 9 * colmax +13, 124
    mov Tablero + 11 * colmax +13, 124
    mov Tablero + 13 * colmax +13, 124
    mov Tablero + 15 * colmax +13, 124

	mov Tablero + 1 * colmax +16, 124
	mov Tablero + 3 * colmax +16, 124
	mov Tablero + 5 * colmax +16, 124
	mov Tablero + 7 * colmax +16, 124
	mov Tablero + 9 * colmax +16, 124
    mov Tablero + 11 * colmax +16, 124
    mov Tablero + 13 * colmax +16, 124
    mov Tablero + 15 * colmax +16, 124

    mov Tablero + 1 * colmax +19, 124
	mov Tablero + 3 * colmax +19, 124
	mov Tablero + 5 * colmax +19, 124
	mov Tablero + 7 * colmax +19, 124
	mov Tablero + 9 * colmax +19, 124
    mov Tablero + 11 * colmax +19, 124
    mov Tablero + 13 * colmax +19, 124
    mov Tablero + 15 * colmax +19, 124

    mov Tablero + 1 * colmax +22, 124
	mov Tablero + 3 * colmax +22, 124
	mov Tablero + 5 * colmax +22, 124
	mov Tablero + 7 * colmax +22, 124
	mov Tablero + 9 * colmax +22, 124
    mov Tablero + 11 * colmax +22, 124
    mov Tablero + 13 * colmax +22, 124
    mov Tablero + 15 * colmax +22, 124

    mov Tablero + 1 * colmax +25, 124
	mov Tablero + 3 * colmax +25, 124
	mov Tablero + 5 * colmax +25, 124
	mov Tablero + 7 * colmax +25, 124
	mov Tablero + 9 * colmax +25, 124
    mov Tablero + 11 * colmax +25, 124
    mov Tablero + 13 * colmax +25, 124
    mov Tablero + 15 * colmax +25, 124
  
	mov Tablero + 17 * colmax +0, 32; Espacio en blanco 	
	mov Tablero + 17 * colmax +1, 32; Espacio en blanco 	
	mov Tablero + 17 * colmax +2, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +3, 65
    mov Tablero + 17 * colmax +4, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +5, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +6, 66
    mov Tablero + 17 * colmax +7, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +8, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +9, 67
    mov Tablero + 17 * colmax +10, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +11, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +12, 68
    mov Tablero + 17 * colmax +13, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +14, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +15, 69
    mov Tablero + 17 * colmax +16, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +17, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +18, 70
    mov Tablero + 17 * colmax +19, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +20, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +21, 71
    mov Tablero + 17 * colmax +22, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +23, 32; Espacio en blanco 	
    mov Tablero + 17 * colmax +24, 72
    mov Tablero + 17 * colmax +25, 32; Espacio en blanco

endm

InLogTa macro
	loop ciclo	
	int 21h	
endm
    


.model large	
.stack 200h
.const
colmax equ 28;
.data


;--------------------------------
;VARIABLES A UTILIZAR 
;--------------------------------
nuevaLinea  db  0ah,0dh, '$'
opcion db 'Seleccione la opcion que desee: ','$'

;Matriz
Tablero db 18 dup (26 dup("-"),10,13) db 10,13,"$"
		  
LogicTa db 	8 dup(8 dup ("0"),10,13)
			db 10,13,"$"
			
ruta db 60 dup(0) ;Variable que se utiliza para la ruta del archivo  
contenido db 75 dup(0) ;variable para obtener el contenido del archivo 

contenido2 db 75 dup(0) ;variable para obtener el contenido del archivo

contenido3 db 75 dup(0) ;variable para obtener el contenido del archivo

handle dw ? 
			
;----------------
;MENSAJES PARA EL MANEJO DE ARCHIVOS
;----------------
pathArchivo db 'Ingrese el path del archivo: ','$' ;pathArchivo
noAbre	db	'No se pudo abrir el archivo.$','$'
noLee	db	'No se puede leer el archivo.$','$'
noCierra	db	'No se puede cerrar el archivo.$','$'
msgNoExisteA db 'El archivo no existe.',0ah,0dh, '$' ;msgNoExisteA
msgErrorA db 'Error al leer el archivo.',0ah,0dh, '$' ;carga3
msgCargaA db 'El archivo se ha cargado.',0ah,0dh, '$' ;carga4
msgNoFormato db 'El archivo no tiene el formato.',0ah,0dh, '$'  ;carga5
msgReporte db 'El reporte se ha creado.',0ah,0dh, '$' ;carga6			


;--------------------------------------------------------------------------------------------------
;DATOS DEL ENCABEZADO
;--------------------------------------------------------------------------------------------------
enc0        db  'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA', 0ah,0dh, '$'
enc1        db  'FACULTAD DE INGENIERIA', 0ah,0dh, '$'
enc2        db  'ESCUELA DE CIENCIAS Y SISTEMAS', 0ah,0dh, '$'
enc3        db  'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1', 0ah,0dh, '$'
enc4        db  'NOMBRE: JUAN CARLOS MALDONADO SOLORZANO', 0ah,0dh, '$'
enc5        db  'CARNET: 201404179', 0ah,0dh, '$'
enc6        db  'SECCION: A',0ah,0dh, '$'

;-------------------------
;DATOS DEL MENU
;-------------------------
msg0       db '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%', 0ah,0dh, '$'
ml1        db 'Cargar', 0ah,0dh, '$'
ml2        db 'Leer caracter', 0ah,0dh, '$'
ml3        db 'abrir', 0ah,0dh, '$'
ml4        db 'leer', 0ah,0dh, '$'
ml5        db 'leerContenido', 0ah,0dh, '$'
msg1       db '%%%%%%%%%%%% MENU PRINCIPAL %%%%%%%',0ah,0dh,'$'
msg2       db '%%    1.Iniciar Juego.%%', 0ah,0dh,'$'
msg3       db '%%    2.Cargar Juego. %%',0ah,0dh,'$'
msg4       db '%%    3.Salir. %%', 0ah,0dh,'$'                                                                         
msg5 db 'ingrese : numero: ', 0ah,0dh,'$'
msg6 db '##    Seleciono opcion1 %%', 0ah,0dh,'$' 
msg7 db '## Seleciono opcion2 %%', 0ah,0dh,'$' 
msg8 db '##    Seleciono salir%%', 0ah,0dh,'$' 

print MACRO msg                ;macro definition
        PUSH AX
        PUSH DX
        MOV AH,09H
        MOV DX,offset msg
        INT 21H
        POP DX
        POP AX
        ENDM
.code
Inicio:
;Inicio a llamar los archivos
iniMax	
		
	
;--------------------------------------------------------------------------------------------------
;MOSTRAMOS EL ENCABEZADO
;--------------------------------------------------------------------------------------------------
mostrar enc0
mostrar enc1
mostrar enc2
mostrar enc3
mostrar enc4
mostrar enc5
mostrar enc6
mostrar nuevaLinea

;---------------------------------------------------------------------------------------------------
;MOSTRAMOS EL MENU PRINCIPAL
;--------------------------------------------------------------------------------------------------
mostrarMenu:
mostrar msg0
mostrar msg1
mostrar msg0
mostrar msg2
mostrar msg3
mostrar msg4
mostrar msg5
mostrar msg0
mostrar nuevaLinea
mostrar opcion
mostrar nuevaLinea


; Tablero
;------------------------
;Se obtiene el valor del teclado------------------

mov     ah,0dh
int     21h
;Se compara el valor obtenido------

mov    ah,01h     
int     21h        ;interrupcion DOS 

;segun la opcion seleccionada se muestra 
cmp al,31h         ;numero 1
;jmp menu_2
je  IniJuego ;Inicia el juego
cmp al ,32h
je  CargarJuego
cmp al,33h
je final
jmp mostrarMenu ;si es cualquier otro caracter regresa al menu principal


;---------------------
;METODO PARA INICIAR EL JUEGO IMPRIME EL TABLERO
;---------------------
IniJuego proc	
mostrar Tablero
	call final
	ret
 IniJuego endp
;------------------------
;METODO PARA CARGAR JUEGO ARCHIVO CON EXTENSION .arq
;------------------------
leerCaracter proc
    mov ah, 01h ;Se lee el caracter
    int 21h 
    mov ruta[si], al ;Se guarda en el vector
    inc si
    cmp al,0dh ;Comparo si no es nuevaLinea
    jnz leerCaracter 
    mov ruta[si-1], "$" ;Se agrega 0 al final de cadena
    mostrar nuevaLinea
    ;mostrar ruta
    mov ruta[si-1], 0
   ; jmp abrir
   ret
leerCaracter endp
CargarJuego proc
	mostrar pathArchivo
    mov si,00
	call leerCaracter
	call final
	ret
CargarJuego endp


final:
    MOV AH,4CH
	INT 21H
 
   	
;--------------------------------
;METODO  DE SALIDA DE JUEGO
;--------------------------

imprimir:
	MOV AH,9H ;9H ES FUNCION DE 21H
	INT 21H
	RET 

end Inicio
