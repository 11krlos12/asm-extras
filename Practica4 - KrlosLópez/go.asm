;--------------------------------------------------
;Directiva para indicar el tama�o del programa
;--------------------------------------------------
.model Large
;--------------------------------------------------
;Directiva para indicar el tama�o de la pila
;--------------------------------------------------
.stack ; si no se especifica el defaul es 1kbyte
;--------------------------------------------------
;Directiva de constantes
;--------------------------------------------------
.const
colmax equ 6;
numero equ 0  
MaMax equ 32 
varjc equ 3
;--------------------------------------------------
;Directiva para indicar datos (variables)
;--------------------------------------------------
.data
;--------------------------------------------------
;Variables contadores
;--------------------------------------------------
contjc db 0
conta32 db 0
numconv DB ?, ?, 0ah,0dh,'$' 
numconv2 DB ?, ?, 0ah,0dh,'$' 

datosi dw 0


contjcFN db 0
FNconv DB ?, ?, 0ah,0dh,'$' 
msgpiezanegra DB 'Numero de puntos Fichas Negras:', 0ah,0dh,'$' 
contjcFB db 0
FBconv DB ?, ?, 0ah,0dh,'$' 
msgpiezablanca DB 'Numero de puntos Fichas Blancas:', 0ah,0dh,'$' 

GanadorNegras DB 'Ganador Fichas Negras Felicidades !', 0ah,0dh,'$'
GanadorBlancas DB 'Ganador Fichas Blancas Felicidades !', 0ah,0dh,'$'



;--------------------------------------------------
;Variables para la fecha
;--------------------------------------------------
diaF DB ?, '$'
mesF DB ?, '$'
dmconv DB ?, ?
mconv DB ?, ?
fechaseparador DB " /"
msjfecha db 'Fecha: '
fechaanio DB '2020'

;--------------------------------------------------
;Variables para la hora
;--------------------------------------------------
ho DB ?, '$'
mi DB ?, '$'
se DB ?, '$'
hconv db ?, ?
miconv db ?, ?
seconv db ?, ?
msj_separador DB " :"
msjhora db "Hora: "

;-------------------------
;PARA LOS ARCHIVOS HTML
;-------------------------
html1 db 0ah,0dh,'<html><head><title>201222687</title></head><body>' 
html2 db 0ah,0dh,'<br><br><br><br><table bgcolor="#FACC2E" border="0.8" style="margin: 0 auto;">'
htmltr db 0ah,0dh,'<tr>'
htmltrcierre db 0ah,0dh,'</tr>'
htmlth db 0ah,0dh,'<th>'
htmlthcierre db 0ah,0dh,'</th>'
htmlthDD db 0ah,0dh,'<td colspan="2">'
htmlh1 db 0ah,0dh,'<h1 align="center">'
htmlh1cierre db 0ah,0dh,'</h1>'
htmltablecierre db 0ah,0dh,'</table>'
html4 db 0ah,0dh,'</body></html> '



htmlFB db 0ah,0dh,'<img src="fb.png" alt="ficha fb" width="30" height="30">'
htmlFN db 0ah,0dh,'<img src="fn.png" alt="ficha fn" width="30" height="30">'
htmlFcuadrado db 0ah,0dh,'<img src="cuadrado.png" alt="ficha fn" width="30" height="30">'
htmlFcirculo db 0ah,0dh,'<img src="circulo.png" alt="ficha fn" width="30" height="30">'
htmlFtriangulo db 0ah,0dh,'<img src="triangulo.png" alt="ficha fn" width="30" height="30">'

entradArchivo db 'C:\estadoTablero.HTML',00h,'$'
 
MsgHtmlCreado db 'Archivo html generado con exito ',0ah,0dh,'$'

MsjReporteFinal db 'Creando ReporteFinal ',0ah,0dh,'$'

espacio db 0ah,0dh,'<tr><td colspan="2"> </td><td>|</td><td></td><td></td><td></td><td>|</td><td></td><td></td><td></td><td>|</td><td></td>'
espacio2 db 0ah,0dh,'<td></td><td></td><td>|</td><td></td><td></td><td></td><td>|</td><td></td><td></td><td></td><td>|</td><td></td><td></td><td></td><td>|</td><td></td><td></td><td></td><td>|</td></tr>'

contandolinea db 'linea------ ',0ah,0dh,'$'
;-------------------------
;PARA LA MATRIZ
;-------------------------
;0ah para fin de linea 0dh constante para el return '$' fin de cadena
;matrizuno db 4 dup(4 dup("x")),0ah,'$';

TFJC db 16 dup(31 dup("-"),0ah),0dh,'$'  

tablero db 5 dup(5 dup("x"),0ah),0dh,'$'

JuegoFinal db  'mj','$'



matrintn db 'Turno negras: ', 0ah,0dh,'$' 
matrintb db 'Turno blancas: ', 0ah,0dh,'$' 

A1 db 'A1$' 
numerox dw 3
varjcx db 0

fichax db 'E'
guionX db '-'
fichaxFuera db 'x','$'
msgiguales db 'son iguales $'
coordenaIncorrecta db 'Coordenada incorrecta', 0ah,0dh,'$' 
Pasoturno db 'Sedio el turno. ', 0ah,0dh,'$' 
Salidalmenu db 'Salida del juego.', 0ah,0dh,'$' 
GuardarArchivo db 'Se guardo juego. ', 0ah,0dh,'$' 
MostrarJuego db 'Se mostro el juego imagen. ', 0ah,0dh,'$' 
PosOcupada db 'Posicion ya ocupada', 0ah,0dh,'$' 
PosSuicidio db 'Suicidio - Regla ko ingrese otra posicion', 0ah,0dh,'$' 
ReglaKo db 'La regla del KO ', 0ah,0dh,'$' 
;-------------------------
;PARA LOS ARCHIVOS
;-------------------------
bufferentrada db 50 dup('$')
arreglo db 7 dup('$'),'$'
arreglo2 db 7 dup('$'),'$'
handlerentrada dw ?
bufferInformacion db 515 dup('$')

archia db 'Ingrese ruta del archivo', 0ah,0dh,'$' 
archib db '  .', 0ah,0dh,'$' 
archic db 'Juego guardado con exito!.', 0ah,0dh,'$' 
archid db 'Ingrese nombre para guardar:', 0ah,0dh,'$' 
EjmGuardar db 'c:\nombrearchivo.arq', 0ah,0dh,'$' 
archie db 'Ingrese texto para guardar:', 0ah,0dh,'$' 

erroru db 'Error al abrir el archivo puede que no exista', 0ah,0dh,'$' 
errord db 'Error al cerrar el archivo', 0ah,0dh,'$' 
errort db 'Error al escribir en el archivo', 0ah,0dh,'$' 
errorc db 'Error al crear el archivo', 0ah,0dh,'$' 
errorleer db 'Error al leer el archivo', 0ah,0dh,'$' 

;---------------------------------------------------
;DATOS DEL ENCABEZADO
;---------------------------------------------------
enc0 db  'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA', 0ah,0dh, '$'
enc1 db  'FACULTAD DE INGENIERIA', 0ah,0dh, '$'
enc2 db  'CIENCIAS Y SISTEMAS', 0ah,0dh, '$'
enc3 db  'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1', 0ah,0dh, '$'
enc4 db  'NOMBRE: JUAN CARLOS MALDONADO SOLORZANO', 0ah,0dh, '$'
enc5 db  'CARNET: 201222687', 0ah,0dh, '$'
enc6 db  'SECCION: A',0ah,0dh, '$'

;-------------------------
;DATOS DEL MENU
;-------------------------
LineaVacia db  0ah,0dh, '$'

msg1 db '1.Iniciar Juego.', 0ah,0dh,'$'
msg2 db '2.Cargar Juego.',0ah,0dh,'$'
msg3 db '3.Salir.', 0ah,0dh,'$'                             

Vopcion db 'Seleccione la opcion que desee: ',0ah,0dh,'$' 

msga db 'Juego iniciando', 0ah,0dh,'$' 
msgb db ' Cargando Archivo Ingrese ruta del archivo: ', 0ah,0dh,'$' 
msgc db 'Finalizando juego.', 0ah,0dh,'$' 
msgd db 'Opcion incorrecta intente de nuevo.', 0ah,0dh,'$' 

;--------------------------------------------------
;Directiva para indicar codigo
;--------------------------------------------------
.code
;--------------------------------------------------
;CONVERTIR UN DB A DECIMAL
;--------------------------------------------------
CambioDbDecimal macro XdbNumero,XdbConvertido
	mov al, [XdbNumero]
	mov cl, 10
	mov ah, 0
	div cl
	or ax, 3030h
	mov bx, offset XdbConvertido
	mov [bx], al
	inc bx
	mov [bx], ah
endm
;--------------------------------------------------
;Macro para la hora 
;--------------------------------------------------
HoraActual macro horas,minutos,segundos,horas_conv,min_conv,seg_conv

mov ah, 2Ch
int 21h

mov  horas, ch
mov  minutos, cl
mov  segundos, dh

mov al, [horas]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset horas_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [minutos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset min_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [segundos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset seg_conv
mov [bx], al
inc bx
mov [bx], ah

endm
;--------------------------------------------------
;MACRO PARA LA FECHA
;--------------------------------------------------
FechaActual macro  dia_mes,mes,dia_mes_conv,mes_ano_conv
	mov ah, 2AH
	int 21h
	;mov  fechaanio, cx
	mov  mes, dh
	mov  dia_mes, dl	
;Obtener el n�mero del d�a del mes
;Dividimos entre 10 para obtener el n�mero en decimal
;Luego lo transformamos a ASCII
mov al, [dia_mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset dia_mes_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset mes_ano_conv 
mov [bx], al
inc bx
mov [bx], ah

endm
;--------------------------------------------------
;MACRO PARA ABRIR ARCHIVO
;--------------------------------------------------
Abrir macro buffer,handler
mov ah,3dh
mov al,02h
lea dx,buffer
int 21h
jc Error1
mov handler,ax
endm
;--------------------------------------------------
;MACRO PARA CERRAR ARCHIVO
;--------------------------------------------------
Cerrar macro handler
mov ah,3eh
mov bx,handler
int 21h
jc Error2
endm
;--------------------------------------------------
;MACRO PARA LEER ARCHIVO
;--------------------------------------------------
Leer macro handler,buffer,numbytes
mov ah,3fh
mov bx,handler
mov cx,numbytes
lea dx,buffer
int 21h
jc  Error3
endm
;--------------------------------------------------
;MACRO PARA LIMPIAR VARIABLES
;--------------------------------------------------
Limpiar macro buffer,numbytes,caracter
LOCAL Repetir
xor si,si
xor cx,cx
mov cx,numbytes
Repetir:
mov buffer[si],caracter
inc si
Loop Repetir
endm
;--------------------------------------------------
;MACRO PARA CREAR ARCHIVO
;--------------------------------------------------
CrearArchivo macro buffer,handler
mov ah,3ch
mov cx,00h
lea dx,buffer
int 21h
jc Error4
mov handler,ax
endm
;--------------------------------------------------
;MACRO PARA ESCRIBIR ARCHIVO
;--------------------------------------------------
Escribir macro handler,buffer,numbytes
mov ah,40h
mov bx,handler
mov cx,numbytes
lea dx,buffer
int 21h
jc Error5
endm
;--------------------------------------------------
;MACRO PARA IMPRIMIR EN PANTALLA
;--------------------------------------------------
mostrar macro texto
    mov ax,@data    
    mov ds,ax  
    mov ah,09h ; numero de funcion para imprimir cadena en pantalla
;igual a lea dx,cadena,inicializa en dx la posicion donde comienza la cadena
    mov dx,offset texto 
    int 21h
endm

;--------------------------------------------------
;MACRO PARA OBTENER CARACTER
;--------------------------------------------------
getchar macro
mov ah,01h
int 21h
endm
;--------------------------------------------------
;MACRO PARA OBTENER TEXTO
;--------------------------------------------------
ObtenerTexto macro buffer
LOCAL obtenerchar,FinOT
xor si,si 	  ;igual a mov si,0
obtenerchar:
getchar
cmp al,0dh 	  ;ascii del salto de linea en hexadecimal
je FinOT
mov buffer[si],al ;mov destino,fuente
inc si ;si=si+1
jmp obtenerchar
FinOT:
mov al,24h ;ascii del signo dolar
mov buffer[si],al
endm
;--------------------------------------------------
;MACRO PARA OBTENER RUTA
;--------------------------------------------------
ObtenerRuta macro buffer
LOCAL obtenerchar,FinOT
xor si,si ; igual a mov si,0
obtenerchar:
getchar
cmp al,0dh 
; ascii del salto de linea en hexadecimal
je FinOT
mov buffer[si],al ;mov destino,fuente
inc si ;si=si+1
jmp obtenerchar
FinOT:
mov al,00h ;ascii del signo null
mov buffer[si],al
endm

;--------------------------------------------------
;MACRO PARA OBTENER RUTA
;--------------------------------------------------
ArreglandoMatriz macro 

	mov TFJC +0*MaMax+0,56 ;
	mov TFJC +2*MaMax+0,55 ;
	mov TFJC +4*MaMax+0,54 ;
	mov TFJC +6*MaMax+0,53 ;
	mov TFJC +8*MaMax+0,52 ;
	mov TFJC +10*MaMax+0,51;
	mov TFJC +12*MaMax+0,50;
	mov TFJC +14*MaMax+0,49;

	mov TFJC +1*MaMax+0,32 ;
	mov TFJC +3*MaMax+0,32 ;
	mov TFJC +5*MaMax+0,32 ;
	mov TFJC +7*MaMax+0,32 ;
	mov TFJC +9*MaMax+0,32 ;
	mov TFJC +11*MaMax+0,32;
	mov TFJC +13*MaMax+0,32;
	mov TFJC +15*MaMax+0,32;

	mov TFJC +15*MaMax+2,65 ;
	mov TFJC +15*MaMax+6,66 ;
	mov TFJC +15*MaMax+10,67 ;
	mov TFJC +15*MaMax+14,68 ;
	mov TFJC +15*MaMax+18,69 ;
	mov TFJC +15*MaMax+22,70;
	mov TFJC +15*MaMax+26,71;
	mov TFJC +15*MaMax+30,72;

	mov TFJC +15*MaMax+0,32 ;
	mov TFJC +15*MaMax+1,32 ;
	mov TFJC +15*MaMax+3,32 ;
	mov TFJC +15*MaMax+4,32 ;
	mov TFJC +15*MaMax+5,32 ;
	mov TFJC +15*MaMax+7,32;
	mov TFJC +15*MaMax+8,32;
	mov TFJC +15*MaMax+9,32;
	mov TFJC +15*MaMax+11,32 ;
	mov TFJC +15*MaMax+12,32 ;
	mov TFJC +15*MaMax+13,32 ;
	mov TFJC +15*MaMax+15,32 ;
	mov TFJC +15*MaMax+16,32 ;
	mov TFJC +15*MaMax+17,32;
	mov TFJC +15*MaMax+19,32;
	mov TFJC +15*MaMax+20,32;
	mov TFJC +15*MaMax+21,32;
	mov TFJC +15*MaMax+23,32;
	mov TFJC +15*MaMax+24,32;
	mov TFJC +15*MaMax+25,32;
	mov TFJC +15*MaMax+27,32;
	mov TFJC +15*MaMax+28,32;
	mov TFJC +15*MaMax+29,32;

	mov TFJC +0*MaMax+1,32;
	mov TFJC +0*MaMax+2,32;
	mov TFJC +0*MaMax+3,32;
	mov TFJC +0*MaMax+6,32;
	mov TFJC +0*MaMax+7,32;
	mov TFJC +0*MaMax+10,32;
	mov TFJC +0*MaMax+11,32;
	mov TFJC +0*MaMax+14,32;
	mov TFJC +0*MaMax+15,32;
	mov TFJC +0*MaMax+18,32;
	mov TFJC +0*MaMax+19,32;
	mov TFJC +0*MaMax+22,32;
	mov TFJC +0*MaMax+23,32;
	mov TFJC +0*MaMax+26,32;
	mov TFJC +0*MaMax+27,32;
	mov TFJC +0*MaMax+29,45;
	mov TFJC +0*MaMax+30,32;

	mov TFJC +2*MaMax+1,32;
	mov TFJC +2*MaMax+2,32;
	mov TFJC +2*MaMax+3,32;
	mov TFJC +2*MaMax+6,32;
	mov TFJC +2*MaMax+7,32;
	mov TFJC +2*MaMax+10,32;
	mov TFJC +2*MaMax+11,32;
	mov TFJC +2*MaMax+14,32;
	mov TFJC +2*MaMax+15,32;
	mov TFJC +2*MaMax+18,32;
	mov TFJC +2*MaMax+19,32;
	mov TFJC +2*MaMax+22,32;
	mov TFJC +2*MaMax+23,32;
	mov TFJC +2*MaMax+26,32;
	mov TFJC +2*MaMax+27,32;
	mov TFJC +2*MaMax+29,45;
	mov TFJC +2*MaMax+30,32;

	mov TFJC +4*MaMax+1,32;
	mov TFJC +4*MaMax+2,32;
	mov TFJC +4*MaMax+3,32;
	mov TFJC +4*MaMax+6,32;
	mov TFJC +4*MaMax+7,32;
	mov TFJC +4*MaMax+10,32;
	mov TFJC +4*MaMax+11,32;
	mov TFJC +4*MaMax+14,32;
	mov TFJC +4*MaMax+15,32;
	mov TFJC +4*MaMax+18,32;
	mov TFJC +4*MaMax+19,32;
	mov TFJC +4*MaMax+22,32;
	mov TFJC +4*MaMax+23,32;
	mov TFJC +4*MaMax+26,32;
	mov TFJC +4*MaMax+27,32;
	mov TFJC +4*MaMax+29,45;
	mov TFJC +4*MaMax+30,32;

	mov TFJC +6*MaMax+1,32;
	mov TFJC +6*MaMax+2,32;
	mov TFJC +6*MaMax+3,32;
	mov TFJC +6*MaMax+6,32;
	mov TFJC +6*MaMax+7,32;
	mov TFJC +6*MaMax+10,32;
	mov TFJC +6*MaMax+11,32;
	mov TFJC +6*MaMax+14,32;
	mov TFJC +6*MaMax+15,32;
	mov TFJC +6*MaMax+18,32;
	mov TFJC +6*MaMax+19,32;
	mov TFJC +6*MaMax+22,32;
	mov TFJC +6*MaMax+23,32;
	mov TFJC +6*MaMax+26,32;
	mov TFJC +6*MaMax+27,32;
	mov TFJC +6*MaMax+29,45;
	mov TFJC +6*MaMax+30,32;

	mov TFJC +8*MaMax+1,32;
	mov TFJC +8*MaMax+2,32;
	mov TFJC +8*MaMax+3,32;
	mov TFJC +8*MaMax+6,32;
	mov TFJC +8*MaMax+7,32;
	mov TFJC +8*MaMax+10,32;
	mov TFJC +8*MaMax+11,32;
	mov TFJC +8*MaMax+14,32;
	mov TFJC +8*MaMax+15,32;
	mov TFJC +8*MaMax+18,32;
	mov TFJC +8*MaMax+19,32;
	mov TFJC +8*MaMax+22,32;
	mov TFJC +8*MaMax+23,32;
	mov TFJC +8*MaMax+26,32;
	mov TFJC +8*MaMax+27,32;
	mov TFJC +8*MaMax+29,45;
	mov TFJC +8*MaMax+30,32;


	mov TFJC +10*MaMax+1,32;
	mov TFJC +10*MaMax+2,32;
	mov TFJC +10*MaMax+3,32;
	mov TFJC +10*MaMax+6,32;
	mov TFJC +10*MaMax+7,32;
	mov TFJC +10*MaMax+10,32;
	mov TFJC +10*MaMax+11,32;
	mov TFJC +10*MaMax+14,32;
	mov TFJC +10*MaMax+15,32;
	mov TFJC +10*MaMax+18,32;
	mov TFJC +10*MaMax+19,32;
	mov TFJC +10*MaMax+22,32;
	mov TFJC +10*MaMax+23,32;
	mov TFJC +10*MaMax+26,32;
	mov TFJC +10*MaMax+27,32;
	mov TFJC +10*MaMax+29,45;
	mov TFJC +10*MaMax+30,32;

	mov TFJC +12*MaMax+1,32;
	mov TFJC +12*MaMax+2,32;
	mov TFJC +12*MaMax+3,32;
	mov TFJC +12*MaMax+6,32;
	mov TFJC +12*MaMax+7,32;
	mov TFJC +12*MaMax+10,32;
	mov TFJC +12*MaMax+11,32;
	mov TFJC +12*MaMax+14,32;
	mov TFJC +12*MaMax+15,32;
	mov TFJC +12*MaMax+18,32;
	mov TFJC +12*MaMax+19,32;
	mov TFJC +12*MaMax+22,32;
	mov TFJC +12*MaMax+23,32;
	mov TFJC +12*MaMax+26,32;
	mov TFJC +12*MaMax+27,32;
	mov TFJC +12*MaMax+29,45;
	mov TFJC +12*MaMax+30,32;

	mov TFJC +14*MaMax+1,32;
	mov TFJC +14*MaMax+2,32;
	mov TFJC +14*MaMax+3,32;
	mov TFJC +14*MaMax+6,32;
	mov TFJC +14*MaMax+7,32;
	mov TFJC +14*MaMax+10,32;
	mov TFJC +14*MaMax+11,32;
	mov TFJC +14*MaMax+14,32;
	mov TFJC +14*MaMax+15,32;
	mov TFJC +14*MaMax+18,32;
	mov TFJC +14*MaMax+19,32;
	mov TFJC +14*MaMax+22,32;
	mov TFJC +14*MaMax+23,32;
	mov TFJC +14*MaMax+26,32;
	mov TFJC +14*MaMax+27,32;
	mov TFJC +14*MaMax+29,45;
	mov TFJC +14*MaMax+30,32;


	mov TFJC +1*MaMax+0,32;
	mov TFJC +1*MaMax+1,32;
	mov TFJC +1*MaMax+2,124;
	mov TFJC +1*MaMax+3,32;
	mov TFJC +1*MaMax+4,32;
	mov TFJC +1*MaMax+5,32;
	mov TFJC +1*MaMax+6,124;
	mov TFJC +1*MaMax+7,32;
	mov TFJC +1*MaMax+8,32;
	mov TFJC +1*MaMax+9,32;
	mov TFJC +1*MaMax+10,124;
	mov TFJC +1*MaMax+11,32;
	mov TFJC +1*MaMax+12,32;
	mov TFJC +1*MaMax+13,32;
	mov TFJC +1*MaMax+14,124;
	mov TFJC +1*MaMax+15,32;
	mov TFJC +1*MaMax+16,32;
	mov TFJC +1*MaMax+17,32;
	mov TFJC +1*MaMax+18,124;
	mov TFJC +1*MaMax+19,32;
	mov TFJC +1*MaMax+20,32;
	mov TFJC +1*MaMax+21,32;
	mov TFJC +1*MaMax+22,124;
	mov TFJC +1*MaMax+23,32;
	mov TFJC +1*MaMax+24,32;
	mov TFJC +1*MaMax+25,32;
	mov TFJC +1*MaMax+26,124;
	mov TFJC +1*MaMax+27,32;
	mov TFJC +1*MaMax+28,32;
	mov TFJC +1*MaMax+29,32;
	mov TFJC +1*MaMax+30,124;

	mov TFJC +3*MaMax+0,32;
	mov TFJC +3*MaMax+1,32;
	mov TFJC +3*MaMax+2,124;
	mov TFJC +3*MaMax+3,32;
	mov TFJC +3*MaMax+4,32;
	mov TFJC +3*MaMax+5,32;
	mov TFJC +3*MaMax+6,124;
	mov TFJC +3*MaMax+7,32;
	mov TFJC +3*MaMax+8,32;
	mov TFJC +3*MaMax+9,32;
	mov TFJC +3*MaMax+10,124;
	mov TFJC +3*MaMax+11,32;
	mov TFJC +3*MaMax+12,32;
	mov TFJC +3*MaMax+13,32;
	mov TFJC +3*MaMax+14,124;
	mov TFJC +3*MaMax+15,32;
	mov TFJC +3*MaMax+16,32;
	mov TFJC +3*MaMax+17,32;
	mov TFJC +3*MaMax+18,124;
	mov TFJC +3*MaMax+19,32;
	mov TFJC +3*MaMax+20,32;
	mov TFJC +3*MaMax+21,32;
	mov TFJC +3*MaMax+22,124;
	mov TFJC +3*MaMax+23,32;
	mov TFJC +3*MaMax+24,32;
	mov TFJC +3*MaMax+25,32;
	mov TFJC +3*MaMax+26,124;
	mov TFJC +3*MaMax+27,32;
	mov TFJC +3*MaMax+28,32;
	mov TFJC +3*MaMax+29,32;
	mov TFJC +3*MaMax+30,124;

	mov TFJC +5*MaMax+0,32;
	mov TFJC +5*MaMax+1,32;
	mov TFJC +5*MaMax+2,124;
	mov TFJC +5*MaMax+3,32;
	mov TFJC +5*MaMax+4,32;
	mov TFJC +5*MaMax+5,32;
	mov TFJC +5*MaMax+6,124;
	mov TFJC +5*MaMax+7,32;
	mov TFJC +5*MaMax+8,32;
	mov TFJC +5*MaMax+9,32;
	mov TFJC +5*MaMax+10,124;
	mov TFJC +5*MaMax+11,32;
	mov TFJC +5*MaMax+12,32;
	mov TFJC +5*MaMax+13,32;
	mov TFJC +5*MaMax+14,124;
	mov TFJC +5*MaMax+15,32;
	mov TFJC +5*MaMax+16,32;
	mov TFJC +5*MaMax+17,32;
	mov TFJC +5*MaMax+18,124;
	mov TFJC +5*MaMax+19,32;
	mov TFJC +5*MaMax+20,32;
	mov TFJC +5*MaMax+21,32;
	mov TFJC +5*MaMax+22,124;
	mov TFJC +5*MaMax+23,32;
	mov TFJC +5*MaMax+24,32;
	mov TFJC +5*MaMax+25,32;
	mov TFJC +5*MaMax+26,124;
	mov TFJC +5*MaMax+27,32;
	mov TFJC +5*MaMax+28,32;
	mov TFJC +5*MaMax+29,32;
	mov TFJC +5*MaMax+30,124;

	mov TFJC +7*MaMax+0,32;
	mov TFJC +7*MaMax+1,32;
	mov TFJC +7*MaMax+2,124;
	mov TFJC +7*MaMax+3,32;
	mov TFJC +7*MaMax+4,32;
	mov TFJC +7*MaMax+5,32;
	mov TFJC +7*MaMax+6,124;
	mov TFJC +7*MaMax+7,32;
	mov TFJC +7*MaMax+8,32;
	mov TFJC +7*MaMax+9,32;
	mov TFJC +7*MaMax+10,124;
	mov TFJC +7*MaMax+11,32;
	mov TFJC +7*MaMax+12,32;
	mov TFJC +7*MaMax+13,32;
	mov TFJC +7*MaMax+14,124;
	mov TFJC +7*MaMax+15,32;
	mov TFJC +7*MaMax+16,32;
	mov TFJC +7*MaMax+17,32;
	mov TFJC +7*MaMax+18,124;
	mov TFJC +7*MaMax+19,32;
	mov TFJC +7*MaMax+20,32;
	mov TFJC +7*MaMax+21,32;
	mov TFJC +7*MaMax+22,124;
	mov TFJC +7*MaMax+23,32;
	mov TFJC +7*MaMax+24,32;
	mov TFJC +7*MaMax+25,32;
	mov TFJC +7*MaMax+26,124;
	mov TFJC +7*MaMax+27,32;
	mov TFJC +7*MaMax+28,32;
	mov TFJC +7*MaMax+29,32;
	mov TFJC +7*MaMax+30,124;

	mov TFJC +9*MaMax+0,32;
	mov TFJC +9*MaMax+1,32;
	mov TFJC +9*MaMax+2,124;
	mov TFJC +9*MaMax+3,32;
	mov TFJC +9*MaMax+4,32;
	mov TFJC +9*MaMax+5,32;
	mov TFJC +9*MaMax+6,124;
	mov TFJC +9*MaMax+7,32;
	mov TFJC +9*MaMax+8,32;
	mov TFJC +9*MaMax+9,32;
	mov TFJC +9*MaMax+10,124;
	mov TFJC +9*MaMax+11,32;
	mov TFJC +9*MaMax+12,32;
	mov TFJC +9*MaMax+13,32;
	mov TFJC +9*MaMax+14,124;
	mov TFJC +9*MaMax+15,32;
	mov TFJC +9*MaMax+16,32;
	mov TFJC +9*MaMax+17,32;
	mov TFJC +9*MaMax+18,124;
	mov TFJC +9*MaMax+19,32;
	mov TFJC +9*MaMax+20,32;
	mov TFJC +9*MaMax+21,32;
	mov TFJC +9*MaMax+22,124;
	mov TFJC +9*MaMax+23,32;
	mov TFJC +9*MaMax+24,32;
	mov TFJC +9*MaMax+25,32;
	mov TFJC +9*MaMax+26,124;
	mov TFJC +9*MaMax+27,32;
	mov TFJC +9*MaMax+28,32;
	mov TFJC +9*MaMax+29,32;
	mov TFJC +9*MaMax+30,124;

	mov TFJC +11*MaMax+0,32;
	mov TFJC +11*MaMax+1,32;
	mov TFJC +11*MaMax+2,124;
	mov TFJC +11*MaMax+3,32;
	mov TFJC +11*MaMax+4,32;
	mov TFJC +11*MaMax+5,32;
	mov TFJC +11*MaMax+6,124;
	mov TFJC +11*MaMax+7,32;
	mov TFJC +11*MaMax+8,32;
	mov TFJC +11*MaMax+9,32;
	mov TFJC +11*MaMax+10,124;
	mov TFJC +11*MaMax+11,32;
	mov TFJC +11*MaMax+12,32;
	mov TFJC +11*MaMax+13,32;
	mov TFJC +11*MaMax+14,124;
	mov TFJC +11*MaMax+15,32;
	mov TFJC +11*MaMax+16,32;
	mov TFJC +11*MaMax+17,32;
	mov TFJC +11*MaMax+18,124;
	mov TFJC +11*MaMax+19,32;
	mov TFJC +11*MaMax+20,32;
	mov TFJC +11*MaMax+21,32;
	mov TFJC +11*MaMax+22,124;
	mov TFJC +11*MaMax+23,32;
	mov TFJC +11*MaMax+24,32;
	mov TFJC +11*MaMax+25,32;
	mov TFJC +11*MaMax+26,124;
	mov TFJC +11*MaMax+27,32;
	mov TFJC +11*MaMax+28,32;
	mov TFJC +11*MaMax+29,32;
	mov TFJC +11*MaMax+30,124;

	mov TFJC +13*MaMax+0,32;
	mov TFJC +13*MaMax+1,32;
	mov TFJC +13*MaMax+2,124;
	mov TFJC +13*MaMax+3,32;
	mov TFJC +13*MaMax+4,32;
	mov TFJC +13*MaMax+5,32;
	mov TFJC +13*MaMax+6,124;
	mov TFJC +13*MaMax+7,32;
	mov TFJC +13*MaMax+8,32;
	mov TFJC +13*MaMax+9,32;
	mov TFJC +13*MaMax+10,124;
	mov TFJC +13*MaMax+11,32;
	mov TFJC +13*MaMax+12,32;
	mov TFJC +13*MaMax+13,32;
	mov TFJC +13*MaMax+14,124;
	mov TFJC +13*MaMax+15,32;
	mov TFJC +13*MaMax+16,32;
	mov TFJC +13*MaMax+17,32;
	mov TFJC +13*MaMax+18,124;
	mov TFJC +13*MaMax+19,32;
	mov TFJC +13*MaMax+20,32;
	mov TFJC +13*MaMax+21,32;
	mov TFJC +13*MaMax+22,124;
	mov TFJC +13*MaMax+23,32;
	mov TFJC +13*MaMax+24,32;
	mov TFJC +13*MaMax+25,32;
	mov TFJC +13*MaMax+26,124;
	mov TFJC +13*MaMax+27,32;
	mov TFJC +13*MaMax+28,32;
	mov TFJC +13*MaMax+29,32;
	mov TFJC +13*MaMax+30,124;
endm
;--------------------------------------------------
;procedimiento principal
;--------------------------------------------------
main  proc              
;Inicia proceso
;-------------------------------------------- 
;---------------------------------------------
;MOSTRAMOS EL ENCABEZADO
;----------------------------------------------
    
     
	


   encabezado:

	   mostrar enc0
	   mostrar enc1
	   mostrar enc2
	   mostrar enc3
	   mostrar enc4
	   mostrar enc5
	   mostrar enc6

	;mov JuegoFinal[0],'m'
	;mov JuegoFinal[1],'s'

	

    menufirst:
    ArreglandoMatriz 	

    mostrar LineaVacia 
	mostrar LineaVacia 
	mostrar msg1 
	mostrar msg2 
	mostrar msg3 
	mostrar Vopcion

	   getchar
	   cmp al,31h ;codigo ascii del numero 1 en hexadecimal
           je opcion1 
	   cmp al,32h ;codigo ascii del numero 2 en hexadecimal 
           je opcion2 
	   cmp al,33h ;codigo ascii del numero 3 en hexadecimal 
           je opcion3	   
;Si es cualquier otro caracter regresa al menu principal
	   jmp opcion4


     opcion1:
	mostrar msga
	mov contjcFN,0
	mov contjcFB,0
 	jmp Matriz
	jmp menufirst
     opcion2:
	mostrar  msgb 
	mostrar EjmGuardar
	mostrar LineaVacia 	 	
	Limpiar bufferentrada,SIZEOF bufferentrada,24h
	ObtenerRuta bufferentrada 
	;mostrar bufferentrada 
	Abrir bufferentrada,handlerentrada 	
	Limpiar bufferInformacion,SIZEOF bufferInformacion,24h
    Leer handlerentrada,bufferInformacion,SIZEOF bufferInformacion 
	;mostrar bufferInformacion
   	Cerrar handlerentrada 

   	mov al,bufferInformacion[2]	
	mov TFJC+0*MaMax+2,al
	mov al,bufferInformacion[3]
	mov TFJC+0*MaMax+3,al

	mov al,bufferInformacion[6]	
	mov TFJC+0*MaMax+6,al
	mov al,bufferInformacion[7]
	mov TFJC+0*MaMax+7,al	

   	mov al,bufferInformacion[10]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+0*MaMax+10,al
	mov al,bufferInformacion[11]
	mov TFJC+0*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[14]	
	mov TFJC+0*MaMax+14,al
	mov al,bufferInformacion[15]
	mov TFJC+0*MaMax+15,al

	mov al,bufferInformacion[18]	
	mov TFJC+0*MaMax+18,al
	mov al,bufferInformacion[19]
	mov TFJC+0*MaMax+19,al
	
	mov al,bufferInformacion[22]	
	mov TFJC+0*MaMax+22,al
	mov al,bufferInformacion[23]
	mov TFJC+0*MaMax+23,al

	mov al,bufferInformacion[26]	
	mov TFJC+0*MaMax+26,al
	mov al,bufferInformacion[27]
	mov TFJC+0*MaMax+27,al

	mov al,bufferInformacion[29]	
	mov TFJC+0*MaMax+29,al
	mov al,bufferInformacion[30]
	mov TFJC+0*MaMax+30,al
;-------------------------------------
	
	mov al,bufferInformacion[66]	
	mov TFJC+2*MaMax+2,al
	mov al,bufferInformacion[67]
	mov TFJC+2*MaMax+3,al

	mov al,bufferInformacion[70]	
	mov TFJC+2*MaMax+6,al
	mov al,bufferInformacion[71]
	mov TFJC+2*MaMax+7,al


   	mov al,bufferInformacion[74]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+2*MaMax+10,al
	mov al,bufferInformacion[75]
	mov TFJC+2*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[78]	
	mov TFJC+2*MaMax+14,al
	mov al,bufferInformacion[79]
	mov TFJC+2*MaMax+15,al

	mov al,bufferInformacion[82]	
	mov TFJC+2*MaMax+18,al
	mov al,bufferInformacion[83]
	mov TFJC+2*MaMax+19,al
	
	mov al,bufferInformacion[86]	
	mov TFJC+2*MaMax+22,al
	mov al,bufferInformacion[87]
	mov TFJC+2*MaMax+23,al

	mov al,bufferInformacion[90]	
	mov TFJC+2*MaMax+26,al
	mov al,bufferInformacion[91]
	mov TFJC+2*MaMax+27,al

	mov al,bufferInformacion[93]	
	mov TFJC+2*MaMax+29,al
	mov al,bufferInformacion[94]
	mov TFJC+2*MaMax+30,al

;-------------------------------------
	;-------------------------------------
	
	mov al,bufferInformacion[130]	
	mov TFJC+4*MaMax+2,al
	mov al,bufferInformacion[131]
	mov TFJC+4*MaMax+3,al

	mov al,bufferInformacion[134]	
	mov TFJC+4*MaMax+6,al
	mov al,bufferInformacion[135]
	mov TFJC+4*MaMax+7,al


   	mov al,bufferInformacion[138]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+4*MaMax+10,al
	mov al,bufferInformacion[139]
	mov TFJC+4*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[142]	
	mov TFJC+4*MaMax+14,al
	mov al,bufferInformacion[143]
	mov TFJC+4*MaMax+15,al

	mov al,bufferInformacion[146]	
	mov TFJC+4*MaMax+18,al
	mov al,bufferInformacion[147]
	mov TFJC+4*MaMax+19,al
	
	mov al,bufferInformacion[150]	
	mov TFJC+4*MaMax+22,al
	mov al,bufferInformacion[151]
	mov TFJC+4*MaMax+23,al

	mov al,bufferInformacion[154]	
	mov TFJC+4*MaMax+26,al
	mov al,bufferInformacion[155]
	mov TFJC+4*MaMax+27,al

	mov al,bufferInformacion[157]	
	mov TFJC+4*MaMax+29,al
	mov al,bufferInformacion[158]
	mov TFJC+4*MaMax+30,al

;-------------------------------------
;-------------------------------------
	
	mov al,bufferInformacion[194]	
	mov TFJC+6*MaMax+2,al
	mov al,bufferInformacion[195]
	mov TFJC+6*MaMax+3,al

	mov al,bufferInformacion[198]	
	mov TFJC+6*MaMax+6,al
	mov al,bufferInformacion[199]
	mov TFJC+6*MaMax+7,al


   	mov al,bufferInformacion[202]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+6*MaMax+10,al
	mov al,bufferInformacion[203]
	mov TFJC+6*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[206]	
	mov TFJC+6*MaMax+14,al
	mov al,bufferInformacion[207]
	mov TFJC+6*MaMax+15,al

	mov al,bufferInformacion[210]	
	mov TFJC+6*MaMax+18,al
	mov al,bufferInformacion[211]
	mov TFJC+6*MaMax+19,al
	
	mov al,bufferInformacion[214]	
	mov TFJC+6*MaMax+22,al
	mov al,bufferInformacion[215]
	mov TFJC+6*MaMax+23,al

	mov al,bufferInformacion[218]	
	mov TFJC+6*MaMax+26,al
	mov al,bufferInformacion[219]
	mov TFJC+6*MaMax+27,al

	mov al,bufferInformacion[221]	
	mov TFJC+6*MaMax+29,al
	mov al,bufferInformacion[222]
	mov TFJC+6*MaMax+30,al

;-------------------------------------
;-------------------------------------
	
	mov al,bufferInformacion[258]	
	mov TFJC+8*MaMax+2,al
	mov al,bufferInformacion[259]
	mov TFJC+8*MaMax+3,al

	mov al,bufferInformacion[262]	
	mov TFJC+8*MaMax+6,al
	mov al,bufferInformacion[263]
	mov TFJC+8*MaMax+7,al


   	mov al,bufferInformacion[266]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+8*MaMax+10,al
	mov al,bufferInformacion[267]
	mov TFJC+8*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[270]	
	mov TFJC+8*MaMax+14,al
	mov al,bufferInformacion[271]
	mov TFJC+8*MaMax+15,al

	mov al,bufferInformacion[274]	
	mov TFJC+8*MaMax+18,al
	mov al,bufferInformacion[275]
	mov TFJC+8*MaMax+19,al
	
	mov al,bufferInformacion[278]	
	mov TFJC+8*MaMax+22,al
	mov al,bufferInformacion[279]
	mov TFJC+8*MaMax+23,al

	mov al,bufferInformacion[282]	
	mov TFJC+8*MaMax+26,al
	mov al,bufferInformacion[283]
	mov TFJC+8*MaMax+27,al

	mov al,bufferInformacion[285]	
	mov TFJC+8*MaMax+29,al
	mov al,bufferInformacion[286]
	mov TFJC+8*MaMax+30,al

;-------------------------------------
;-------------------------------------
	
	mov al,bufferInformacion[322]	
	mov TFJC+10*MaMax+2,al
	mov al,bufferInformacion[323]
	mov TFJC+10*MaMax+3,al

	mov al,bufferInformacion[326]	
	mov TFJC+10*MaMax+6,al
	mov al,bufferInformacion[327]
	mov TFJC+10*MaMax+7,al


   	mov al,bufferInformacion[330]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+10*MaMax+10,al
	mov al,bufferInformacion[331]
	mov TFJC+10*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[334]	
	mov TFJC+10*MaMax+14,al
	mov al,bufferInformacion[335]
	mov TFJC+10*MaMax+15,al

	mov al,bufferInformacion[338]	
	mov TFJC+10*MaMax+18,al
	mov al,bufferInformacion[339]
	mov TFJC+10*MaMax+19,al
	
	mov al,bufferInformacion[342]	
	mov TFJC+10*MaMax+22,al
	mov al,bufferInformacion[343]
	mov TFJC+10*MaMax+23,al

	mov al,bufferInformacion[346]	
	mov TFJC+10*MaMax+26,al
	mov al,bufferInformacion[347]
	mov TFJC+10*MaMax+27,al

	mov al,bufferInformacion[349]	
	mov TFJC+10*MaMax+29,al
	mov al,bufferInformacion[350]
	mov TFJC+10*MaMax+30,al

;-------------------------------------
;-------------------------------------
	
	mov al,bufferInformacion[386]	
	mov TFJC+12*MaMax+2,al
	mov al,bufferInformacion[387]
	mov TFJC+12*MaMax+3,al

	mov al,bufferInformacion[390]	
	mov TFJC+12*MaMax+6,al
	mov al,bufferInformacion[391]
	mov TFJC+12*MaMax+7,al


   	mov al,bufferInformacion[394]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+12*MaMax+10,al
	mov al,bufferInformacion[395]
	mov TFJC+12*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[398]	
	mov TFJC+12*MaMax+14,al
	mov al,bufferInformacion[399]
	mov TFJC+12*MaMax+15,al

	mov al,bufferInformacion[402]	
	mov TFJC+12*MaMax+18,al
	mov al,bufferInformacion[403]
	mov TFJC+12*MaMax+19,al
	
	mov al,bufferInformacion[406]	
	mov TFJC+12*MaMax+22,al
	mov al,bufferInformacion[407]
	mov TFJC+12*MaMax+23,al

	mov al,bufferInformacion[410]	
	mov TFJC+12*MaMax+26,al
	mov al,bufferInformacion[411]
	mov TFJC+12*MaMax+27,al

	mov al,bufferInformacion[413]	
	mov TFJC+12*MaMax+29,al
	mov al,bufferInformacion[414]
	mov TFJC+12*MaMax+30,al

;-------------------------------------
;-------------------------------------
	
	mov al,bufferInformacion[450]	
	mov TFJC+14*MaMax+2,al
	mov al,bufferInformacion[451]
	mov TFJC+14*MaMax+3,al

	mov al,bufferInformacion[454]	
	mov TFJC+14*MaMax+6,al
	mov al,bufferInformacion[455]
	mov TFJC+14*MaMax+7,al


   	mov al,bufferInformacion[458]
	;mov fichaxFuera,al
	;mostrar fichaxFuera
	mov TFJC+14*MaMax+10,al
	mov al,bufferInformacion[459]
	mov TFJC+14*MaMax+11,al
	;mov fichaxFuera,al
	;mostrar fichaxFuera

	mov al,bufferInformacion[462]	
	mov TFJC+14*MaMax+14,al
	mov al,bufferInformacion[463]
	mov TFJC+14*MaMax+15,al

	mov al,bufferInformacion[466]	
	mov TFJC+14*MaMax+18,al
	mov al,bufferInformacion[467]
	mov TFJC+14*MaMax+19,al
	
	mov al,bufferInformacion[470]	
	mov TFJC+14*MaMax+22,al
	mov al,bufferInformacion[471]
	mov TFJC+14*MaMax+23,al

	mov al,bufferInformacion[474]	
	mov TFJC+14*MaMax+26,al
	mov al,bufferInformacion[475]
	mov TFJC+14*MaMax+27,al

	mov al,bufferInformacion[477]	
	mov TFJC+14*MaMax+29,al
	mov al,bufferInformacion[478]
	mov TFJC+14*MaMax+30,al
;-------------------------------------
	
	jmp Matriz
	;jmp menufirst
     opcion3:
    	mostrar  msgc 
	jmp salir
     opcion4:
	mostrar msgd 
        jmp menufirst
    
     
    Error1:
	mostrar erroru
	jmp menufirst

    Error2:
	mostrar errord
	jmp menufirst

    Error3:
	mostrar errorleer
	jmp menufirst

    Error4:
	mostrar errorc 
	jmp menufirst

     Error5:
	mostrar errort
	jmp menufirst 

 	opcion7:	
	mostrar LineaVacia 
	mostrar archid
	mostrar EjmGuardar
	mostrar LineaVacia 
	Limpiar bufferentrada,SIZEOF bufferentrada,24h
	ObtenerRuta bufferentrada 
	CrearArchivo bufferentrada,handlerentrada 	
    Escribir handlerentrada,TFJC,SIZEOF TFJC
   	Cerrar handlerentrada 	
   	mostrar archic
	jmp Matriz    


	opcion6:
	;mostrar LineaVacia 
	;mostrar archia 
	;mostrar LineaVacia 		
    ;Limpiar bufferentrada,SIZEOF bufferentrada,24h
	;ObtenerRuta bufferentrada 
	;mostrar entradArchivo
	;mostrar bufferentrada 
	CrearArchivo entradArchivo,handlerentrada 
  
	Escribir handlerentrada,html1,SIZEOF html1
	Escribir handlerentrada,html2,SIZEOF html2
	
	;fila cero
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[0*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+3],'B'
	je 	ficha03
	cmp TFJC[0*MaMax+3],'N'
	je 	ficha03N
	jmp fichasi03	
	ficha03:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi03	
	ficha03N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi03:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+7],'B'
	je 	ficha07
	cmp TFJC[0*MaMax+7],'N'
	je 	ficha07N
	jmp fichasi07	
	ficha07:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi07	
	ficha07N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi07:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+11],'B'
	je 	ficha011
	cmp TFJC[0*MaMax+11],'N'
	je 	ficha011N
	jmp fichasi011	
	ficha011:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi011	
	ficha011N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi011:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+15],'B'
	je 	ficha015
	cmp TFJC[0*MaMax+15],'N'
	je 	ficha015N
	jmp fichasi015	
	ficha015:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi015	
	ficha015N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi015:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+19],'B'
	je 	ficha019
	cmp TFJC[0*MaMax+19],'N'
	je 	ficha019N
	jmp fichasi019	
	ficha019:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi019	
	ficha019N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi019:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+23],'B'
	je 	ficha023
	cmp TFJC[0*MaMax+23],'N'
	je 	ficha023N
	jmp fichasi023	
	ficha023:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi023	
	ficha023N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi023:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+27],'B'
	je 	ficha027
	cmp TFJC[0*MaMax+27],'N'
	je 	ficha027N
	jmp fichasi027	
	ficha027:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi027	
	ficha027N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi027:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[0*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[0*MaMax+30],'B'
	je 	ficha030
	cmp TFJC[0*MaMax+30],'N'
	je 	ficha030N
	jmp fichasi030	
	ficha030:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi030	
	ficha030N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi030:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila cero
	;fila uno
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila uno
	;fila  dos
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[2*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[2*MaMax+3],'B'
	je 	ficha23
	cmp TFJC[2*MaMax+3],'N'
	je 	ficha23N
	jmp fichasi23	
	ficha23:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi23	
	ficha23N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi23:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[2*MaMax+7],'B'
	je 	ficha27
	cmp TFJC[2*MaMax+7],'N'
	je 	ficha27N
	jmp fichasi27	
	ficha27:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi27	
	ficha27N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi27:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[2*MaMax+11],'B'
	je 	ficha211
	cmp TFJC[2*MaMax+11],'N'
	je 	ficha211N
	jmp fichasi211	
	ficha211:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi211	
	ficha211N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi211:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[2*MaMax+15],'B'
	je 	ficha215
	cmp TFJC[2*MaMax+15],'N'
	je 	ficha215N
	jmp fichasi215	
	ficha215:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi215	
	ficha215N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi215:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[2*MaMax+19],'B'
	je 	ficha219
	cmp TFJC[2*MaMax+19],'N'
	je 	ficha219N
	jmp fichasi219	
	ficha219:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi219	
	ficha219N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi219:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[2*MaMax+23],'B'
	je 	ficha223
	cmp TFJC[2*MaMax+23],'N'
	je 	ficha223N
	jmp fichasi223	
	ficha223:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi223	
	ficha223N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi223:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[2*MaMax+27],'B'
	je 	ficha227
	cmp TFJC[2*MaMax+27],'N'
	je 	ficha227N
	jmp fichasi227	
	ficha227:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi227	
	ficha227N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi227:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[2*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[2*MaMax+30],'B'
	je 	ficha230
	cmp TFJC[2*MaMax+30],'N'
	je 	ficha230N
	jmp fichasi230	
	ficha230:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi230	
	ficha230N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi230:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila dos
	;fila tres	
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila tres
	;fila  cuatro
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[4*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[4*MaMax+3],'B'
	je 	ficha43
	cmp TFJC[4*MaMax+3],'N'
	je 	ficha43N
	jmp fichasi43	
	ficha43:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi43	
	ficha43N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi43:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[4*MaMax+7],'B'
	je 	ficha47
	cmp TFJC[4*MaMax+7],'N'
	je 	ficha47N
	jmp fichasi47	
	ficha47:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi47	
	ficha47N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi47:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[4*MaMax+11],'B'
	je 	ficha411
	cmp TFJC[4*MaMax+11],'N'
	je 	ficha411N
	jmp fichasi411	
	ficha411:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi411	
	ficha411N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi411:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[4*MaMax+15],'B'
	je 	ficha415
	cmp TFJC[4*MaMax+15],'N'
	je 	ficha415N
	jmp fichasi415	
	ficha415:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi415	
	ficha415N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi415:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[4*MaMax+19],'B'
	je 	ficha419
	cmp TFJC[4*MaMax+19],'N'
	je 	ficha419N
	jmp fichasi419	
	ficha419:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi419	
	ficha419N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi419:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[4*MaMax+23],'B'
	je 	ficha423
	cmp TFJC[4*MaMax+23],'N'
	je 	ficha423N
	jmp fichasi423	
	ficha423:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi423	
	ficha423N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi423:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[4*MaMax+27],'B'
	je 	ficha427
	cmp TFJC[4*MaMax+27],'N'
	je 	ficha427N
	jmp fichasi427	
	ficha427:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi427	
	ficha427N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi427:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[4*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[4*MaMax+30],'B'
	je 	ficha430
	cmp TFJC[4*MaMax+30],'N'
	je 	ficha430N
	jmp fichasi430	
	ficha430:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi430	
	ficha430N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi430:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila cuatro
	;fila CINCO	
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila CINCO
	;fila  SIES 
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[6*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[6*MaMax+3],'B'
	je 	ficha63
	cmp TFJC[6*MaMax+3],'N'
	je 	ficha63N
	jmp fichasi63	
	ficha63:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi63	
	ficha63N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi63:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[6*MaMax+7],'B'
	je 	ficha67
	cmp TFJC[6*MaMax+7],'N'
	je 	ficha67N
	jmp fichasi67	
	ficha67:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi67	
	ficha67N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi67:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[6*MaMax+11],'B'
	je 	ficha611
	cmp TFJC[6*MaMax+11],'N'
	je 	ficha611N
	jmp fichasi611	
	ficha611:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi611	
	ficha611N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi611:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[6*MaMax+15],'B'
	je 	ficha615
	cmp TFJC[6*MaMax+15],'N'
	je 	ficha615N
	jmp fichasi615	
	ficha615:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi615	
	ficha615N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi615:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[6*MaMax+19],'B'
	je 	ficha619
	cmp TFJC[6*MaMax+19],'N'
	je 	ficha619N
	jmp fichasi619	
	ficha619:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi619	
	ficha619N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi619:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[6*MaMax+23],'B'
	je 	ficha623
	cmp TFJC[6*MaMax+23],'N'
	je 	ficha623N
	jmp fichasi623	
	ficha623:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi623	
	ficha623N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi623:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[6*MaMax+27],'B'
	je 	ficha627
	cmp TFJC[6*MaMax+27],'N'
	je 	ficha627N
	jmp fichasi627	
	ficha627:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi627	
	ficha627N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi627:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[6*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[6*MaMax+30],'B'
	je 	ficha630
	cmp TFJC[6*MaMax+30],'N'
	je 	ficha630N
	jmp fichasi630	
	ficha630:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi630	
	ficha630N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi630:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila SEIS 
	;fila SIETE
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila SIETE
	;fila  OCHO 
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[8*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[8*MaMax+3],'B'
	je 	ficha83
	cmp TFJC[8*MaMax+3],'N'
	je 	ficha83N
	jmp fichasi83	
	ficha83:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi83	
	ficha83N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi83:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[8*MaMax+7],'B'
	je 	ficha87
	cmp TFJC[8*MaMax+7],'N'
	je 	ficha87N
	jmp fichasi87	
	ficha87:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi87	
	ficha87N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi87:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[8*MaMax+11],'B'
	je 	ficha811
	cmp TFJC[8*MaMax+11],'N'
	je 	ficha811N
	jmp fichasi811	
	ficha811:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi811	
	ficha811N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi811:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[8*MaMax+15],'B'
	je 	ficha815
	cmp TFJC[8*MaMax+15],'N'
	je 	ficha815N
	jmp fichasi815	
	ficha815:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi815	
	ficha815N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi815:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[8*MaMax+19],'B'
	je 	ficha819
	cmp TFJC[8*MaMax+19],'N'
	je 	ficha819N
	jmp fichasi819	
	ficha819:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi819	
	ficha819N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi819:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[8*MaMax+23],'B'
	je 	ficha823
	cmp TFJC[8*MaMax+23],'N'
	je 	ficha823N
	jmp fichasi823	
	ficha823:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi823	
	ficha823N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi823:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[8*MaMax+27],'B'
	je 	ficha827
	cmp TFJC[8*MaMax+27],'N'
	je 	ficha827N
	jmp fichasi827	
	ficha827:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi827	
	ficha827N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi827:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[8*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[8*MaMax+30],'B'
	je 	ficha830
	cmp TFJC[8*MaMax+30],'N'
	je 	ficha830N
	jmp fichasi830	
	ficha830:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi830	
	ficha830N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi830:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila OCHO
	;fila NUEVE
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila NUEVE
	;fila  DIEZ
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[10*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[10*MaMax+3],'B'
	je 	ficha103
	cmp TFJC[10*MaMax+3],'N'
	je 	ficha103N
	jmp fichasi103	
	ficha103:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi103	
	ficha103N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi103:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[10*MaMax+7],'B'
	je 	ficha107
	cmp TFJC[10*MaMax+7],'N'
	je 	ficha107N
	jmp fichasi107	
	ficha107:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi107	
	ficha107N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi107:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[10*MaMax+11],'B'
	je 	ficha1011
	cmp TFJC[10*MaMax+11],'N'
	je 	ficha1011N
	jmp fichasi1011	
	ficha1011:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1011	
	ficha1011N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1011:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[10*MaMax+15],'B'
	je 	ficha1015
	cmp TFJC[10*MaMax+15],'N'
	je 	ficha1015N
	jmp fichasi1015	
	ficha1015:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1015	
	ficha1015N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1015:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[10*MaMax+19],'B'
	je 	ficha1019
	cmp TFJC[10*MaMax+19],'N'
	je 	ficha1019N
	jmp fichasi1019	
	ficha1019:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1019	
	ficha1019N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1019:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[10*MaMax+23],'B'
	je 	ficha1023
	cmp TFJC[10*MaMax+23],'N'
	je 	ficha1023N
	jmp fichasi1023	
	ficha1023:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1023	
	ficha1023N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1023:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[10*MaMax+27],'B'
	je 	ficha1027
	cmp TFJC[10*MaMax+27],'N'
	je 	ficha1027N
	jmp fichasi1027	
	ficha1027:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1027	
	ficha1027N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1027:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[10*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[10*MaMax+30],'B'
	je 	ficha1030
	cmp TFJC[10*MaMax+30],'N'
	je 	ficha1030N
	jmp fichasi1030	
	ficha1030:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1030	
	ficha1030N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1030:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila DIEZ
	;fila ONCE
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila ONCE
	;fila  DOCE
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[12*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[12*MaMax+3],'B'
	je 	ficha123
	cmp TFJC[12*MaMax+3],'N'
	je 	ficha123N
	jmp fichasi123	
	ficha123:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi123	
	ficha123N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi123:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[12*MaMax+7],'B'
	je 	ficha127
	cmp TFJC[12*MaMax+7],'N'
	je 	ficha127N
	jmp fichasi127	
	ficha127:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi127	
	ficha127N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi127:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[12*MaMax+11],'B'
	je 	ficha1211
	cmp TFJC[12*MaMax+11],'N'
	je 	ficha1211N
	jmp fichasi1211	
	ficha1211:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1211	
	ficha1211N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1211:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[12*MaMax+15],'B'
	je 	ficha1215
	cmp TFJC[12*MaMax+15],'N'
	je 	ficha1215N
	jmp fichasi1215	
	ficha1215:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1215	
	ficha1215N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1215:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[12*MaMax+19],'B'
	je 	ficha1219
	cmp TFJC[12*MaMax+19],'N'
	je 	ficha1219N
	jmp fichasi1219	
	ficha1219:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1219	
	ficha1219N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1219:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[12*MaMax+23],'B'
	je 	ficha1223
	cmp TFJC[12*MaMax+23],'N'
	je 	ficha1223N
	jmp fichasi1223	
	ficha1223:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1223	
	ficha1223N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1223:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[12*MaMax+27],'B'
	je 	ficha1227
	cmp TFJC[12*MaMax+27],'N'
	je 	ficha1227N
	jmp fichasi1227	
	ficha1227:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1227	
	ficha1227N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1227:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[12*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[12*MaMax+30],'B'
	je 	ficha1230
	cmp TFJC[12*MaMax+30],'N'
	je 	ficha1230N
	jmp fichasi1230	
	ficha1230:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1230	
	ficha1230N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1230:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila DOCE
	;fila TRECE
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[1*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila TRECE
	;fila  CATORCE
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	mov al,TFJC[14*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[14*MaMax+3],'B'
	je 	ficha143
	cmp TFJC[14*MaMax+3],'N'
	je 	ficha143N
	jmp fichasi143	
	ficha143:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi143	
	ficha143N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi143:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[14*MaMax+7],'B'
	je 	ficha147
	cmp TFJC[14*MaMax+7],'N'
	je 	ficha147N
	jmp fichasi147	
	ficha147:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi147	
	ficha147N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi147:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[14*MaMax+11],'B'
	je 	ficha1411
	cmp TFJC[14*MaMax+11],'N'
	je 	ficha1411N
	jmp fichasi1411	
	ficha1411:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1411	
	ficha1411N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1411:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[14*MaMax+15],'B'
	je 	ficha1415
	cmp TFJC[14*MaMax+15],'N'
	je 	ficha1415N
	jmp fichasi1415	
	ficha1415:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1415	
	ficha1415N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1415:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[14*MaMax+19],'B'
	je 	ficha1419
	cmp TFJC[14*MaMax+19],'N'
	je 	ficha1419N
	jmp fichasi1419	
	ficha1419:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1419	
	ficha1419N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1419:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	cmp TFJC[14*MaMax+23],'B'
	je 	ficha1423
	cmp TFJC[14*MaMax+23],'N'
	je 	ficha1423N
	jmp fichasi1423	
	ficha1423:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1423	
	ficha1423N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1423:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[14*MaMax+27],'B'
	je 	ficha1427
	cmp TFJC[14*MaMax+27],'N'
	je 	ficha1427N
	jmp fichasi1427	
	ficha1427:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1427	
	ficha1427N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1427:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth
	mov al,TFJC[14*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,guionX,SIZEOF guionX
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	 
	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 
	;mov al,TFJC[0*MaMax+3]
	;mov fichax,al
	cmp TFJC[14*MaMax+30],'B'
	je 	ficha1430
	cmp TFJC[14*MaMax+30],'N'
	je 	ficha1430N
	jmp fichasi1430	
	ficha1430:
	Escribir handlerentrada,htmlFB,SIZEOF htmlFB
	jmp fichasi1430	
	ficha1430N:
	Escribir handlerentrada,htmlFN,SIZEOF htmlFN 
	fichasi1430:
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila CATORCE
	
	;fila Quince
	Escribir handlerentrada,htmltr ,SIZEOF htmltr 

	Escribir handlerentrada,htmlthDD ,SIZEOF htmlthDD 	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 

	Escribir handlerentrada,htmlth ,SIZEOF htmlth	
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	Escribir handlerentrada,htmlth ,SIZEOF htmlth 
	mov al,TFJC[15*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,fichax,SIZEOF fichax 
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre 
	
	Escribir handlerentrada,htmltrcierre ,SIZEOF htmltrcierre    
	;fila Quince 	

   Escribir handlerentrada,htmltablecierre,SIZEOF htmltablecierre

   Escribir handlerentrada,htmlh1,SIZEOF htmlh1

   FechaActual diaF,mesF,dmconv,mconv
   Escribir handlerentrada,msjfecha,SIZEOF msjfecha
   Escribir handlerentrada,dmconv,SIZEOF dmconv   
   Escribir handlerentrada,fechaseparador,SIZEOF fechaseparador
   Escribir handlerentrada,mconv,SIZEOF mconv
   Escribir handlerentrada,fechaseparador,SIZEOF fechaseparador
   Escribir handlerentrada,fechaanio,SIZEOF fechaanio

   HoraActual ho,mi,se,hconv,miconv,seconv
   Escribir handlerentrada,msjhora,SIZEOF msjhora
   Escribir handlerentrada,hconv,SIZEOF hconv   
   Escribir handlerentrada,msj_separador,SIZEOF msj_separador
   Escribir handlerentrada,miconv,SIZEOF miconv
   Escribir handlerentrada,msj_separador,SIZEOF msj_separador
   Escribir handlerentrada,seconv,SIZEOF seconv

   Escribir handlerentrada,htmlh1cierre,SIZEOF htmlh1cierre
   Escribir handlerentrada,html4,SIZEOF html4

   Cerrar handlerentrada    	

   jmp Matriz  

; CIERRE TOTAL DEL PRIMER ARCHIVO

   opcionReporteFinal:
    mostrar MsjReporteFinal 
    ;mostrar LineaVacia 
 	mostrar archia 
	mostrar LineaVacia 		
    Limpiar bufferentrada,SIZEOF bufferentrada,24h
	ObtenerRuta bufferentrada 	
	CrearArchivo bufferentrada,handlerentrada 

	Escribir handlerentrada,html1,SIZEOF html1
	Escribir handlerentrada,html2,SIZEOF html2
	
	cicloUno:
	cmp contjc,8
	je fueracicloUno

	;mov si,contjc
	;mov si,datosi
	Escribir handlerentrada,htmltr,SIZEOF htmltr	
	;cicloDos:
	;cmp conta32,4
	;je fueracicloDos
	;mov al,contjc
	
	;mov fichax,al
	;mostrar fichax

	cmp contjc,0
	je cordenada0
	cmp contjc,1
	je cordenada2
	cmp contjc,2
	je cordenada4
	cmp contjc,3
	je cordenada6
	cmp contjc,4
	je cordenada8
	cmp contjc,5
	je cordenada10
	cmp contjc,6
	je cordenada12
	cmp contjc,7
	je cordenada14
	jmp finrenglon


	cordenada0:
	mov al,TFJC[0*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[0*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
    jmp finrenglon	



    cordenada2:
	mov al,TFJC[2*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[2*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

    jmp finrenglon	

	cordenada4:
	mov al,TFJC[4*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[4*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	jmp finrenglon	

	cordenada6:
	mov al,TFJC[6*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[6*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	jmp finrenglon	

	cordenada8:
	mov al,TFJC[8*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[8*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	jmp finrenglon	

	cordenada10:
	mov al,TFJC[10*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[10*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	jmp finrenglon

	cordenada12:
	mov al,TFJC[12*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+28]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+29]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre

	mov al,TFJC[12*MaMax+30]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	jmp finrenglon	

	cordenada14:
	mov al,TFJC[14*MaMax+0]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+1]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+2]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+3]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+4]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+5]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+6]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+7]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+8]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+9]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+10]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+11]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+12]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+13]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+14]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+15]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre	
	mov al,TFJC[14*MaMax+16]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+17]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+18]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+19]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+20]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+21]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+22]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+23]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+24]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+25]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+26]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre
	mov al,TFJC[14*MaMax+27]
	mov fichax,al
	Escribir handlerentrada,htmlth,SIZEOF htmlth
	Escribir handlerentrada,fichax,SIZEOF fichax
	Escribir handlerentrada,htmlthcierre,SIZEOF htmlthcierre	
	
	jmp sinfilafinal		
	Escribir handlerentrada,htmltrcierre,SIZEOF htmltrcierre
	finrenglon:
	Escribir handlerentrada,espacio,SIZEOF espacio
	Escribir handlerentrada,espacio2,SIZEOF espacio2
	sinfilafinal:
	inc contjc 
	inc datosi
	jmp cicloUno	
	fueracicloUno:
	mov contjc,0


	Escribir handlerentrada,htmltablecierre,SIZEOF htmltablecierre
	FechaActual diaF,mesF,dmconv,mconv

	
	Escribir handlerentrada,htmlh1,SIZEOF htmlh1

	Escribir handlerentrada,msjfecha,SIZEOF msjfecha
	Escribir handlerentrada,dmconv,SIZEOF dmconv
	Escribir handlerentrada,fechaseparador,SIZEOF fechaseparador
	Escribir handlerentrada,mconv,SIZEOF mconv
	Escribir handlerentrada,fechaseparador,SIZEOF fechaseparador
	Escribir handlerentrada,fechaanio,SIZEOF fechaanio
	
	Escribir handlerentrada,htmlh1cierre,SIZEOF htmlh1cierre
	
	Escribir handlerentrada,htmlh1,SIZEOF htmlh1	
	
	HoraActual ho,mi,se,hconv,miconv,seconv
	Escribir handlerentrada,msjhora,SIZEOF msjhora
	Escribir handlerentrada,hconv,SIZEOF hconv
	Escribir handlerentrada,msj_separador,SIZEOF msj_separador
	Escribir handlerentrada,miconv,SIZEOF miconv
	Escribir handlerentrada,msj_separador,SIZEOF msj_separador
	Escribir handlerentrada,seconv,SIZEOF seconv

	Escribir handlerentrada,htmlh1cierre,SIZEOF htmlh1cierre


  
	CambioDbDecimal contjcFN,FNconv
	CambioDbDecimal contjcFB,FBconv  
	
    mostrar msgpiezanegra
    mostrar FNconv
    mostrar msgpiezablanca
    mostrar FBconv
	mov al, contjcFB
	cmp contjcFN,al; COMPARA EL PRIMERO
	je Empatados
	jg gnn
	jmp Empatados

	Empatados: 
	mostrar GanadorBlancas
	Escribir handlerentrada,htmlh1,SIZEOF htmlh1
	Escribir handlerentrada,GanadorBlancas,SIZEOF GanadorBlancas
	Escribir handlerentrada,htmlh1cierre,SIZEOF htmlh1cierre	
	jmp finalizandoReporte
 	
 	gnn:
   	mostrar GanadorNegras
   	Escribir handlerentrada,htmlh1,SIZEOF htmlh1
	Escribir handlerentrada,GanadorNegras,SIZEOF GanadorNegras
	Escribir handlerentrada,htmlh1cierre,SIZEOF htmlh1cierre	
   	finalizandoReporte:

   	
    Escribir handlerentrada,htmlh1,SIZEOF htmlh1

    Escribir handlerentrada,msgpiezanegra,SIZEOF msgpiezanegra
    Escribir handlerentrada,FNconv,SIZEOF FNconv

    Escribir handlerentrada,msgpiezablanca,SIZEOF msgpiezablanca
    Escribir handlerentrada,FBconv,SIZEOF FBconv

    Escribir handlerentrada,htmlh1cierre,SIZEOF htmlh1cierre	

   	Escribir handlerentrada,html4,SIZEOF html4	

    Cerrar handlerentrada    	
   	jmp encabezado
     
    salir:
   	   mov  ah,4ch       
	   xor  al,al
	   int  21h          

;----------------------------------------------
;comienza el juego
;----------------------------------------------
 
   Matriz:   

     mostrar TFJC 
 	 
 	 Limpiar JuegoFinal,SIZEOF JuegoFinal,24h
     
     TurnoNegras:
	Limpiar bufferInformacion,SIZEOF bufferInformacion,24h
	mostrar matrintn 
        ObtenerTexto bufferInformacion
	mostrar LineaVacia 
	;mostrar bufferInformacion
	mostrar LineaVacia
	
	; PARA FICHAS NEGRAS
	cmp bufferInformacion[0],'A'	
	je posiciona
	cmp bufferInformacion[0],'B'	
	je posicionb
	cmp bufferInformacion[0],'C'	
	je posicionc
	cmp bufferInformacion[0],'D'	
	je posiciond	
	cmp bufferInformacion[0],'E'	
	je  exittCe
	cmp bufferInformacion[0],'F'
	je posicionf	
	cmp bufferInformacion[0],'G'	
	je posiciong	
	cmp bufferInformacion[0],'H'	
	je posicionh	
	cmp bufferInformacion[0],'P'	
	je  pass1	
	cmp bufferInformacion[0],'S'	
	je  saveshow
	jmp pasonegra

	saveshow:	
	cmp bufferInformacion[1],'A'
	je  save
	cmp bufferInformacion[1],'H'	
	je  show
	jmp TurnoNegras	

	save:
	cmp bufferInformacion[2],'V'	
	jne TurnoNegras
	cmp bufferInformacion[3],'E'	
	jne TurnoNegras	
	;mostrar GuardarArchivo 
	jmp opcion7

	show:	
	cmp bufferInformacion[2],'O'	
	jne TurnoNegras	
	cmp bufferInformacion[3],'W'	
	jne TurnoNegras	
	mostrar MsgHtmlCreado 
	mostrar MostrarJuego 		
	jmp opcion6

	pass1:
	cmp bufferInformacion[1],'A'
	jne TurnoNegras
	cmp bufferInformacion[2],'S'	
	jne TurnoNegras
	cmp bufferInformacion[3],'S'	
	jne TurnoNegras	
	;PassNegras
	mov al,'1'	
	mov JuegoFinal[0],al

    cmp JuegoFinal[0],'1'    
	jne	TurnoNegras
	cmp JuegoFinal[1],'1'
	jne TurnoBlancas
	jmp opcionReporteFinal


	exittCe:
	cmp bufferInformacion[1],'X'
	je exituno
	cmp bufferInformacion[1],'1'
	je asignape1
	cmp bufferInformacion[1],'2'
	je asignape2
	cmp bufferInformacion[1],'3'
	je asignape3
	cmp bufferInformacion[1],'4'
	je asignape4
	cmp bufferInformacion[1],'5'
	je asignape5
	cmp bufferInformacion[1],'6'
	je asignape6
	cmp bufferInformacion[1],'7'
	je asignape7
	cmp bufferInformacion[1],'8'
	je asignape8
	jmp pasonegra	


	exituno:		
	cmp bufferInformacion[2],'I'	
	jne TurnoNegras
	cmp bufferInformacion[3],'T'	
	jne TurnoNegras
	jmp encabezado


	asignape1:

	cmp TFJC[12*MaMax+19],'B'	   	
	jne cadena1419
	cmp TFJC[14*MaMax+15],'B'	   
	jne cadena1419
	cmp TFJC[14*MaMax+23],'B'	   
	jne cadena1419
	cmp TFJC[12*MaMax+23],'N'	   
	jne cadena1419
	cmp TFJC[14*MaMax+27],'N'
	jne cadena1419			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena1419:
    cmp TFJC[12*MaMax+19],'N'	   	
	je conlibertad1419
	cmp TFJC[14*MaMax+15],'N'	
	je  conlibertad1419
	cmp TFJC[14*MaMax+23],'N'	
	je  conlibertad1419
	jmp ComLib1419    

	ComLib1419:
	cmp TFJC[12*MaMax+19],32	   	
	je conlibertad1419
	cmp TFJC[14*MaMax+15],32	   	
	je  conlibertad1419
	cmp TFJC[14*MaMax+23],32	   	
	je  conlibertad1419
	jmp sinlibertad1419
	
	sinlibertad1419:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1419:
	cmp TFJC[14*MaMax+19],32
	je  pintarNegra1419 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1419:
    mov TFJC +14*MaMax+18,70 
    mov TFJC +14*MaMax+19,78 
    inc contjcFN		
	jmp salidablancas    

	asignape2:	

	cmp TFJC[10*MaMax+19],'N'	   	
	je conlibertad1219
	cmp TFJC[12*MaMax+15],'N'	
	je  conlibertad1219
	cmp TFJC[12*MaMax+23],'N'	
	je  conlibertad1219
	cmp TFJC[14*MaMax+19],'N'	
	je  conlibertad1219
	jmp ComLib1219    

	ComLib1219:
	cmp TFJC[10*MaMax+19],32	   	
	je conlibertad1219
	cmp TFJC[12*MaMax+15],32	   	
	je  conlibertad1219
	cmp TFJC[12*MaMax+23],32	   	
	je  conlibertad1219
	cmp TFJC[14*MaMax+19],32	   	
	je  conlibertad1219
	jmp sinlibertad1219
	
	sinlibertad1219:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1219:
	cmp TFJC[12*MaMax+19],32
	je  pintarNegra1219 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1219:
    mov TFJC +12*MaMax+18,70 
    mov TFJC +12*MaMax+19,78 
    inc contjcFN		
	jmp salidablancas
    

	asignape3:	
	cmp TFJC[8*MaMax+19],'N'	   	
	je conlibertad1019
	cmp TFJC[10*MaMax+15],'N'	
	je  conlibertad1019
	cmp TFJC[10*MaMax+23],'N'	
	je  conlibertad1019
	cmp TFJC[12*MaMax+19],'N'	
	je  conlibertad1019
	jmp ComLib1019    

	ComLib1019:
	cmp TFJC[8*MaMax+19],32	   	
	je conlibertad1019
	cmp TFJC[10*MaMax+15],32
	je  conlibertad1019
	cmp TFJC[10*MaMax+23],32
	je  conlibertad1019
	cmp TFJC[12*MaMax+19],32
	je  conlibertad1019
	jmp sinlibertad1019
	
	sinlibertad1019:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1019:
	cmp TFJC[10*MaMax+19],32
	je  pintarNegra1019 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1019:
    mov TFJC +10*MaMax+18,70 
    mov TFJC +10*MaMax+19,78 
    inc contjcFN			
	jmp salidablancas    

	asignape4:
	cmp TFJC[6*MaMax+19],'N'	   	
	je conlibertad819
	cmp TFJC[8*MaMax+15],'N'
	je  conlibertad819
	cmp TFJC[8*MaMax+23],'N'
	je  conlibertad819
	cmp TFJC[10*MaMax+19],'N'
	je  conlibertad819
	jmp ComLib819    

	ComLib819:
	cmp TFJC[6*MaMax+19],32	   	
	je conlibertad819
	cmp TFJC[8*MaMax+15],32	   	
	je  conlibertad819
	cmp TFJC[8*MaMax+23],32	   	
	je  conlibertad819
	cmp TFJC[10*MaMax+19],32	   	
	je  conlibertad819
	jmp sinlibertad819
	
	sinlibertad819:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad819:
	cmp TFJC[8*MaMax+19],32
	je  pintarNegra819
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra819:
    mov TFJC +8*MaMax+18,70 
    mov TFJC +8*MaMax+19,78 
    inc contjcFN		
	jmp salidablancas   

	asignape5:

	cmp TFJC[4*MaMax+19],'N'	   	
	je conlibertad619
	cmp TFJC[6*MaMax+15],'N'
	je  conlibertad619
	cmp TFJC[6*MaMax+23],'N'
	je  conlibertad619
	cmp TFJC[8*MaMax+19],'N'
	je  conlibertad619
	jmp ComLib619    

	ComLib619:
	cmp TFJC[4*MaMax+19],32	   	
	je conlibertad619
	cmp TFJC[6*MaMax+15],32
	je  conlibertad619
	cmp TFJC[6*MaMax+23],32
	je  conlibertad619
	cmp TFJC[8*MaMax+19],32
	je  conlibertad619
	jmp sinlibertad619
	
	sinlibertad619:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad619:
	cmp TFJC[6*MaMax+19],32
	je  pintarNegra619 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra619:    
    mov TFJC +6*MaMax+18,70 
    mov TFJC +6*MaMax+19,78 
    inc contjcFN			
	jmp salidablancas

	asignape6:
	cmp TFJC[2*MaMax+19],'N'	   	
	je conlibertad419
	cmp TFJC[4*MaMax+15],'N'
	je  conlibertad419
	cmp TFJC[4*MaMax+23],'N'
	je  conlibertad419
	cmp TFJC[6*MaMax+19],'N'
	je  conlibertad419
	jmp ComLib419    

	ComLib419:
	cmp TFJC[2*MaMax+19],32	   	
	je conlibertad419
	cmp TFJC[4*MaMax+15],32
	je  conlibertad419
	cmp TFJC[4*MaMax+23],32
	je  conlibertad419
	cmp TFJC[6*MaMax+19],32
	je  conlibertad419
	jmp sinlibertad419
	
	sinlibertad419:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad419:
	cmp TFJC[4*MaMax+19],32
	je  pintarNegra419 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra419:
    mov TFJC +4*MaMax+18,70 
    mov TFJC +4*MaMax+19,78 
    inc contjcFN		
	jmp salidablancas


	asignape7:	
	cmp TFJC[0*MaMax+19],'N'	   	
	je conlibertad219
	cmp TFJC[2*MaMax+15],'N'
	je  conlibertad219
	cmp TFJC[2*MaMax+23],'N'
	je  conlibertad219
	cmp TFJC[4*MaMax+19],'N'
	je  conlibertad219
	jmp ComLib219    

	ComLib219:
	cmp TFJC[0*MaMax+19],32	   	
	je conlibertad219
	cmp TFJC[2*MaMax+15],32
	je  conlibertad219
	cmp TFJC[2*MaMax+23],32
	je  conlibertad219
	cmp TFJC[4*MaMax+19],32
	je  conlibertad219
	jmp sinlibertad219
	
	sinlibertad219:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad219:
	cmp TFJC[2*MaMax+19],32
	je  pintarNegra219 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra219:
    mov TFJC +2*MaMax+18,70 
    mov TFJC +2*MaMax+19,78 
    inc contjcFN		
	jmp salidablancas

	asignape8:

	cmp TFJC[0*MaMax+15],'B'	   	
	jne cadena019
	cmp TFJC[0*MaMax+23],'B'	   
	jne cadena019
	cmp TFJC[2*MaMax+19],'B'	   
	jne cadena019
	cmp TFJC[0*MaMax+27],'N'	   
	jne cadena019
	cmp TFJC[2*MaMax+23],'N'
	jne cadena019			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena019:
    cmp TFJC[0*MaMax+15],'N'	   	
	je conlibertad019
	cmp TFJC[0*MaMax+23],'N'	
	je  conlibertad019
	cmp TFJC[2*MaMax+19],'N'	
	je  conlibertad019
	jmp ComLib019    

	ComLib019:
	cmp TFJC[0*MaMax+15],32	   	
	je conlibertad019
	cmp TFJC[0*MaMax+23],32	
	je  conlibertad019
	cmp TFJC[2*MaMax+19],32	
	je  conlibertad019
	jmp sinlibertad019
	
	sinlibertad019:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad019:
	cmp TFJC[0*MaMax+19],32
	je  pintarNegra019 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra019:
    mov TFJC +0*MaMax+18,70 
    mov TFJC +0*MaMax+19,78 
    inc contjcFN		
	jmp salidablancas


	
	posiciona:
	cmp bufferInformacion[1],'1'
	je asignapa1
	cmp bufferInformacion[1],'2'
	je asignapa2
	cmp bufferInformacion[1],'3'
	je asignapa3
	cmp bufferInformacion[1],'4'
	je asignapa4
	cmp bufferInformacion[1],'5'
	je asignapa5
	cmp bufferInformacion[1],'6'
	je asignapa6
	cmp bufferInformacion[1],'7'
	je asignapa7
	cmp bufferInformacion[1],'8'
	je asignapa8
	jmp pasonegra
	
	;----------------------------posicion inicio
	asignapa1:	

	cmp TFJC[12*MaMax+3],'N'	   	
	je conlibertad143
	cmp TFJC[14*MaMax+7],'N'	
	je conlibertad143	
	jmp ComLib143

	ComLib143:
	cmp TFJC[12*MaMax+3],32	   	
	je conlibertad143
	cmp TFJC[14*MaMax+7],32		
	je  conlibertad143	
	jmp sinlibertad143

	
	sinlibertad143:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad143:
	cmp TFJC[14*MaMax+3],32
	je  pintarNegra143 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra143:
    mov TFJC +14*MaMax+2,70 
    mov TFJC +14*MaMax+3,78 
    inc contjcFN	
	jmp salidablancas

	;----------------------------posicion fin

	;----------------------------posicion inicio
	asignapa2:	
    
    cmp TFJC[8*MaMax+3],'N'	   	
	jne cadena123
	cmp TFJC[10*MaMax+3],'B'	   
	jne cadena123
	cmp TFJC[10*MaMax+7],'N'	   
	jne cadena123
	cmp TFJC[12*MaMax+7],'B'	   
	jne cadena123
	cmp TFJC[14*MaMax+3],'B'
	jne cadena123			
	mostrar ReglaKo
	jmp salidablancas

	cadena123:
	cmp TFJC[10*MaMax+3],'N'	   	
	je conlibertad123
	cmp TFJC[12*MaMax+7],'N'		
	je  conlibertad123
	cmp TFJC[14*MaMax+3],'N'		
	je  conlibertad123
	jmp ComLib123

	ComLib123:	
	cmp TFJC[10*MaMax+3],32	   	
	je conlibertad123
	cmp TFJC[12*MaMax+7],32		
	je  conlibertad123
	cmp TFJC[14*MaMax+3],32		
	je  conlibertad123
	jmp sinlibertad123

	sinlibertad123:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad123:
	cmp TFJC[12*MaMax+3],32
	je  pintarNegra123 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra123:
    mov TFJC +12*MaMax+2,70 
    mov TFJC +12*MaMax+3,78 
    inc contjcFN		
	jmp salidablancas

	;----------------------------posicion fin


	asignapa3:	

	cmp TFJC[6*MaMax+3],'N'	   	
	jne cadena103
	cmp TFJC[8*MaMax+3],'B'	   
	jne cadena103
	cmp TFJC[8*MaMax+7],'N'	   
	jne cadena103
	cmp TFJC[10*MaMax+7],'B'	   
	jne cadena103
	cmp TFJC[12*MaMax+3],'B'
	jne cadena103			
	mostrar ReglaKo
	jmp salidablancas

	cmp TFJC[8*MaMax+3],'B'	   	
	jne cadena103
	cmp TFJC[10*MaMax+7],'B'	   
	jne cadena103
	cmp TFJC[12*MaMax+3],'B'	   
	jne cadena103
	cmp TFJC[12*MaMax+7],'N'	   
	jne cadena103
	cmp TFJC[14*MaMax+3],'N'
	jne cadena103			
	mostrar ReglaKo
	jmp salidablancas


	cadena103:
	cmp TFJC[8*MaMax+3],'N'	   	
	je conlibertad103
	cmp TFJC[10*MaMax+7],'N'
	je  conlibertad103
	cmp TFJC[12*MaMax+3],'N'
	je  conlibertad103
	jmp ComLib103

	ComLib103:
    cmp TFJC[8*MaMax+3],32	   	
	je conlibertad103
	cmp TFJC[10*MaMax+7],32		
	je  conlibertad103
	cmp TFJC[12*MaMax+3],32		
	je  conlibertad103
	jmp sinlibertad103

	sinlibertad103:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad103:
	cmp TFJC[10*MaMax+3],32
	je  pintarNegra103 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra103:
    mov TFJC +10*MaMax+2,70 
    mov TFJC +10*MaMax+3,78 
    inc contjcFN		
	jmp salidablancas

	asignapa4:	
    
    cmp TFJC[4*MaMax+3],'N'	   	
	jne cadena83
	cmp TFJC[6*MaMax+3],'B'	   
	jne cadena83
	cmp TFJC[6*MaMax+7],'N'	   
	jne cadena83
	cmp TFJC[8*MaMax+7],'B'	   
	jne cadena83
	cmp TFJC[10*MaMax+3],'B'
	jne cadena83			
	mostrar ReglaKo
	jmp salidablancas

	cmp TFJC[6*MaMax+3],'B'	   	
	jne cadena83
	cmp TFJC[8*MaMax+7],'B'	   
	jne cadena83
	cmp TFJC[10*MaMax+3],'B'	   
	jne cadena83
	cmp TFJC[10*MaMax+7],'N'	   
	jne cadena83
	cmp TFJC[12*MaMax+3],'N'
	jne cadena83			
	mostrar ReglaKo
	jmp salidablancas


	cadena83:
	cmp TFJC[6*MaMax+3],'N'	   	
	je conlibertad83
	cmp TFJC[8*MaMax+7],'N'	   	
	je  conlibertad83
	cmp TFJC[10*MaMax+3],'N'	   	
	je  conlibertad83
	jmp ComLib83

    ComLib83:
    cmp TFJC[6*MaMax+3],32	   	
	je conlibertad83
	cmp TFJC[8*MaMax+7],32		
	je  conlibertad83
	cmp TFJC[10*MaMax+3],32		
	je  conlibertad83
	jmp sinlibertad83

	sinlibertad83:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad83:
	cmp TFJC[8*MaMax+3],32
	je  pintarNegra83 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra83:
    mov TFJC +8*MaMax+2,70 
    mov TFJC +8*MaMax+3,78 	
    inc contjcFN	
	jmp salidablancas

	asignapa5:	
    
    cmp TFJC[2*MaMax+3],'N'	   	
	jne cadena63
	cmp TFJC[4*MaMax+3],'B'	   
	jne cadena63
	cmp TFJC[4*MaMax+7],'N'	   
	jne cadena63
	cmp TFJC[6*MaMax+7],'B'	   
	jne cadena63
	cmp TFJC[8*MaMax+3],'B'
	jne cadena63			
	mostrar ReglaKo
	jmp salidablancas

	cmp TFJC[4*MaMax+3],'B'	   	
	jne cadena63
	cmp TFJC[6*MaMax+7],'B'	   
	jne cadena63
	cmp TFJC[8*MaMax+3],'B'	   
	jne cadena63
	cmp TFJC[8*MaMax+7],'N'	   
	jne cadena63
	cmp TFJC[10*MaMax+3],'N'
	jne cadena63			
	mostrar ReglaKo
	jmp salidablancas

	cadena63:
	cmp TFJC[4*MaMax+3],'N'	   	
	je conlibertad63
	cmp TFJC[6*MaMax+7],'N'		
	je  conlibertad63
	cmp TFJC[8*MaMax+3],'N'		
	je  conlibertad63
	jmp ComLib163

    ComLib163:           
    cmp TFJC[4*MaMax+3],32	   	
	je conlibertad63
	cmp TFJC[6*MaMax+7],32		
	je  conlibertad63
	cmp TFJC[8*MaMax+3],32		
	je  conlibertad63
	jmp sinlibertad63

	sinlibertad63:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad63:
	cmp TFJC[6*MaMax+3],32
	je  pintarNegra63 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra63:
    mov TFJC +6*MaMax+2,70 
    mov TFJC +6*MaMax+3,78 	
    inc contjcFN	
	jmp salidablancas


	asignapa6:

    cmp TFJC[0*MaMax+3],'N'	   	
	jne cadena43
	cmp TFJC[2*MaMax+3],'B'	   
	jne cadena43
	cmp TFJC[2*MaMax+7],'N'	   
	jne cadena43
	cmp TFJC[4*MaMax+7],'B'	   
	jne cadena43
	cmp TFJC[6*MaMax+3],'B'
	jne cadena43			
	mostrar ReglaKo
	jmp salidablancas

	cmp TFJC[2*MaMax+3],'B'	   	
	jne cadena43
	cmp TFJC[4*MaMax+7],'B'	   
	jne cadena43
	cmp TFJC[6*MaMax+3],'B'	   
	jne cadena43
	cmp TFJC[6*MaMax+7],'N'	   
	jne cadena43
	cmp TFJC[8*MaMax+3],'N'
	jne cadena43			
	mostrar ReglaKo
	jmp salidablancas

	cadena43:
    cmp TFJC[2*MaMax+3],'N'	   	
	je conlibertad43
	cmp TFJC[4*MaMax+7],'N'
	je  conlibertad43
	cmp TFJC[6*MaMax+3],'N'
	je  conlibertad43
	jmp ComLib43

    ComLib43:   
    cmp TFJC[2*MaMax+3],32	   	
	je conlibertad43
	cmp TFJC[4*MaMax+7],32		
	je  conlibertad43
	cmp TFJC[6*MaMax+3],32		
	je  conlibertad43
	jmp sinlibertad43

	sinlibertad43:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad43:
	cmp TFJC[4*MaMax+3],32
	je  pintarNegra43 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra43:
    mov TFJC +4*MaMax+2,70 
    mov TFJC +4*MaMax+3,78 	
    inc contjcFN	
	jmp salidablancas

	asignapa7:	
    
    cmp TFJC[0*MaMax+3],'B'	   	
	jne cadena23
	cmp TFJC[2*MaMax+7],'B'	   
	jne cadena23
	cmp TFJC[4*MaMax+3],'B'	   
	jne cadena23
	cmp TFJC[4*MaMax+7],'N'	   
	jne cadena23
	cmp TFJC[6*MaMax+3],'N'
	jne cadena23			
	mostrar ReglaKo
	jmp salidablancas

	cadena23:
    cmp TFJC[0*MaMax+3],'N'	   	
	je conlibertad23
	cmp TFJC[2*MaMax+7],'N'	
	je  conlibertad23
	cmp TFJC[6*MaMax+3],'N'	
	je  conlibertad23
	jmp ComLib23


    ComLib23:
    cmp TFJC[0*MaMax+3],32	   	
	je conlibertad23
	cmp TFJC[2*MaMax+7],32		
	je  conlibertad23
	cmp TFJC[6*MaMax+3],32		
	je  conlibertad23
	jmp sinlibertad23

	sinlibertad23:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad23:
	cmp TFJC[2*MaMax+3],32
	je  pintarNegra23 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra23:
    mov TFJC +2*MaMax+2,70 
    mov TFJC +2*MaMax+3,78 	
    inc contjcFN	
	jmp salidablancas


	asignapa8:    

    cmp TFJC[0*MaMax+7],'N'	   	
	je conlibertad03
	cmp TFJC[2*MaMax+3],'N'		
	je  conlibertad03	
	jmp ComLib03

    ComLib03:
    cmp TFJC[0*MaMax+7],32	   	
	je conlibertad03
	cmp TFJC[2*MaMax+3],32		
	je  conlibertad03	
	jmp sinlibertad03

	sinlibertad03:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad03:
	cmp TFJC[0*MaMax+3],32
	je  pintarNegra03 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra03:
    mov TFJC +0*MaMax+2,70 
    mov TFJC +0*MaMax+3,78 	
    inc contjcFN	
	jmp salidablancas

	posicionb:
	cmp bufferInformacion[1],'1'
	je asignapb1
	cmp bufferInformacion[1],'2'
	je asignapb2
	cmp bufferInformacion[1],'3'
	je asignapb3
	cmp bufferInformacion[1],'4'
	je asignapb4
	cmp bufferInformacion[1],'5'
	je asignapb5
	cmp bufferInformacion[1],'6'
	je asignapb6
	cmp bufferInformacion[1],'7'
	je asignapb7
	cmp bufferInformacion[1],'8'
	je asignapb8
	jmp pasonegra
	
	asignapb1:

	cmp TFJC[12*MaMax+7],'B'	   	
	jne cadena147
	cmp TFJC[14*MaMax+3],'B'	   
	jne cadena147
	cmp TFJC[14*MaMax+11],'B'	   
	jne cadena147
	cmp TFJC[12*MaMax+11],'N'	   
	jne cadena147
	cmp TFJC[14*MaMax+15],'N'
	jne cadena147			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena147:
    cmp TFJC[12*MaMax+7],'N'	   	
	je conlibertad147
	cmp TFJC[14*MaMax+3],'N'	
	je  conlibertad147
	cmp TFJC[14*MaMax+11],'N'	
	je  conlibertad147
	jmp ComLib147    

	ComLib147:
	cmp TFJC[12*MaMax+7],32	   	
	je conlibertad147
	cmp TFJC[14*MaMax+3],32	
	je conlibertad147	
	cmp TFJC[14*MaMax+11],32
	je conlibertad147	
	jmp sinlibertad147
	
	sinlibertad147:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad147:
	cmp TFJC[14*MaMax+7],32
	je  pintarNegra147 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra147:
    mov TFJC +14*MaMax+6,70 
    mov TFJC +14*MaMax+7,78 
    inc contjcFN		
	jmp salidablancas    

	asignapb2:	

	cmp TFJC[10*MaMax+7],'N'	   	
	je conlibertad127
	cmp TFJC[12*MaMax+3],'N'	
	je  conlibertad127
	cmp TFJC[12*MaMax+11],'N'	
	je  conlibertad127
	cmp TFJC[14*MaMax+7],'N'	
	je  conlibertad127
	jmp ComLib127    

	ComLib127:
	cmp TFJC[10*MaMax+7],32	   	
	je conlibertad127
	cmp TFJC[12*MaMax+3],32	
	je  conlibertad127
	cmp TFJC[12*MaMax+11],32	
	je  conlibertad127
	cmp TFJC[14*MaMax+7],32	
	je  conlibertad127
	jmp sinlibertad127
	
	sinlibertad127:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad127:
	cmp TFJC[12*MaMax+7],32
	je  pintarNegra127 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra127:
    mov TFJC +12*MaMax+6,70 
    mov TFJC +12*MaMax+7,78 
    inc contjcFN		
	jmp salidablancas


	asignapb3:	

	cmp TFJC[8*MaMax+7],'N'	   	
	je conlibertad107
	cmp TFJC[10*MaMax+3],'N'	
	je  conlibertad107
	cmp TFJC[10*MaMax+11],'N'	
	je  conlibertad107
	cmp TFJC[12*MaMax+7],'N'	
	je  conlibertad107
	jmp ComLib107    

	ComLib107:
	cmp TFJC[8*MaMax+7],32	   	
	je conlibertad107
	cmp TFJC[10*MaMax+3],32	
	je  conlibertad107
	cmp TFJC[10*MaMax+11],32	
	je  conlibertad107
	cmp TFJC[12*MaMax+7],32	
	je  conlibertad107
	jmp sinlibertad107
	
	sinlibertad107:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad107:
	cmp TFJC[10*MaMax+7],32
	je  pintarNegra107 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra107:
    mov TFJC +10*MaMax+6,70 
    mov TFJC +10*MaMax+7,78 
    inc contjcFN			
	jmp salidablancas
	    
	asignapb4:	

	cmp TFJC[6*MaMax+7],'N'	   	
	je conlibertad87
	cmp TFJC[8*MaMax+3],'N'
	je  conlibertad87
	cmp TFJC[8*MaMax+11],'N'
	je  conlibertad87
	cmp TFJC[10*MaMax+7],'N'
	je  conlibertad87
	jmp ComLib87    

	ComLib87:
	cmp TFJC[6*MaMax+7],32	   	
	je conlibertad87
	cmp TFJC[8*MaMax+3],32	
	je  conlibertad87
	cmp TFJC[8*MaMax+11],32	
	je  conlibertad87
	cmp TFJC[10*MaMax+7],32	
	je  conlibertad87
	jmp sinlibertad87
	
	sinlibertad87:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad87:
	cmp TFJC[8*MaMax+7],32
	je  pintarNegra87 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra87:
    mov TFJC +8*MaMax+6,70 
    mov TFJC +8*MaMax+7,78 	
    inc contjcFN	
	jmp salidablancas


	asignapb5:

	cmp TFJC[4*MaMax+7],'N'	   	
	je conlibertad67
	cmp TFJC[6*MaMax+3],'N'
	je  conlibertad67
	cmp TFJC[6*MaMax+11],'N'
	je  conlibertad67
	cmp TFJC[8*MaMax+7],'N'
	je  conlibertad67
	jmp ComLib67    

	ComLib67:
	cmp TFJC[4*MaMax+7],'N'	   	
	je conlibertad67
	cmp TFJC[6*MaMax+3],'N'
	je  conlibertad67
	cmp TFJC[6*MaMax+11],'N'
	je  conlibertad67
	cmp TFJC[8*MaMax+7],'N'
	je  conlibertad67
	jmp sinlibertad67
	
	sinlibertad67:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad67:
	cmp TFJC[6*MaMax+7],32
	je  pintarNegra67 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra67:
    mov TFJC +6*MaMax+6,70 
    mov TFJC +6*MaMax+7,78 	
    inc contjcFN		
	jmp salidablancas

	asignapb6:

	cmp TFJC[2*MaMax+7],'N'	   	
	je conlibertad47
	cmp TFJC[4*MaMax+3],'N'
	je  conlibertad47
	cmp TFJC[4*MaMax+11],'N'
	je  conlibertad47
	cmp TFJC[6*MaMax+7],'N'
	je  conlibertad47
	jmp ComLib47    

	ComLib47:
	cmp TFJC[2*MaMax+7],32	   	
	je conlibertad47
	cmp TFJC[4*MaMax+3],32
	je  conlibertad47
	cmp TFJC[4*MaMax+11],32
	je  conlibertad47
	cmp TFJC[6*MaMax+7],32
	je  conlibertad47
	jmp sinlibertad47
	
	sinlibertad47:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad47:
	cmp TFJC[4*MaMax+7],32
	je  pintarNegra47 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra47:
    mov TFJC +4*MaMax+6,70 
    mov TFJC +4*MaMax+7,78 	
    inc contjcFN	
	jmp salidablancas


	asignapb7:	

	cmp TFJC[0*MaMax+7],'N'	   	
	je conlibertad27
	cmp TFJC[2*MaMax+3],'N'
	je  conlibertad27
	cmp TFJC[2*MaMax+11],'N'
	je  conlibertad27
	cmp TFJC[4*MaMax+7],'N'
	je  conlibertad27
	jmp ComLib27    

	ComLib27:
	cmp TFJC[0*MaMax+7],32	   	
	je conlibertad27
	cmp TFJC[2*MaMax+3],32
	je  conlibertad27
	cmp TFJC[2*MaMax+11],32
	je  conlibertad27
	cmp TFJC[4*MaMax+7],32
	je  conlibertad27
	jmp sinlibertad27
	
	sinlibertad27:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad27:
	cmp TFJC[2*MaMax+7],32
	je  pintarNegra27 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra27:
    mov TFJC +2*MaMax+6,70 
    mov TFJC +2*MaMax+7,78 	
    inc contjcFN	
	jmp salidablancas

	asignapb8:	
    
    cmp TFJC[0*MaMax+3],'B'	   	
	jne cadena07
	cmp TFJC[0*MaMax+11],'B'	   
	jne cadena07
	cmp TFJC[2*MaMax+7],'B'	   
	jne cadena07
	cmp TFJC[0*MaMax+15],'N'	   
	jne cadena07
	cmp TFJC[2*MaMax+11],'N'
	jne cadena07			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena07:
    cmp TFJC[0*MaMax+3],'N'	   	
	je conlibertad07
	cmp TFJC[0*MaMax+11],'N'	
	je  conlibertad07
	cmp TFJC[2*MaMax+7],'N'	
	je  conlibertad07
	jmp ComLib07    

	ComLib07:
	cmp TFJC[0*MaMax+3],32	   	
	je conlibertad07
	cmp TFJC[0*MaMax+11],32	
	je  conlibertad07
	cmp TFJC[2*MaMax+7],32	
	je  conlibertad07
	jmp sinlibertad07
	
	sinlibertad07:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad07:
	cmp TFJC[0*MaMax+7],32
	je  pintarNegra07 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra07:
    mov TFJC +0*MaMax+6,70 
    mov TFJC +0*MaMax+7,78 	
    inc contjcFN	
	jmp salidablancas

	posicionc:
	cmp bufferInformacion[1],'1'
	je asignapc1
	cmp bufferInformacion[1],'2'
	je asignapc2
	cmp bufferInformacion[1],'3'
	je asignapc3
	cmp bufferInformacion[1],'4'
	je asignapc4
	cmp bufferInformacion[1],'5'
	je asignapc5
	cmp bufferInformacion[1],'6'
	je asignapc6
	cmp bufferInformacion[1],'7'
	je asignapc7
	cmp bufferInformacion[1],'8'
	je asignapc8
	jmp pasonegra
	
	asignapc1:	

	cmp TFJC[12*MaMax+11],'B'	   	
	jne cadena1411
	cmp TFJC[14*MaMax+7],'B'	   
	jne cadena1411
	cmp TFJC[14*MaMax+15],'B'	   
	jne cadena1411
	cmp TFJC[12*MaMax+15],'N'	   
	jne cadena1411
	cmp TFJC[14*MaMax+19],'N'
	jne cadena1411			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena1411:
    cmp TFJC[12*MaMax+11],'N'	   	
	je conlibertad1411
	cmp TFJC[14*MaMax+7],'N'	
	je  conlibertad1411
	cmp TFJC[14*MaMax+15],'N'	
	je  conlibertad1411
	jmp ComLib1411    

	ComLib1411:
	cmp TFJC[12*MaMax+11],32	   	
	je conlibertad1411
	cmp TFJC[14*MaMax+7],32	
	je  conlibertad1411
	cmp TFJC[14*MaMax+15],32	
	je  conlibertad1411
	jmp sinlibertad1411
	
	sinlibertad1411:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1411:
	cmp TFJC[14*MaMax+11],32
	je  pintarNegra1411 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1411:
    mov TFJC +14*MaMax+10,70 
    mov TFJC +14*MaMax+11,78
    inc contjcFN	 	
	jmp salidablancas

	asignapc2:

	cmp TFJC[10*MaMax+11],'N'	   	
	je conlibertad1211
	cmp TFJC[12*MaMax+7],'N'	
	je  conlibertad1211
	cmp TFJC[12*MaMax+15],'N'	
	je  conlibertad1211
	cmp TFJC[14*MaMax+11],'N'	
	je  conlibertad1211
	jmp ComLib1211    

	ComLib1211:
	cmp TFJC[10*MaMax+11],32	   	
	je conlibertad1211
	cmp TFJC[12*MaMax+7],32	
	je  conlibertad1211
	cmp TFJC[12*MaMax+15],32
	je  conlibertad1211
	cmp TFJC[14*MaMax+11],32
	je  conlibertad1211
	jmp sinlibertad1211
	
	sinlibertad1211:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1211:
	cmp TFJC[12*MaMax+11],32
	je  pintarNegra1211 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1211:
    mov TFJC +12*MaMax+10,70 
    mov TFJC +12*MaMax+11,78
    inc contjcFN	 	
	jmp salidablancas
    

	asignapc3:

	cmp TFJC[8*MaMax+11],'N'	   	
	je conlibertad1011
	cmp TFJC[10*MaMax+7],'N'	
	je  conlibertad1011
	cmp TFJC[10*MaMax+15],'N'	
	je  conlibertad1011
	cmp TFJC[12*MaMax+11],'N'	
	je  conlibertad1011
	jmp ComLib1011    

	ComLib1011:
	cmp TFJC[8*MaMax+11],32	   	
	je conlibertad1011
	cmp TFJC[10*MaMax+7],32
	je  conlibertad1011
	cmp TFJC[10*MaMax+15],32
	je  conlibertad1011
	cmp TFJC[12*MaMax+11],32
	je  conlibertad1011
	jmp sinlibertad1011
	
	sinlibertad1011:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1011:
	cmp TFJC[10*MaMax+11],32
	je  pintarNegra1011 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1011:
    mov TFJC +10*MaMax+10,70 
    mov TFJC +10*MaMax+11,78
    inc contjcFN	 		
	jmp salidablancas

	asignapc4:	

	cmp TFJC[6*MaMax+11],'N'	   	
	je conlibertad811
	cmp TFJC[8*MaMax+7],'N'
	je  conlibertad811
	cmp TFJC[8*MaMax+15],'N'
	je  conlibertad811
	cmp TFJC[10*MaMax+11],'N'
	je  conlibertad811
	jmp ComLib811    

	ComLib811:
	cmp TFJC[6*MaMax+11],32	   	
	je conlibertad811
	cmp TFJC[8*MaMax+7],32	   	
	je  conlibertad811
	cmp TFJC[8*MaMax+15],32	   	
	je  conlibertad811
	cmp TFJC[10*MaMax+11],32	   	
	je  conlibertad811
	jmp sinlibertad811
	
	sinlibertad811:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad811:
	cmp TFJC[8*MaMax+11],32
	je  pintarNegra811 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra811:
    mov TFJC +8*MaMax+10,70 
    mov TFJC +8*MaMax+11,78 
    inc contjcFN		
	jmp salidablancas    

	asignapc5:

	cmp TFJC[4*MaMax+11],'N'	   	
	je conlibertad611
	cmp TFJC[6*MaMax+7],'N'
	je  conlibertad611
	cmp TFJC[6*MaMax+15],'N'
	je  conlibertad611
	cmp TFJC[8*MaMax+11],'N'
	je  conlibertad611
	jmp ComLib611    

	ComLib611:
	cmp TFJC[4*MaMax+11],32	   	
	je conlibertad611
	cmp TFJC[6*MaMax+7],32	   	
	je  conlibertad611
	cmp TFJC[6*MaMax+15],32	   	
	je  conlibertad611
	cmp TFJC[8*MaMax+11],32	   	
	je  conlibertad611
	jmp sinlibertad611
	
	sinlibertad611:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad611:
	cmp TFJC[6*MaMax+11],32
	je  pintarNegra611 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra611:
    mov TFJC +6*MaMax+10,70 
    mov TFJC +6*MaMax+11,78 
    inc contjcFN			
	jmp salidablancas    

	asignapc6:	

	cmp TFJC[2*MaMax+11],'N'	   	
	je conlibertad411
	cmp TFJC[4*MaMax+7],'N'
	je  conlibertad411
	cmp TFJC[4*MaMax+15],'N'
	je  conlibertad411
	cmp TFJC[6*MaMax+11],'N'
	je  conlibertad411
	jmp ComLib411    

	ComLib411:
	cmp TFJC[2*MaMax+11],32	   	
	je conlibertad411
	cmp TFJC[4*MaMax+7],32	   	
	je  conlibertad411
	cmp TFJC[4*MaMax+15],32	   	
	je  conlibertad411
	cmp TFJC[6*MaMax+11],32	   	
	je  conlibertad411
	jmp sinlibertad411
	
	sinlibertad411:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad411:
	cmp TFJC[4*MaMax+11],32
	je  pintarNegra411 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra411:
    mov TFJC +4*MaMax+10,70 
    mov TFJC +4*MaMax+11,78 
    inc contjcFN		
	jmp salidablancas


	asignapc7:

	cmp TFJC[0*MaMax+11],'N'	   	
	je conlibertad211
	cmp TFJC[2*MaMax+7],'N'
	je  conlibertad211
	cmp TFJC[2*MaMax+15],'N'
	je  conlibertad211
	cmp TFJC[4*MaMax+11],'N'
	je  conlibertad211
	jmp ComLib211    

	ComLib211:
	cmp TFJC[0*MaMax+11],32	   	
	je conlibertad211
	cmp TFJC[2*MaMax+7],32	   	
	je  conlibertad211
	cmp TFJC[2*MaMax+15],32	   	
	je  conlibertad211
	cmp TFJC[4*MaMax+11],32	   	
	je  conlibertad211
	jmp sinlibertad211
	
	sinlibertad211:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad211:
	cmp TFJC[2*MaMax+11],32
	je  pintarNegra211 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra211:
    mov TFJC +2*MaMax+10,70 
    mov TFJC +2*MaMax+11,78 
    inc contjcFN		
	jmp salidablancas

	asignapc8:	

	cmp TFJC[0*MaMax+7],'B'	   	
	jne cadena011
	cmp TFJC[0*MaMax+15],'B'	   
	jne cadena011
	cmp TFJC[2*MaMax+11],'B'	   
	jne cadena011
	cmp TFJC[0*MaMax+19],'N'	   
	jne cadena011
	cmp TFJC[2*MaMax+15],'N'
	jne cadena011			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena011:
    cmp TFJC[0*MaMax+7],'N'	   	
	je conlibertad011
	cmp TFJC[0*MaMax+15],'N'	
	je  conlibertad011
	cmp TFJC[2*MaMax+11],'N'	
	je  conlibertad011
	jmp ComLib011    

	ComLib011:
	cmp TFJC[0*MaMax+7],32	   	
	je conlibertad011
	cmp TFJC[0*MaMax+15],32	   	
	je  conlibertad011
	cmp TFJC[2*MaMax+11],32	   	
	je  conlibertad011
	jmp sinlibertad011
	
	sinlibertad011:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad011:
	cmp TFJC[0*MaMax+11],32
	je  pintarNegra011 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra011:
    mov TFJC +0*MaMax+10,70 
    mov TFJC +0*MaMax+11,78 
    inc contjcFN		
	jmp salidablancas
    
	posiciond:
	cmp bufferInformacion[1],'1'
	je asignapd1
	cmp bufferInformacion[1],'2'
	je asignapd2
	cmp bufferInformacion[1],'3'
	je asignapd3
	cmp bufferInformacion[1],'4'
	je asignapd4
	cmp bufferInformacion[1],'5'
	je asignapd5
	cmp bufferInformacion[1],'6'
	je asignapd6
	cmp bufferInformacion[1],'7'
	je asignapd7
	cmp bufferInformacion[1],'8'
	je asignapd8
	jmp pasonegra
	
	asignapd1:	

	cmp TFJC[12*MaMax+15],'B'	   	
	jne cadena1415
	cmp TFJC[14*MaMax+11],'B'	   
	jne cadena1415
	cmp TFJC[14*MaMax+19],'B'	   
	jne cadena1415
	cmp TFJC[12*MaMax+19],'N'	   
	jne cadena1415
	cmp TFJC[14*MaMax+23],'N'
	jne cadena1415			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena1415:
    cmp TFJC[12*MaMax+15],'N'	   	
	je conlibertad1415
	cmp TFJC[14*MaMax+11],'N'	
	je  conlibertad1415
	cmp TFJC[14*MaMax+19],'N'	
	je  conlibertad1415
	jmp ComLib1415    

	ComLib1415:
	cmp TFJC[12*MaMax+15],32	   	
	je conlibertad1415
	cmp TFJC[14*MaMax+11],32	   	
	je  conlibertad1415
	cmp TFJC[14*MaMax+19],32	   	
	je  conlibertad1415
	jmp sinlibertad1415
	
	sinlibertad1415:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1415:
	cmp TFJC[14*MaMax+15],32
	je  pintarNegra1415 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1415:
    mov TFJC +14*MaMax+14,70 
    mov TFJC +14*MaMax+15,78
    inc contjcFN	 	
	jmp salidablancas

	asignapd2:

	cmp TFJC[10*MaMax+15],'N'	   	
	je conlibertad1215
	cmp TFJC[12*MaMax+11],'N'	
	je  conlibertad1215
	cmp TFJC[12*MaMax+19],'N'	
	je  conlibertad1215
	cmp TFJC[14*MaMax+15],'N'	
	je  conlibertad1215
	jmp ComLib1215    

	ComLib1215:
	cmp TFJC[10*MaMax+15],32	   	
	je conlibertad1215
	cmp TFJC[12*MaMax+11],32	   	
	je  conlibertad1215
	cmp TFJC[12*MaMax+19],32	   	
	je  conlibertad1215
	cmp TFJC[14*MaMax+15],32	   	
	je  conlibertad1215
	jmp sinlibertad1215
	
	sinlibertad1215:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1215:
	cmp TFJC[12*MaMax+15],32
	je  pintarNegra1215 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1215:
    mov TFJC +12*MaMax+14,70 
    mov TFJC +12*MaMax+15,78 	
    inc contjcFN	
	jmp salidablancas    

	asignapd3:

	cmp TFJC[8*MaMax+15],'N'	   	
	je conlibertad1015
	cmp TFJC[10*MaMax+11],'N'	
	je  conlibertad1015
	cmp TFJC[10*MaMax+19],'N'	
	je  conlibertad1015
	cmp TFJC[12*MaMax+15],'N'	
	je  conlibertad1015
	jmp ComLib1015    

	ComLib1015:
	cmp TFJC[8*MaMax+15],32	   	
	je conlibertad1015
	cmp TFJC[10*MaMax+11],32
	je  conlibertad1015
	cmp TFJC[10*MaMax+19],32
	je  conlibertad1015
	cmp TFJC[12*MaMax+15],32
	je  conlibertad1015
	jmp sinlibertad1015
	
	sinlibertad1015:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1015:
	cmp TFJC[10*MaMax+15],32
	je  pintarNegra1015 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1015:
    mov TFJC +10*MaMax+14,70 
    mov TFJC +10*MaMax+15,78 	
    inc contjcFN		
	jmp salidablancas

	asignapd4:
	cmp TFJC[6*MaMax+15],'N'	   	
	je conlibertad815
	cmp TFJC[8*MaMax+11],'N'
	je  conlibertad815
	cmp TFJC[8*MaMax+19],'N'
	je  conlibertad815
	cmp TFJC[10*MaMax+15],'N'
	je  conlibertad815
	jmp ComLib815    

	ComLib815:
	cmp TFJC[6*MaMax+15],32	   	
	je conlibertad815
	cmp TFJC[8*MaMax+11],32	   	
	je  conlibertad815
	cmp TFJC[8*MaMax+19],32	   	
	je  conlibertad815
	cmp TFJC[10*MaMax+15],32	   	
	je  conlibertad815
	jmp sinlibertad815
	
	sinlibertad815:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad815:
	cmp TFJC[8*MaMax+15],32
	je  pintarNegra815 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra815:
    mov TFJC +8*MaMax+14,70 
    mov TFJC +8*MaMax+15,78 	
    inc contjcFN	
	jmp salidablancas

	asignapd5:	

	cmp TFJC[4*MaMax+15],'N'	   	
	je conlibertad615
	cmp TFJC[6*MaMax+11],'N'
	je  conlibertad615
	cmp TFJC[6*MaMax+19],'N'
	je  conlibertad615
	cmp TFJC[8*MaMax+15],'N'
	je  conlibertad615
	jmp ComLib615    

	ComLib615:
	cmp TFJC[4*MaMax+15],32	   	
	je conlibertad615
	cmp TFJC[6*MaMax+11],32	   	
	je  conlibertad615
	cmp TFJC[6*MaMax+19],32	   	
	je  conlibertad615
	cmp TFJC[8*MaMax+15],32	   	
	je  conlibertad615
	jmp sinlibertad615
	
	sinlibertad615:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad615:
	cmp TFJC[6*MaMax+15],32
	je  pintarNegra615 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra615:
    mov TFJC +6*MaMax+14,70 
    mov TFJC +6*MaMax+15,78 	
    inc contjcFN		
	jmp salidablancas

    

	asignapd6:

	cmp TFJC[2*MaMax+15],'N'	   	
	je conlibertad415
	cmp TFJC[4*MaMax+11],'N'
	je  conlibertad415
	cmp TFJC[4*MaMax+19],'N'
	je  conlibertad415
	cmp TFJC[6*MaMax+15],'N'
	je  conlibertad415
	jmp ComLib415    

	ComLib415:
	cmp TFJC[2*MaMax+15],32	   	
	je conlibertad415
	cmp TFJC[4*MaMax+11],32	   	
	je  conlibertad415
	cmp TFJC[4*MaMax+19],32	   	
	je  conlibertad415
	cmp TFJC[6*MaMax+15],32	   	
	je  conlibertad415
	jmp sinlibertad415
	
	sinlibertad415:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad415:
	cmp TFJC[4*MaMax+15],32
	je  pintarNegra415 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra415:
    mov TFJC +4*MaMax+14,70 
    mov TFJC +4*MaMax+15,78 	
    inc contjcFN	
	jmp salidablancas

	asignapd7:	

	cmp TFJC[0*MaMax+15],'N'	   	
	je conlibertad215
	cmp TFJC[2*MaMax+11],'N'
	je  conlibertad215
	cmp TFJC[2*MaMax+19],'N'
	je  conlibertad215
	cmp TFJC[4*MaMax+15],'N'
	je  conlibertad215
	jmp ComLib215    

	ComLib215:
	cmp TFJC[0*MaMax+15],32	   	
	je conlibertad215
	cmp TFJC[2*MaMax+11],32
	je  conlibertad215
	cmp TFJC[2*MaMax+19],32
	je  conlibertad215
	cmp TFJC[4*MaMax+15],32
	je  conlibertad215
	jmp sinlibertad215
	
	sinlibertad215:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad215:
	cmp TFJC[2*MaMax+15],32
	je  pintarNegra215 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra215:
    mov TFJC +2*MaMax+14,70 
    mov TFJC +2*MaMax+15,78 	
    inc contjcFN	
	jmp salidablancas

    

	asignapd8:

	cmp TFJC[0*MaMax+11],'B'	   	
	jne cadena015
	cmp TFJC[0*MaMax+19],'B'	   
	jne cadena015
	cmp TFJC[2*MaMax+15],'B'	   
	jne cadena015
	cmp TFJC[0*MaMax+23],'N'	   
	jne cadena015
	cmp TFJC[2*MaMax+19],'N'
	jne cadena015			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena015:
    cmp TFJC[0*MaMax+11],'N'	   	
	je conlibertad015
	cmp TFJC[0*MaMax+19],'N'	
	je  conlibertad015
	cmp TFJC[2*MaMax+15],'N'	
	je  conlibertad015
	jmp ComLib015    

	ComLib015:
	cmp TFJC[0*MaMax+11],32	   	
	je conlibertad015
	cmp TFJC[0*MaMax+19],32	
	je  conlibertad015
	cmp TFJC[2*MaMax+15],32	
	je  conlibertad015
	jmp sinlibertad015
	
	sinlibertad015:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad015:
	cmp TFJC[0*MaMax+15],32
	je  pintarNegra015 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra015:
    mov TFJC +0*MaMax+14,70 
    mov TFJC +0*MaMax+15,78 	
    inc contjcFN	
	jmp salidablancas    

	posicionf:
	cmp bufferInformacion[1],'1'
	je asignapf1
	cmp bufferInformacion[1],'2'
	je asignapf2
	cmp bufferInformacion[1],'3'
	je asignapf3
	cmp bufferInformacion[1],'4'
	je asignapf4
	cmp bufferInformacion[1],'5'
	je asignapf5
	cmp bufferInformacion[1],'6'
	je asignapf6
	cmp bufferInformacion[1],'7'
	je asignapf7
	cmp bufferInformacion[1],'8'
	je asignapf8
	jmp pasonegra
	
	asignapf1:

	cmp TFJC[12*MaMax+23],'B'	   	
	jne cadena1423
	cmp TFJC[14*MaMax+19],'B'	   
	jne cadena1423
	cmp TFJC[14*MaMax+27],'B'	   
	jne cadena1423
	cmp TFJC[12*MaMax+27],'N'	   
	jne cadena1423
	cmp TFJC[14*MaMax+30],'N'
	jne cadena1423			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena1423:
    cmp TFJC[12*MaMax+23],'N'	   	
	je conlibertad1423
	cmp TFJC[14*MaMax+19],'N'	
	je  conlibertad1423
	cmp TFJC[14*MaMax+27],'N'	
	je  conlibertad1423
	jmp ComLib1423    

	ComLib1423:
	cmp TFJC[12*MaMax+23],32	   	
	je conlibertad1423
	cmp TFJC[14*MaMax+19],32	
	je  conlibertad1423
	cmp TFJC[14*MaMax+27],32	
	je  conlibertad1423
	jmp sinlibertad1423
	
	sinlibertad1423:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1423:
	cmp TFJC[14*MaMax+23],32
	je  pintarNegra1423 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1423:
    mov TFJC +14*MaMax+22,70 
    mov TFJC +14*MaMax+23,78 	
    inc contjcFN	
	jmp salidablancas

	asignapf2:
	cmp TFJC[10*MaMax+23],'N'	   	
	je conlibertad1223
	cmp TFJC[12*MaMax+19],'N'	
	je  conlibertad1223
	cmp TFJC[12*MaMax+27],'N'	
	je  conlibertad1223
	cmp TFJC[14*MaMax+23],'N'	
	je  conlibertad1223
	jmp ComLib1223    

	ComLib1223:
	cmp TFJC[10*MaMax+23],32	   	
	je conlibertad1223
	cmp TFJC[12*MaMax+19],32	
	je  conlibertad1223
	cmp TFJC[12*MaMax+27],32	
	je  conlibertad1223
	cmp TFJC[14*MaMax+23],32
	je  conlibertad1223
	jmp sinlibertad1223
	
	sinlibertad1223:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1223:
	cmp TFJC[12*MaMax+23],32
	je  pintarNegra1223 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1223:
    mov TFJC +12*MaMax+22,70 
    mov TFJC +12*MaMax+23,78 	
    inc contjcFN	
	jmp salidablancas

	asignapf3:	
	cmp TFJC[8*MaMax+23],'N'	   	
	je conlibertad1023
	cmp TFJC[10*MaMax+19],'N'	
	je  conlibertad1023
	cmp TFJC[10*MaMax+27],'N'	
	je  conlibertad1023
	cmp TFJC[12*MaMax+23],'N'	
	je  conlibertad1023
	jmp ComLib1023    

	ComLib1023:
	cmp TFJC[8*MaMax+23],32	   	
	je conlibertad1023
	cmp TFJC[10*MaMax+19],32
	je  conlibertad1023
	cmp TFJC[10*MaMax+27],32
	je  conlibertad1023
	cmp TFJC[12*MaMax+23],32
	je  conlibertad1023
	jmp sinlibertad1023
	
	sinlibertad1023:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1023:
	cmp TFJC[10*MaMax+23],32
	je  pintarNegra1023
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1023:  
    mov TFJC +10*MaMax+22,70 
    mov TFJC +10*MaMax+23,78 	
    inc contjcFN		
	jmp salidablancas

	asignapf4:	
	cmp TFJC[6*MaMax+23],'N'	   	
	je  conlibertad823
	cmp TFJC[8*MaMax+19],'N'
	je  conlibertad823
	cmp TFJC[8*MaMax+27],'N'
	je  conlibertad823
	cmp TFJC[10*MaMax+23],'N'
	je  conlibertad823
	jmp ComLib823    

	ComLib823:
	cmp TFJC[6*MaMax+23],32	   	
	je  conlibertad823
	cmp TFJC[8*MaMax+19],32
	je  conlibertad823
	cmp TFJC[8*MaMax+27],32
	je  conlibertad823
	cmp TFJC[10*MaMax+23],32
	je  conlibertad823
	jmp sinlibertad823
	
	sinlibertad823:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad823:
	cmp TFJC[8*MaMax+23],32
	je  pintarNegra823
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra823:
    mov TFJC +8*MaMax+22,70 
    mov TFJC +8*MaMax+23,78 	
    inc contjcFN	
	jmp salidablancas

	asignapf5:
	cmp TFJC[4*MaMax+23],'N'	   	
	je conlibertad623
	cmp TFJC[6*MaMax+19],'N'
	je  conlibertad623
	cmp TFJC[6*MaMax+27],'N'
	je  conlibertad623
	cmp TFJC[8*MaMax+23],'N'
	je  conlibertad623
	jmp ComLib623    

	ComLib623:
	cmp TFJC[4*MaMax+23],32	   	
	je conlibertad623
	cmp TFJC[6*MaMax+19],32
	je  conlibertad623
	cmp TFJC[6*MaMax+27],32
	je  conlibertad623
	cmp TFJC[8*MaMax+23],32
	je  conlibertad623
	jmp sinlibertad623
	
	sinlibertad623:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad623:
	cmp TFJC[6*MaMax+23],32
	je  pintarNegra623 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra623:    
    mov TFJC +6*MaMax+22,70 
    mov TFJC +6*MaMax+23,78 	
    inc contjcFN		
	jmp salidablancas

	asignapf6:	
	cmp TFJC[2*MaMax+23],'N'	   	
	je conlibertad423
	cmp TFJC[4*MaMax+19],'N'
	je  conlibertad423
	cmp TFJC[4*MaMax+27],'N'
	je  conlibertad423
	cmp TFJC[6*MaMax+23],'N'
	je  conlibertad423
	jmp ComLib423    

	ComLib423:
	cmp TFJC[2*MaMax+23],32	   	
	je conlibertad423
	cmp TFJC[4*MaMax+19],32
	je  conlibertad423
	cmp TFJC[4*MaMax+27],32
	je  conlibertad423
	cmp TFJC[6*MaMax+23],32
	je  conlibertad423
	jmp sinlibertad423
	
	sinlibertad423:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad423:
	cmp TFJC[4*MaMax+23],32
	je  pintarNegra423 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra423:    
    mov TFJC +4*MaMax+22,70 
    mov TFJC +4*MaMax+23,78 	
    inc contjcFN	
	jmp salidablancas


	asignapf7:
	cmp TFJC[0*MaMax+23],'N'	   	
	je conlibertad223
	cmp TFJC[2*MaMax+19],'N'
	je  conlibertad223
	cmp TFJC[2*MaMax+27],'N'
	je  conlibertad223
	cmp TFJC[4*MaMax+23],'N'
	je  conlibertad223
	jmp ComLib223    

	ComLib223:
	cmp TFJC[0*MaMax+23],32	   	
	je conlibertad223
	cmp TFJC[2*MaMax+19],32
	je  conlibertad223
	cmp TFJC[2*MaMax+27],32
	je  conlibertad223
	cmp TFJC[4*MaMax+23],32
	je  conlibertad223
	jmp sinlibertad223
	
	sinlibertad223:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad223:
	cmp TFJC[2*MaMax+23],32
	je  pintarNegra223 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra223:
    mov TFJC +2*MaMax+22,70 
    mov TFJC +2*MaMax+23,78 	
    inc contjcFN	
	jmp salidablancas

	asignapf8:	
	cmp TFJC[0*MaMax+19],'B'	   	
	jne cadena023
	cmp TFJC[0*MaMax+27],'B'	   
	jne cadena023
	cmp TFJC[2*MaMax+23],'B'	   
	jne cadena023
	cmp TFJC[0*MaMax+30],'N'	   
	jne cadena023
	cmp TFJC[2*MaMax+27],'N'
	jne cadena023			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena023:
    cmp TFJC[0*MaMax+19],'N'	   	
	je conlibertad023
	cmp TFJC[0*MaMax+27],'N'	
	je  conlibertad023
	cmp TFJC[2*MaMax+23],'N'	
	je  conlibertad023
	jmp ComLib023    

	ComLib023:
	cmp TFJC[0*MaMax+19],32	   	
	je conlibertad023
	cmp TFJC[0*MaMax+27],32	
	je  conlibertad023
	cmp TFJC[2*MaMax+23],32	
	je  conlibertad023
	jmp sinlibertad023
	
	sinlibertad023:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad023:
	cmp TFJC[0*MaMax+23],32
	je  pintarNegra023 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra023:
    mov TFJC +0*MaMax+22,70 
    mov TFJC +0*MaMax+23,78 
    inc contjcFN		
	jmp salidablancas
	
	posiciong:
	cmp bufferInformacion[1],'1'
	je asignapg1
	cmp bufferInformacion[1],'2'
	je asignapg2
	cmp bufferInformacion[1],'3'
	je asignapg3
	cmp bufferInformacion[1],'4'
	je asignapg4
	cmp bufferInformacion[1],'5'
	je asignapg5
	cmp bufferInformacion[1],'6'
	je asignapg6
	cmp bufferInformacion[1],'7'
	je asignapg7
	cmp bufferInformacion[1],'8'
	je asignapg8
	jmp pasonegra
	
	asignapg1:
	

	cadena1427:
    cmp TFJC[12*MaMax+27],'N'	   	
	je conlibertad1427
	cmp TFJC[14*MaMax+23],'N'	
	je  conlibertad1427
	cmp TFJC[14*MaMax+30],'N'	
	je  conlibertad1427
	jmp ComLib1427    

	ComLib1427:
	cmp TFJC[12*MaMax+27],32	   	
	je conlibertad1427
	cmp TFJC[14*MaMax+23],32	
	je  conlibertad1427
	cmp TFJC[14*MaMax+30],32	
	je  conlibertad1427
	jmp sinlibertad1427
	
	sinlibertad1427:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1427:
	cmp TFJC[14*MaMax+27],32
	je  pintarNegra1427 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1427:  
    mov TFJC +14*MaMax+26,70 
    mov TFJC +14*MaMax+27,78 
    inc contjcFN		
	jmp salidablancas

	asignapg2:	
	cmp TFJC[10*MaMax+27],'N'	   	
	je conlibertad1227
	cmp TFJC[12*MaMax+23],'N'	
	je  conlibertad1227
	cmp TFJC[12*MaMax+30],'N'	
	je  conlibertad1227
	cmp TFJC[14*MaMax+27],'N'	
	je  conlibertad1227
	jmp ComLib1227    

	ComLib1227:
	cmp TFJC[10*MaMax+27],32	   	
	je conlibertad1227
	cmp TFJC[12*MaMax+23],32	
	je  conlibertad1227
	cmp TFJC[12*MaMax+30],32	
	je  conlibertad1227
	cmp TFJC[14*MaMax+27],32	
	je  conlibertad1227
	jmp sinlibertad1227
	
	sinlibertad1227:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1227:
	cmp TFJC[12*MaMax+27],32
	je  pintarNegra1227 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1227:  
    mov TFJC +12*MaMax+26,70 
    mov TFJC +12*MaMax+27,78 
    inc contjcFN		
	jmp salidablancas

	asignapg3:	
	cmp TFJC[8*MaMax+27],'N'	   	
	je conlibertad1027
	cmp TFJC[10*MaMax+23],'N'	
	je  conlibertad1027
	cmp TFJC[10*MaMax+30],'N'	
	je  conlibertad1027
	cmp TFJC[12*MaMax+27],'N'	
	je  conlibertad1027
	jmp ComLib1027    

	ComLib1027:
	cmp TFJC[8*MaMax+27],32	   	
	je conlibertad1027
	cmp TFJC[10*MaMax+23],32	
	je  conlibertad1027
	cmp TFJC[10*MaMax+30],32	
	je  conlibertad1027
	cmp TFJC[12*MaMax+27],32	
	je  conlibertad1027
	jmp sinlibertad1027
	
	sinlibertad1027:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1027:
	cmp TFJC[10*MaMax+27],32
	je  pintarNegra1027
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra1027:  
    mov TFJC +10*MaMax+26,70 
    mov TFJC +10*MaMax+27,78 
    inc contjcFN			
	jmp salidablancas

	asignapg4:	
	cmp TFJC[6*MaMax+27],'N'	   	
	je  conlibertad827
	cmp TFJC[8*MaMax+23],'N'
	je  conlibertad827
	cmp TFJC[8*MaMax+30],'N'
	je  conlibertad827
	cmp TFJC[10*MaMax+27],'N'
	je  conlibertad827
	jmp ComLib827    

	ComLib827:
	cmp TFJC[6*MaMax+27],32	   	
	je  conlibertad827
	cmp TFJC[8*MaMax+23],32
	je  conlibertad827
	cmp TFJC[8*MaMax+30],32
	je  conlibertad827
	cmp TFJC[10*MaMax+27],32
	je  conlibertad827
	jmp sinlibertad827
	
	sinlibertad827:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad827:
	cmp TFJC[8*MaMax+27],32
	je  pintarNegra827
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra827:
    mov TFJC +8*MaMax+26,70 
    mov TFJC +8*MaMax+27,78 	
    inc contjcFN	
	jmp salidablancas

	asignapg5:
	cmp TFJC[4*MaMax+27],'N'	   	
	je conlibertad627
	cmp TFJC[6*MaMax+23],'N'
	je  conlibertad627
	cmp TFJC[6*MaMax+30],'N'
	je  conlibertad627
	cmp TFJC[8*MaMax+27],'N'
	je  conlibertad627
	jmp ComLib627    

	ComLib627:
	cmp TFJC[4*MaMax+27],32	   	
	je conlibertad627
	cmp TFJC[6*MaMax+23],32
	je  conlibertad627
	cmp TFJC[6*MaMax+30],32
	je  conlibertad627
	cmp TFJC[8*MaMax+27],32
	je  conlibertad627
	jmp sinlibertad627
	
	sinlibertad627:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad627:
	cmp TFJC[6*MaMax+27],32
	je  pintarNegra627 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra627:    
    mov TFJC +6*MaMax+26,70 
    mov TFJC +6*MaMax+27,78 	
    inc contjcFN		
	jmp salidablancas

	asignapg6:
	cmp TFJC[2*MaMax+27],'N'	   	
	je conlibertad427
	cmp TFJC[4*MaMax+23],'N'
	je  conlibertad427
	cmp TFJC[4*MaMax+30],'N'
	je  conlibertad427
	cmp TFJC[6*MaMax+27],'N'
	je  conlibertad427
	jmp ComLib427    

	ComLib427:
	cmp TFJC[2*MaMax+27],32	   	
	je conlibertad427
	cmp TFJC[4*MaMax+23],32
	je  conlibertad427
	cmp TFJC[4*MaMax+30],32
	je  conlibertad427
	cmp TFJC[6*MaMax+27],32
	je  conlibertad427
	jmp sinlibertad427
	
	sinlibertad427:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad427:
	cmp TFJC[4*MaMax+27],32
	je  pintarNegra427 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra427:    
    mov TFJC +4*MaMax+26,70 
    mov TFJC +4*MaMax+27,78 	
    inc contjcFN	
	jmp salidablancas


	asignapg7:
	cmp TFJC[0*MaMax+27],'N'	   	
	je conlibertad227
	cmp TFJC[2*MaMax+23],'N'
	je  conlibertad227
	cmp TFJC[2*MaMax+30],'N'
	je  conlibertad227
	cmp TFJC[4*MaMax+27],'N'
	je  conlibertad227
	jmp ComLib227    

	ComLib227:
	cmp TFJC[0*MaMax+27],32	   	
	je conlibertad227
	cmp TFJC[2*MaMax+23],32
	je  conlibertad227
	cmp TFJC[2*MaMax+30],32
	je  conlibertad227
	cmp TFJC[4*MaMax+27],32
	je  conlibertad227
	jmp sinlibertad227
	
	sinlibertad227:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad227:
	cmp TFJC[2*MaMax+27],32
	je  pintarNegra227 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra227:    
    mov TFJC +2*MaMax+26,70 
    mov TFJC +2*MaMax+27,78 	
    inc contjcFN	
	jmp salidablancas

	asignapg8:
	

	cadena027:
    cmp TFJC[0*MaMax+23],'N'	   	
	je conlibertad027
	cmp TFJC[0*MaMax+30],'N'	
	je  conlibertad027
	cmp TFJC[2*MaMax+27],'N'	
	je  conlibertad027
	jmp ComLib027    

	ComLib027:
	cmp TFJC[0*MaMax+23],32	   	
	je conlibertad027
	cmp TFJC[0*MaMax+30],32	
	je  conlibertad027
	cmp TFJC[2*MaMax+27],32	
	je  conlibertad027
	jmp sinlibertad027
	
	sinlibertad027:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad027:
	cmp TFJC[0*MaMax+27],32
	je  pintarNegra027 
    mostrar PosOcupada
	jmp TurnoNegras	

	pintarNegra027:
    mov TFJC +0*MaMax+26,70 
    mov TFJC +0*MaMax+27,78 	
    inc contjcFN	
	jmp salidablancas

	posicionh:
	cmp bufferInformacion[1],'1'
	je asignaph1
	cmp bufferInformacion[1],'2'
	je asignaph2
	cmp bufferInformacion[1],'3'
	je asignaph3
	cmp bufferInformacion[1],'4'
	je asignaph4
	cmp bufferInformacion[1],'5'
	je asignaph5
	cmp bufferInformacion[1],'6'
	je asignaph6
	cmp bufferInformacion[1],'7'
	je asignaph7
	cmp bufferInformacion[1],'8'
	je asignaph8
	jmp pasonegra
	
	asignaph1:

	cmp TFJC[12*MaMax+30],'N'	   	
	je conlibertad1430
	cmp TFJC[14*MaMax+27],'N'	
	je conlibertad1430	
	jmp ComLib1430

	ComLib1430:
	cmp TFJC[12*MaMax+30],32	   	
	je conlibertad1430
	cmp TFJC[14*MaMax+27],32	
	je conlibertad1430	
	jmp sinlibertad1430

	
	sinlibertad1430:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1430:
	cmp TFJC[14*MaMax+30],32
	je  pintarNegra1430 
    mostrar PosOcupada
	jmp salidablancas	

	pintarNegra1430:   
	mov TFJC +14*MaMax+29,70 
    mov TFJC +14*MaMax+30,78 
    inc contjcFN		
	jmp salidablancas    

	asignaph2:	

	
	cmp TFJC[10*MaMax+30],'N'	   	
	je conlibertad1230
	cmp TFJC[12*MaMax+27],'N'		
	je  conlibertad1230
	cmp TFJC[14*MaMax+30],'N'		
	je  conlibertad1230
	jmp ComLib1230

	ComLib1230:	
	cmp TFJC[10*MaMax+30],32	   	
	je conlibertad1230
	cmp TFJC[12*MaMax+27],32		
	je  conlibertad1230
	cmp TFJC[14*MaMax+30],32		
	je  conlibertad1230
	jmp sinlibertad1230

	sinlibertad1230:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1230:
	cmp TFJC[12*MaMax+30],32
	je  pintarNegra1230 
    mostrar PosOcupada
	jmp TurnoNegras

	pintarNegra1230:
    mov TFJC +12*MaMax+29,70 
    mov TFJC +12*MaMax+30,78 
    inc contjcFN		
	jmp salidablancas

	asignaph3:

	
	cmp TFJC[8*MaMax+3],'B'	   	
	jne cadena1030
	cmp TFJC[10*MaMax+27],'B'	   
	jne cadena1030
	cmp TFJC[12*MaMax+30],'B'	   
	jne cadena1030
	cmp TFJC[12*MaMax+27],'N'	   
	jne cadena1030
	cmp TFJC[14*MaMax+30],'N'
	jne cadena1030			
	mostrar ReglaKo
	jmp salidablancas


	cadena1030:
	cmp TFJC[8*MaMax+30],'N'	   	
	je conlibertad1030
	cmp TFJC[10*MaMax+27],'N'
	je  conlibertad1030
	cmp TFJC[12*MaMax+30],'N'
	je  conlibertad1030
	jmp ComLib1030

	ComLib1030:
    cmp TFJC[8*MaMax+30],32	   	
	je conlibertad1030
	cmp TFJC[10*MaMax+27],32
	je  conlibertad1030
	cmp TFJC[12*MaMax+30],32
	je  conlibertad1030
	jmp sinlibertad1030

	sinlibertad1030:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad1030:
	cmp TFJC[10*MaMax+30],32
	je  pintarNegra1030 
    mostrar PosOcupada
	jmp TurnoNegras

	pintarNegra1030:
    mov TFJC +10*MaMax+29,70 
    mov TFJC +10*MaMax+30,78 
    inc contjcFN			
	jmp salidablancas

	asignaph4:


	cmp TFJC[6*MaMax+30],'B'	   	
	jne cadena830
	cmp TFJC[8*MaMax+27],'B'	   
	jne cadena830
	cmp TFJC[10*MaMax+30],'B'	   
	jne cadena830
	cmp TFJC[10*MaMax+27],'N'	   
	jne cadena830
	cmp TFJC[12*MaMax+30],'N'
	jne cadena830			
	mostrar ReglaKo
	jmp TurnoNegras


	cadena830:
	cmp TFJC[6*MaMax+30],'N'	   	
	je conlibertad830
	cmp TFJC[8*MaMax+27],'N'	   	
	je  conlibertad830
	cmp TFJC[10*MaMax+30],'N'	   	
	je  conlibertad830
	jmp ComLib830

    ComLib830:
    cmp TFJC[6*MaMax+30],32	   	
	je conlibertad830
	cmp TFJC[8*MaMax+27],32	   	
	je  conlibertad830
	cmp TFJC[10*MaMax+30],32   	
	je  conlibertad830
	jmp sinlibertad830

	sinlibertad830:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad830:
	cmp TFJC[8*MaMax+30],32
	je  pintarNegra830 
    mostrar PosOcupada
	jmp TurnoNegras

	pintarNegra830:    
    mov TFJC +8*MaMax+29,70 
    mov TFJC +8*MaMax+30,78 	
    inc contjcFN	
	jmp salidablancas

	asignaph5:	
	cmp TFJC[4*MaMax+30],'B'	   	
	jne cadena630
	cmp TFJC[6*MaMax+27],'B'	   
	jne cadena630
	cmp TFJC[8*MaMax+30],'B'	   
	jne cadena630
	cmp TFJC[8*MaMax+27],'N'	   
	jne cadena630
	cmp TFJC[10*MaMax+30],'N'
	jne cadena630			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena630:
	cmp TFJC[4*MaMax+30],'N'	   	
	je conlibertad630
	cmp TFJC[6*MaMax+27],'N'		
	je  conlibertad630
	cmp TFJC[8*MaMax+30],'N'		
	je  conlibertad630
	jmp ComLib1630

    ComLib1630:           
    cmp TFJC[4*MaMax+30],32	   	
	je conlibertad630
	cmp TFJC[6*MaMax+27],32		
	je  conlibertad630
	cmp TFJC[8*MaMax+30],32		
	je  conlibertad630
	jmp sinlibertad630

	sinlibertad630:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad630:
	cmp TFJC[6*MaMax+30],32
	je  pintarNegra630 
    mostrar PosOcupada
	jmp TurnoNegras

	pintarNegra630:
    mov TFJC +6*MaMax+29,70 
    mov TFJC +6*MaMax+30,78 	
    inc contjcFN		
	jmp salidablancas

	asignaph6:	

	cmp TFJC[2*MaMax+30],'B'	   	
	jne cadena430
	cmp TFJC[4*MaMax+27],'B'	   
	jne cadena430
	cmp TFJC[6*MaMax+30],'B'	   
	jne cadena430
	cmp TFJC[6*MaMax+27],'N'	   
	jne cadena430
	cmp TFJC[8*MaMax+30],'N'
	jne cadena430			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena430:
    cmp TFJC[2*MaMax+30],'N'	   	
	je conlibertad430
	cmp TFJC[4*MaMax+27],'N'
	je  conlibertad430
	cmp TFJC[6*MaMax+30],'N'
	je  conlibertad430
	jmp ComLib430

    ComLib430:   
    cmp TFJC[2*MaMax+30],32	   	
	je conlibertad430
	cmp TFJC[4*MaMax+27],32
	je  conlibertad430
	cmp TFJC[6*MaMax+30],32
	je  conlibertad430
	jmp sinlibertad430

	sinlibertad430:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad430:
	cmp TFJC[4*MaMax+30],32
	je  pintarNegra430 
    mostrar PosOcupada
	jmp TurnoNegras

	pintarNegra430:
    mov TFJC +4*MaMax+29,70 
    mov TFJC +4*MaMax+30,78 	
    inc contjcFN	
	jmp salidablancas

	asignaph7:	

	cmp TFJC[0*MaMax+30],'B'	   	
	jne cadena230
	cmp TFJC[2*MaMax+27],'B'	   
	jne cadena230
	cmp TFJC[4*MaMax+30],'B'	   
	jne cadena230
	cmp TFJC[4*MaMax+27],'N'	   
	jne cadena230
	cmp TFJC[6*MaMax+30],'N'
	jne cadena230			
	mostrar ReglaKo
	jmp TurnoNegras

	cadena230:
    cmp TFJC[0*MaMax+30],'N'	   	
	je conlibertad230
	cmp TFJC[2*MaMax+27],'N'	
	je  conlibertad230
	cmp TFJC[6*MaMax+30],'N'	
	je  conlibertad230
	jmp ComLib230


    ComLib230:
    cmp TFJC[0*MaMax+30],32	   	
	je conlibertad230
	cmp TFJC[2*MaMax+27],32	
	je  conlibertad230
	cmp TFJC[6*MaMax+30],32	
	je  conlibertad230
	jmp sinlibertad230

	sinlibertad230:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad230:
	cmp TFJC[2*MaMax+30],32
	je  pintarNegra230 
    mostrar PosOcupada
	jmp TurnoNegras

	pintarNegra230:
    mov TFJC +2*MaMax+29,70 
    mov TFJC +2*MaMax+30,78 	
    inc contjcFN	
	jmp salidablancas

    
	asignaph8:
	cmp TFJC[0*MaMax+27],'N'	   	
	je conlibertad030
	cmp TFJC[2*MaMax+30],'N'		
	je  conlibertad030	
	jmp ComLib030

    ComLib030:
    cmp TFJC[0*MaMax+27],32	   	
	je conlibertad030
	cmp TFJC[2*MaMax+30],32		
	je  conlibertad030	
	jmp sinlibertad030

	sinlibertad030:
	mostrar PosSuicidio
	jmp TurnoNegras

	conlibertad030:
	cmp TFJC[0*MaMax+30],32
	je  pintarNegra030 
    mostrar PosOcupada
	jmp TurnoNegras

	pintarNegra030:
    mov TFJC +0*MaMax+29,70 
    mov TFJC +0*MaMax+30,78 	
    inc contjcFN	
	jmp salidablancas
    
	pasonegra:
	mostrar coordenaIncorrecta
	jmp     TurnoNegras

	salidablancas:
	mostrar TFJC       
	jmp  TurnoBlancas

	
    TurnoBlancas:	
    Limpiar bufferInformacion,SIZEOF bufferInformacion,24h
	mostrar matrintb 
    ObtenerTexto bufferInformacion
	mostrar LineaVacia 
	mostrar LineaVacia


	; PARA FICHAS BLANCAS
	cmp bufferInformacion[0],'A'	
	je  Bposiciona
	cmp bufferInformacion[0],'B'	
	je  Bposicionb
	cmp bufferInformacion[0],'C'	
	je  Bposicionc
	cmp bufferInformacion[0],'D'	
	je  Bposiciond
	cmp bufferInformacion[0],'E'	
	je  BexittCe
	cmp bufferInformacion[0],'F'	
	je  Bposicionf	
	cmp bufferInformacion[0],'G'	
	je  Bposiciong	
	cmp bufferInformacion[0],'H'	
	je  Bposicionh	
	cmp bufferInformacion[0],'P'	
	je  Bpass1
	cmp bufferInformacion[0],'S'	
	je  Bsaveshow
	jmp pasoblancas


	Bpass1:
	cmp bufferInformacion[1],'A'
	jne TurnoBlancas
	cmp bufferInformacion[2],'S'	
	jne TurnoBlancas
	cmp bufferInformacion[3],'S'	
	jne TurnoBlancas
	;PassBlancas
	mov al,'1'	
	mov JuegoFinal[1],al	

    cmp JuegoFinal[0],'1'
	jne	TurnoNegras
	cmp JuegoFinal[1],'1'
	jne TurnoBlancas
	jmp opcionReporteFinal	


	Bsaveshow:	
	cmp bufferInformacion[1],'A'
	je  Bsave
	cmp bufferInformacion[1],'H'	
	je  Bshow
	jmp TurnoBlancas	

	Bsave:
	cmp bufferInformacion[2],'V'	
	jne TurnoBlancas
	cmp bufferInformacion[3],'E'	
	jne TurnoBlancas
	;mostrar GuardarArchivo 
	jmp opcion7

	Bshow:	
	cmp bufferInformacion[2],'O'	
	jne TurnoBlancas
	cmp bufferInformacion[3],'W'	
	jne TurnoBlancas	
	mostrar MsgHtmlCreado 
	mostrar MostrarJuego 		
	jmp opcion6


	BexittCe:
	cmp bufferInformacion[1],'X'
	je Bexituno
	cmp bufferInformacion[1],'1'
	je Basignape1
	cmp bufferInformacion[1],'2'
	je Basignape2
	cmp bufferInformacion[1],'3'
	je Basignape3
	cmp bufferInformacion[1],'4'
	je Basignape4
	cmp bufferInformacion[1],'5'
	je Basignape5
	cmp bufferInformacion[1],'6'
	je Basignape6
	cmp bufferInformacion[1],'7'
	je Basignape7
	cmp bufferInformacion[1],'8'
	je Basignape8
	jmp pasoblancas	

	Bexituno:		
	cmp bufferInformacion[2],'I'	
	jne TurnoBlancas
	cmp bufferInformacion[3],'T'	
	jne TurnoBlancas
	jmp encabezado


	Basignape1:
	cmp TFJC[12*MaMax+19],'N'	   	
	jne Bcadena1419
	cmp TFJC[14*MaMax+15],'N'	   
	jne Bcadena1419
	cmp TFJC[14*MaMax+23],'N'	   
	jne Bcadena1419
	cmp TFJC[12*MaMax+23],'B'	   
	jne Bcadena1419
	cmp TFJC[14*MaMax+27],'B'
	jne Bcadena1419			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena1419:
    cmp TFJC[12*MaMax+19],'B'	   	
	je Bconlibertad1419
	cmp TFJC[14*MaMax+15],'B'	
	je  Bconlibertad1419
	cmp TFJC[14*MaMax+23],'B'	
	je  Bconlibertad1419
	jmp BComLib1419    

	BComLib1419:
	cmp TFJC[12*MaMax+19],32	   	
	je Bconlibertad1419
	cmp TFJC[14*MaMax+15],32	   	
	je  Bconlibertad1419
	cmp TFJC[14*MaMax+23],32	   	
	je  Bconlibertad1419
	jmp Bsinlibertad1419
	
	Bsinlibertad1419:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1419:
	cmp TFJC[14*MaMax+19],32
	je  BpintarNegra1419 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1419:
    mov TFJC +14*MaMax+18,70 
    mov TFJC +14*MaMax+19,66     
    inc contjcFB	
	jmp salidanegras    

	Basignape2:	
	cmp TFJC[10*MaMax+19],'B'	   	
	je Bconlibertad1219
	cmp TFJC[12*MaMax+15],'B'	
	je  Bconlibertad1219
	cmp TFJC[12*MaMax+23],'B'	
	je  Bconlibertad1219
	cmp TFJC[14*MaMax+19],'B'	
	je  Bconlibertad1219
	jmp BComLib1219    

	BComLib1219:
	cmp TFJC[10*MaMax+19],32	   	
	je Bconlibertad1219
	cmp TFJC[12*MaMax+15],32	   	
	je  Bconlibertad1219
	cmp TFJC[12*MaMax+23],32	   	
	je  Bconlibertad1219
	cmp TFJC[14*MaMax+19],32	   	
	je  Bconlibertad1219
	jmp Bsinlibertad1219
	
	Bsinlibertad1219:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1219:
	cmp TFJC[12*MaMax+19],32
	je  BpintarNegra1219 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1219:
    mov TFJC +12*MaMax+18,70 
    mov TFJC +12*MaMax+19,66 
    inc contjcFB	
	jmp salidanegras
   

	Basignape3:

	cmp TFJC[8*MaMax+19],'B'	   	
	je Bconlibertad1019
	cmp TFJC[10*MaMax+15],'B'	
	je  Bconlibertad1019
	cmp TFJC[10*MaMax+23],'B'	
	je  Bconlibertad1019
	cmp TFJC[12*MaMax+19],'B'	
	je  Bconlibertad1019
	jmp BComLib1019    

	BComLib1019:
	cmp TFJC[8*MaMax+19],32	   	
	je Bconlibertad1019
	cmp TFJC[10*MaMax+15],32
	je  Bconlibertad1019
	cmp TFJC[10*MaMax+23],32
	je  Bconlibertad1019
	cmp TFJC[12*MaMax+19],32
	je  Bconlibertad1019
	jmp Bsinlibertad1019
	
	Bsinlibertad1019:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1019:
	cmp TFJC[10*MaMax+19],32
	je  BpintarNegra1019 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1019:
    mov TFJC +10*MaMax+18,70 
    mov TFJC +10*MaMax+19,66 
    inc contjcFB		
	jmp salidanegras

    

	Basignape4:
	cmp TFJC[6*MaMax+19],'B'	   	
	je Bconlibertad819
	cmp TFJC[8*MaMax+15],'B'
	je  Bconlibertad819
	cmp TFJC[8*MaMax+23],'B'
	je  Bconlibertad819
	cmp TFJC[10*MaMax+19],'B'
	je  Bconlibertad819
	jmp BComLib819    

	BComLib819:
	cmp TFJC[6*MaMax+19],32	   	
	je Bconlibertad819
	cmp TFJC[8*MaMax+15],32	   	
	je  Bconlibertad819
	cmp TFJC[8*MaMax+23],32	   	
	je  Bconlibertad819
	cmp TFJC[10*MaMax+19],32	   	
	je  Bconlibertad819
	jmp Bsinlibertad819
	
	Bsinlibertad819:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad819:
	cmp TFJC[8*MaMax+19],32
	je  BpintarNegra819
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra819:
    mov TFJC +8*MaMax+18,70 
    mov TFJC +8*MaMax+19,66 	
    inc contjcFB
	jmp salidanegras  

	Basignape5:

	cmp TFJC[4*MaMax+19],'B'	   	
	je Bconlibertad619
	cmp TFJC[6*MaMax+15],'B'
	je  Bconlibertad619
	cmp TFJC[6*MaMax+23],'B'
	je  Bconlibertad619
	cmp TFJC[8*MaMax+19],'B'
	je  Bconlibertad619
	jmp BComLib619    

	BComLib619:
	cmp TFJC[4*MaMax+19],32	   	
	je Bconlibertad619
	cmp TFJC[6*MaMax+15],32
	je  Bconlibertad619
	cmp TFJC[6*MaMax+23],32
	je  Bconlibertad619
	cmp TFJC[8*MaMax+19],32
	je  Bconlibertad619
	jmp Bsinlibertad619
	
	Bsinlibertad619:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad619:
	cmp TFJC[6*MaMax+19],32
	je  BpintarNegra619 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra619:    
    mov TFJC +6*MaMax+18,70 
    mov TFJC +6*MaMax+19,66 	
    inc contjcFB	
	jmp salidanegras
  

	Basignape6:	
	cmp TFJC[2*MaMax+19],'B'	   	
	je Bconlibertad419
	cmp TFJC[4*MaMax+15],'B'
	je  Bconlibertad419
	cmp TFJC[4*MaMax+23],'B'
	je  Bconlibertad419
	cmp TFJC[6*MaMax+19],'B'
	je  Bconlibertad419
	jmp BComLib419    

	BComLib419:
	cmp TFJC[2*MaMax+19],32	   	
	je Bconlibertad419
	cmp TFJC[4*MaMax+15],32
	je  Bconlibertad419
	cmp TFJC[4*MaMax+23],32
	je  Bconlibertad419
	cmp TFJC[6*MaMax+19],32
	je  Bconlibertad419
	jmp Bsinlibertad419
	
	Bsinlibertad419:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad419:
	cmp TFJC[4*MaMax+19],32
	je  BpintarNegra419 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra419:
    mov TFJC +4*MaMax+18,70 
    mov TFJC +4*MaMax+19,66 	
    inc contjcFB
	jmp salidanegras  

	Basignape7:
	cmp TFJC[0*MaMax+19],'B'	   	
	je Bconlibertad219
	cmp TFJC[2*MaMax+15],'B'
	je  Bconlibertad219
	cmp TFJC[2*MaMax+23],'B'
	je  Bconlibertad219
	cmp TFJC[4*MaMax+19],'B'
	je  Bconlibertad219
	jmp BComLib219    

	BComLib219:
	cmp TFJC[0*MaMax+19],32	   	
	je Bconlibertad219
	cmp TFJC[2*MaMax+15],32
	je  Bconlibertad219
	cmp TFJC[2*MaMax+23],32
	je  Bconlibertad219
	cmp TFJC[4*MaMax+19],32
	je  Bconlibertad219
	jmp Bsinlibertad219
	
	Bsinlibertad219:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad219:
	cmp TFJC[2*MaMax+19],32
	je  BpintarNegra219 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra219:
    mov TFJC +2*MaMax+18,70 
    mov TFJC +2*MaMax+19,66 	
    inc contjcFB
	jmp salidanegras
    

	Basignape8:
	cmp TFJC[0*MaMax+15],'N'	   	
	jne Bcadena019
	cmp TFJC[0*MaMax+23],'N'	   
	jne Bcadena019
	cmp TFJC[2*MaMax+19],'N'	   
	jne Bcadena019
	cmp TFJC[0*MaMax+27],'B'	   
	jne Bcadena019
	cmp TFJC[2*MaMax+23],'B'
	jne Bcadena019			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena019:
    cmp TFJC[0*MaMax+15],'B'	   	
	je Bconlibertad019
	cmp TFJC[0*MaMax+23],'B'	
	je  Bconlibertad019
	cmp TFJC[2*MaMax+19],'B'	
	je  Bconlibertad019
	jmp BComLib019    

	BComLib019:
	cmp TFJC[0*MaMax+15],32	   	
	je Bconlibertad019
	cmp TFJC[0*MaMax+23],32	
	je  Bconlibertad019
	cmp TFJC[2*MaMax+19],32	
	je  Bconlibertad019
	jmp Bsinlibertad019
	
	Bsinlibertad019:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad019:
	cmp TFJC[0*MaMax+19],32
	je  BpintarNegra019 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra019:
    mov TFJC +0*MaMax+18,70 
    mov TFJC +0*MaMax+19,66  
    inc contjcFB	
	jmp salidanegras  

	Bposiciona:

	cmp bufferInformacion[1],'1'
	je Basignapa1
	cmp bufferInformacion[1],'2'
	je Basignapa2
	cmp bufferInformacion[1],'3'
	je Basignapa3
	cmp bufferInformacion[1],'4'
	je Basignapa4
	cmp bufferInformacion[1],'5'
	je Basignapa5
	cmp bufferInformacion[1],'6'
	je Basignapa6
	cmp bufferInformacion[1],'7'
	je Basignapa7
	cmp bufferInformacion[1],'8'
	je Basignapa8
	jmp pasoblancas
	
	Basignapa1:

	cmp TFJC[12*MaMax+3],'B'	   	
	je Bconlibertad143
	cmp TFJC[14*MaMax+7],'B'	
	je Bconlibertad143	
	jmp BComLib143

	BComLib143:
	cmp TFJC[12*MaMax+3],32	   	
	je Bconlibertad143
	cmp TFJC[14*MaMax+7],32		
	je  Bconlibertad143	
	jmp Bsinlibertad143

	
	Bsinlibertad143:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad143:
	cmp TFJC[14*MaMax+3],32
	je  BpintarNegra143 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra143:
    mov TFJC +14*MaMax+2,70 
    mov TFJC +14*MaMax+3,66		
    inc contjcFB
	jmp salidanegras
	
	Basignapa2:

	cmp TFJC[8*MaMax+3],'B'	   	
	jne Bcadena123
	cmp TFJC[10*MaMax+3],'N'	   
	jne Bcadena123
	cmp TFJC[10*MaMax+7],'B'	   
	jne Bcadena123
	cmp TFJC[12*MaMax+7],'N'	   
	jne Bcadena123
	cmp TFJC[14*MaMax+3],'N'
	jne Bcadena123			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena123:
	cmp TFJC[10*MaMax+3],'B'	   	
	je Bconlibertad123
	cmp TFJC[12*MaMax+7],'B'		
	je  Bconlibertad123
	cmp TFJC[14*MaMax+3],'B'		
	je  Bconlibertad123
	jmp BComLib123

	BComLib123:	
	cmp TFJC[10*MaMax+3],32	   	
	je Bconlibertad123
	cmp TFJC[12*MaMax+7],32		
	je  Bconlibertad123
	cmp TFJC[14*MaMax+3],32		
	je  Bconlibertad123
	jmp Bsinlibertad123

	Bsinlibertad123:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad123:
	cmp TFJC[12*MaMax+3],32
	je  BpintarNegra123 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra123:
    mov TFJC +12*MaMax+2,70 
    mov TFJC +12*MaMax+3,66	
    inc contjcFB
	jmp salidanegras
	
    

	Basignapa3:

	cmp TFJC[6*MaMax+3],'B'	   	
	jne Bcadena103
	cmp TFJC[8*MaMax+3],'N'	   
	jne Bcadena103
	cmp TFJC[8*MaMax+7],'B'	   
	jne Bcadena103
	cmp TFJC[10*MaMax+7],'N'	   
	jne Bcadena103
	cmp TFJC[12*MaMax+3],'N'
	jne Bcadena103			
	mostrar ReglaKo
	jmp TurnoBlancas

	cmp TFJC[8*MaMax+3],'N'	   	
	jne Bcadena103
	cmp TFJC[10*MaMax+7],'N'	   
	jne Bcadena103
	cmp TFJC[12*MaMax+3],'N'	   
	jne Bcadena103
	cmp TFJC[12*MaMax+7],'B'	   
	jne Bcadena103
	cmp TFJC[14*MaMax+3],'B'
	jne Bcadena103			
	mostrar ReglaKo
	jmp TurnoBlancas


	Bcadena103:
	cmp TFJC[8*MaMax+3],'B'	   	
	je Bconlibertad103
	cmp TFJC[10*MaMax+7],'B'
	je  Bconlibertad103
	cmp TFJC[12*MaMax+3],'B'
	je  Bconlibertad103
	jmp BComLib103

	BComLib103:
    cmp TFJC[8*MaMax+3],32	   	
	je Bconlibertad103
	cmp TFJC[10*MaMax+7],32		
	je  Bconlibertad103
	cmp TFJC[12*MaMax+3],32		
	je  Bconlibertad103
	jmp Bsinlibertad103

	Bsinlibertad103:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad103:
	cmp TFJC[10*MaMax+3],32
	je  BpintarNegra103 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra103:
    mov TFJC +10*MaMax+2,70 
    mov TFJC +10*MaMax+3,66		
    inc contjcFB	
	jmp salidanegras    

	Basignapa4:	

	cmp TFJC[4*MaMax+3],'B'	   	
	jne Bcadena83
	cmp TFJC[6*MaMax+3],'N'	   
	jne Bcadena83
	cmp TFJC[6*MaMax+7],'B'	   
	jne Bcadena83
	cmp TFJC[8*MaMax+7],'N'	   
	jne Bcadena83
	cmp TFJC[10*MaMax+3],'N'
	jne Bcadena83			
	mostrar ReglaKo
	jmp TurnoBlancas

	cmp TFJC[6*MaMax+3],'N'	   	
	jne Bcadena83
	cmp TFJC[8*MaMax+7],'N'	   
	jne Bcadena83
	cmp TFJC[10*MaMax+3],'N'	   
	jne Bcadena83
	cmp TFJC[10*MaMax+7],'B'	   
	jne Bcadena83
	cmp TFJC[12*MaMax+3],'B'
	jne Bcadena83			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena83:
	cmp TFJC[6*MaMax+3],'B'	   	
	je Bconlibertad83
	cmp TFJC[8*MaMax+7],'B'	   	
	je  Bconlibertad83
	cmp TFJC[10*MaMax+3],'B'	   	
	je  Bconlibertad83
	jmp BComLib83

    BComLib83:
    cmp TFJC[6*MaMax+3],32	   	
	je Bconlibertad83
	cmp TFJC[8*MaMax+7],32		
	je  Bconlibertad83
	cmp TFJC[10*MaMax+3],32		
	je  Bconlibertad83
	jmp Bsinlibertad83

	Bsinlibertad83:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad83:
	cmp TFJC[8*MaMax+3],32
	je  BpintarNegra83 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra83:
    mov TFJC +8*MaMax+2,70 
    mov TFJC +8*MaMax+3,66		
    inc contjcFB
	jmp salidanegras
    

	Basignapa5:	

	cmp TFJC[2*MaMax+3],'B'	   	
	jne Bcadena63
	cmp TFJC[4*MaMax+3],'N'	   
	jne Bcadena63
	cmp TFJC[4*MaMax+7],'B'	   
	jne Bcadena63
	cmp TFJC[6*MaMax+7],'N'	   
	jne Bcadena63
	cmp TFJC[8*MaMax+3],'N'
	jne Bcadena63			
	mostrar ReglaKo
	jmp TurnoBlancas

	cmp TFJC[4*MaMax+3],'N'	   	
	jne Bcadena63
	cmp TFJC[6*MaMax+7],'N'	   
	jne Bcadena63
	cmp TFJC[8*MaMax+3],'N'	   
	jne Bcadena63
	cmp TFJC[8*MaMax+7],'B'	   
	jne Bcadena63
	cmp TFJC[10*MaMax+3],'B'
	jne Bcadena63			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena63:
	cmp TFJC[4*MaMax+3],'B'	   	
	je Bconlibertad63
	cmp TFJC[6*MaMax+7],'B'		
	je Bconlibertad63
	cmp TFJC[8*MaMax+3],'B'		
	je Bconlibertad63
	jmp BComLib163

    BComLib163:           
    cmp TFJC[4*MaMax+3],32	   	
	je Bconlibertad63
	cmp TFJC[6*MaMax+7],32		
	je Bconlibertad63
	cmp TFJC[8*MaMax+3],32		
	je Bconlibertad63
	jmp Bsinlibertad63

	Bsinlibertad63:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad63:
	cmp TFJC[6*MaMax+3],32
	je  BpintarNegra63 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra63:
    mov TFJC +6*MaMax+2,70 
    mov TFJC +6*MaMax+3,66		
    inc contjcFB
	jmp salidanegras


	Basignapa6:	

	cmp TFJC[0*MaMax+3],'B'	   	
	jne Bcadena43
	cmp TFJC[2*MaMax+3],'N'	   
	jne Bcadena43
	cmp TFJC[2*MaMax+7],'B'	   
	jne Bcadena43
	cmp TFJC[4*MaMax+7],'N'	   
	jne Bcadena43
	cmp TFJC[6*MaMax+3],'N'
	jne Bcadena43			
	mostrar ReglaKo
	jmp TurnoBlancas

	cmp TFJC[2*MaMax+3],'N'	   	
	jne Bcadena43
	cmp TFJC[4*MaMax+7],'N'	   
	jne Bcadena43
	cmp TFJC[6*MaMax+3],'N'	   
	jne Bcadena43
	cmp TFJC[6*MaMax+7],'B'	   
	jne Bcadena43
	cmp TFJC[8*MaMax+3],'B'
	jne Bcadena43			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena43:
    cmp TFJC[2*MaMax+3],'B'	   	
	je Bconlibertad43
	cmp TFJC[4*MaMax+7],'B'
	je Bconlibertad43
	cmp TFJC[6*MaMax+3],'B'
	je Bconlibertad43
	jmp BComLib43

    BComLib43:   
    cmp TFJC[2*MaMax+3],32	   	
	je Bconlibertad43
	cmp TFJC[4*MaMax+7],32		
	je Bconlibertad43
	cmp TFJC[6*MaMax+3],32		
	je Bconlibertad43
	jmp Bsinlibertad43

	Bsinlibertad43:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad43:
	cmp TFJC[4*MaMax+3],32
	je  BpintarNegra43 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra43:
    mov TFJC +4*MaMax+2,70 
    mov TFJC +4*MaMax+3,66		
    inc contjcFB
	jmp salidanegras    

	Basignapa7:	

	cmp TFJC[0*MaMax+3],'N'	   	
	jne Bcadena23
	cmp TFJC[2*MaMax+7],'N'	   
	jne Bcadena23
	cmp TFJC[4*MaMax+3],'N'	   
	jne Bcadena23
	cmp TFJC[4*MaMax+7],'B'	   
	jne Bcadena23
	cmp TFJC[6*MaMax+3],'B'
	jne Bcadena23			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena23:
    cmp TFJC[0*MaMax+3],'B'	   	
	je  Bconlibertad23
	cmp TFJC[2*MaMax+7],'B'	
	je  Bconlibertad23
	cmp TFJC[6*MaMax+3],'B'	
	je  Bconlibertad23
	jmp BComLib23


    BComLib23:
    cmp TFJC[0*MaMax+3],32	   	
	je Bconlibertad23
	cmp TFJC[2*MaMax+7],32		
	je Bconlibertad23
	cmp TFJC[6*MaMax+3],32		
	je Bconlibertad23
	jmp Bsinlibertad23

	Bsinlibertad23:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad23:
	cmp TFJC[2*MaMax+3],32
	je  BpintarNegra23 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra23:
    mov TFJC +2*MaMax+2,70 
    mov TFJC +2*MaMax+3,66		
    inc contjcFB
	jmp salidanegras
    

	Basignapa8:	

	cmp TFJC[0*MaMax+7],'B'	   	
	je Bconlibertad03
	cmp TFJC[2*MaMax+3],'B'		
	je  Bconlibertad03	
	jmp BComLib03

    BComLib03:
    cmp TFJC[0*MaMax+7],32	   	
	je Bconlibertad03
	cmp TFJC[2*MaMax+3],32		
	je  Bconlibertad03	
	jmp Bsinlibertad03

	Bsinlibertad03:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad03:
	cmp TFJC[0*MaMax+3],32
	je  BpintarNegra03 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra03:
    mov TFJC +0*MaMax+2,70 
    mov TFJC +0*MaMax+3,66		
    inc contjcFB
	jmp salidanegras
    

	Bposicionb:
	cmp bufferInformacion[1],'1'
	je Basignapb1
	cmp bufferInformacion[1],'2'
	je Basignapb2
	cmp bufferInformacion[1],'3'
	je Basignapb3
	cmp bufferInformacion[1],'4'
	je Basignapb4
	cmp bufferInformacion[1],'5'
	je Basignapb5
	cmp bufferInformacion[1],'6'
	je Basignapb6
	cmp bufferInformacion[1],'7'
	je Basignapb7
	cmp bufferInformacion[1],'8'
	je Basignapb8
	jmp pasoblancas
	
	Basignapb1:	
	cmp TFJC[12*MaMax+7],'N'	   	
	jne Bcadena147
	cmp TFJC[14*MaMax+3],'N'	   
	jne Bcadena147
	cmp TFJC[14*MaMax+11],'N'	   
	jne Bcadena147
	cmp TFJC[12*MaMax+11],'B'	   
	jne Bcadena147
	cmp TFJC[14*MaMax+15],'B'
	jne Bcadena147			
	mostrar ReglaKo
	jmp TurnoNegras

	Bcadena147:
    cmp TFJC[12*MaMax+7],'B'	   	
	je Bconlibertad147
	cmp TFJC[14*MaMax+3],'B'	
	je  Bconlibertad147
	cmp TFJC[14*MaMax+11],'B'	
	je  Bconlibertad147
	jmp BComLib147    

	BComLib147:
	cmp TFJC[12*MaMax+7],32	   	
	je Bconlibertad147
	cmp TFJC[14*MaMax+3],32	
	je Bconlibertad147	
	cmp TFJC[14*MaMax+11],32
	je Bconlibertad147	
	jmp Bsinlibertad147
	
	Bsinlibertad147:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad147:
	cmp TFJC[14*MaMax+7],32
	je  BpintarNegra147 
    mostrar PosOcupada
	jmp salidanegras	

	BpintarNegra147:
    mov TFJC +14*MaMax+6,70 
    mov TFJC +14*MaMax+7,66 	
    inc contjcFB
	jmp salidanegras

	Basignapb2:	

	cmp TFJC[10*MaMax+7],'B'	   	
	je Bconlibertad127
	cmp TFJC[12*MaMax+3],'B'	
	je  Bconlibertad127
	cmp TFJC[12*MaMax+11],'B'	
	je  Bconlibertad127
	cmp TFJC[14*MaMax+7],'B'	
	je  Bconlibertad127
	jmp BComLib127    

	BComLib127:
	cmp TFJC[10*MaMax+7],32	   	
	je Bconlibertad127
	cmp TFJC[12*MaMax+3],32	
	je  Bconlibertad127
	cmp TFJC[12*MaMax+11],32	
	je  Bconlibertad127
	cmp TFJC[14*MaMax+7],32	
	je  Bconlibertad127
	jmp Bsinlibertad127
	
	Bsinlibertad127:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad127:
	cmp TFJC[12*MaMax+7],32
	je  BpintarNegra127 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra127:
    mov TFJC +12*MaMax+6,70 
    mov TFJC +12*MaMax+7,66 	
    inc contjcFB
	jmp salidanegras

	Basignapb3:	

	cmp TFJC[8*MaMax+7],'B'	   	
	je Bconlibertad107
	cmp TFJC[10*MaMax+3],'B'	
	je  Bconlibertad107
	cmp TFJC[10*MaMax+11],'B'	
	je  Bconlibertad107
	cmp TFJC[12*MaMax+7],'B'	
	je  Bconlibertad107
	jmp BComLib107    

	BComLib107:
	cmp TFJC[8*MaMax+7],32	   	
	je Bconlibertad107
	cmp TFJC[10*MaMax+3],32	
	je  Bconlibertad107
	cmp TFJC[10*MaMax+11],32	
	je  Bconlibertad107
	cmp TFJC[12*MaMax+7],32	
	je  Bconlibertad107
	jmp Bsinlibertad107
	
	Bsinlibertad107:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad107:
	cmp TFJC[10*MaMax+7],32
	je  BpintarNegra107 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra107:
    mov TFJC +10*MaMax+6,70 
    mov TFJC +10*MaMax+7,66 
    inc contjcFB		
	jmp salidanegras    

	Basignapb4:	
	cmp TFJC[6*MaMax+7],'B'	   	
	je Bconlibertad87
	cmp TFJC[8*MaMax+3],'B'
	je  Bconlibertad87
	cmp TFJC[8*MaMax+11],'B'
	je  Bconlibertad87
	cmp TFJC[10*MaMax+7],'B'
	je  Bconlibertad87
	jmp BComLib87    

	BComLib87:
	cmp TFJC[6*MaMax+7],32	   	
	je Bconlibertad87
	cmp TFJC[8*MaMax+3],32	
	je  Bconlibertad87
	cmp TFJC[8*MaMax+11],32	
	je  Bconlibertad87
	cmp TFJC[10*MaMax+7],32	
	je  Bconlibertad87
	jmp Bsinlibertad87
	
	Bsinlibertad87:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad87:
	cmp TFJC[8*MaMax+7],32
	je  BpintarNegra87 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra87:
    mov TFJC +8*MaMax+6,70 
    mov TFJC +8*MaMax+7,66
     inc contjcFB 	
	jmp salidanegras

	Basignapb5:	
        mov TFJC +6*MaMax+6,70 
        mov TFJC +6*MaMax+7,66 		
	jmp salidanegras

	Basignapb6:	

	cmp TFJC[2*MaMax+7],'B'	   	
	je Bconlibertad47
	cmp TFJC[4*MaMax+3],'B'
	je  Bconlibertad47
	cmp TFJC[4*MaMax+11],'B'
	je  Bconlibertad47
	cmp TFJC[6*MaMax+7],'B'
	je  Bconlibertad47
	jmp BComLib47    

	BComLib47:
	cmp TFJC[2*MaMax+7],32	   	
	je Bconlibertad47
	cmp TFJC[4*MaMax+3],32
	je  Bconlibertad47
	cmp TFJC[4*MaMax+11],32
	je  Bconlibertad47
	cmp TFJC[6*MaMax+7],32
	je  Bconlibertad47
	jmp Bsinlibertad47
	
	Bsinlibertad47:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad47:
	cmp TFJC[4*MaMax+7],32
	je  BpintarNegra47 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra47:
    mov TFJC +4*MaMax+6,70 
    mov TFJC +4*MaMax+7,66
     inc contjcFB 	
	jmp salidanegras  

	Basignapb7:	

	cmp TFJC[0*MaMax+7],'B'	   	
	je Bconlibertad27
	cmp TFJC[2*MaMax+3],'B'
	je  Bconlibertad27
	cmp TFJC[2*MaMax+11],'B'
	je  Bconlibertad27
	cmp TFJC[4*MaMax+7],'B'
	je  Bconlibertad27
	jmp BComLib27    

	BComLib27:
	cmp TFJC[0*MaMax+7],32	   	
	je Bconlibertad27
	cmp TFJC[2*MaMax+3],32
	je  Bconlibertad27
	cmp TFJC[2*MaMax+11],32
	je  Bconlibertad27
	cmp TFJC[4*MaMax+7],32
	je  Bconlibertad27
	jmp Bsinlibertad27
	
	Bsinlibertad27:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad27:
	cmp TFJC[2*MaMax+7],32
	je  BpintarNegra27 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra27:
    mov TFJC +2*MaMax+6,70 
    mov TFJC +2*MaMax+7,66 	
     inc contjcFB
	jmp salidanegras

	Basignapb8:	

	cmp TFJC[0*MaMax+3],'B'	   	
	jne Bcadena07
	cmp TFJC[0*MaMax+11],'B'	   
	jne Bcadena07
	cmp TFJC[2*MaMax+7],'B'	   
	jne Bcadena07
	cmp TFJC[0*MaMax+15],'N'	   
	jne Bcadena07
	cmp TFJC[2*MaMax+11],'N'
	jne Bcadena07			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena07:
    cmp TFJC[0*MaMax+3],'B'	   	
	je Bconlibertad07
	cmp TFJC[0*MaMax+11],'B'	
	je  Bconlibertad07
	cmp TFJC[2*MaMax+7],'B'	
	je  Bconlibertad07
	jmp BComLib07    

	BComLib07:
	cmp TFJC[0*MaMax+3],32	   	
	je Bconlibertad07
	cmp TFJC[0*MaMax+11],32	
	je  Bconlibertad07
	cmp TFJC[2*MaMax+7],32	
	je  Bconlibertad07
	jmp Bsinlibertad07
	
	Bsinlibertad07:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad07:
	cmp TFJC[0*MaMax+7],32
	je  BpintarNegra07 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra07:
    mov TFJC +0*MaMax+6,70 
    mov TFJC +0*MaMax+7,66 	
     inc contjcFB
	jmp salidanegras
    
	Bposicionc:
	cmp bufferInformacion[1],'1'
	je Basignapc1
	cmp bufferInformacion[1],'2'
	je Basignapc2
	cmp bufferInformacion[1],'3'
	je Basignapc3
	cmp bufferInformacion[1],'4'
	je Basignapc4
	cmp bufferInformacion[1],'5'
	je Basignapc5
	cmp bufferInformacion[1],'6'
	je Basignapc6
	cmp bufferInformacion[1],'7'
	je Basignapc7
	cmp bufferInformacion[1],'8'
	je Basignapc8
	jmp pasoblancas
	
	Basignapc1:	

	cmp TFJC[12*MaMax+11],'N'	   	
	jne Bcadena1411
	cmp TFJC[14*MaMax+7],'N'	   
	jne Bcadena1411
	cmp TFJC[14*MaMax+15],'N'	   
	jne Bcadena1411
	cmp TFJC[12*MaMax+15],'B'	   
	jne Bcadena1411
	cmp TFJC[14*MaMax+19],'B'
	jne Bcadena1411			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena1411:
    cmp TFJC[12*MaMax+11],'B'	   	
	je Bconlibertad1411
	cmp TFJC[14*MaMax+7],'B'	
	je  Bconlibertad1411
	cmp TFJC[14*MaMax+15],'B'	
	je  Bconlibertad1411
	jmp BComLib1411    

	BComLib1411:
	cmp TFJC[12*MaMax+11],32	   	
	je Bconlibertad1411
	cmp TFJC[14*MaMax+7],32	
	je  Bconlibertad1411
	cmp TFJC[14*MaMax+15],32	
	je  Bconlibertad1411
	jmp Bsinlibertad1411
	
	Bsinlibertad1411:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1411:
	cmp TFJC[14*MaMax+11],32
	je  BpintarNegra1411 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1411:
    mov TFJC +14*MaMax+10,70 
    mov TFJC +14*MaMax+11,66 
     inc contjcFB	
	jmp salidanegras    

	Basignapc2:	

	cmp TFJC[10*MaMax+11],'B'	   	
	je Bconlibertad1211
	cmp TFJC[12*MaMax+7],'B'	
	je  Bconlibertad1211
	cmp TFJC[12*MaMax+15],'B'	
	je  Bconlibertad1211
	cmp TFJC[14*MaMax+11],'B'	
	je  Bconlibertad1211
	jmp BComLib1211    

	BComLib1211:
	cmp TFJC[10*MaMax+11],32	   	
	je Bconlibertad1211
	cmp TFJC[12*MaMax+7],32	
	je  Bconlibertad1211
	cmp TFJC[12*MaMax+15],32
	je  Bconlibertad1211
	cmp TFJC[14*MaMax+11],32
	je  Bconlibertad1211
	jmp Bsinlibertad1211
	
	Bsinlibertad1211:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1211:
	cmp TFJC[12*MaMax+11],32
	je  BpintarNegra1211 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1211:    
    mov TFJC +12*MaMax+10,70 
    mov TFJC +12*MaMax+11,66 
     inc contjcFB	
	jmp salidanegras

	Basignapc3:	

	cmp TFJC[8*MaMax+11],'B'	   	
	je Bconlibertad1011
	cmp TFJC[10*MaMax+7],'B'	
	je  Bconlibertad1011
	cmp TFJC[10*MaMax+15],'B'	
	je  Bconlibertad1011
	cmp TFJC[12*MaMax+11],'B'	
	je  Bconlibertad1011
	jmp BComLib1011    

	BComLib1011:
	cmp TFJC[8*MaMax+11],32	   	
	je Bconlibertad1011
	cmp TFJC[10*MaMax+7],32
	je  Bconlibertad1011
	cmp TFJC[10*MaMax+15],32
	je  Bconlibertad1011
	cmp TFJC[12*MaMax+11],32
	je  Bconlibertad1011
	jmp Bsinlibertad1011
	
	Bsinlibertad1011:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1011:
	cmp TFJC[10*MaMax+11],32
	je  BpintarNegra1011 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1011:
    mov TFJC +10*MaMax+10,70 
    mov TFJC +10*MaMax+11,66 
     inc contjcFB		
	jmp salidanegras    

	Basignapc4:

	cmp TFJC[6*MaMax+11],'B'	   	
	je Bconlibertad811
	cmp TFJC[8*MaMax+7],'B'
	je  Bconlibertad811
	cmp TFJC[8*MaMax+15],'B'
	je  Bconlibertad811
	cmp TFJC[10*MaMax+11],'B'
	je  Bconlibertad811
	jmp BComLib811    

	BComLib811:
	cmp TFJC[6*MaMax+11],32	   	
	je Bconlibertad811
	cmp TFJC[8*MaMax+7],32	   	
	je  Bconlibertad811
	cmp TFJC[8*MaMax+15],32	   	
	je  Bconlibertad811
	cmp TFJC[10*MaMax+11],32	   	
	je  Bconlibertad811
	jmp Bsinlibertad811
	
	Bsinlibertad811:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad811:
	cmp TFJC[8*MaMax+11],32
	je  BpintarNegra811 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra811:
    mov TFJC +8*MaMax+10,70 
    mov TFJC +8*MaMax+11,66 
     inc contjcFB	
	jmp salidanegras

	Basignapc5:

	cmp TFJC[4*MaMax+11],'B'	   	
	je Bconlibertad611
	cmp TFJC[6*MaMax+7],'B'
	je  Bconlibertad611
	cmp TFJC[6*MaMax+15],'B'
	je  Bconlibertad611
	cmp TFJC[8*MaMax+11],'B'
	je  Bconlibertad611
	jmp BComLib611    

	BComLib611:
	cmp TFJC[4*MaMax+11],32	   	
	je Bconlibertad611
	cmp TFJC[6*MaMax+7],32	   	
	je  Bconlibertad611
	cmp TFJC[6*MaMax+15],32	   	
	je  Bconlibertad611
	cmp TFJC[8*MaMax+11],32	   	
	je  Bconlibertad611
	jmp Bsinlibertad611
	
	Bsinlibertad611:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad611:
	cmp TFJC[6*MaMax+11],32
	je  BpintarNegra611 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra611:
    mov TFJC +6*MaMax+10,70 
    mov TFJC +6*MaMax+11,66 
     inc contjcFB		
	jmp salidanegras

	Basignapc6:

	cmp TFJC[2*MaMax+11],'B'	   	
	je Bconlibertad411
	cmp TFJC[4*MaMax+7],'B'
	je  Bconlibertad411
	cmp TFJC[4*MaMax+15],'B'
	je  Bconlibertad411
	cmp TFJC[6*MaMax+11],'B'
	je  Bconlibertad411
	jmp BComLib411    

	BComLib411:
	cmp TFJC[2*MaMax+11],32	   	
	je Bconlibertad411
	cmp TFJC[4*MaMax+7],32	   	
	je  Bconlibertad411
	cmp TFJC[4*MaMax+15],32	   	
	je  Bconlibertad411
	cmp TFJC[6*MaMax+11],32	   	
	je  Bconlibertad411
	jmp Bsinlibertad411
	
	Bsinlibertad411:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad411:
	cmp TFJC[4*MaMax+11],32
	je  BpintarNegra411 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra411:
    mov TFJC +4*MaMax+10,70 
    mov TFJC +4*MaMax+11,66 
     inc contjcFB	
	jmp salidanegras

	Basignapc7:	

	cmp TFJC[0*MaMax+11],'B'	   	
	je Bconlibertad211
	cmp TFJC[2*MaMax+7],'B'
	je  Bconlibertad211
	cmp TFJC[2*MaMax+15],'B'
	je  Bconlibertad211
	cmp TFJC[4*MaMax+11],'B'
	je  Bconlibertad211
	jmp BComLib211    

	BComLib211:
	cmp TFJC[0*MaMax+11],32	   	
	je Bconlibertad211
	cmp TFJC[2*MaMax+7],32	   	
	je  Bconlibertad211
	cmp TFJC[2*MaMax+15],32	   	
	je  Bconlibertad211
	cmp TFJC[4*MaMax+11],32	   	
	je  Bconlibertad211
	jmp Bsinlibertad211
	
	Bsinlibertad211:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad211:
	cmp TFJC[2*MaMax+11],32
	je  BpintarNegra211 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra211:
    mov TFJC +2*MaMax+10,70 
    mov TFJC +2*MaMax+11,66 
     inc contjcFB	
	jmp salidanegras

    

	Basignapc8:

	cmp TFJC[0*MaMax+7],'B'	   	
	jne Bcadena011
	cmp TFJC[0*MaMax+15],'B'	   
	jne Bcadena011
	cmp TFJC[2*MaMax+11],'B'	   
	jne Bcadena011
	cmp TFJC[0*MaMax+19],'N'	   
	jne Bcadena011
	cmp TFJC[2*MaMax+15],'N'
	jne Bcadena011			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena011:
    cmp TFJC[0*MaMax+7],'B'	   	
	je Bconlibertad011
	cmp TFJC[0*MaMax+15],'B'	
	je  Bconlibertad011
	cmp TFJC[2*MaMax+11],'B'	
	je  Bconlibertad011
	jmp BComLib011    

	BComLib011:
	cmp TFJC[0*MaMax+7],32	   	
	je Bconlibertad011
	cmp TFJC[0*MaMax+15],32	   	
	je  Bconlibertad011
	cmp TFJC[2*MaMax+11],32	   	
	je  Bconlibertad011
	jmp Bsinlibertad011
	
	Bsinlibertad011:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad011:
	cmp TFJC[0*MaMax+11],32
	je  BpintarNegra011 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra011:
    mov TFJC +0*MaMax+10,70 
    mov TFJC +0*MaMax+11,66 
     inc contjcFB	
	jmp salidanegras
    

	Bposiciond:
	cmp bufferInformacion[1],'1'
	je Basignapd1
	cmp bufferInformacion[1],'2'
	je Basignapd2
	cmp bufferInformacion[1],'3'
	je Basignapd3
	cmp bufferInformacion[1],'4'
	je Basignapd4
	cmp bufferInformacion[1],'5'
	je Basignapd5
	cmp bufferInformacion[1],'6'
	je Basignapd6
	cmp bufferInformacion[1],'7'
	je Basignapd7
	cmp bufferInformacion[1],'8'
	je Basignapd8
	jmp pasoblancas
	
	Basignapd1:	

	cmp TFJC[12*MaMax+15],'N'	   	
	jne Bcadena1415
	cmp TFJC[14*MaMax+11],'N'	   
	jne Bcadena1415
	cmp TFJC[14*MaMax+19],'N'	   
	jne Bcadena1415
	cmp TFJC[12*MaMax+19],'B'	   
	jne Bcadena1415
	cmp TFJC[14*MaMax+23],'B'
	jne Bcadena1415			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena1415:
    cmp TFJC[12*MaMax+15],'B'	   	
	je Bconlibertad1415
	cmp TFJC[14*MaMax+11],'B'	
	je  Bconlibertad1415
	cmp TFJC[14*MaMax+19],'B'	
	je  Bconlibertad1415
	jmp BComLib1415    

	BComLib1415:
	cmp TFJC[12*MaMax+15],32	   	
	je Bconlibertad1415
	cmp TFJC[14*MaMax+11],32	   	
	je  Bconlibertad1415
	cmp TFJC[14*MaMax+19],32	   	
	je  Bconlibertad1415
	jmp Bsinlibertad1415
	
	Bsinlibertad1415:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1415:
	cmp TFJC[14*MaMax+15],32
	je  BpintarNegra1415 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1415:
    mov TFJC +14*MaMax+14,70 
    mov TFJC +14*MaMax+15,66 
     inc contjcFB	
	jmp salidanegras    
    

	Basignapd2:

	cmp TFJC[10*MaMax+15],'B'	   	
	je Bconlibertad1215
	cmp TFJC[12*MaMax+11],'B'	
	je  Bconlibertad1215
	cmp TFJC[12*MaMax+19],'B'	
	je  Bconlibertad1215
	cmp TFJC[14*MaMax+15],'B'	
	je  Bconlibertad1215
	jmp BComLib1215    

	BComLib1215:
	cmp TFJC[10*MaMax+15],32	   	
	je Bconlibertad1215
	cmp TFJC[12*MaMax+11],32	   	
	je  Bconlibertad1215
	cmp TFJC[12*MaMax+19],32	   	
	je  Bconlibertad1215
	cmp TFJC[14*MaMax+15],32	   	
	je  Bconlibertad1215
	jmp Bsinlibertad1215
	
	Bsinlibertad1215:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1215:
	cmp TFJC[12*MaMax+15],32
	je  BpintarNegra1215 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1215:
    mov TFJC +12*MaMax+14,70 
    mov TFJC +12*MaMax+15,66 
     inc contjcFB	
	jmp salidanegras
    

	Basignapd3:

	cmp TFJC[8*MaMax+15],'B'	   	
	je Bconlibertad1015
	cmp TFJC[10*MaMax+11],'B'	
	je  Bconlibertad1015
	cmp TFJC[10*MaMax+19],'B'	
	je  Bconlibertad1015
	cmp TFJC[12*MaMax+15],'B'	
	je  Bconlibertad1015
	jmp BComLib1015    

	BComLib1015:
	cmp TFJC[8*MaMax+15],32	   	
	je Bconlibertad1015
	cmp TFJC[10*MaMax+11],32
	je Bconlibertad1015
	cmp TFJC[10*MaMax+19],32
	je Bconlibertad1015
	cmp TFJC[12*MaMax+15],32
	je Bconlibertad1015
	jmp Bsinlibertad1015
	
	Bsinlibertad1015:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1015:
	cmp TFJC[10*MaMax+15],32
	je  BpintarNegra1015 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1015:
    mov TFJC +10*MaMax+14,70 
    mov TFJC +10*MaMax+15,66 
     inc contjcFB		
	jmp salidanegras

    

	Basignapd4:

	cmp TFJC[6*MaMax+15],'B'	   	
	je Bconlibertad815
	cmp TFJC[8*MaMax+11],'B'
	je  Bconlibertad815
	cmp TFJC[8*MaMax+19],'B'
	je  Bconlibertad815
	cmp TFJC[10*MaMax+15],'B'
	je  Bconlibertad815
	jmp BComLib815    

	BComLib815:
	cmp TFJC[6*MaMax+15],32	   	
	je Bconlibertad815
	cmp TFJC[8*MaMax+11],32	   	
	je  Bconlibertad815
	cmp TFJC[8*MaMax+19],32	   	
	je  Bconlibertad815
	cmp TFJC[10*MaMax+15],32	   	
	je  Bconlibertad815
	jmp Bsinlibertad815
	
	Bsinlibertad815:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad815:
	cmp TFJC[8*MaMax+15],32
	je  BpintarNegra815 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra815:
    mov TFJC +8*MaMax+14,70 
    mov TFJC +8*MaMax+15,66 
     inc contjcFB	
	jmp salidanegras    

	Basignapd5:
	cmp TFJC[4*MaMax+15],'B'	   	
	je Bconlibertad615
	cmp TFJC[6*MaMax+11],'B'
	je  Bconlibertad615
	cmp TFJC[6*MaMax+19],'B'
	je  Bconlibertad615
	cmp TFJC[8*MaMax+15],'B'
	je  Bconlibertad615
	jmp BComLib615    

	BComLib615:
	cmp TFJC[4*MaMax+15],32	   	
	je Bconlibertad615
	cmp TFJC[6*MaMax+11],32	   	
	je  Bconlibertad615
	cmp TFJC[6*MaMax+19],32	   	
	je  Bconlibertad615
	cmp TFJC[8*MaMax+15],32	   	
	je  Bconlibertad615
	jmp Bsinlibertad615
	
	Bsinlibertad615:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad615:
	cmp TFJC[6*MaMax+15],32
	je  BpintarNegra615 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra615:
    mov TFJC +6*MaMax+14,70 
    mov TFJC +6*MaMax+15,66 
     inc contjcFB		
	jmp salidanegras    

	Basignapd6:	

	cmp TFJC[2*MaMax+15],'B'	   	
	je Bconlibertad415
	cmp TFJC[4*MaMax+11],'B'
	je  Bconlibertad415
	cmp TFJC[4*MaMax+19],'B'
	je  Bconlibertad415
	cmp TFJC[6*MaMax+15],'B'
	je  Bconlibertad415
	jmp BComLib415    

	BComLib415:
	cmp TFJC[2*MaMax+15],32	   	
	je Bconlibertad415
	cmp TFJC[4*MaMax+11],32	   	
	je  Bconlibertad415
	cmp TFJC[4*MaMax+19],32	   	
	je  Bconlibertad415
	cmp TFJC[6*MaMax+15],32	   	
	je  Bconlibertad415
	jmp Bsinlibertad415
	
	Bsinlibertad415:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad415:
	cmp TFJC[4*MaMax+15],32
	je  BpintarNegra415 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra415:
    mov TFJC +4*MaMax+14,70 
    mov TFJC +4*MaMax+15,66 
     inc contjcFB	
	jmp salidanegras  


	Basignapd7:	
	cmp TFJC[0*MaMax+15],'B'	   	
	je Bconlibertad215
	cmp TFJC[2*MaMax+11],'B'
	je  Bconlibertad215
	cmp TFJC[2*MaMax+19],'B'
	je  Bconlibertad215
	cmp TFJC[4*MaMax+15],'B'
	je  Bconlibertad215
	jmp BComLib215    

	BComLib215:
	cmp TFJC[0*MaMax+15],32	   	
	je Bconlibertad215
	cmp TFJC[2*MaMax+11],32
	je  Bconlibertad215
	cmp TFJC[2*MaMax+19],32
	je  Bconlibertad215
	cmp TFJC[4*MaMax+15],32
	je  Bconlibertad215
	jmp Bsinlibertad215
	
	Bsinlibertad215:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad215:
	cmp TFJC[2*MaMax+15],32
	je  BpintarNegra215 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra215:
    mov TFJC +2*MaMax+14,70 
    mov TFJC +2*MaMax+15,66 
     inc contjcFB	
	jmp salidanegras

    

	Basignapd8:	
	cmp TFJC[0*MaMax+11],'N'	   	
	jne Bcadena015
	cmp TFJC[0*MaMax+19],'N'	   
	jne Bcadena015
	cmp TFJC[2*MaMax+15],'N'	   
	jne Bcadena015
	cmp TFJC[0*MaMax+23],'B'	   
	jne Bcadena015
	cmp TFJC[2*MaMax+19],'B'
	jne Bcadena015			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena015:
    cmp TFJC[0*MaMax+11],'B'	   	
	je Bconlibertad015
	cmp TFJC[0*MaMax+19],'B'	
	je  Bconlibertad015
	cmp TFJC[2*MaMax+15],'B'	
	je  Bconlibertad015
	jmp BComLib015    

	BComLib015:
	cmp TFJC[0*MaMax+11],32	   	
	je Bconlibertad015
	cmp TFJC[0*MaMax+19],32	
	je  Bconlibertad015
	cmp TFJC[2*MaMax+15],32	
	je  Bconlibertad015
	jmp Bsinlibertad015
	
	Bsinlibertad015:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad015:
	cmp TFJC[0*MaMax+15],32
	je  BpintarNegra015 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra015:
    mov TFJC +0*MaMax+14,70 
    mov TFJC +0*MaMax+15,66 
     inc contjcFB	
	jmp salidanegras

    

	Bposicionf:
	cmp bufferInformacion[1],'1'
	je Basignapf1
	cmp bufferInformacion[1],'2'
	je Basignapf2
	cmp bufferInformacion[1],'3'
	je Basignapf3
	cmp bufferInformacion[1],'4'
	je Basignapf4
	cmp bufferInformacion[1],'5'
	je Basignapf5
	cmp bufferInformacion[1],'6'
	je Basignapf6
	cmp bufferInformacion[1],'7'
	je Basignapf7
	cmp bufferInformacion[1],'8'
	je Basignapf8
	jmp pasoblancas
	
	Basignapf1:

	cmp TFJC[12*MaMax+23],'N'	   	
	jne Bcadena1423
	cmp TFJC[14*MaMax+19],'N'	   
	jne Bcadena1423
	cmp TFJC[14*MaMax+27],'N'	   
	jne Bcadena1423
	cmp TFJC[12*MaMax+27],'B'	   
	jne Bcadena1423
	cmp TFJC[14*MaMax+30],'B'
	jne Bcadena1423			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena1423:
    cmp TFJC[12*MaMax+23],'B'	   	
	je Bconlibertad1423
	cmp TFJC[14*MaMax+19],'B'	
	je  Bconlibertad1423
	cmp TFJC[14*MaMax+27],'B'	
	je  Bconlibertad1423
	jmp BComLib1423    

	BComLib1423:
	cmp TFJC[12*MaMax+23],32	   	
	je Bconlibertad1423
	cmp TFJC[14*MaMax+19],32	
	je  Bconlibertad1423
	cmp TFJC[14*MaMax+27],32	
	je  Bconlibertad1423
	jmp Bsinlibertad1423
	
	Bsinlibertad1423:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1423:
	cmp TFJC[14*MaMax+23],32
	je  BpintarNegra1423 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1423:
    mov TFJC +14*MaMax+22,70 
    mov TFJC +14*MaMax+23,66 
     inc contjcFB	
	jmp salidanegras

	Basignapf2:	
	cmp TFJC[10*MaMax+23],'B'	   	
	je Bconlibertad1223
	cmp TFJC[12*MaMax+19],'B'	
	je  Bconlibertad1223
	cmp TFJC[12*MaMax+27],'B'	
	je  Bconlibertad1223
	cmp TFJC[14*MaMax+23],'B'	
	je  Bconlibertad1223
	jmp BComLib1223    

	BComLib1223:
	cmp TFJC[10*MaMax+23],32	   	
	je Bconlibertad1223
	cmp TFJC[12*MaMax+19],32	
	je  Bconlibertad1223
	cmp TFJC[12*MaMax+27],32	
	je  Bconlibertad1223
	cmp TFJC[14*MaMax+23],32
	je  Bconlibertad1223
	jmp Bsinlibertad1223
	
	Bsinlibertad1223:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1223:
	cmp TFJC[12*MaMax+23],32
	je  BpintarNegra1223 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1223:
    mov TFJC +12*MaMax+22,70 
    mov TFJC +12*MaMax+23,66 
     inc contjcFB	
	jmp salidanegras

	Basignapf3:
	cmp TFJC[8*MaMax+23],'B'	   	
	je Bconlibertad1023
	cmp TFJC[10*MaMax+19],'B'	
	je  Bconlibertad1023
	cmp TFJC[10*MaMax+27],'B'	
	je  Bconlibertad1023
	cmp TFJC[12*MaMax+23],'B'	
	je  Bconlibertad1023
	jmp BComLib1023    

	BComLib1023:
	cmp TFJC[8*MaMax+23],32	   	
	je Bconlibertad1023
	cmp TFJC[10*MaMax+19],32
	je  Bconlibertad1023
	cmp TFJC[10*MaMax+27],32
	je  Bconlibertad1023
	cmp TFJC[12*MaMax+23],32
	je  Bconlibertad1023
	jmp Bsinlibertad1023
	
	Bsinlibertad1023:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1023:
	cmp TFJC[10*MaMax+23],32
	je  BpintarNegra1023
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1023:  
    mov TFJC +10*MaMax+22,70 
    mov TFJC +10*MaMax+23,66 
     inc contjcFB		
	jmp salidanegras

	Basignapf4:	
	cmp TFJC[6*MaMax+23],'B'	   	
	je  Bconlibertad823
	cmp TFJC[8*MaMax+19],'B'
	je  Bconlibertad823
	cmp TFJC[8*MaMax+27],'B'
	je  Bconlibertad823
	cmp TFJC[10*MaMax+23],'B'
	je  Bconlibertad823
	jmp BComLib823    

	BComLib823:
	cmp TFJC[6*MaMax+23],32	   	
	je  Bconlibertad823
	cmp TFJC[8*MaMax+19],32
	je  Bconlibertad823
	cmp TFJC[8*MaMax+27],32
	je  Bconlibertad823
	cmp TFJC[10*MaMax+23],32
	je  Bconlibertad823
	jmp Bsinlibertad823
	
	Bsinlibertad823:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad823:
	cmp TFJC[8*MaMax+23],32
	je  BpintarNegra823
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra823:
    mov TFJC +8*MaMax+22,70 
    mov TFJC +8*MaMax+23,66 
     inc contjcFB	
	jmp salidanegras

	Basignapf5:	
	cmp TFJC[4*MaMax+23],'B'	   	
	je Bconlibertad623
	cmp TFJC[6*MaMax+19],'B'
	je  Bconlibertad623
	cmp TFJC[6*MaMax+27],'B'
	je  Bconlibertad623
	cmp TFJC[8*MaMax+23],'B'
	je  Bconlibertad623
	jmp BComLib623    

	BComLib623:
	cmp TFJC[4*MaMax+23],32	   	
	je Bconlibertad623
	cmp TFJC[6*MaMax+19],32
	je  Bconlibertad623
	cmp TFJC[6*MaMax+27],32
	je  Bconlibertad623
	cmp TFJC[8*MaMax+23],32
	je  Bconlibertad623
	jmp Bsinlibertad623
	
	Bsinlibertad623:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad623:
	cmp TFJC[6*MaMax+23],32
	je  BpintarNegra623 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra623:    
    mov TFJC +6*MaMax+22,70 
    mov TFJC +6*MaMax+23,66 
     inc contjcFB		
	jmp salidanegras

	Basignapf6:	
	cmp TFJC[2*MaMax+23],'B'	   	
	je Bconlibertad423
	cmp TFJC[4*MaMax+19],'B'
	je  Bconlibertad423
	cmp TFJC[4*MaMax+27],'B'
	je  Bconlibertad423
	cmp TFJC[6*MaMax+23],'B'
	je  Bconlibertad423
	jmp BComLib423    

	BComLib423:
	cmp TFJC[2*MaMax+23],32	   	
	je Bconlibertad423
	cmp TFJC[4*MaMax+19],32
	je  Bconlibertad423
	cmp TFJC[4*MaMax+27],32
	je  Bconlibertad423
	cmp TFJC[6*MaMax+23],32
	je  Bconlibertad423
	jmp Bsinlibertad423
	
	Bsinlibertad423:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad423:
	cmp TFJC[4*MaMax+23],32
	je  BpintarNegra423 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra423:        
    mov TFJC +4*MaMax+22,70 
    mov TFJC +4*MaMax+23,66 
     inc contjcFB	
	jmp salidanegras


	Basignapf7:	
	cmp TFJC[0*MaMax+23],'B'	   	
	je Bconlibertad223
	cmp TFJC[2*MaMax+19],'B'
	je  Bconlibertad223
	cmp TFJC[2*MaMax+27],'B'
	je  Bconlibertad223
	cmp TFJC[4*MaMax+23],'B'
	je  Bconlibertad223
	jmp BComLib223    

	BComLib223:
	cmp TFJC[0*MaMax+23],32	   	
	je Bconlibertad223
	cmp TFJC[2*MaMax+19],32
	je  Bconlibertad223
	cmp TFJC[2*MaMax+27],32
	je  Bconlibertad223
	cmp TFJC[4*MaMax+23],32
	je  Bconlibertad223
	jmp Bsinlibertad223
	
	Bsinlibertad223:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad223:
	cmp TFJC[2*MaMax+23],32
	je  BpintarNegra223 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra223:    
    mov TFJC +2*MaMax+22,70 
    mov TFJC +2*MaMax+23,66 
     inc contjcFB	
	jmp salidanegras

	Basignapf8:
	cmp TFJC[0*MaMax+19],'N'	   	
	jne Bcadena023
	cmp TFJC[0*MaMax+27],'N'	   
	jne Bcadena023
	cmp TFJC[2*MaMax+23],'N'	   
	jne Bcadena023
	cmp TFJC[0*MaMax+30],'B'	   
	jne Bcadena023
	cmp TFJC[2*MaMax+27],'B'
	jne Bcadena023			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena023:
    cmp TFJC[0*MaMax+19],'B'	   	
	je Bconlibertad023
	cmp TFJC[0*MaMax+27],'B'	
	je  Bconlibertad023
	cmp TFJC[2*MaMax+23],'B'	
	je  Bconlibertad023
	jmp BComLib023    

	BComLib023:
	cmp TFJC[0*MaMax+19],32	   	
	je Bconlibertad023
	cmp TFJC[0*MaMax+27],32	
	je  Bconlibertad023
	cmp TFJC[2*MaMax+23],32	
	je  Bconlibertad023
	jmp Bsinlibertad023
	
	Bsinlibertad023:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad023:
	cmp TFJC[0*MaMax+23],32
	je  BpintarNegra023 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra023:    
    mov TFJC +0*MaMax+22,70 
    mov TFJC +0*MaMax+23,66 
     inc contjcFB	
	jmp salidanegras
	
	Bposiciong:
	cmp bufferInformacion[1],'1'
	je Basignapg1
	cmp bufferInformacion[1],'2'
	je Basignapg2
	cmp bufferInformacion[1],'3'
	je Basignapg3
	cmp bufferInformacion[1],'4'
	je Basignapg4
	cmp bufferInformacion[1],'5'
	je Basignapg5
	cmp bufferInformacion[1],'6'
	je Basignapg6
	cmp bufferInformacion[1],'7'
	je Basignapg7
	cmp bufferInformacion[1],'8'
	je Basignapg8
	jmp pasoblancas
	
	Basignapg1:	

	Bcadena1427:
    cmp TFJC[12*MaMax+27],'B'	   	
	je Bconlibertad1427
	cmp TFJC[14*MaMax+23],'B'	
	je  Bconlibertad1427
	cmp TFJC[14*MaMax+30],'B'	
	je  Bconlibertad1427
	jmp BComLib1427    

	BComLib1427:
	cmp TFJC[12*MaMax+27],32	   	
	je Bconlibertad1427
	cmp TFJC[14*MaMax+23],32	
	je  Bconlibertad1427
	cmp TFJC[14*MaMax+30],32	
	je  Bconlibertad1427
	jmp Bsinlibertad1427
	
	Bsinlibertad1427:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1427:
	cmp TFJC[14*MaMax+27],32
	je  BpintarNegra1427 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1427:  
    mov TFJC +14*MaMax+26,70 
    mov TFJC +14*MaMax+27,66 
     inc contjcFB	
	jmp salidanegras

	Basignapg2:

	cmp TFJC[10*MaMax+27],'B'	   	
	je Bconlibertad1227
	cmp TFJC[12*MaMax+23],'B'	
	je  Bconlibertad1227
	cmp TFJC[12*MaMax+30],'B'	
	je  Bconlibertad1227
	cmp TFJC[14*MaMax+27],'B'	
	je  Bconlibertad1227
	jmp BComLib1227    

	BComLib1227:
	cmp TFJC[10*MaMax+27],32	   	
	je Bconlibertad1227
	cmp TFJC[12*MaMax+23],32	
	je  Bconlibertad1227
	cmp TFJC[12*MaMax+30],32	
	je  Bconlibertad1227
	cmp TFJC[14*MaMax+27],32	
	je  Bconlibertad1227
	jmp Bsinlibertad1227
	
	Bsinlibertad1227:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1227:
	cmp TFJC[12*MaMax+27],32
	je  BpintarNegra1227 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1227:  
    mov TFJC +12*MaMax+26,70 
    mov TFJC +12*MaMax+27,66 
     inc contjcFB	
	jmp salidanegras    

	Basignapg3:	

	cmp TFJC[8*MaMax+27],'B'	   	
	je Bconlibertad1027
	cmp TFJC[10*MaMax+23],'B'	
	je  Bconlibertad1027
	cmp TFJC[10*MaMax+30],'B'	
	je  Bconlibertad1027
	cmp TFJC[12*MaMax+27],'B'	
	je  Bconlibertad1027
	jmp BComLib1027    

	BComLib1027:
	cmp TFJC[8*MaMax+27],32	   	
	je Bconlibertad1027
	cmp TFJC[10*MaMax+23],32	
	je  Bconlibertad1027
	cmp TFJC[10*MaMax+30],32	
	je  Bconlibertad1027
	cmp TFJC[12*MaMax+27],32	
	je  Bconlibertad1027
	jmp Bsinlibertad1027
	
	Bsinlibertad1027:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1027:
	cmp TFJC[10*MaMax+27],32
	je  BpintarNegra1027
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1027:  
    mov TFJC +10*MaMax+26,70 
    mov TFJC +10*MaMax+27,66 
     inc contjcFB		
	jmp salidanegras

	Basignapg4:	

	cmp TFJC[6*MaMax+27],'B'	   	
	je  Bconlibertad827
	cmp TFJC[8*MaMax+23],'B'
	je  Bconlibertad827
	cmp TFJC[8*MaMax+30],'B'
	je  Bconlibertad827
	cmp TFJC[10*MaMax+27],'B'
	je  Bconlibertad827
	jmp BComLib827    

	BComLib827:
	cmp TFJC[6*MaMax+27],32	   	
	je  Bconlibertad827
	cmp TFJC[8*MaMax+23],32
	je  Bconlibertad827
	cmp TFJC[8*MaMax+30],32
	je  Bconlibertad827
	cmp TFJC[10*MaMax+27],32
	je  Bconlibertad827
	jmp Bsinlibertad827
	
	Bsinlibertad827:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad827:
	cmp TFJC[8*MaMax+27],32
	je  BpintarNegra827
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra827:  
    mov TFJC +8*MaMax+26,70 
    mov TFJC +8*MaMax+27,66 
     inc contjcFB	
	jmp salidanegras

	Basignapg5:	

	cmp TFJC[4*MaMax+27],'B'	   	
	je Bconlibertad627
	cmp TFJC[6*MaMax+23],'B'
	je  Bconlibertad627
	cmp TFJC[6*MaMax+30],'B'
	je  Bconlibertad627
	cmp TFJC[8*MaMax+27],'B'
	je  Bconlibertad627
	jmp BComLib627    

	BComLib627:
	cmp TFJC[4*MaMax+27],32	   	
	je Bconlibertad627
	cmp TFJC[6*MaMax+23],32
	je  Bconlibertad627
	cmp TFJC[6*MaMax+30],32
	je  Bconlibertad627
	cmp TFJC[8*MaMax+27],32
	je  Bconlibertad627
	jmp Bsinlibertad627
	
	Bsinlibertad627:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad627:
	cmp TFJC[6*MaMax+27],32
	je  BpintarNegra627 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra627:    
     mov TFJC +6*MaMax+26,70 
     mov TFJC +6*MaMax+27,66 
      inc contjcFB		
	jmp salidanegras

	Basignapg6:	
	cmp TFJC[2*MaMax+27],'B'	   	
	je Bconlibertad427
	cmp TFJC[4*MaMax+23],'B'
	je  Bconlibertad427
	cmp TFJC[4*MaMax+30],'B'
	je  Bconlibertad427
	cmp TFJC[6*MaMax+27],'B'
	je  Bconlibertad427
	jmp BComLib427    

	BComLib427:
	cmp TFJC[2*MaMax+27],32	   	
	je Bconlibertad427
	cmp TFJC[4*MaMax+23],32
	je  Bconlibertad427
	cmp TFJC[4*MaMax+30],32
	je  Bconlibertad427
	cmp TFJC[6*MaMax+27],32
	je  Bconlibertad427
	jmp Bsinlibertad427
	
	Bsinlibertad427:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad427:
	cmp TFJC[4*MaMax+27],32
	je  BpintarNegra427 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra427:    
    mov TFJC +4*MaMax+26,70 
    mov TFJC +4*MaMax+27,66 
     inc contjcFB	
	jmp salidanegras


	Basignapg7:	
	cmp TFJC[0*MaMax+27],'B'	   	
	je Bconlibertad227
	cmp TFJC[2*MaMax+23],'B'
	je  Bconlibertad227
	cmp TFJC[2*MaMax+30],'B'
	je  Bconlibertad227
	cmp TFJC[4*MaMax+27],'B'
	je  Bconlibertad227
	jmp BComLib227    

	BComLib227:
	cmp TFJC[0*MaMax+27],32	   	
	je Bconlibertad227
	cmp TFJC[2*MaMax+23],32
	je  Bconlibertad227
	cmp TFJC[2*MaMax+30],32
	je  Bconlibertad227
	cmp TFJC[4*MaMax+27],32
	je  Bconlibertad227
	jmp Bsinlibertad227
	
	Bsinlibertad227:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad227:
	cmp TFJC[2*MaMax+27],32
	je  BpintarNegra227 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra227:    
    mov TFJC +2*MaMax+26,70 
    mov TFJC +2*MaMax+27,66 
     inc contjcFB	
	jmp salidanegras

	Basignapg8:	

	Bcadena027:
    cmp TFJC[0*MaMax+23],'B'	   	
	je Bconlibertad027
	cmp TFJC[0*MaMax+30],'B'	
	je  Bconlibertad027
	cmp TFJC[2*MaMax+27],'B'	
	je  Bconlibertad027
	jmp BComLib027    

	BComLib027:
	cmp TFJC[0*MaMax+23],32	   	
	je Bconlibertad027
	cmp TFJC[0*MaMax+30],32	
	je  Bconlibertad027
	cmp TFJC[2*MaMax+27],32	
	je  Bconlibertad027
	jmp Bsinlibertad027
	
	Bsinlibertad027:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad027:
	cmp TFJC[0*MaMax+27],32
	je  BpintarNegra027 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra027:
    mov TFJC +0*MaMax+26,70 
    mov TFJC +0*MaMax+27,66 
     inc contjcFB	
	jmp salidanegras

	Bposicionh:
	cmp bufferInformacion[1],'1'
	je Basignaph1
	cmp bufferInformacion[1],'2'
	je Basignaph2
	cmp bufferInformacion[1],'3'
	je Basignaph3
	cmp bufferInformacion[1],'4'
	je Basignaph4
	cmp bufferInformacion[1],'5'
	je Basignaph5
	cmp bufferInformacion[1],'6'
	je Basignaph6
	cmp bufferInformacion[1],'7'
	je Basignaph7
	cmp bufferInformacion[1],'8'
	je Basignaph8
	jmp pasoblancas
	
	Basignaph1:	
	cmp TFJC[12*MaMax+30],'B'	   	
	je Bconlibertad1430
	cmp TFJC[14*MaMax+27],'B'	
	je Bconlibertad1430	
	jmp BComLib1430

	BComLib1430:
	cmp TFJC[12*MaMax+30],32	   	
	je Bconlibertad1430
	cmp TFJC[14*MaMax+27],32	
	je Bconlibertad1430	
	jmp Bsinlibertad1430
	
	Bsinlibertad1430:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1430:
	cmp TFJC[14*MaMax+30],32
	je  BpintarNegra1430 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1430:   
	mov TFJC +14*MaMax+29,70 
    mov TFJC +14*MaMax+30,66 
     inc contjcFB	
	jmp salidanegras    

	Basignaph2:	

	cmp TFJC[10*MaMax+30],'B'	   	
	je Bconlibertad1230
	cmp TFJC[12*MaMax+27],'B'		
	je  Bconlibertad1230
	cmp TFJC[14*MaMax+30],'B'		
	je  Bconlibertad1230
	jmp BComLib1230

	BComLib1230:	
	cmp TFJC[10*MaMax+30],32	   	
	je Bconlibertad1230
	cmp TFJC[12*MaMax+27],32		
	je  Bconlibertad1230
	cmp TFJC[14*MaMax+30],32		
	je  Bconlibertad1230
	jmp Bsinlibertad1230

	Bsinlibertad1230:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1230:
	cmp TFJC[12*MaMax+30],32
	je  BpintarNegra1230 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1230:
    mov TFJC +12*MaMax+29,70 
    mov TFJC +12*MaMax+30,66 
     inc contjcFB	
	jmp salidanegras

	Basignaph3:	

	cmp TFJC[8*MaMax+3],'N'	   	
	jne Bcadena1030
	cmp TFJC[10*MaMax+27],'N'	   
	jne Bcadena1030
	cmp TFJC[12*MaMax+30],'N'	   
	jne Bcadena1030
	cmp TFJC[12*MaMax+27],'B'	   
	jne Bcadena1030
	cmp TFJC[14*MaMax+30],'B'
	jne Bcadena1030			
	mostrar ReglaKo
	jmp TurnoBlancas


	Bcadena1030:
	cmp TFJC[8*MaMax+30],'B'	   	
	je Bconlibertad1030
	cmp TFJC[10*MaMax+27],'B'
	je  Bconlibertad1030
	cmp TFJC[12*MaMax+30],'B'
	je  Bconlibertad1030
	jmp BComLib1030

	BComLib1030:
    cmp TFJC[8*MaMax+30],32	   	
	je Bconlibertad1030
	cmp TFJC[10*MaMax+27],32
	je  Bconlibertad1030
	cmp TFJC[12*MaMax+30],32
	je  Bconlibertad1030
	jmp Bsinlibertad1030

	Bsinlibertad1030:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad1030:
	cmp TFJC[10*MaMax+30],32
	je  BpintarNegra1030 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra1030:
    mov TFJC +10*MaMax+29,70 
    mov TFJC +10*MaMax+30,66 
     inc contjcFB		
	jmp salidanegras

	Basignaph4:

	cmp TFJC[6*MaMax+30],'N'	   	
	jne Bcadena830
	cmp TFJC[8*MaMax+27],'N'	   
	jne Bcadena830
	cmp TFJC[10*MaMax+30],'N'	   
	jne Bcadena830
	cmp TFJC[10*MaMax+27],'B'	   
	jne Bcadena830
	cmp TFJC[12*MaMax+30],'B'
	jne Bcadena830			
	mostrar ReglaKo
	jmp TurnoBlancas


	Bcadena830:
	cmp TFJC[6*MaMax+30],'B'	   	
	je Bconlibertad830
	cmp TFJC[8*MaMax+27],'B'	   	
	je  Bconlibertad830
	cmp TFJC[10*MaMax+30],'B'	   	
	je  Bconlibertad830
	jmp BComLib830

    BComLib830:
    cmp TFJC[6*MaMax+30],32	   	
	je Bconlibertad830
	cmp TFJC[8*MaMax+27],32	   	
	je  Bconlibertad830
	cmp TFJC[10*MaMax+30],32   	
	je  Bconlibertad830
	jmp Bsinlibertad830

	Bsinlibertad830:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad830:
	cmp TFJC[8*MaMax+30],32
	je  BpintarNegra830 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra830:    
    mov TFJC +8*MaMax+29,70 
    mov TFJC +8*MaMax+30,66 
     inc contjcFB	
	jmp salidanegras

	Basignaph5:	
	cmp TFJC[4*MaMax+30],'N'	   	
	jne Bcadena630
	cmp TFJC[6*MaMax+27],'N'	   
	jne Bcadena630
	cmp TFJC[8*MaMax+30],'N'	   
	jne Bcadena630
	cmp TFJC[8*MaMax+27],'B'	   
	jne Bcadena630
	cmp TFJC[10*MaMax+30],'B'
	jne Bcadena630			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena630:
	cmp TFJC[4*MaMax+30],'B'	   	
	je Bconlibertad630
	cmp TFJC[6*MaMax+27],'B'		
	je  Bconlibertad630
	cmp TFJC[8*MaMax+30],'B'		
	je  Bconlibertad630
	jmp BComLib1630

    BComLib1630:           
    cmp TFJC[4*MaMax+30],32	   	
	je Bconlibertad630
	cmp TFJC[6*MaMax+27],32		
	je  Bconlibertad630
	cmp TFJC[8*MaMax+30],32		
	je  Bconlibertad630
	jmp Bsinlibertad630

	Bsinlibertad630:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad630:
	cmp TFJC[6*MaMax+30],32
	je  BpintarNegra630 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra630:
    mov TFJC +6*MaMax+29,70 
    mov TFJC +6*MaMax+30,66 
     inc contjcFB		
	jmp salidanegras

	Basignaph6:

	cmp TFJC[2*MaMax+30],'N'	   	
	jne Bcadena430
	cmp TFJC[4*MaMax+27],'N'	   
	jne Bcadena430
	cmp TFJC[6*MaMax+30],'N'	   
	jne Bcadena430
	cmp TFJC[6*MaMax+27],'B'	   
	jne Bcadena430
	cmp TFJC[8*MaMax+30],'B'
	jne Bcadena430			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena430:
    cmp TFJC[2*MaMax+30],'B'	   	
	je Bconlibertad430
	cmp TFJC[4*MaMax+27],'B'
	je  Bconlibertad430
	cmp TFJC[6*MaMax+30],'B'
	je  Bconlibertad430
	jmp BComLib430

    BComLib430:   
    cmp TFJC[2*MaMax+30],32	   	
	je Bconlibertad430
	cmp TFJC[4*MaMax+27],32
	je  Bconlibertad430
	cmp TFJC[6*MaMax+30],32
	je  Bconlibertad430
	jmp Bsinlibertad430

	Bsinlibertad430:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad430:
	cmp TFJC[4*MaMax+30],32
	je  BpintarNegra430 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra430:
    mov TFJC +4*MaMax+29,70 
    mov TFJC +4*MaMax+30,66 
     inc contjcFB	
	jmp salidanegras


	Basignaph7:	

	cmp TFJC[0*MaMax+30],'N'	   	
	jne Bcadena230
	cmp TFJC[2*MaMax+27],'N'	   
	jne Bcadena230
	cmp TFJC[4*MaMax+30],'N'	   
	jne Bcadena230
	cmp TFJC[4*MaMax+27],'B'	   
	jne Bcadena230
	cmp TFJC[6*MaMax+30],'B'
	jne Bcadena230			
	mostrar ReglaKo
	jmp TurnoBlancas

	Bcadena230:
    cmp TFJC[0*MaMax+30],'B'	   	
	je Bconlibertad230
	cmp TFJC[2*MaMax+27],'B'	
	je  Bconlibertad230
	cmp TFJC[6*MaMax+30],'B'	
	je  Bconlibertad230
	jmp BComLib230

    BComLib230:
    cmp TFJC[0*MaMax+30],32	   	
	je Bconlibertad230
	cmp TFJC[2*MaMax+27],32	
	je  Bconlibertad230
	cmp TFJC[6*MaMax+30],32	
	je  Bconlibertad230
	jmp Bsinlibertad230

	Bsinlibertad230:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad230:
	cmp TFJC[2*MaMax+30],32
	je  BpintarNegra230 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra230:
    mov TFJC +2*MaMax+29,70 
    mov TFJC +2*MaMax+30,66 
     inc contjcFB	
	jmp salidanegras

	Basignaph8:	
	cmp TFJC[0*MaMax+27],'B'	   	
	je Bconlibertad030
	cmp TFJC[2*MaMax+30],'B'		
	je  Bconlibertad030	
	jmp BComLib030

    BComLib030:
    cmp TFJC[0*MaMax+27],32	   	
	je Bconlibertad030
	cmp TFJC[2*MaMax+30],32		
	je  Bconlibertad030	
	jmp Bsinlibertad030

	Bsinlibertad030:
	mostrar PosSuicidio
	jmp TurnoBlancas

	Bconlibertad030:
	cmp TFJC[0*MaMax+30],32
	je  BpintarNegra030 
    mostrar PosOcupada
	jmp TurnoBlancas

	BpintarNegra030:
    mov TFJC +0*MaMax+29,70 
    mov TFJC +0*MaMax+30,66 
     inc contjcFB	
	jmp salidanegras

	
	pasoblancas:
	mostrar coordenaIncorrecta
	jmp     TurnoBlancas

	salidanegras:
	mostrar TFJC 
	jmp  TurnoNegras
 
main  endp
;--------------------------------------------------
;Termina proceso
;--------------------------------------------------
end main