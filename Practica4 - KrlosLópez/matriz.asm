;--------------------------------------------------
;Directiva para indicar el tama�o del programa
;--------------------------------------------------
.model small
.stack
;--------------------------------------------------
;Directiva para indicar datos (variables)
;--------------------------------------------------
.data
;-------------------------
;PARA LOS ARCHIVOS
;-------------------------
bufferentrada db 50 dup('$')
arreglo db 7 dup('$'),'$'
arreglo2 db 7 dup('$'),'$'
handlerentrada dw ?
bufferInformacion db 200 dup('$')

archia db 'Ingrese ruta del archivo', 0ah,0dh,'$' 
archib db '  .', 0ah,0dh,'$' 
archic db 'Juego guardado con exito!.', 0ah,0dh,'$' 
archid db 'Ingrese nombre para guardar:', 0ah,0dh,'$' 
archie db 'Ingrese texto para guardar:', 0ah,0dh,'$' 

erroru db 'Error al abrir el archivo puede que no exista', 0ah,0dh,'$' 
errord db 'Error al cerrar el archivo', 0ah,0dh,'$' 
errort db 'Error al escribir en el archivo', 0ah,0dh,'$' 
errorc db 'Error al crear el archivo', 0ah,0dh,'$' 
errorleer db 'Error al leer el archivo', 0ah,0dh,'$' 

;---------------------------------------------------
;DATOS DEL ENCABEZADO
;---------------------------------------------------
enc0 db  'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA', 0ah,0dh, '$'
enc1 db  'FACULTAD DE INGENIERIA', 0ah,0dh, '$'
enc2 db  'CIENCIAS Y SISTEMAS', 0ah,0dh, '$'
enc3 db  'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1', 0ah,0dh, '$'
enc4 db  'NOMBRE: JUAN CARLOS MALDONADO SOLORZANO', 0ah,0dh, '$'
enc5 db  'CARNET: 201222687', 0ah,0dh, '$'
enc6 db  'SECCION: A',0ah,0dh, '$'

;-------------------------
;DATOS DEL MENU
;-------------------------
LineaVacia db  0ah,0dh, '$'

msg1 db '1.Iniciar Juego.', 0ah,0dh,'$'
msg2 db '2.Cargar Juego.',0ah,0dh,'$'
msg3 db '3.Salir.', 0ah,0dh,'$'                             

Vopcion db 'Seleccione la opcion que desee: ',0ah,0dh,'$' 

msga db 'Juego iniciando', 0ah,0dh,'$' 
msgb db 'Cargando el juego.', 0ah,0dh,'$' 
msgc db 'Finalizando juego.', 0ah,0dh,'$' 
msgd db 'Opcion incorrecta intente de nuevo.', 0ah,0dh,'$' 

;--------------------------------------------------
;Directiva para indicar codigo
;--------------------------------------------------
.code
;--------------------------------------------------
;MACRO PARA ABRIR ARCHIVO
;--------------------------------------------------
Abrir macro buffer,handler
mov ah,3dh
mov al,02h
lea dx,buffer
int 21h
jc Error1
mov handler,ax
endm
;--------------------------------------------------
;MACRO PARA CERRAR ARCHIVO
;--------------------------------------------------
Cerrar macro handler
mov ah,3eh
mov bx,handler
int 21h
jc Error2
endm
;--------------------------------------------------
;MACRO PARA LEER ARCHIVO
;--------------------------------------------------
Leer macro handler,buffer,numbytes
mov ah,3fh
mov bx,handler
mov cx,numbytes
lea dx,buffer
int 21h
jc  Error3
endm
;--------------------------------------------------
;MACRO PARA LIMPIAR VARIABLES
;--------------------------------------------------
Limpiar macro buffer,numbytes,caracter
LOCAL Repetir
xor si,si
xor cx,cx
mov cx,numbytes
Repetir:
mov buffer[si],caracter
inc si
Loop Repetir
endm
;--------------------------------------------------
;MACRO PARA CREAR ARCHIVO
;--------------------------------------------------
CrearArchivo macro buffer,handler
mov ah,3ch
mov cx,00h
lea dx,buffer
int 21h
jc Error4
mov handler,ax
endm
;--------------------------------------------------
;MACRO PARA ESCRIBIR ARCHIVO
;--------------------------------------------------
Escribir macro handler,buffer,numbytes
mov ah,40h
mov bx,handler
mov cx,numbytes
lea dx,buffer
int 21h
jc Error5
endm
;--------------------------------------------------
;MACRO PARA IMPRIMIR EN PANTALLA
;--------------------------------------------------
mostrar macro texto
    mov ax,@data    
    mov ds,ax  
    mov ah,09h ; numero de funcion para imprimir cadena en pantalla
;igual a lea dx,cadena,inicializa en dx la posicion donde comienza la cadena
    mov dx,offset texto 
    int 21h
endm

;--------------------------------------------------
;MACRO PARA OBTENER CARACTER
;--------------------------------------------------
getchar macro
mov ah,01h
int 21h
endm
;--------------------------------------------------
;MACRO PARA OBTENER TEXTO
;--------------------------------------------------
ObtenerTexto macro buffer
LOCAL obtenerchar,FinOT
xor si,si 	  ;igual a mov si,0
obtenerchar:
getchar
cmp al,0dh 	  ;ascii del salto de linea en hexadecimal
je FinOT
mov buffer[si],al ;mov destino,fuente
inc si ;si=si+1
jmp obtenerchar
FinOT:
mov al,24h ;ascii del signo dolar
mov buffer[si],al
endm
;--------------------------------------------------
;MACRO PARA OBTENER RUTA
;--------------------------------------------------
ObtenerRuta macro buffer
LOCAL obtenerchar,FinOT
xor si,si ; igual a mov si,0
obtenerchar:
getchar
cmp al,0dh ; ascii del salto de linea en hexadecimal
je FinOT
mov buffer[si],al ;mov destino,fuente
inc si ;si=si+1
jmp obtenerchar
FinOT:
mov al,00h ;ascii del signo null
mov buffer[si],al
endm
;--------------------------------------------------
;procedimiento principal
;--------------------------------------------------
main  proc              

;Inicia proceso
;--------------------------------------------


;---------------------------------------------
;MOSTRAMOS EL ENCABEZADO
;----------------------------------------------
   encabezado:

	   mostrar enc0
	   mostrar enc1
	   mostrar enc2
	   mostrar enc3
	   mostrar enc4
	   mostrar enc5
	   mostrar enc6


    menufirst:

        mostrar LineaVacia 
	mostrar LineaVacia 
	mostrar msg1 
	mostrar msg2 
	mostrar msg3 
	mostrar Vopcion
	   getchar
	   cmp al,31h ;codigo ascii del numero 1 en hexadecimal
           je opcion1 
	   cmp al,32h ;codigo ascii del numero 2 en hexadecimal 
           je opcion2 
	   cmp al,33h ;codigo ascii del numero 3 en hexadecimal 
           je opcion3
	   cmp al,34h ;codigo ascii del numero 4 en hexadecimal 
           je opcion5
	   cmp al,35h ;codigo ascii del numero 5 en hexadecimal 
           je opcion6
;Si es cualquier otro caracter regresa al menu principal
	   jmp opcion4

     opcion1:
	mostrar  msga 
	jmp menufirst
     opcion2:
	mostrar  msgb 
	jmp menufirst
     opcion3:
    	mostrar  msgc 
	jmp salir
     opcion4:
	mostrar msgd 
        jmp menufirst
    
     
    Error1:
	mostrar erroru
	jmp menufirst

    Error2:
	mostrar errord
	jmp menufirst

    Error3:
	mostrar errorleer
	jmp menufirst

    Error4:
	mostrar errorc 
	jmp menufirst

     Error5:
	mostrar errort
	jmp menufirst 

    opcion5:
	mostrar LineaVacia 
	mostrar archia 
	mostrar LineaVacia 
	Limpiar bufferentrada,SIZEOF bufferentrada,24h
	ObtenerRuta bufferentrada 
	;mostrar bufferentrada 
	Abrir bufferentrada,handlerentrada 	
	Limpiar bufferInformacion,SIZEOF bufferInformacion,24h
   Leer handlerentrada,bufferInformacion,SIZEOF bufferInformacion 
	mostrar bufferInformacion
   	Cerrar handlerentrada 	
	jmp menufirst

    opcion6:
	mostrar LineaVacia 
	mostrar archia 
	mostrar LineaVacia 
	Limpiar bufferentrada,SIZEOF bufferentrada,24h
	ObtenerRuta bufferentrada 
	CrearArchivo bufferentrada,handlerentrada 
	Limpiar bufferInformacion,SIZEOF bufferInformacion,24h
	mostrar LineaVacia 
	mostrar archie 
	mostrar LineaVacia 
	ObtenerTexto bufferInformacion 
  Escribir handlerentrada,bufferInformacion,SIZEOF bufferInformacion
   	Cerrar handlerentrada 	
	jmp menufirst    


     salir:
   	   mov  ah,4ch       
	   xor  al,al
	   int  21h          
 
main  endp
;--------------------------------------------------
;Termina proceso
;--------------------------------------------------
end main