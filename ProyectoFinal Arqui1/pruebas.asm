;--------------------------------------------------
;Directiva para indicar el tama�o del programa
;--------------------------------------------------
.model small
.stack
;--------------------------------------------------
;Directiva para indicar datos (variables)
;--------------------------------------------------
.data
xPar        dw 0,'$'
yPar        dw 0,'$'

;Variables para guardar los valores en hexadecimal
contador1   db 0,'$'
misNumeros dw 25 dup(0) , '$'
desordenados    db  25 dup(1) , '$' 
viniciocarro        dw 0,'$'
;TFJC db 16 dup(31 dup("-"),0ah),0dh,'$'  
;misNumeros2     db  25 dup(0) , '$'
;tablero db 5 dup(5 dup("x"),0ah),0dh,'$'
;---------------------------------------------------
;DATOS DEL ENCABEZADO
;---------------------------------------------------
opcionuno db  'contando uno ',0ah,0dh, '$'
opciondos db  'contando dos ',0ah,0dh, '$'
opcioncero db  'contando cero ',0ah,0dh, '$'

coma   db 'd',0ah,0dh,'$'

vuser        db "Usuario:",'$'
vnivel       db "Nivel: "
vnivel2      db "0",'$'


vpuntos       db "Puntos: "
vpuntos2      db "0",'$'


tTime       db "Tiempo: "
tMinut      db "00:"
tSecond     db "00:"
tMili       db "000",'$'

enc0 db  'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA', 0ah,0dh, '$'
enc1 db  'FACULTAD DE INGENIERIA', 0ah,0dh, '$'
enc2 db  'CIENCIAS Y SISTEMAS', 0ah,0dh, '$'
enc3 db  'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1', 0ah,0dh, '$'
enc4 db  'NOMBRE: JUAN CARLOS MALDONADO SOLORZANO', 0ah,0dh, '$'
enc5 db  'CARNET: 201222687', 0ah,0dh, '$'
enc6 db  'SECCION: A',0ah,0dh, '$'

;-------------------------
;DATOS DEL MENU
;-------------------------
LineaVacia db  0ah,0dh, '$'

msg1 db '1.Iniciar Juego.', 0ah,0dh,'$'
msg2 db '2.Cargar Juego.',0ah,0dh,'$'
msg3 db '3.Salir.', 0ah,0dh,'$'                             

Vopcion db 'Seleccione la opcion que desee: ',0ah,0dh,'$' 

msga db 'juego iniciando', 0ah,0dh,'$' 
msgb db 'cargando el juego.', 0ah,0dh,'$' 
msgc db 'finalizando juego.', 0ah,0dh,'$' 

msgtopederecha db 'tope derecha', 0ah,0dh,'$' 
msgtopeizquierda db 'tope izquierda', 0ah,0dh,'$' 

;--------------------------------------------------
;Directiva para indicar codigo
;--------------------------------------------------

;--------------------------------------------------
;Variables para la hora
;--------------------------------------------------
ho DB ?, '$'
mi DB ?, '$'
se DB ?, '$'
hconv db ?, ?, '$'
miconv db ?, ?, '$'
seconv db ?, ?, '$'
msj_separador DB " :",'$'
msjhora db "Hora: ",'$'
;--------------------------------------------------
;Variables para la fecha
;--------------------------------------------------
diaF DB ?, '$'
mesF DB ?, '$'
dmconv DB ?, ?, '$'
mconv DB ?, ?, '$'
fechaseparador DB " /",'$'
msjfecha db 'Fecha: ','$'
fechaanio DB '2020','$'


resAx       dw 0,'$'
resBx       dw 0,'$'
resCx       dw 0,'$'
resDx       dw 0,'$'
resSi       dw 0,'$'
resDi       dw 0,'$'

.code

;--------------------------------------------------
;MACRO PARA IMPRIMIR EN PANTALLA
;--------------------------------------------------
mostrar macro texto
    mov ax,@data    
    mov ds,ax  
    mov ah,09h ; numero de funcion para imprimir cadena en pantalla
;igual a lea dx,cadena,inicializa en dx la posicion donde comienza la cadena
    mov dx,offset texto 
    int 21h
endm



HoraActual macro horas,minutos,segundos,horas_conv,min_conv,seg_conv

mov ah, 2Ch
int 21h

mov  horas, ch
mov  minutos, cl
mov  segundos, dh

mov al, [horas]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset horas_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [minutos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset min_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [segundos]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset seg_conv
mov [bx], al
inc bx
mov [bx], ah

endm

FechaActual macro  dia_mes,mes,dia_mes_conv,mes_ano_conv
	mov ah, 2AH
	int 21h
	;mov  fechaanio, cx
	mov  mes, dh
	mov  dia_mes, dl	
;Obtener el n�mero del d�a del mes
;Dividimos entre 10 para obtener el n�mero en decimal
;Luego lo transformamos a ASCII
mov al, [dia_mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset dia_mes_conv
mov [bx], al
inc bx
mov [bx], ah

mov al, [mes]
mov cl, 10
mov ah, 0
div cl
or ax, 3030h
mov bx, offset mes_ano_conv 
mov [bx], al
inc bx
mov [bx], ah

endm
;--------------------------------------------------
;MACRO PARA OBTENER CARACTER
;--------------------------------------------------
getchar macro
mov ah,01h
int 21h
endm
;--------------------------------------------------
;MACRO MODO VIDEO
;--------------------------------------------------
modovideo macro
mov ah,00h
mov al ,13h
int 10h
mov ax, 0A000h
mov ds, ax  ; DS = A000h (memoria de graficos).
endm
;--------------------------------------------------
;MACRO PINTAR PELOTA
;--------------------------------------------------
pintarpelota macro  pos,color
push dx
mov di, pos
mov dl, color

mov [di],dl
mov [di+1],dl
mov [di+2],dl

mov [di +320],dl
mov [di +321],dl
mov [di +322],dl

mov [di +640],dl
mov [di +641],dl
mov [di +642],dl

pop dx
endm

;--------------------------------------------------
;MACRO PINTAR CARRO
;--------------------------------------------------

pintarcarro macro  pos,color,colorllanta
push dx
mov di, pos
mov dl, color

mov [di],dl
mov [di+1],dl
mov [di+2],dl
mov [di+3],dl
mov [di+4],dl
mov [di+5],dl
mov [di+6],dl
mov [di+7],dl
mov [di+8],dl
mov [di+9],dl
mov [di+10],dl
mov [di+11],dl
mov [di+12],dl
mov [di+13],dl
mov [di+14],dl
mov [di+15],dl
mov [di+16],dl
mov [di+17],dl
mov [di+18],dl
mov [di+19],dl
mov [di+20],dl


mov [di +320],dl
mov [di +321],dl
mov [di +322],dl
mov [di +323],dl
mov [di +324],dl
mov [di +325],dl
mov [di +326],dl
mov [di +327],dl
mov [di +328],dl
mov [di +329],dl
mov [di +330],dl
mov [di +331],dl
mov [di +332],dl
mov [di +333],dl
mov [di +334],dl
mov [di +335],dl
mov [di +336],dl
mov [di +337],dl
mov [di +338],dl
mov [di +339],dl
mov [di +340],dl

mov [di +640],dl
mov [di +641],dl
mov [di +642],dl
mov [di +643],dl
mov [di +644],dl
mov [di +645],dl
mov [di +646],dl
mov [di +647],dl
mov [di +648],dl
mov [di +649],dl
mov [di +650],dl
mov [di +651],dl
mov [di +652],dl
mov [di +653],dl
mov [di +654],dl
mov [di +655],dl
mov [di +656],dl
mov [di +657],dl
mov [di +658],dl
mov [di +659],dl
mov [di +660],dl

mov dl, colorllanta
mov [di +957],dl
mov [di +958],dl
mov [di +959],dl
mov dl, color

mov [di +960],dl
mov [di +961],dl
mov [di +962],dl
mov [di +963],dl
mov [di +964],dl
mov [di +965],dl
mov [di +966],dl
mov [di +967],dl
mov [di +968],dl
mov [di +969],dl
mov [di +970],dl
mov [di +971],dl
mov [di +972],dl
mov [di +973],dl
mov [di +974],dl
mov [di +975],dl
mov [di +976],dl
mov [di +977],dl
mov [di +978],dl
mov [di +979],dl
mov [di +980],dl

mov dl, colorllanta
mov [di +981],dl
mov [di +982],dl
mov [di +983],dl

mov [di +1277],dl
mov [di +1278],dl
mov [di +1279],dl

mov dl, color


mov [di +1280],dl
mov [di +1281],dl
mov [di +1282],dl
mov [di +1283],dl
mov [di +1284],dl
mov [di +1285],dl
mov [di +1286],dl
mov [di +1287],dl
mov [di +1288],dl
mov [di +1289],dl
mov [di +1290],dl
mov [di +1291],dl
mov [di +1292],dl
mov [di +1293],dl
mov [di +1294],dl
mov [di +1295],dl
mov [di +1296],dl
mov [di +1297],dl
mov [di +1298],dl
mov [di +1299],dl
mov [di +1300],dl

mov dl, colorllanta
mov [di +1301],dl
mov [di +1302],dl
mov [di +1303],dl

mov [di +1597],dl
mov [di +1598],dl
mov [di +1599],dl

mov dl, color



mov [di +1600],dl
mov [di +1601],dl
mov [di +1602],dl
mov [di +1603],dl
mov [di +1604],dl
mov [di +1605],dl
mov [di +1606],dl
mov [di +1607],dl
mov [di +1608],dl
mov [di +1609],dl
mov [di +1610],dl
mov [di +1611],dl
mov [di +1612],dl
mov [di +1613],dl
mov [di +1614],dl
mov [di +1615],dl
mov [di +1616],dl
mov [di +1617],dl
mov [di +1618],dl
mov [di +1619],dl
mov [di +1620],dl

mov dl, colorllanta
mov [di +1621],dl
mov [di +1622],dl
mov [di +1623],dl
mov dl, color



mov [di +1920],dl
mov [di +1921],dl
mov [di +1922],dl
mov [di +1923],dl
mov [di +1924],dl
mov [di +1925],dl
mov [di +1926],dl
mov [di +1927],dl
mov [di +1928],dl
mov [di +1929],dl
mov [di +1930],dl
mov [di +1931],dl
mov [di +1932],dl
mov [di +1933],dl
mov [di +1934],dl
mov [di +1935],dl
mov [di +1936],dl
mov [di +1937],dl
mov [di +1938],dl
mov [di +1939],dl
mov [di +1940],dl

mov [di +2240],dl
mov [di +2241],dl
mov [di +2242],dl
mov [di +2243],dl
mov [di +2244],dl
mov [di +2245],dl
mov [di +2246],dl
mov [di +2247],dl
mov [di +2248],dl
mov [di +2249],dl
mov [di +2250],dl
mov [di +2251],dl
mov [di +2252],dl
mov [di +2253],dl
mov [di +2254],dl
mov [di +2255],dl
mov [di +2256],dl
mov [di +2257],dl
mov [di +2258],dl
mov [di +2259],dl
mov [di +2260],dl

mov [di +2560],dl
mov [di +2561],dl
mov [di +2562],dl
mov [di +2563],dl
mov [di +2564],dl
mov [di +2565],dl
mov [di +2566],dl
mov [di +2567],dl
mov [di +2568],dl
mov [di +2569],dl
mov [di +2570],dl
mov [di +2571],dl
mov [di +2572],dl
mov [di +2573],dl
mov [di +2574],dl
mov [di +2575],dl
mov [di +2576],dl
mov [di +2577],dl
mov [di +2578],dl
mov [di +2579],dl
mov [di +2580],dl

mov [di +2880],dl
mov [di +2881],dl
mov [di +2882],dl
mov [di +2883],dl
mov [di +2884],dl
mov [di +2885],dl
mov [di +2886],dl
mov [di +2887],dl
mov [di +2888],dl
mov [di +2889],dl
mov [di +2890],dl
mov [di +2891],dl
mov [di +2892],dl
mov [di +2893],dl
mov [di +2894],dl
mov [di +2895],dl
mov [di +2896],dl
mov [di +2897],dl
mov [di +2898],dl
mov [di +2899],dl
mov [di +2900],dl



mov [di +3200],dl
mov [di +3201],dl
mov [di +3202],dl
mov [di +3203],dl
mov [di +3204],dl
mov [di +3205],dl
mov [di +3206],dl
mov [di +3207],dl
mov [di +3208],dl
mov [di +3209],dl
mov [di +3210],dl
mov [di +3211],dl
mov [di +3212],dl
mov [di +3213],dl
mov [di +3214],dl
mov [di +3215],dl
mov [di +3216],dl
mov [di +3217],dl
mov [di +3218],dl
mov [di +3219],dl
mov [di +3220],dl

mov [di +3520],dl
mov [di +3521],dl
mov [di +3522],dl
mov [di +3523],dl
mov [di +3524],dl
mov [di +3525],dl
mov [di +3526],dl
mov [di +3527],dl
mov [di +3528],dl
mov [di +3529],dl
mov [di +3530],dl
mov [di +3531],dl
mov [di +3532],dl
mov [di +3533],dl
mov [di +3534],dl
mov [di +3535],dl
mov [di +3536],dl
mov [di +3537],dl
mov [di +3538],dl
mov [di +3539],dl
mov [di +3540],dl

mov dl, colorllanta
mov [di +3837],dl
mov [di +3838],dl
mov [di +3839],dl
mov dl, color

mov [di +3840],dl
mov [di +3841],dl
mov [di +3842],dl
mov [di +3843],dl
mov [di +3844],dl
mov [di +3845],dl
mov [di +3846],dl
mov [di +3847],dl
mov [di +3848],dl
mov [di +3849],dl
mov [di +3850],dl
mov [di +3851],dl
mov [di +3852],dl
mov [di +3853],dl
mov [di +3854],dl
mov [di +3855],dl
mov [di +3856],dl
mov [di +3857],dl
mov [di +3858],dl
mov [di +3859],dl
mov [di +3860],dl

mov dl, colorllanta
mov [di +3861],dl
mov [di +3862],dl
mov [di +3863],dl

mov [di +4157],dl
mov [di +4158],dl
mov [di +4159],dl

mov dl, color

mov [di +4160],dl
mov [di +4161],dl
mov [di +4162],dl
mov [di +4163],dl
mov [di +4164],dl
mov [di +4165],dl
mov [di +4166],dl
mov [di +4167],dl
mov [di +4168],dl
mov [di +4169],dl
mov [di +4170],dl
mov [di +4171],dl
mov [di +4172],dl
mov [di +4173],dl
mov [di +4174],dl
mov [di +4175],dl
mov [di +4176],dl
mov [di +4177],dl
mov [di +4178],dl
mov [di +4179],dl
mov [di +4180],dl

mov dl, colorllanta
mov [di +4181],dl
mov [di +4182],dl
mov [di +4183],dl

mov [di +4477],dl
mov [di +4478],dl
mov [di +4479],dl

mov dl, color

mov [di +4480],dl
mov [di +4481],dl
mov [di +4482],dl
mov [di +4483],dl
mov [di +4484],dl
mov [di +4485],dl
mov [di +4486],dl
mov [di +4487],dl
mov [di +4488],dl
mov [di +4489],dl
mov [di +4490],dl
mov [di +4491],dl
mov [di +4492],dl
mov [di +4493],dl
mov [di +4494],dl
mov [di +4495],dl
mov [di +4496],dl
mov [di +4497],dl
mov [di +4498],dl
mov [di +4499],dl
mov [di +4500],dl

mov dl, colorllanta
mov [di +4501],dl
mov [di +4502],dl
mov [di +4503],dl

mov [di +4797],dl
mov [di +4798],dl
mov [di +4799],dl

mov dl, color

mov [di +4800],dl
mov [di +4801],dl
mov [di +4802],dl
mov [di +4803],dl
mov [di +4804],dl
mov [di +4805],dl
mov [di +4806],dl
mov [di +4807],dl
mov [di +4808],dl
mov [di +4809],dl
mov [di +4810],dl
mov [di +4811],dl
mov [di +4812],dl
mov [di +4813],dl
mov [di +4814],dl
mov [di +4815],dl
mov [di +4816],dl
mov [di +4817],dl
mov [di +4818],dl
mov [di +4819],dl
mov [di +4820],dl

mov dl, colorllanta
mov [di +4821],dl
mov [di +4822],dl
mov [di +4823],dl
mov dl, color

mov [di +5120],dl
mov [di +5121],dl
mov [di +5122],dl
mov [di +5123],dl
mov [di +5124],dl
mov [di +5125],dl
mov [di +5126],dl
mov [di +5127],dl
mov [di +5128],dl
mov [di +5129],dl
mov [di +5130],dl
mov [di +5131],dl
mov [di +5132],dl
mov [di +5133],dl
mov [di +5134],dl
mov [di +5135],dl
mov [di +5136],dl
mov [di +5137],dl
mov [di +5138],dl
mov [di +5139],dl
mov [di +5140],dl

pop dx
endm
;186     60,000   59700

;--------------------------------------------------
;MACRO PINTAR  BLOQUE AMARILLO
;--------------------------------------------------
pintarbloque macro pos,color
push dx
mov di, pos
mov dl, color

mov [di],dl
mov [di+1],dl

mov [di+319],dl
mov [di+320],dl
mov [di+321],dl
mov [di+322],dl

mov [di +638],dl
mov [di +639],dl
mov [di +640],dl
mov [di +641],dl
mov [di +642],dl
mov [di +643],dl

mov [di+959],dl
mov [di+960],dl
mov [di+961],dl
mov [di+962],dl

mov [di+1280],dl
mov [di+1281],dl


pop dx
endm

;--------------------------------------------------
;MACRO PINTAR MARGEN JUEGO
;--------------------------------------------------
;mov [di],30h
pintarmargen macro color
LOCAL primera,segunda,tercera,cuarta
mov dl,color
; empiza en pixel (i,j) = (20,0)= 320*20 +0 = 6400
;barra horizontal superior 
mov di,6405
primera:
mov [di],dl
inc di
cmp di,6714 
jne primera
;barra horizontal inferior
;empieza en el pixel (i,j)=(190,0)= 320*190 + 0 = 60800
mov di,60805
segunda:
mov [di],dl
inc di
cmp di,61114
jne segunda

; barra vertical izquierda
mov di,6405 
tercera:
mov [di],dl
add di, 320
cmp di, 60805
jne tercera

; barra vertical derecha
mov di,6714 
cuarta:
mov [di],dl
add di, 320
cmp di, 61114
jne cuarta

endm


procobstaculos proc
     mov dx,7050
     accion: 
       pintarbloque dx,0 ; (110,160) = 110*320 + 160
       add dx,1280
       ;add dx,318
       pintarbloque dx,6
       mov  ah, 08h  ;7
       int  21h
       cmp di , 58250
       je salirobstaculo 
     jmp accion  

     salirobstaculo:
        pintarbloque dx,0 ; (110,160) = 110*320 + 160
        ret    
procobstaculos endp

draw_pixel MACRO x,y,color

    mov resAx,ax
    mov resBx,bx
    mov resCx,cx
    mov resDx,dx  
    
    mov cx,x
    mov dx,y
    mov al,color
    mov ah,0ch
    int 10h

    mov ax,resAx
    mov bx,resBx
    mov cx,resCx
    mov dx,resDx
    
ENDM

draw_0 MACRO x,y

    mov ax,x
    mov xPar,ax

    mov ax,y
    mov yPar,ax

    inc xPar
    draw_pixel xPar,yPar,15
    inc xPar
    draw_pixel xPar,yPar,15

    mov ax,x
    mov xPar,ax
    inc yPar

    draw_pixel xPar,yPar,15
    add xPar,3
    draw_pixel xPar,yPar,15

    mov ax,x
    mov xPar,ax
    inc yPar

    draw_pixel xPar,yPar,15
    add xPar,3
    draw_pixel xPar,yPar,15

    mov ax,x
    mov xPar,ax
    inc yPar

    draw_pixel xPar,yPar,15
    add xPar,3
    draw_pixel xPar,yPar,15

    mov ax,x
    mov xPar,ax
    inc yPar

    draw_pixel xPar,yPar,15
    add xPar,3
    draw_pixel xPar,yPar,15

    mov ax,x
    mov xPar,ax
    inc yPar

    draw_pixel xPar,yPar,15
    add xPar,3
    draw_pixel xPar,yPar,15

    mov ax,x
    mov xPar,ax
    inc yPar

    inc xPar
    draw_pixel xPar,yPar,15
    inc xPar
    draw_pixel xPar,yPar,15


ENDM

;--------------------------------------------------
;procedimiento principal
;--------------------------------------------------
main  proc           
;Inicia proceso
;--------------------------------------------   

   encabezado:
        jmp comenzando 
	   mostrar enc0
	   mostrar enc1
	   mostrar enc2
	   mostrar enc3
	   mostrar enc4
	   mostrar enc5
	   mostrar enc6
       getchar
       
       

        comenzando:
       ;modovideo

       mov ax ,13h
       int 10h

       ;draw_pixel 6,20,15
       draw_0 6,20
       ;mostrar vuser
       ;mostrar vnivel
       ;mostrar vpuntos
       ;mostrar tTime

       ;pintarmargen 5
       jmp salir 
       
       ;mov dx,35360
       ;mostrar enc6
       ;accion: 
       ;pintarpelota dx,0 ; (110,160) = 110*320 + 160
       ;sub dx,319
       ;pintarpelota dx,2
       ;jmp accion  
       call procobstaculos
       ;pintarbloque 16050,2
       mov viniciocarro, 54560
       ;mov dx,54560
       pintarcarro  viniciocarro,1 ,4    ;(170,160) = 170*320+160

            etjugando:                 
            ;getchar            
            mov  ah, 08h;7
            int  21h
            ;getchar            
            ;call oyendo
            cmp al,04dh       ; derecha
            je etfderecha
            cmp al,04bh       ; izuierda
            je etfizquierda
            cmp al,01bH       ;escape
            je etpausa
            cmp al,20H         ; barra
            je etjuegotermina
            jmp etjugando


            etfderecha: 
            cmp di,54686
            je etjugando   ;   170*320+160+21=  maximo  54719
            pintarcarro viniciocarro,0,0 ; (170,160) = 170*320 + 160                       
            add viniciocarro,21                                                 
            ;mov [di+20],dl 
            cmp di,54686
            je etjugando   ;   170*320+160+21=  maximo  54719
            pintarcarro viniciocarro,1,4
            jmp etjugando 

            etfizquierda:                            
            cmp di, 54413
            je etjugando       ;             54400
            pintarcarro viniciocarro,0,0 ; (170,160) = 170*320 + 160
            sub viniciocarro,21            
            cmp di, 54413
            je etjugando
            pintarcarro viniciocarro,1,4                        
            jmp etjugando

            etpausa:
            ;imprimir vesca
            getchar    
            jmp etjugando

            etjuegotermina:
            ;imprimir vbarra
            jmp salir


            ettopederecha:
            mostrar msgtopederecha
            mostrar msgtopeizquierda
            jmp salir
            ;mostrar desordenados

            mov misNumeros[0],48
            
            mov misNumeros[2],49            
        
            mov misNumeros[4],50            
        
            mov misNumeros[6],51            
        
            mov misNumeros[8],52            
        
            mov misNumeros[10],53            
        
            mov misNumeros[12],54            
        
            mov misNumeros[14],55            
        
            mov misNumeros[16],56            
        
            mov misNumeros[18],57
        
     ;mostrar contador1
     
    ;cmp contador1,0
    ;je etnumerocero
    ;cmp contador1,1
    ;je etnumerouno
    ;cmp contador1,2
    ;je etnumerodos
    ;jmp encabezado
    
    
    ;mostrar opciondos

    ;etnumerocero:
    ;mostrar opcioncero
    ;getchar
    ;inc contador1
    ;mov desordenados[0],48
    ;mov desordenados[1],49            
    ;jmp encabezado

    ;etnumerouno:
    ;mostrar opcionuno
    ;getchar
    ;inc contador1
    ;jmp encabezado
    ;mostrar misNumeros

    ;etnumerodos:
    ;mostrar opciondos
    ;getchar
    ;mov contador1,0
    ;jmp encabezado
      

     
     
     ;mostrar misNumeros2
     ;mostrar tablero
     ;mostrar TFJC
     ;FechaActual diaF,mesF,dmconv,mconv

     ;mostrar msjfecha
     ;mostrar dmconv
     ;mostrar fechaseparador
     ;mostrar mconv
     ;mostrar fechaseparador
     ;mostrar fechaanio
;---------------------------------------------
;MOSTRAMOS EL ENCABEZADO
;----------------------------------------------	

    
     

     salir:
   	   mov  ah,4ch       
	   xor  al,al
	   int  21h          
 
main  endp             

;--------------------------------------------------
;Termina proceso
;--------------------------------------------------
end main