WriteString proc
        ;--------------------------------------------------------------------;
        ;   Recibe:      [bp+4] apunta a la cadena                           ;
        ;                                                                    ;
        ;   Devuelve:    Nada.                                               ;
        ;                                                                    ;
        ;   Comentarios: Recibe una cadena con terminacion nula y            ;
        ;                lo imprime en la salida estandar                    ;
        ;--------------------------------------------------------------------;

        ;Subrutina proglogo
            push bp                    ;almacenamos el puntero base
            mov  bp,sp                 ;ebp contiene la direccion de esp
            pusha
        ;Ini Codigo--
            mov ah,09
            mov dx,word ptr[bp+4]
            int 21h
        ;Fin Codigo--

        ;Subrutina epilogo
            popa
            mov sp,bp               ;esp vuelve apuntar al inicio y elimina las variables locales
            pop bp                  ;restaura el valor del puntro base listo para el ret
        ret 2
    WriteString endp

