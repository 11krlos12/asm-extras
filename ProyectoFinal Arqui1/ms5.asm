.model small

.stack

.data
EncabezadoGeneral db 0ah,0dh, 
'UNIVERSIDAD DE SAN CARLOS DE GUATEMALA',0ah,0dh,
'FACULTAD DE INGENIERIA', 0ah,0dh, 
'ESCUELA DE CIENCIAS Y SISTEMAS',0ah,0dh,
'ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1 A',0ah,0dh, 
'PRIMER SEMESTRE 2020',0ah,0dh, 
'JUAN CARLOS MALDONADO SOLORZANO',0ah,0dh,
'201222687',0ah,0dh,
'QUINTA PRACTICA','$'

EncabezadoUno db 0ah,0dh, 
'Menu principal',0ah,0dh,0ah,0dh,
'1) Ingresar funcion',9fh,'(x)',0ah,0dh, 
'2) Funcion en memoria',0ah,0dh, 
'3) Derivada ',9fh,27h,'(x)',0ah,0dh, 
'4) Integral F(x)',0ah,0dh, 
'5) Graficar funciones',0ah,0dh, 
'6) Reporte ',0ah,0dh, 
'7) Modo calculadora ',0ah,0dh, 
'8) Salir', '$'

enc2 db 0ah,0dh, 'Reporte practica No. 5','$'
enc3 db 0ah,0dh, '  Fecha: ','$'
enc4 db 0ah,0dh, '  Hora: ','$'
enc5 db ':','$'
enc6 db ' de abril de 2020','$'

enc7 db 0ah,0dh, 
'Menu Graficas',0ah,0dh,0ah,0dh,
'1) Grafica original ',9fh,'(x)',0ah,0dh, 
'2) Grafica derivada ',9fh,27h,'(x)',0ah,0dh, 
'3) Grafica integral F(x)',0ah,0dh, 
'4) Regresar', '$'


msg8 db '   Limite superior: ','$'
msg9 db 0ah,0dh, '  valor de x: ','$'
msg10 db 0ah,0dh, ' valor de y: ','$'
msg11 db '  pos ','$'
msg12 db '  neg ','$'
msg13 db 0ah,0dh, ' Seguir: ','$'
msg14 db 0ah,0dh, ' VerMayor ','$'
msg15 db 0ah,0dh, ' VerMayor2 ','$'
msg16 db 0ah,0dh, ' NoDibujar ','$'
msg17 db ',','$'
msg18 db 0ah,0dh, ' Ingrese el valor de la constante de integracion: ','$'
elij db 0ah,0dh, 'Ingrese una opcion: ','$'

;--------------------------------------------------
;Variables para la calculadora
;--------------------------------------------------
archid db 0ah,0dh,'Ingrese la ruta del archivo abrir:', 0ah,0dh,'$' 
EjmGuardar db 0ah,0dh,'@@nombrearchivo.arq@@', 0ah,0dh,'$' 

bufferentradajc db 50 dup('$')
handlerentradajc dw ?
bufferInformacionjc db 515 dup('$')
erroru db 'Error al abrir el archivo puede que no exista', 0ah,0dh,'$' 
errorleer db 'Error al leer el archivo', 0ah,0dh,'$'
errord db 'Error al cerrar el archivo', 0ah,0dh,'$' 
fichax db 'E','$'

RutaMala db 'Sintaxis de la ruta incorrecta', 0ah,0dh,'$' 
ExtencionMala db 'Extencion mala, correcta= .arq ', 0ah,0dh,'$' 

;-------------------------------------------------------------------------------------

msg1 db 0ah,0dh, 'Funcion guardada exitosamente. ','$'
msg2 db 0ah,0dh, '  Funcion original: ','$'
msg3 db 0ah,0dh, '  Reporte creado exitosamente. ',0ah,0dh,'$'
msg4 db 0ah,0dh, '  Funcion derivada: ','$'
msg5 db 0ah,0dh, '  Funcion integral: ','$'
msg6 db 0ah,0dh, '  Ingrese intervalo entre [-99,99] ','$'
msg7 db 0ah,0dh, '  Limite inferior: ','$'


Lim0 dw ?
Lim1 dw ?
doble dw ?
y2 dw ?
hayFuncion dw 0
EsIntegral dw 0
cero dw 0
y dw ?
x dw ?
Cintegracion dw ?
x0 dw 0
x1 dw 0
x2 dw 0
x3 dw 0
x4 dw 0
Dx0 dw 0
Dx1 dw 0
Dx2 dw 0
Dx3 dw 0
buffX0 db 4 dup('$'),'$'
buffX1 db 4 dup('$'),'$'
buffX2 db 4 dup('$'),'$'
buffX3 db 4 dup('$'),'$'
buffX4 db 4 dup('$'),'$'
FuncionOriginal db 100 dup('$'),'$'
FuncionDerivada db 100 dup('$'),'$'
FuncionIntegral db 100 dup('$'),'$'
numtemp dw ?
bufftemp db 6 dup('$'),'$'
resMenor dw 0
resMayor dw 100
tamano dw ?
handlerReporte dw ?
FileReporte db 'c:\reporte.rep',00h
hora db 2 dup('$'),'$'
minutos db 2 dup('$'),'$'
segundos db 2 dup('$'),'$'
dia db 2 dup('$'),'$'

centro dw 159
relacion dw ?

salt db 0ah,0dh,'$'
coef0 db 'Coeficiente de X0: ','$'
coef1 db 'Coeficiente de X1: ','$'
coef2 db 'Coeficiente de X2: ','$'
coef3 db 'Coeficiente de X3: ','$'
coef4 db 'Coeficiente de X4: ','$'



err db 0ah,0dh,  'Error generla. No tiene que llegar hasta aqui.','$'
err1 db 0ah,0dh, 'Error caracter no valido,Ingrese numeros o signo +, -','$'
err2 db 0ah,0dh, 'Error ingreso mas de la cantidad de caracteres soportados.','$'
err3 db 0ah,0dh, 'No hay funcion almacenada para operar.',0ah,0dh,'$'
err4 db 0ah,0dh, 'El limite inferior no es menor que el superor.','$'
err5 db 0ah,0dh, 'Caracter no es valido.','$'
err7 db 0ah,0dh, 'Error al crear archivo','$'
err8 db 0ah,0dh, 'Error al abrir el archivo',0ah,0dh,
'Posiblemente el archivo no exista','$'
err9 db 0ah,0dh,  'Error al escribir en el archivo','$'
err10 db 0ah,0dh, 'Error al leer el archivo','$'
err11 db 0ah,0dh, 'Error al cerrar el archivo','$'
err12 db 0ah,0dh, 'Los limites no son cuadrados, datos incorrectos.','$'


.code
;--------------------------------------------------
;MACRO PARA ABRIR ARCHIVO
;--------------------------------------------------
Abrirjc macro buffer,handler
mov ah,3dh
mov al,02h
lea dx,buffer
int 21h
jc Errorjc1
mov handler,ax
endm
;--------------------------------------------------
;MACRO PARA LEER ARCHIVO
;--------------------------------------------------
Leerjc macro handler,buffer,numbytes
mov ah,3fh
mov bx,handler
mov cx,numbytes
lea dx,buffer
int 21h
jc  Errorjc2
endm
;--------------------------------------------------
;MACRO PARA CERRAR ARCHIVO
;--------------------------------------------------
Cerrarjc macro handler
mov ah,3eh
mov bx,handler
int 21h
jc Errorjc3
endm
;--------------------------------------------------
;MACRO PARA LIMPIAR VARIABLES
;--------------------------------------------------
Limpiarjc macro buffer,numbytes,caracter
LOCAL Repetir
xor si,si
xor cx,cx
mov cx,numbytes
Repetir:
mov buffer[si],caracter
inc si
Loop Repetir
endm
;--------------------------------------------------
;MACRO PARA OBTENER RUTA
;--------------------------------------------------
ObtenerRutajc macro buffer
LOCAL obtenerchar,FinOT
xor si,si ; igual a mov si,0
obtenerchar:
getchar  ;@@carlos.arq@@
cmp al,0dh 
; ascii del salto de linea en hexadecimal
je FinOT
mov buffer[si],al ;mov destino,fuente
inc si ;si=si+1
jmp obtenerchar
FinOT:
mov al,00h ;ascii del signo null
mov buffer[si],al
endm


ModoTexto macro

mov ax,0003h    
int 10h 
endm


ModoGrafico macro
mov ax, 0013h  
int 10h 
endm


pixel macro x0, y0, color  
push cx
mov ah, 0ch
mov al, color
mov bh, 0h
mov dx, y0
mov cx, x0
int 10h 
pop cx
endm


PintarX macro
LOCAL eje_x

mov cx,13eh 
eje_x:  
pixel cx,63h,4fh
loop eje_x 
endm


PintarY macro cent
LOCAL eje_y

mov cx,0c6h
eje_y:  
pixel cent,cx,4fh  
loop eje_y 
endm

ObtenerFuncion macro buffer,n0,n1,n2,n3,n4
LOCAL SeguirN3,SeguirN2,SeguirN1,SeguirN0,FinOF
xor si,si
xor ax,ax
xor dx,dx
xor cx,cx
mov dl,10
mov buffer[si],09h
inc si
mov buffer[si],66h 
inc si
mov buffer[si],28h 
inc si
mov buffer[si],78h 
inc si
mov buffer[si],29h 
inc si
mov buffer[si],20h 
inc si
mov buffer[si],3dh 
inc si
mov buffer[si],20h 
inc si
cmp n4,0
je SeguirN3
MeterNumero n4,34h,buffer
SeguirN3:
cmp n3,0
je SeguirN2
MeterNumero n3,33h,buffer
SeguirN2:
cmp n2,0
je SeguirN1
MeterNumero n2,32h,buffer
SeguirN1:
cmp n1,0
je SeguirN0
MeterNumero n1,31h,buffer
SeguirN0:
cmp n0,0
je FinOF
MeterNumero n0,30h,buffer
FinOF:
endm


MeterNumero macro n,exp,buffer
LOCAL Pos,Dividir44,Dividir4,Fin4,OmitirEx
mov ax,n
test ax,1000000000000000
jz Pos
neg ax
mov buffer[si],2dh 
inc si
mov buffer[si],20h 
inc si
jmp Dividir4
Pos:
mov buffer[si],2bh 
inc si
mov buffer[si],20h 
inc si
jmp Dividir4
Dividir44:
xor ah,ah
Dividir4:
div dl
inc cx
push ax
cmp al,00h 
je Fin4
jmp Dividir44
Fin4:
pop ax
add ah,30h
mov buffer[si],ah
inc si
loop Fin4
mov bl,exp
cmp bl,30h
je OmitirEx
mov buffer[si],2ah 
inc si
mov buffer[si],78h 
inc si
mov buffer[si],exp 
inc si
mov buffer[si],20h 
inc si
OmitirEx:
endm

ObtenerConstantesD macro c1,c2,c3,c4,dc1,dc2,dc3,dc4
LOCAL Saltar1,Saltar2,Saltar3,Saltar4
xor ax,ax
xor dx,dx
mov dc1,0
mov dc2,0
mov dc3,0
mov dc4,0


cmp c1,0
je Saltar1
mov ax,c1
mov dc1,ax

Saltar1:

cmp c2,0
je Saltar2
mov dl,02h
mov ax,c2
mul dx
mov dc2,ax

Saltar2:

cmp c3,0
je Saltar3
mov dl,03h
mov ax,c3
mul dx
mov dc3,ax

Saltar3:

cmp c4,0
je Saltar4
mov dl,04h
mov ax,c4
mul dx
mov dc4,ax

Saltar4:
endm

ObtenerFuncionD macro buffer,n0,n1,n2,n3
LOCAL SeguirN3,SeguirN2,SeguirN1,SeguirN0,FinOF
xor si,si
xor ax,ax
xor dx,dx
xor cx,cx
mov dl,10
mov buffer[si],09h 
inc si
mov buffer[si],66h 
inc si
mov buffer[si],27h 
inc si
mov buffer[si],28h 
inc si
mov buffer[si],78h 
inc si
mov buffer[si],29h 
inc si
mov buffer[si],20h 
inc si
mov buffer[si],3dh 
inc si
mov buffer[si],20h 
inc si
cmp n3,0
je SeguirN2
MeterNumero n3,33h,buffer
SeguirN2:
cmp n2,0
je SeguirN1
MeterNumero n2,32h,buffer
SeguirN1:
cmp n1,0
je SeguirN0
MeterNumero n1,31h,buffer
SeguirN0:
cmp n0,0
je FinOF
MeterNumero n0,30h,buffer
FinOF:
endm


ObtenerFuncionI macro buffer,n0,n1,n2,n3,n4
LOCAL SeguirN3,SeguirN2,SeguirN1,SeguirN0,FinOF
xor si,si
xor ax,ax
xor dx,dx
xor cx,cx
mov dl,10
mov buffer[si],09h 
inc si
mov buffer[si],46h 
inc si
mov buffer[si],28h 
inc si
mov buffer[si],78h 
inc si
mov buffer[si],29h 
inc si
mov buffer[si],20h 
inc si
mov buffer[si],3dh 
inc si
mov buffer[si],20h 
inc si
cmp n4,0
je SeguirN3
MeterNumero2 n4,35h,buffer
SeguirN3:
cmp n3,0
je SeguirN2
MeterNumero2 n3,34h,buffer
SeguirN2:
cmp n2,0
je SeguirN1
MeterNumero2 n2,33h,buffer
SeguirN1:
cmp n1,0
je SeguirN0
MeterNumero2 n1,32h,buffer
SeguirN0:
cmp n0,0
je FinOF
MeterNumero2 n0,31h,buffer
FinOF:
mov buffer[si],2bh 
inc si
mov buffer[si],20h 
inc si
mov buffer[si],43h 
inc si
endm


MeterNumero2 macro n,exp,buffer
LOCAL Pos,Dividir44,Dividir4,Fin4,OmitirEx,OmitirExp
mov ax,n
test ax,1000000000000000
jz Pos
neg ax
mov buffer[si],2dh 
inc si
mov buffer[si],20h 
inc si
jmp Dividir4
Pos:
mov buffer[si],2bh 
inc si
mov buffer[si],20h 
inc si
jmp Dividir4
Dividir44:
xor ah,ah
Dividir4:
div dl
inc cx
push ax
cmp al,00h 
je Fin4
jmp Dividir44
Fin4:
pop ax
add ah,30h
mov buffer[si],ah
inc si
loop Fin4

mov bl,exp
cmp bl,31h
je OmitirExp
mov buffer[si],2fh 
inc si
mov buffer[si],exp
inc si

OmitirExp:
mov bl,exp
cmp bl,30h
je OmitirEx
mov buffer[si],2ah 
inc si
mov buffer[si],78h 
inc si
mov buffer[si],exp 
inc si
mov buffer[si],20h 
inc si
OmitirEx:
endm


PedirRango macro inf,sup,buffer,cen
print msg6 
print msg7 
LimpiarArr buffer,24h,SIZEOF buffer
ObtenerNumero2 buffer,4
ConvertToInt inf,buffer
print msg8 
LimpiarArr buffer,24h,SIZEOF buffer
ObtenerNumero2 buffer,4
ConvertToInt sup,buffer
LimpiarArr buffer,24h,SIZEOF buffer 
VerificarMayor inf,sup,cen
endm


PedirConstanteIntegracion macro num
LimpiarArr bufftemp,24h,SIZEOF bufftemp
print msg18
ObtenerNumero2 bufftemp,4
ConvertToInt num,bufftemp
endm

VerificarMayor macro limI,limS,cen
LOCAL PosI,NegI,PosPos,PosNeg,NegPos,NegNeg,FinV,Comparar,CentroNormal
mov ax,limI
test ax,1000000000000000
jz PosI
jmp NegI

PosI:
mov ax,limS
test ax,1000000000000000
jz PosPos
jmp PosNeg

NegI:
mov ax,limS
test ax,1000000000000000
jz NegPos
jmp NegNeg

PosPos:
mov ax,limI
cmp ax,limS
ja Error4
je Error4
xor dx,dx
mov ax,319
mov bx,limI
mov cx,limS
add bx,cx 
div bx    
mov bx,limI
mul bx    
mov cen,ax
jmp FinV

PosNeg:
jmp Error4

NegPos:
mov ax,limI
neg ax
Comparar:
cmp ax,limS
je CentroNormal
xor dx,dx
mov ax,319
mov bx,limI
neg bx
mov cx,limS
add bx,cx 
div bx    
mov bx,limI
neg bx
mul bx    
mov cen,ax
jmp FinV

NegNeg:
mov ax,limI
mov bx,limS
neg ax
neg bx
cmp bx,ax
ja Error4
je Error4
jmp FinV

CentroNormal:
mov cen,159
FinV:
endm


EvaluarFuncion macro c0,c1,c2,c3,c4,c5,inf,sup,rel,cent,yy,xx,rM,isIntegral
LOCAL CuadranteIyIV,Fin,Seguir,NoDoble,Doblel,nodob,SiPin
xor ax,ax
xor bx,bx
xor cx,cx
xor dx,dx
mov doble,0

cmp c5,0
jne Doblel
cmp c4,0
jne NoDoble
cmp c3,0
je NoDoble
Doblel:
mov doble,1
NoDoble:

mov ax,centro
mov dx,sup
div dl
xor ah,ah
mov rel,ax

mov cx,sup
mov tamano,cx


CuadranteIyIV:
xor bx,bx
xor dx,dx
push bx

mov cx,5
Valuar c5,tamano,cx
mov cx,4
Valuar c4,tamano,cx
mov cx,3
Valuar c3,tamano,cx
mov cx,2
Valuar c2,tamano,cx
mov cx,1
Valuar c1,tamano,cx
mov cx,0
Valuar c0,tamano,cx 



mov bx,sup
cmp tamano,bx
jne Seguir
mov rM,ax
Seguir:
mov numtemp,ax


ObtenerCoordenadaY rM,numtemp,yy

ObtenerCoordenadaX cent,rel,tamano,xx


cmp yy,200
je nodob
pixel xx,yy,0ch
cmp doble,1
jne nodob
cmp tamano,0
jne SiPin
mov doble,0
jmp nodob
SiPin:
ObtenerCoordenadaX2 cent,rel,tamano,xx
pixel xx,y2,0ch
nodob:


mov dx,inf
cmp tamano,dx
je Fin
dec tamano
jmp CuadranteIyIV

Fin:
mov doble,0
xor ax,ax
xor bx,bx
xor cx,cx
xor dx,dx
endm


Valuar macro constante,val,potencia
LOCAL SoloNumero,Fin,Operar,Mientras,Multiplicar
cmp constante,0
je Fin
cmp potencia,0
je SoloNumero
jmp Operar
SoloNumero:
pop bx
mov ax,constante
add ax,bx
push ax
jmp Fin
Operar:
xor bx,bx
mov ax,val
mov cx,potencia
mov bx,ax
jmp Mientras
Multiplicar:
mul bx
Mientras:
loop Multiplicar
mov bx,constante
mul bx
pop bx
add ax,bx
push ax
jmp Fin
Fin:
endm


ObtenerCoordenadaY macro rM,res,coordenada
LOCAL Fin,RespuestaEsMenor,Seguir,Seguir2,VerMayor,NoDibujar,VerMayor2,Saltar,NoDoble
xor dx,dx
mov ax,rM
test ax,1000000000000000
jz Saltar
neg ax
Saltar:
cmp ax,99
jb RespuestaEsMenor
je RespuestaEsMenor


mov bx,99
div bx
mov cx,ax
jmp Seguir

RespuestaEsMenor:
mov bx,ax
mov ax,99
div bx
mov cx,ax
jmp Seguir2

Seguir2:
xor dx,dx
mov ax,res
mul cx 
test ax,1000000000000000
jz VerMayor
jmp VerMayor2

Seguir:
xor dx,dx
xor ax,ax
mov ax,res
div cx 
test ax,1000000000000000 
jz VerMayor
jmp VerMayor2

VerMayor:
cmp ax,99
ja NoDibujar
mov bx,99
sub bx,ax 
mov coordenada,bx
cmp doble,1
jne NoDoble
mov bx,99
add bx,ax
mov y2,bx
jmp Fin
NoDoble:
mov y2,200
jmp Fin

VerMayor2: 
neg ax
mov bx,99
add bx,ax 
cmp bx,198
ja NoDibujar
mov coordenada,bx
jmp Fin

NoDibujar:
mov coordenada,200
jmp Fin

Fin:


endm


ObtenerCoordenadaX macro cent,rel,val,xx
mov ax,rel
mov dx,val
mul dx
mov cx,cent
add cx,ax
mov xx,cx
endm


ObtenerCoordenadaX2 macro cent,rel,val,xx
mov ax,rel
mov dx,val
mul dx
mov cx,cent
sub cx,ax
mov xx,cx
endm

ObtenerSize macro buffer,tam
mov tam,0
mov tam,SIZEOF buffer
dec tam
endm

ObtenerSize2 macro buffer,tam
LOCAL see,FinBs,FinBs2
mov tam,0
mov cx,0
xor si,si
see:
cmp buffer[si],24h 
je FinBs
cmp buffer[si],3bh 
je FinBs2
inc cx
inc si
jmp see
FinBs2:
inc cx
FinBs:
mov tam,cx
endm

LimpiarConsola macro
mov ax,0600h
mov bh,89h
mov cx,0000h
mov dx,184Fh
int 10h
endm

getChar macro
mov ah,0dh
int 21h
mov ah,01h
int 21h
endm

print macro cadena
push ax
push dx
mov ax,@data
mov ds,ax
mov ah,09
mov dx,offset cadena 
int 21h
pop dx
pop ax
endm

printChar macro caracter
mov ah,02h
mov dl,caracter
int 21h
endm


ConvertToInt macro num,buffer
LOCAL FinNumero,SaveNum,OmitirMas,NoComplementoA2
xor si,si
xor cx,cx
xor ax,ax
xor dx,dx
xor bx,bx
mov dl,10

cmp buffer[si],2bh 
je OmitirMas
cmp buffer[si],2dh 
jne SaveNum

mov cx,1 
OmitirMas:
inc si 

SaveNum:

mov bl,buffer[si] 
sub bl,30h 
inc si 
add al,bl   
cmp buffer[si],20h 
je FinNumero
cmp buffer[si],24h 
je FinNumero
mul dl  
jmp SaveNum

FinNumero:
cmp cx,1
jne NoComplementoA2
neg ax
NoComplementoA2:
mov num,ax

endm


ConvertToString macro res,buffer
LOCAL Dividir,Dividir2,FinCR3   
push ax
push cx
push dx

xor si,si
xor cx,cx
xor ax,ax
xor dx,dx

mov ax,res
mov dl,0ah

test ax,1000000000000000
jz Dividir2

neg ax
jmp Dividir2

Dividir:
xor ah,ah
Dividir2:
div dl
inc cx
push ax
cmp al,00h 
je FinCR3
jmp Dividir

FinCR3:
pop ax
add ah,30h
mov buffer[si],ah
inc si
loop FinCR3

mov ah,24h 
mov buffer[si],ah
inc si
pop dx
pop cx
pop ax
endm



CopiarArray macro fuente, destino
LOCAL copiar2, FinCAB11, FinCAB2
xor si,si
xor cx,cx
copiar2:

cmp cx,SIZEOF fuente
je FinCAB2
cmp fuente[si],3bh 
je FinCAB11

mov al,fuente[si]
mov destino[si],al
inc si
inc cx
jmp copiar2
FinCAB11:
mov al,fuente[si]
mov destino[si],al
FinCAB2:
xor si,si
xor cx,cx
endm



ObtenerHora macro hour,min,seg,numDia
xor ax,ax
xor bx,bx
mov ah,2ch
int 21h
mov bl,ch
ConvertToString bx,hour
mov bl,cl
ConvertToString bx,min
mov bl,dh
ConvertToString bx,seg
mov ah,2ah
int 21h
mov bl,dl
ConvertToString bx,numDia
endm


ObtenerNumero macro buffer,lim
LOCAL ON2,Signo2,valido2,FinON2,NoCero
xor cx,cx
xor si,si

ON2:
cmp cx,lim
je Error2 
mov ah,01h
int 21h
cmp al,0dh 
je FinON2
cmp al,30h 
jb Signo2
cmp al,39h 
ja Error1
jmp valido2

Signo2:
cmp cx,0
jne Error1  
cmp al,2bh 
je valido2
cmp al,2dh 
je valido2
jmp Error1

valido2:
mov buffer[si],al
inc si
inc cx
jmp ON2

FinON2:
cmp cx,0 
jne NoCero
mov buffer[si],30h
NoCero:
xor si,si
endm


ObtenerNumero2 macro buffer,lim
LOCAL ON2,Signo2,valido2,FinON2
xor cx,cx
xor si,si

ON2:
cmp cx,lim
je Error2 
mov ah,01h
int 21h
cmp al,0dh
je FinON2
cmp al,30h
jb Signo2
cmp al,39h
ja Error5
jmp valido2

Signo2:
cmp cx,0
jne Error5  
cmp al,2bh 
je valido2
cmp al,2dh 
je valido2
jmp Error5

valido2:
mov buffer[si],al
inc si
inc cx
jmp ON2

FinON2:
xor si,si
endm


LimpiarArr macro buffer,caracter,lim
LOCAL Limpiar, FinLA
pop cx
xor si,si
xor cx,cx

Limpiar:
cmp cx,lim
je FinLA
mov buffer[si],caracter
inc si
inc cx
jmp Limpiar

FinLA:
push cx
endm


crear macro ruta,handle
lea dx,ruta
mov ah,3ch
mov cx,00h
int 21h
mov handle,ax
jc Error7
endm

abrir macro ruta,handle
;impCad ruta
lea dx,ruta
mov ah,3dh 
mov al, 00h 
int 21h 
mov handle,ax 
jc Error8
endm 

escribir macro numbytes,databuffer,handle 
mov ah,40h 
mov bx,handle 
mov cx,numbytes 
lea dx,databuffer 
int 21h
jc Error9
endm 

leer macro numbytes,databuffer,handle 
mov ah,3fh 
mov bx,handle
mov cx,numbytes
lea dx,databuffer
int 21h
jc Error10
endm 

cerrar macro handle
mov ah,3eh 
mov bx,handle 
int 21h
jc Error11
endm 

main proc

    EncabezadoMenu:   
        LimpiarConsola
        print EncabezadoGeneral
        print salt
        print EncabezadoUno
        print salt
        print elij
        getChar
        cmp al,31h
        je OpcionUno
        cmp al,32h
        je OpcionDos
        cmp al,33h
        je OpcionTres
        cmp al,34h
        je OpcionCuatro
        cmp al,35h
        je OpcionCinco 
        cmp al,36h
        je OpcionSeis ;Reporte
        cmp al,37h
        je Opcion7
        cmp al,38h
        je Salir
        jmp EncabezadoMenu

    OpcionUno:
        print salt
        mov hayFuncion,1
        mov x0,0
        mov x1,0
        mov x2,0
        mov x3,0
        mov x4,0
        LimpiarArr buffX0, 24h,SIZEOF buffX0
        LimpiarArr buffX1, 24h,SIZEOF buffX0
        LimpiarArr buffX2, 24h,SIZEOF buffX0
        LimpiarArr buffX3, 24h,SIZEOF buffX0
        LimpiarArr buffX4, 24h,SIZEOF buffX0
        print coef4
        ObtenerNumero buffX4,5
        print coef3
        ObtenerNumero buffX3,5
        print coef2
        ObtenerNumero buffX2,5
        print coef1
        ObtenerNumero buffX1,5
        print coef0
        ObtenerNumero buffX0,5
        ConvertToInt x0,buffX0
        ConvertToInt x1,buffX1
        ConvertToInt x2,buffX2
        ConvertToInt x3,buffX3
        ConvertToInt x4,buffX4
        print salt
        print msg1
        getChar
        jmp EncabezadoMenu

    OpcionDos:
        cmp hayFuncion,0
        je Error3
        LimpiarArr FuncionOriginal,24h,SIZEOF FuncionOriginal
        ObtenerFuncion FuncionOriginal,x0,x1,x2,x3,x4
        print salt
        print salt
        print msg2
        print salt
        print FuncionOriginal
        print salt
        getChar
        jmp EncabezadoMenu

    OpcionTres:
        cmp hayFuncion,0
        je Error3
        LimpiarArr FuncionDerivada,24h,SIZEOF FuncionDerivada
        ObtenerConstantesD x1,x2,x3,x4,Dx0,Dx1,Dx2,Dx3
        ObtenerFuncionD FuncionDerivada,Dx0,Dx1,Dx2,Dx3
        print salt
        print salt
        print msg4
        print salt
        print FuncionDerivada
        print salt
        getChar
        jmp EncabezadoMenu

    OpcionCuatro:
        cmp hayFuncion,0
        je Error3
        LimpiarArr FuncionIntegral,24h,SIZEOF FuncionIntegral
        ObtenerFuncionI FuncionIntegral,x0,x1,x2,x3,x4
        print salt
        print salt
        print msg5
        print salt
        print FuncionIntegral
        print salt
        getChar
        jmp EncabezadoMenu

    OpcionCinco:
        cmp hayFuncion,0
        je Error3
        LimpiarConsola
        print enc7
        print salt
        print elij
        getChar
        cmp al,31h
        je GraficarOriginal
        cmp al,32h
        je GraficarDerivada
        cmp al,33h
        je GraficarIntegral
        cmp al,34h
        je EncabezadoMenu
        jmp OpcionCinco

    GraficarOriginal:
        
        PedirRango Lim0,Lim1,bufftemp,centro

        ModoGrafico
        PintarX
        PintarY centro
        EvaluarFuncion x0,x1,x2,x3,x4,cero,Lim0,Lim1,relacion,centro,y,x,resMayor,EsIntegral

        getChar
        ModoTexto
        
        jmp OpcionCinco

    GraficarDerivada:
        
        PedirRango Lim0,Lim1,bufftemp,centro

        ModoGrafico
        PintarX
        PintarY centro
        EvaluarFuncion Dx0,Dx1,Dx2,Dx3,cero,cero,Lim0,Lim1,relacion,centro,y,x,resMayor,EsIntegral

        getChar
        ModoTexto        
        jmp OpcionCinco

    GraficarIntegral:        
        PedirRango Lim0,Lim1,bufftemp,centro
        PedirConstanteIntegracion Cintegracion
        mov EsIntegral,1

        ModoGrafico
        PintarX
        PintarY centro
        EvaluarFuncion Cintegracion,x0,x1,x2,x3,x4,Lim0,Lim1,relacion,centro,y,x,resMayor,EsIntegral

        getChar
        ModoTexto        
        mov EsIntegral,0
        jmp OpcionCinco

    OpcionSeis:
        crear FileReporte,handlerReporte
        ObtenerSize EncabezadoGeneral,tamano
        escribir tamano,EncabezadoGeneral,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerSize enc2,tamano
        escribir tamano,enc2,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerHora hora,minutos,segundos,dia
        ObtenerSize enc3, tamano
        escribir tamano,enc3,handlerReporte 
        ObtenerSize dia,tamano
        escribir tamano,dia,handlerReporte 
        ObtenerSize enc6, tamano
        escribir tamano,enc6,handlerReporte
        ObtenerSize enc4,tamano
        escribir tamano,enc4,handlerReporte 
        ObtenerSize hora,tamano
        escribir tamano,hora,handlerReporte 
        ObtenerSize enc5, tamano
        escribir tamano,enc5,handlerReporte 
        ObtenerSize minutos,tamano
        escribir tamano,minutos,handlerReporte 
        ObtenerSize enc5, tamano
        escribir tamano,enc5,handlerReporte 
        ObtenerSize segundos,tamano
        escribir tamano,segundos,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerSize msg2,tamano
        escribir tamano,msg2,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerSize2 FuncionOriginal,tamano
        escribir tamano,FuncionOriginal,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerSize msg4,tamano
        escribir tamano,msg4,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerSize2 FuncionDerivada,tamano
        escribir tamano,FuncionDerivada,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerSize msg5,tamano
        escribir tamano,msg5,handlerReporte 
        ObtenerSize salt,tamano
        escribir tamano,salt,handlerReporte 
        ObtenerSize2 FuncionIntegral,tamano
        escribir tamano,FuncionIntegral,handlerReporte 
        cerrar handlerReporte
        print msg3
        getChar
        jmp EncabezadoMenu

	Opcion7:	
	print archid
	print EjmGuardar		
	Limpiarjc bufferentradajc,SIZEOF bufferentradajc,24h
	ObtenerRutajc bufferentradajc 
		
	cmp bufferentradajc[0],64	   	
	jne Errorjc4
	cmp bufferentradajc[1],64		
	jne Errorjc4	
	cmp bufferentradajc[13],64		
	jne Errorjc4
	cmp bufferentradajc[14],64		
	jne Errorjc4	
	cmp bufferentradajc[10],'a'		
	jne Errorjc5
	cmp bufferentradajc[11],'r'		
	jne Errorjc5	
	cmp bufferentradajc[12],'q'		
	jne Errorjc5	

	mov al,bufferentradajc[2]		
	mov bufferentradajc[0],al
	mov al,bufferentradajc[3]
	mov bufferentradajc[1],al
	mov al,bufferentradajc[4]
	mov bufferentradajc[2],al
	mov al,bufferentradajc[5]
	mov bufferentradajc[3],al
	mov al,bufferentradajc[6]
	mov bufferentradajc[4],al
	mov al,bufferentradajc[7]
	mov bufferentradajc[5],al
	mov al,bufferentradajc[8]
	mov bufferentradajc[6],al
	mov bufferentradajc[7],'.'
	mov bufferentradajc[8],'a'
	mov bufferentradajc[9],'r'
	mov bufferentradajc[10],'q'
	mov bufferentradajc[11],'$'
	
	Limpiarjc bufferInformacionjc,SIZEOF bufferInformacionjc,24h 
	Abrirjc bufferentradajc,handlerentradajc 	
	Leerjc handlerentradajc,bufferInformacionjc,SIZEOF bufferInformacionjc 
	print bufferInformacionjc
   	Cerrarjc handlerentradajc 
   	getChar
	jmp EncabezadoMenu

    Errorjc1:
	print erroru
	getChar
	jmp EncabezadoMenu

	Errorjc2:
	print errorleer
	getChar
	jmp EncabezadoMenu

	Errorjc3:
	print errord
	getChar
	jmp EncabezadoMenu

	Errorjc4:
	print RutaMala
	getChar
	jmp EncabezadoMenu	

	Errorjc5:
	print ExtencionMala
	getChar
	jmp EncabezadoMenu	

    Error: 
        print err
        getChar
        jmp EncabezadoMenu
    Error1: 
        print err1
        print salt
        getChar
        jmp OpcionUno
    Error2: 
        print err2
        print salt
        getChar
        jmp OpcionUno
    Error3: 
        print err3
        print salt
        getChar
        jmp EncabezadoMenu
    Error4: 
        print err4
        print salt
        getChar
        jmp OpcionCinco
    Error5: 
        print err5
        getChar
        jmp OpcionCinco
    Error7: 
        print err7
        getChar
        jmp EncabezadoMenu
    Error8: 
        print err8
        getChar
        jmp EncabezadoMenu
    Error9: 
        print err9
        getChar
        jmp EncabezadoMenu
    Error10:
        print err10
        getChar
        jmp EncabezadoMenu
    Error11: 
        print err11
        getChar
        jmp EncabezadoMenu
    Error12:
        print err12
        getChar
        jmp OpcionCinco


    Salir:
        mov ah, 4ch 
        xor al,al
        int 21h
main endp
end main