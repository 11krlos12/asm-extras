count_c MACRO string
	LOCAL CICLO,FIN
   XOR cx,cx
   MOV SI, 0
CICLO:   CMP string[SI],'$'
         JE FIN
         INC cx
         INC SI
         JMP CICLO
FIN:
ENDM


fopen MACRO filename,handle 
	LEA dx,filename
	MOV ah,3dh 
	MOV al, 00h 
	INT 21h 
	MOV handle,ax 
	JNC S1
	MOV ax,-1 
S1:
ENDM


fwrite MACRO
	LOCAL SALIR
		MOV    ah,3ch
        MOV    cx,00
        LEA    dx,nombre
        INT    21h

        JC     salir
        MOV    handler,ax

        MOV    ah,40h
        MOV    bx,handler
        count_c jsonFile
        LEA    dx,jsonFile

        INT    21h

        MOV    ah,3eh
        MOV    bx,handler
        INT    21h	
ENDM 


fread MACRO numbytes,databuffer,handle 
	MOV ah,3fh 
	MOV bx,handle 
	MOV cx,numbytes 
	LEA dx,databuffer 
	INT 21h 
ENDM 


fclose MACRO handle 
	MOV ah,3eh 
	MOV bx,handle 
	INT 21h 
ENDM


clear_string MACRO string
	LOCAL CICLO, FIN
		MOV SI,0
CICLO:	CMP string[SI],'$'
		JE FIN
		MOV string[SI],'$'
		INC SI
		JMP CICLO
FIN:
ENDM


fix_url MACRO
	LOCAL CICLO,QUITAR
		MOV SI, 0
		CMP file[SI],'%'
		JNE QUITAR
		INC SI
		CMP file[SI],'%'
		JNE QUITAR
		INC SI
		CMP file[SI],'<'
		JNE QUITAR
		INC SI

		MOV DI,0

CICLOZ: CMP file[SI],'>'
		JE CMP4

		MOV al,file[SI]
		MOV file2[DI],al

		INC SI
		INC DI
		JMP CICLOZ


CMP4:	CMP file[SI],'%'
		JNE QUITAR
		CMP file[SI],'%'
		JNE QUITAR


CICLO: 	CMP file[SI],13
		JE QUITAR
		INC SI
		JMP CICLO
QUITAR: MOV file[SI],00h
		MOV file2[DI],00h
ENDM 


display_string Macro string
  MOV  dx, offset string
  MOV  ah, 9
  INT  21h
ENDM


clear_screen Macro
  mov  ah, 0
  mov  al, 3
  int  10H
ENDM


read_choice Macro  
  MOV ah,01h
  int 21h
  sub al,30h
  mov seleccion,al

  mov ah,09
  lea dx,salto
  int 21h

ENDM


read_url Macro  
  MOV ah, 3fh
  MOV bx, 00
  MOV cx, 100
  MOV dx, offset [file]
  int 21h
ENDM


write_file Macro
	LOCAL SALIR
		MOV    ah,3ch
        MOV    cx,00
        LEA    dx,nombre
        INT    21h

        JC     salir
        MOV    handler,ax

        MOV    ah,40h
        MOV    bx,handler
        count_c jsonFile
        LEA    dx,jsonFile
        INT    21h

        MOV    ah,3eh
        MOV    bx,handler
        INT    21h

SALIR:
ENDM

add_buffer Macro str1
	LOCAL C1,FIN

	XOR BX,BX

C1:	CMP str1[BX],'$'
	JE FIN

	MOV al,str1[BX]
	MOV bufferAux[DI],al
	INC BX
	INC DI
	JMP C1

FIN:
ENDM


toLowerCase Macro
	LOCAL CICLO1,MOV1,CMP1,INC1,FIN1

	MOV    ah,3ch
    MOV    cx,00
    LEA    dx,nombre
    INT    21h

    MOV    handler,ax

    MOV    ah,40h
    MOV    bx,handler
    count_c jsonFile
    LEA    dx,jsonFile
    INT    21h


;----------------------- TO LOWER CASE -----------------------


		MOV DI,0
		MOV SI,0

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		INC DI

CICLO1:	CMP buffer[SI],'$'
		JE FIN1

		CMP buffer[SI],13
		JNE MOV1

		MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],','
		INC DI

	
MOV1:	MOV al, buffer[SI]
		MOV bufferAux[DI],al

		CMP buffer[SI],10
		JNE CMP1

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34

CMP1:	CMP bufferAux[DI],65
		JB INC1
		CMP bufferAux[DI],90
		JA INC1
		ADD bufferAux[DI],32

INC1:	INC SI
		INC DI
		JMP CICLO1

FIN1:	MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI

		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0

;----------------------- END LOWER CASE -----------------------


;-----------------------TO UPPER CASE-----------------------		


		add_buffer toUp

		MOV SI,0

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		INC DI

CICLO2:	CMP buffer[SI],'$'
		JE FIN2

		CMP buffer[SI],13
		JNE MOV2

		MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],','
		INC DI

	
MOV2:	MOV al, buffer[SI]
		MOV bufferAux[DI],al

		CMP buffer[SI],10
		JNE CMP2

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34

CMP2:	CMP bufferAux[DI],97
		JB INC2
		CMP bufferAux[DI],122
		JA INC2
		SUB bufferAux[DI],32

INC2:	INC SI
		INC DI
		JMP CICLO2

FIN2:	MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI

		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0


;----------------------- END UPPER CASE -----------------------



;----------------------- TO CAPITALIZE -----------------------


		add_buffer toCap

		MOV SI,0

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		INC DI

CICLO3:	CMP buffer[SI],'$'
		JE FIN3

		CMP buffer[SI],13
		JNE MOV3

		MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],','
		INC DI

	
MOV3:	MOV al, buffer[SI]
		MOV bufferAux[DI],al

		CMP buffer[SI],10
		JNE CMP3

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		MOV f1,1
		JMP INC3

CMP3:	CMP f1,1
		JNE SAL3
		MOV f1,0
		CMP bufferAux[DI],97
		JB SAL3
		CMP bufferAux[DI],122
		JA SAL3
		SUB bufferAux[DI],32
		JMP INC3


SAL3:	CMP bufferAux[DI],32
		JNE INC3
		MOV f1,1

INC3:	INC SI
		INC DI
		JMP CICLO3

FIN3:	MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI

		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0


;----------------------- END CAPITALIZE -----------------------


;----------------------- TO SUM -----------------------


		add_buffer toSum


		MOV SI,0

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		INC DI

		XOR AX,AX
		XOR DX,DX

CICLO4:	CMP buffer[SI],'$'
		JE FIN4

		CMP buffer[SI],13
		JNE MOV4

		MOV AX,DX
		AAM

		MOV  DX, AX
		MOV  AH, 9
  		INT  21h

  		MOV uni,AL
		MOV AL,AH

		AAM
		MOV cen,AH

		MOV dece,AL

		MOV AL,cen
		ADD AL,30h
		MOV bufferAux[DI],AL
		INC DI
		MOV AL,dece
		ADD AL,30h
		MOV bufferAux[DI],AL
		INC DI
		MOV AL,uni
		ADD AL,30h
		MOV bufferAux[DI],AL
		INC DI

		MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],','
		INC DI
		MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI

		XOR AX,AX
		XOR DX,DX

	
MOV4:	ADD DL, buffer[SI]

		CMP buffer[SI],10
		JNE INC4

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34

		INC DI
INC4:	INC SI
		JMP CICLO4

FIN4:	MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI



		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0




;----------------------- END SUM -----------------------


;----------------------- TO CONCAT -----------------------


		add_buffer toCon

		MOV SI,0

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		INC DI

CICLO5:	CMP buffer[SI],'$'
		JE FIN5

		CMP buffer[SI],13
		JE INCZ

		CMP buffer[SI],10
		MOV bufferAux[DI],32
		JE INC5
		
	
MOV5:	MOV al, buffer[SI]
		MOV bufferAux[DI],al

INC5:	INC DI
INCZ:	INC SI
		JMP CICLO5

FIN5:	MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI

		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0


;----------------------- END CONCAT -----------------------


;----------------------- TO REVERSE -----------------------


		add_buffer toRev

		MOV SI,0

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		INC DI

		PUSH '$'
		PUSH 13
		PUSH ','
		PUSH 34

CICLO6:	CMP buffer[SI],'$'
		JE FIN6

		CMP buffer[SI],13
		JNE MOV6
		XOR ax,ax

NCICLO:	POP ax
		CMP al,'$'
		JE SAL4
		MOV bufferAux[DI],al
		INC DI
		XOR ax,ax
		JMP NCICLO
		

SAL4:	PUSH '$'
		PUSH 13
		INC SI
		PUSH ','
		PUSH 34
		JMP SAL5

	
MOV6:	XOR ax,ax
		MOV al, buffer[SI]
		PUSH ax


SAL5:	CMP buffer[SI],10
		JNE INCX

		MOV bufferAux[DI],10
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34


INC6:	INC DI
INCX:	INC SI
		JMP CICLO6


FIN6:	POP ax
		CMP al,'$'
		JE FIND
		CMP al,','
		JE FIND
		MOV bufferAux[DI],al
		INC DI
		XOR ax,ax
		JMP FIN6

FIND:	MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI

		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0


;----------------------- END REVERSE -----------------------


;----------------------- TO STRING -----------------------


		add_buffer toString

		MOV SI,0

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34
		INC DI

CICLO7:	CMP buffer[SI],'$'
		JE FIN7

		CMP buffer[SI],13
		JNE MOV7

		MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],','
		INC DI

	
MOV7:	MOV al, buffer[SI]
		MOV bufferAux[DI],al

		CMP buffer[SI],10
		JNE INC7

		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],09
		INC DI
		MOV bufferAux[DI],34

INC7:	INC SI
		INC DI
		JMP CICLO7

FIN7:	MOV bufferAux[DI],34
		INC DI
		MOV bufferAux[DI],13
		INC DI
		MOV bufferAux[DI],10
		INC DI

		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0

;----------------------- END TO STRING -----------------------

		add_buffer toFin

		MOV    ah,40h
	    MOV    bx,handler
	    count_c bufferAux
	    LEA    dx,bufferAux
	    INT    21h

	    clear_string bufferAux
	    MOV DI,0

		MOV    ah,3eh
        MOV    bx,handler
        INT    21h

ENDM


.286
.model large
.stack 100h
.data

encabezado  db "UNIVERSIDAD DE SAN CARLOS DE GUATEMALA",13,10
            db "FACULTAD DE INGENIERIA",13,10
            db "ESCUELA DE CIENCIAS Y SISTEMAS",13,10
            db "ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1A",13,10
            db "SEGUNDO SEMESTRE 2018",13,10
	        db "Ariel Alejandro Bautista Mendez",13,10
            db "201503910",13,10
            db "Tercera Practica",13,10,13,10,'$'

menu        db "POR FAVOR ELIJA UNA OPCION",13,10
            db "1. Cargar Archivo",13,10
            db "2. Crear Reporte",13,10
            db "3. Salir",13,10,13,10,'$'

load_menu   db "CARGA DE ARCHIVOS",13,10,13,10
            db "Ingrese una ruta",13,10,13,10,'$'

report_menu db "REPORTES",13,10,13,10,'$'

fin         db "Finalizando el programa...",13,10
            db "Pulse cualquier tecla para continuar",13,10,13,10,'$'

incorrecto  db "Seleccion incorrecta",13,10,13,10,'$'

cadselect   db "La direccion es:",13,10,13,10,'$'

seleccion   db 0

salto       db " ",13,10,'$'

file 		db 100 dup('$')

mensaje 	db 'Archivo Leido','$'

handler 	dw ? 

buffer 		db 25601 dup('$')

error		db 'Error Al Abrir El Archivo','$'

ofile		db 'Rep.jso',00h

columa		db 0

jsonFile	db '{',13,10
			db 09

fecha 		db 	34,"Fecha",34
			db	": ",34

date		db	"00/00/0000",32

time		db	"00:00:00",34 
			db 	",",0dh, 0ah

demas		db 	09,34, "nombre",34
			db 	": "
			db 	34, "Ariel Alejandro Bautista Mendez",34
			db 	",",13,10

			db 	09,34, "carne",34
			db 	": "
			db 	"201503910,",13,10

			db 	09,34,"resultados",34
			db 	": {",13,10
			db 	09,32,32, "toLowerCase: [",13,10,'$'

bufferAux	db 28001 dup('$')

nombre   	db 	"c:\REP.JSO",00h

toUp		db 	09, 32, 32, "],",13, 10, 09, 32,32
			db 	"toUpperCase: [",13,10,'$'

toCap		db 	09, 32, 32, "],",13, 10, 09, 32,32
			db 	"toCapitalize: [",13,10,'$'

toSum		db 	09, 32, 32, "],",13, 10, 09, 32,32
			db 	"Sum: [",13,10,'$'

toCon		db 	09, 32, 32, "],",13, 10, 09, 32,32
			db 	"Concat: [",13,10,'$'

toRev		db 	09, 32, 32, "],",13, 10, 09, 32,32
			db 	"reverseString: [",13,10,'$'

toString	db 	09, 32, 32, "],",13, 10, 09, 32,32
			db 	"toString: [",13,10,'$'

toFin		db 	09, 32, 32, "]",13, 10
			db 	09, '}', 13, 10
			db 	'}','$'

f1 			db 	1

file2		db 100 dup('$')

cen 		db 	0

dece	 	db 	0

uni 		db 	0


.code
start:

;INITIALIZE DATA SEGMENT.
  MOV  ax, @data
  MOV  ds, ax

      
INICIO: clear_screen
        display_string encabezado
        display_string menu
        read_choice
        display_string salto
        
        CMP seleccion,1
        JE SELECCION1

        CMP seleccion,2
        JE SELECCION2

        CMP seleccion, 3
        JE FINALIZAR

        display_string incorrecto
        mov  ah, 7
        int  21h   
        CALL INICIO

;FINISH PROGRAM.

SELECCION1:   	CALL read_file
              	read_url

              	display_string salto
              	fix_url	
              	fopen file2, handler
              	JNC ABRIR 
				
				
				display_string salto
				display_string error
				display_string salto
				display_string salto
				JMP SELECCION1

ABRIR:			display_string cadselect
              	display_string file
				fread 25600,buffer,handler
				fclose handler

CM3:			display_string salto
				display_string buffer
				display_string salto
				display_string salto
				display_string mensaje
				display_string salto
				display_string file2
				clear_string file
				clear_string file2
				display_string salto

				MOV  ah, 7
              	INT  21h

              JMP INICIO

SELECCION2: CALL show_report
			toLowerCase
			;write_file
            JMP FINALIZAR

;---------------------------------------------


read_file proc
  display_string load_menu
  ret
read_file endp

show_report proc
  display_string report_menu
  MOV  ax, @data
  MOV  ds, ax

  call    get_date
  call    get_time
  ret
show_report endp

get_date proc
    mov     ah, 04h
    int     1ah

    mov     bx, offset date
    mov     al, dl
    call    put_bcd2

    inc     bx
    mov     al, dh
    call    put_bcd2

    inc     bx
    mov     al, ch
    call    put_bcd2
    mov     al, cl
    call    put_bcd2

    ret
get_date endp


get_time proc
    mov     ah, 02h
    int     1ah

    mov     bx, offset time
    mov     al, ch
    call    put_bcd2

    inc     bx
    mov     al, cl
    call    put_bcd2

    inc     bx
    mov     al, dh
    call    put_bcd2

    ret
get_time endp


put_bcd2 proc
    PUSH    ax
    SHR     ax, 04
    AND     ax, 0fh
    ADD     ax, '0'
    MOV     [bx], al
    INC     bx
    POP     ax

    AND     ax, 0fh
    ADD     ax, '0'
    MOV     [bx], al

    INC     bx

    RET
put_bcd2 endp


FINALIZAR:    display_string fin

              mov  ah, 7
              int  21h

              mov  ax, 4c00h
              int  21h

end start