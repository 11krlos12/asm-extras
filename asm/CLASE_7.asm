;===============SECCION DE MACROS ===========================
include c7_m.asm

;================= DECLARACION TIPO DE EJECUTABLE ============
.model small 
.stack 100h 
.data 
;================ SECCION DE DATOS ========================
encab db 0ah,0dh,' CLASE 7 ARQUI 1',0ah,0dh,'1) Abrir archivo', 0ah,0dh,'2) Crear Archivo',0ah,0dh,'3) Leer Archivo',0ah,0dh,'4) Salir',0ah,0dh,'$'
msm1 db 0ah,0dh,'FUNCION ABRIR',0ah,0dh,'$'
msm2 db 0ah,0dh,'FUNCION CREAR',0ah,0dh,'$'
msm3 db 0ah,0dh,'FUNCION LEER',0ah,0dh,'$'
msmError1 db 0ah,0dh,'Error al abrir archivo','$'
msmError2 db 0ah,0dh,'Error al leer archivo','$'
msmError3 db 0ah,0dh,'Error al crear archivo','$'
rutaArchivo db 100 dup('$')
bufferLectura db 100 dup('$')
handleFichero dw ?
.code ;segmento de código
;================== SECCION DE CODIGO ===========================
	main proc 
		Menu:
			print encab
			getChar
			cmp al,'1'
			je ABRIR
			cmp al,'2'
			je CREAR
			cmp al,'4'
			je SALIR
			cmp al,'3'
			jmp LEER
			jmp Menu
		ABRIR:
			print msm1
			getRuta rutaArchivo
			getChar
			jmp Menu
		LEER:
			print msm3
			getChar
			jmp Menu
		CREAR:
			print msm2
			getChar
			jmp Menu
	    ErrorAbrir:
	    	print msmError1
	    	getChar
	    	jmp Menu
	    ErrorLeer:
	    	print msmError2
	    	getChar
	    	jmp Menu
	    ErrorCrear:
	    	print msmError3
	    	getChar
	    	jmp Menu
		SALIR: 
			MOV ah,4ch 
			int 21h
	main endp
;================ FIN DE SECCION DE CODIGO ========================
end